/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

public interface Types extends HasTypes, Iterable<Type>
{
    @Override default Types getRelationTypes() {return this;}

    List<Type> getTypes();

    int size();

    boolean contains(Type type);

    String cypher();

    boolean isEmpty();

    boolean isNotEmpty();

    Types plus(Type other);

    Types plus(Types other);

    static Types empty() {return EmptyTypes.INSTANCE;}

    static Types of(final Type type)
    {
        Objects.requireNonNull(type, "type");

        // discard repeated type
        return type;
    }

    static Types of(final Type first, final Type... types)
    {
        if (null == types || 0 == types.length) {
            return of(first);
        }
        // discard repeated types
        final var resultTypes = new LinkedHashSet<Type>(types.length + 1);
        resultTypes.add(first);
        resultTypes.addAll(List.of(types));
        if (1 == resultTypes.size()) {
            return first;
        } else {
            return new DefaultTypes(resultTypes);
        }
    }

    static Types of(final Collection<Type> types)
    {
        Objects.requireNonNull(types, "types");

        if (types.isEmpty()) {
            return empty();
        }
        if (1 == types.size()) {
            return of(types.iterator().next());
        }
        // discard repeated types
        final var resultTypes = new LinkedHashSet<>(types);
        if (1 == resultTypes.size()) {
            return of(resultTypes.iterator().next());
        } else {
            return new DefaultTypes(resultTypes);
        }
    }
}

enum EmptyTypes implements Types
{
    INSTANCE;

    @java.lang.Override public List<Type> getTypes()
    {
        return List.of();
    }

    @java.lang.Override public Iterator<Type> iterator()
    {
        return getTypes().iterator();
    }

    @java.lang.Override public boolean contains(final Type type)
    {
        return false;
    }

    @java.lang.Override public String cypher()
    {
        return "";
    }

    @java.lang.Override public boolean isEmpty()
    {
        return true;
    }

    @java.lang.Override public boolean isNotEmpty()
    {
        return false;
    }

    @java.lang.Override public int size()
    {
        return 0;
    }

    @java.lang.Override public Types plus(final Type type)
    {
        return type;
    }

    @java.lang.Override public Types plus(final Types types)
    {
        return types;
    }
}

final class DefaultTypes implements Types
{
    private final List<Type> types;
    private final String cypher;

    DefaultTypes(final Collection<Type> types)
    {
        this.types = List.copyOf(types);
        this.cypher = makeCypherFor(this.types);
    }

    private static String makeCypherFor(final List<Type> types)
    {
        final var cypher = new StringBuilder();
        final var i = types.iterator();
        cypher.append(i.next().cypher());
        while (i.hasNext()) {
            cypher.append('|');
            cypher.append(i.next().name());
        }
        return cypher.toString();
    }

    @Override public List<Type> getTypes() {return this.types;}

    @Override public Iterator<Type> iterator()
    {
        return this.types.iterator();
    }

    @Override public boolean contains(final Type type) {return this.types.contains(type);}

    @Override public String cypher() {return this.cypher;}

    @Override public boolean isEmpty() {return this.types.isEmpty();}

    @Override public boolean isNotEmpty() {return !this.types.isEmpty();}

    @Override public int size()
    {
        return this.types.size();
    }

    @Override public Types plus(final Type type)
    {
        if (null == type || contains(type)) {
            return this;
        }
        final var types = new java.util.ArrayList<Type>(size() + 1);
        types.addAll(this.types);
        types.add(type);
        return new DefaultTypes(types);
    }

    @Override public DefaultTypes plus(final Types other)
    {
        if (null == other || other.isEmpty()) {
            return this;
        }
        final var types = new java.util.LinkedHashSet<Type>(size() + other.size());
        types.addAll(this.types);
        for (final var type : other) {
            types.add(type);
        }
        return new DefaultTypes(types);
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof DefaultTypes) {
            final var other = (DefaultTypes) o;

            return this.cypher.equals(other.cypher);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.cypher.hashCode();
    }

    @Override public String toString() {return this.cypher;}

}
