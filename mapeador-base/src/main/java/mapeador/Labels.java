/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

public interface Labels extends Iterable<Label>, HasLabels
{
    @Override default Labels getNodeLabels() {return this;}

    List<Label> getLabels();

    int size();

    boolean contains(Label label);

    String cypher();

    boolean isEmpty();

    boolean isNotEmpty();

    Labels plus(Label other);

    Labels plus(Labels others);

    static Labels empty() {return EmptyLabels.INSTANCE;}

    static Labels of(final Label label)
    {
        Objects.requireNonNull(label, "label");
        return label;
    }

    static Labels of(final Label first, final Label... labels)
    {
        if (null == labels || 0 == labels.length) {
            return of(first);
        }
        // discard repeated labels
        final var resultLabels = new LinkedHashSet<Label>(labels.length + 1);
        resultLabels.add(first);
        resultLabels.addAll(List.of(labels));
        return new DefaultLabels(resultLabels);
    }

    static Labels of(final Collection<Label> labels)
    {
        Objects.requireNonNull(labels, "labels");

        if (labels.isEmpty()) {
            return empty();
        }
        if (1 == labels.size()) {
            return of(labels.iterator().next());
        }
        // discard repeated labels
        final var resultLabels = labels instanceof java.util.Set? labels :new LinkedHashSet<>(labels);
        return new DefaultLabels(resultLabels);
    }
}

enum EmptyLabels implements Labels
{
    INSTANCE;

    @Override public List<Label> getLabels() {return List.of();}

    @Override public int size() {return 0;}

    @Override public boolean contains(final Label label) {return false;}

    @Override public String cypher() {return "";}

    @Override public boolean isEmpty() {return true;}

    @Override public boolean isNotEmpty() {return false;}

    @Override public Labels plus(final Label other) {return null == other ? this : Labels.of(other);}

    @Override public Labels plus(final Labels others) {return null == others ? this : others;}

    @Override public Iterator<Label> iterator() {return Collections.emptyIterator();}
}

final class DefaultLabels implements Labels
{
    private final List<Label> labels;
    private final String cypher;

    DefaultLabels(final Label label)
    {
        this.labels = List.of(label);
        this.cypher = label.cypher();
    }

    DefaultLabels(final Collection<Label> labels)
    {
        final var cypher = new StringBuilder();
        final var classes = new StringBuilder();
        for (final var label : labels) {
            cypher.append(label.cypher());
        }
        this.labels = List.copyOf(labels);
        this.cypher = cypher.toString();
    }

    @Override public List<Label> getLabels() {return this.labels;}

    @Override public int size() {return this.labels.size();}

    @Override public boolean contains(final Label label) {return this.labels.contains(label);}

    @Override public String cypher() {return this.cypher;}

    @Override public boolean isEmpty() {return false;}

    @Override public boolean isNotEmpty() {return true;}

    @Override public Labels plus(final Label other)
    {
        if (null == other || contains(other)) {
            return this;
        }
        final var labels = new java.util.ArrayList<Label>(1 + size());
        labels.addAll(this.labels);
        labels.add(other);
        return new DefaultLabels(labels);
    }

    @Override public Labels plus(final Labels others)
    {
        if (null == others || others.isEmpty()) {
            return this;
        }
        final var labels = new java.util.TreeSet<>(this.labels);
        labels.addAll(others.getLabels());
        return new DefaultLabels(labels);
    }

    @Override public Iterator<Label> iterator() {return this.labels.iterator();}
}
