/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Label;
import mapeador.Type;
import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.$;
import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.MERGE;
import static mapeador.cypher.Cypher.labels;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.r;
import static mapeador.cypher.Cypher.timestamp;
import static mapeador.cypher.Cypher.type;
import static mapeador.generated.nodes.City;
import static mapeador.generated.nodes.Movie;
import static mapeador.generated.nodes.Person;
import static mapeador.generated.relationships.ACTED_IN;
import static mapeador.generated.relationships.BORN_IN;
import static mapeador.generated.relationships.DIRECTED;
import static mapeador.generated.relationships.KNOWS;
import static mapeador.models.Movie.title;
import static mapeador.models.Person.name;
import static mapeador.models.Person.surname;
import static org.assertj.core.api.Assertions.assertThat;

public class MergeClause_tests
{
    @Test void can_merge_single_node_with_a_label()
    {
        final var expected = "MERGE (robert :Critic) RETURN robert, labels(robert)";

        final var query = MERGE("robert", Label.of("Critic"))
            .RETURN(r("robert"), labels("robert"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_single_node_with_properties()
    {
        final var expected = "MERGE (charlie {name: 'Charlie Sheen', age: 10}) RETURN charlie";

        final var query = MERGE("charlie", map(name, "Charlie Sheen", "age", 10)).RETURN(r("charlie"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_single_node_specifying_both_label_and_property()
    {
        final var expected = "MERGE (michael :Person {name: 'Michael Douglas'}) RETURN michael.name, michael.bornIn";

        final var query = MERGE("michael", Person, map(name, "Michael Douglas")).RETURN(r("michael.name, michael.bornIn"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_single_node_derived_from_an_existing_node_property()
    {
        final var expected = "MATCH (person :Person)\n" +
                             "MERGE (city :City {name: person.bornIn})\n" +
                             "RETURN person.name, person.bornIn, city";

        final var person = Person("person");
        final var city = City("city");

        final var query = MATCH(person)
            .MERGE(city, map(name, person.bornIn))
            .RETURN(person.name, person.bornIn, city);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_a_node_and_set_properties_if_the_node_needs_to_be_created()
    {
        final var expected = "MERGE (keanu :Person {name: 'Keanu Reeves'})\n" +
                             "ON CREATE\n" +
                             "SET keanu.created = timestamp()\n" +
                             "RETURN keanu.name, keanu.created";

        final var keanu = Person("keanu");

        final var query = MERGE(keanu, map(name, "Keanu Reeves"))
            .ON_CREATE_SET(keanu.created.to(timestamp()))
            .RETURN(keanu.name, keanu.created);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_a_node_and_set_properties_if_the_node_needs_to_be_updated()
    {
        final var expected = "MERGE (keanu :Person {name: 'Keanu Reeves'})\n" +
                             "ON MATCH\n" +
                             "SET keanu.updated = timestamp()\n" +
                             "RETURN keanu.name, keanu.updated";

        final var keanu = Person("keanu");

        final var query = MERGE(keanu, map(name, "Keanu Reeves"))
            .ON_MATCH_SET(keanu.updated.to(timestamp()))
            .RETURN(keanu.name, keanu.updated);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_a_node_and_set_properties_if_the_node_needs_to_be_created_or_updated()
    {
        final var expected = "MERGE (keanu :Person {name: 'Keanu Reeves'})\n" +
                             "ON CREATE\n" +
                             "SET keanu.created = timestamp()\n" +
                             "ON MATCH\n" +
                             "SET keanu.updated = timestamp()\n" +
                             "RETURN keanu.name, keanu.updated";

        final var keanu = Person("keanu");
        final var query = MERGE(keanu, map(name, "Keanu Reeves"))
            .ON_CREATE_SET(keanu.created.to(timestamp()))
            .ON_MATCH_SET(keanu.updated.to(timestamp()))
            .RETURN(keanu.name, keanu.updated);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_with_ON_MATCH_setting_multiple_properties()
    {
        final var expected = "MERGE (person :Person)\n" +
                             "ON MATCH\n" +
                             "SET person.name = 'Pepe', person.updated = timestamp()\n" +
                             "RETURN person.name, person.updated";

        final var person = Person("person");
        final var query = MERGE(person)
            .ON_MATCH_SET(person.name.to("Pepe"), person.updated.to(timestamp()))
            .RETURN(person.name, person.updated);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_on_relationships()
    {
        final var expected = "MATCH (charlie :Person {name: 'Charlie Sheen'}), (wallStreet :Movie {title: 'Wall Street'})\n" +
                             "MERGE (charlie)-[r :ACTED_IN]->(wallStreet)\n" +
                             "RETURN charlie.name, type(r), wallStreet.title";

        final var charlie = Person("charlie");
        final var wallStreet = Movie("wallStreet");
        final var r = ACTED_IN("r");
        final var query = MATCH(charlie, map(name, "Charlie Sheen")).and(wallStreet, map(title, "Wall Street"))
                                                                    .MERGE(charlie.name()).outgoing(r).to(wallStreet.name())
                                                                    .RETURN(charlie.name, type(r), wallStreet.title);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_on_multiple_relationships()
    {
        final var expected = "MATCH (oliver :Person {name: 'Oliver Stone'}), (reiner :Person {name: 'Rob Reiner'})\n" +
                             "MERGE (oliver)-[:DIRECTED]->(movie :Movie)<-[:ACTED_IN]-(reiner)\n" +
                             "RETURN movie";

        final var oliver = Person("oliver");
        final var reiner = Person("reiner");
        final var movie = Movie("movie");
        final var query = MATCH(oliver, map(name, "Oliver Stone")).and(reiner, map(name, "Rob Reiner"))
                                                                  .MERGE(oliver.name())
                                                                  .outgoing(DIRECTED)
                                                                  .to(movie)
                                                                  .incoming(ACTED_IN)
                                                                  .from(reiner.name())
                                                                  .RETURN(movie);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_on_undirected_relationships()
    {
        final var expected = "MATCH (charlie :Person {name: 'Charlie Sheen'}), (oliver :Person {name: 'Oliver Stone'})\n" +
                             "MERGE (charlie)-[r :KNOWS]-(oliver)\n" +
                             "RETURN r";

        final var oliver = Person("oliver");
        final var charlie = Person("charlie");
        final var r = KNOWS("r");
        final var query = MATCH(charlie, map(name, "Charlie Sheen")).and(oliver, map(name, "Oliver Stone"))
                                                                    .MERGE(charlie.name()).related(r).with(oliver.name())
                                                                    .RETURN(r);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_merge_on_a_relationship_between_2_existing_nodes()
    {
        final var expected = "MATCH (person :Person)\n" +
                             "MERGE (city :City {name: person.bornIn})\n" +
                             "MERGE (person)-[r :BORN_IN]->(city)\n" +
                             "RETURN person.name, person.bornIn, city";

        final var person = Person("person");
        final var city = City("city");

        final var query = MATCH(person)
            .MERGE(city, map(name, person.bornIn))
            .MERGE(person.name()).outgoing("r", BORN_IN).to(city.name())
            .RETURN(person.name, person.bornIn, city);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_Merge_on_a_relationship_between_an_existing_node_and_a_merged_node_derived_from_a_node_property()
    {
        final var expected = "MATCH (person :Person)\n" +
                             "MERGE (person)-[r :HAS_CHAUFFEUR]->(chauffeur :Chauffeur {name: person.chauffeurName})\n" +
                             "RETURN person.name, person.chauffeurName, chauffeur";

        final var person = Person("person");

        final var query = MATCH(person)
            .MERGE("person").outgoing("r", Type.of("HAS_CHAUFFEUR")).to("chauffeur", Label.of("Chauffeur"), map("name", r("person.chauffeurName")))
            .RETURN(person.name, r("person.chauffeurName, chauffeur"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_use_map_parameters_with_MERGE()
    {
        final var expected = "MERGE (person :Person {name: $param.name, surname: $param.surname}) RETURN person.name, person.surname";

        final var person = Person("person");
        final var $param = $("param");
        final var query = MERGE(person, map(name, $param.get("name"), surname, $param.get("surname")))
            .RETURN(person.name, person.surname);

        assertThat(query.toCypher()).isEqualTo(expected);
    }
}
