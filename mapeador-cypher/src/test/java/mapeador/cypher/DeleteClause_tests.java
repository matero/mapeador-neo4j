/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Label;
import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.r;
import static mapeador.generated.nodes.Person;
import static mapeador.generated.relationships.ACTED_IN;
import static mapeador.models.Person.name;
import static org.assertj.core.api.Assertions.assertThat;

class DeleteClause_tests
{
    @Test void can_delete_a_single_node()
    {
        final var expected = "MATCH (n :Person {name: 'UNKNOWN'})\n" +
                             "DELETE n";

        // raw

        var query = MATCH("n", Label.of("Person"), map("name", "UNKNOWN"))
            .DELETE(r("n"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        query = MATCH("n", Person, map(name, "UNKNOWN"))
            .DELETE(r("n"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // using generated code
        final var n = Person("n");
        query = MATCH(n, map(name, "UNKNOWN"))
            .DELETE(n);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_delete_all_nodes_and_relationships()
    {
        final var expected = "MATCH (n)\n" +
                             "DETACH DELETE n";

        assertThat(MATCH("n").DETACH_DELETE(r("n")).toCypher()).isEqualTo(expected);
    }

    @Test void can_delete_a_node_with_all_its_relationships()
    {
        final var expected = "MATCH (n {name: 'Andy'})\n" +
                             "DETACH DELETE n";

        assertThat(MATCH("n", map("name", "Andy")).DETACH_DELETE(r("n")).toCypher()).isEqualTo(expected);
    }

    @Test void can_delete_relationships_only()
    {
        final var expected = "MATCH (n {name: 'Andy'})-[r :ACTED_IN]->()\n" +
                             "DELETE r";

        final var r = ACTED_IN("r");
        final var delete = MATCH("n", map("name", "Andy")).outgoing(r).to()
                                                          .DELETE(r);

        assertThat(delete.toCypher()).isEqualTo(expected);
    }
}
