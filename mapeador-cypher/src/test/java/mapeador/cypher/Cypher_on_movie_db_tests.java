/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.MATCH_shortestPath;
import static mapeador.cypher.Cypher.NOT;
import static mapeador.cypher.Cypher.anyHops;
import static mapeador.cypher.Cypher.countAll;
import static mapeador.cypher.Cypher.hops;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.pattern;
import static mapeador.cypher.Cypher.r;
import static mapeador.cypher.Cypher.type;
import static mapeador.generated.nodes.Movie;
import static mapeador.generated.nodes.Person;
import static mapeador.generated.relationships.ACTED_IN;
import static mapeador.generated.relationships.DIRECTED;
import static mapeador.models.Movie.title;
import static mapeador.models.Person.name;
import static org.assertj.core.api.Assertions.assertThat;

class Cypher_on_movie_db_tests
{
    @Test void can_find_an_actor_by_name()
    {
        final var expected = "MATCH (tom {name: 'Tom Hanks'}) RETURN tom";

        // raw
        var query = MATCH("tom", map("name", "Tom Hanks")).RETURN(r("tom"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        final var tom = NodeVariable.of("tom");
        query = MATCH(tom, map(name, "Tom Hanks")).RETURN(tom);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_find_10_people()
    {
        final var expected = "MATCH (people) RETURN people.name ORDER BY people.name LIMIT 10";

        // raw
        var query = MATCH("people").RETURN(r("people.name")).ORDER_BY(r("people.name")).LIMIT(10);
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        final var people = Person.variable("people");
        query = MATCH(people).RETURN(people.name).ORDER_BY(people.name).LIMIT(10);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_find_movies_released_in_the_1990s()
    {
        final var expected = "MATCH (nineties :Movie) " +
                             "WHERE nineties.released >= 1990 AND nineties.released < 2000 " +
                             "RETURN nineties.title " +
                             "ORDER BY nineties.title";

        // mapeador way, using raw expressions (not type-safe)
        var query = MATCH("nineties", Movie).WHERE(r("nineties.released >= 1990 AND nineties.released < 2000"))
                                            .RETURN(r("nineties.title"))
                                            .ORDER_BY(r("nineties.title"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // mapeador way, using node-variable (type-safe)
        final var nineties = Movie.node("nineties");
        query = MATCH(nineties).WHERE(nineties.released.GE(1990).AND(nineties.released.LT(2000)))
                               .RETURN(nineties.title)
                               .ORDER_BY(nineties.title);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_list_all_Tom_Hanks_movies()
    {
        final var expected = "MATCH (tom :Person {name: 'Tom Hanks'})-[:ACTED_IN]->(movie) " +
                             "RETURN movie.title " +
                             "ORDER BY movie.released, movie.title";

        // mapeador, no-typed way
        var query = MATCH("tom", Person, map("name", "Tom Hanks")).outgoing(ACTED_IN).to("movie")
                                                                .RETURN(r("movie.title"))
                                                                .ORDER_BY(r("movie.released, movie.title"));

        assertThat(query.toCypher()).isEqualTo(expected);

        // mapeador, typed way
        final var movie = Movie.variable("movie");
        final var tom = Person.node("tom");
        query = MATCH(tom, map(name, "Tom Hanks")).outgoing(ACTED_IN).to(movie).RETURN(movie.title).ORDER_BY(movie.released, movie.title);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_match_who_directed_cloud_atlas()
    {
        final var expected = "MATCH (cloudAtlas {title: 'Cloud Atlas'})<-[:DIRECTED]-(directors) RETURN directors.name";

        var query = MATCH("cloudAtlas", map("title", "Cloud Atlas")).incoming(DIRECTED).from("directors").RETURN(r("directors.name"));

        assertThat(query.toCypher()).isEqualTo(expected);

        final var directors = Person.variable("directors");
        query = MATCH("cloudAtlas", map(title, "Cloud Atlas")).incoming(DIRECTED).from(directors).RETURN(directors.name);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_fetch_tom_hanks_coActors()
    {
        final var expected = "MATCH (tom :Person {name: 'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActor) " +
                             "RETURN coActor.name " +
                             "ORDER BY coActor.name";

        // mapeador, no-typed way
        var query = MATCH("tom", Person, map("name", "Tom Hanks")).outgoing(ACTED_IN).to("m").incoming(ACTED_IN).from("coActor")
                                                                .RETURN(r("coActor.name"))
                                                                .ORDER_BY(r("coActor.name"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // mapeador, typed way
        final var tom = Person.node("tom");
        final var coActor = Person.variable("coActor");
        query = MATCH(tom, map(name, "Tom Hanks")).outgoing(ACTED_IN).to("m").incoming(ACTED_IN).from(coActor)
                                                .RETURN(coActor.name)
                                                .ORDER_BY(coActor.name);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_see_how_people_is_related_to_Cloud_Atlas_movie()
    {
        final var expected = "MATCH (people)-[relatedTo]-(:Movie {title: 'Cloud Atlas'}) " +
                             "RETURN people.name AS name, type(relatedTo) AS type, relatedTo AS related";

        // mapeador, no-typed way
        var query = MATCH("people").related("relatedTo").with(Movie, map(title, "Cloud Atlas"))
                                   .RETURN(r("people.name AS name, type(relatedTo) AS type, relatedTo AS related"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // mapeador, typed way
        final var people = Person.variable("people");
        final var relatedTo = RelationshipVariable.of("relatedTo");
        query = MATCH(people).related(relatedTo).with(Movie, map(title, "Cloud Atlas"))
                             .RETURN(people.name.AS("name"), type(relatedTo).AS("type"), relatedTo.AS("related"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_match_Movies_and_Actors_up_to_4_hops_away_from_Kevin_Bacon()
    {
        final var expected = "MATCH (bacon :Person {name: 'Kevin Bacon'})-[*1..4]-(hollywood) RETURN DISTINCT hollywood";

        var query = MATCH("bacon", Person, map("name", "Kevin Bacon")).related(hops(1, 4)).with("hollywood").RETURN_DISTINCT(r("hollywood"));
        assertThat(query.toCypher()).isEqualTo(expected);

        final var bacon = Person.node("bacon");
        final var hollywood = NodeVariable.of("hollywood");
        query = MATCH(bacon, map(name, "Kevin Bacon")).related(hops(1, 4)).with(hollywood).RETURN_DISTINCT(hollywood);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_match_the_shortest_path_from_Kevin_Bacon_to_Meg_Ryan()
    {
        final var expected = "MATCH p = shortestPath((bacon :Person {name: 'Kevin Bacon'})-[*]-(meg :Person {name: 'Meg Ryan'})) RETURN p";

        var query = MATCH_shortestPath("p")
            .node("bacon", Person, map("name", "Kevin Bacon")).related(anyHops()).with("meg", Person, map("name", "Meg Ryan"))
            .RETURN(r("p"));
        assertThat(query.toCypher()).isEqualTo(expected);

        final var bacon = Person.node("bacon");
        final var meg = Person.node("meg");
        query = MATCH_shortestPath("p")
            .node(bacon, map(name, "Kevin Bacon")).related(anyHops()).with(meg, map(name, "Meg Ryan"))
            .RETURN(r("p"));
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_find_co_co_actors_who_havent_worked_with_Tom_Hanks()
    {
        final var expected = "MATCH (tom :Person {name: 'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors), " +
                             "(coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cocoActors) " +
                             "WHERE NOT (tom)-[:ACTED_IN]->()<-[:ACTED_IN]-(cocoActors) AND tom <> cocoActors " +
                             "RETURN cocoActors.name AS Recommended, count(*) AS Strength ORDER BY Strength DESC";

        // raw
        var query = MATCH("tom", Person, map("name", "Tom Hanks"))
            .outgoing(ACTED_IN).to("m").incoming(ACTED_IN).from("coActors")
            .and("coActors").outgoing(ACTED_IN).to("m2").incoming(ACTED_IN).from("cocoActors")
            .WHERE(NOT(pattern("tom").outgoing(ACTED_IN).to().incoming(ACTED_IN).from("cocoActors")).AND("tom <> cocoActors"))
            .RETURN(r("cocoActors.name AS Recommended, count(*) AS Strength"))
            .ORDER_BY(r("Strength DESC"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        final var tom = Person.node("tom");
        final var coActors = Person.variable("coActors");
        final var cocoActors = Person.variable("cocoActors");
        final var Strength = countAll().AS("Strength");
        query = MATCH(tom, map(name, "Tom Hanks")).outgoing(ACTED_IN).to("m").incoming(ACTED_IN).from(coActors)
                                                .and(coActors).outgoing(ACTED_IN).to("m2").incoming(ACTED_IN).from(cocoActors)
                                                .WHERE(NOT(pattern(tom.name()).outgoing(ACTED_IN)
                                                                              .to()
                                                                              .incoming(ACTED_IN)
                                                                              .from(cocoActors)).AND(tom.NE(cocoActors)))
                                                .RETURN(cocoActors.name.AS("Recommended"), Strength)
                                                .ORDER_BY(Strength.DESC());
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void should_be_able_to_Find_someone_to_introduce_Tom_Hanks_to_Tom_Cruise()
    {
        // raw java driver way
        final var expected = "MATCH (tom :Person {name: 'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors)," +
                             " (coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cruise :Person {name: 'Tom Cruise'}) " +
                             "RETURN m.title + ' <== ' + coActors.name + ' ==> ' + m2.title";

        // raw
        var query = MATCH("tom", Person, map("name", "Tom Hanks"))
            .outgoing(ACTED_IN).to("m").incoming(ACTED_IN).from("coActors")
            .and("coActors").outgoing(ACTED_IN).to("m2").incoming(ACTED_IN).from("cruise", Person, map("name", "Tom Cruise"))
            .RETURN(r("m.title + ' <== ' + coActors.name + ' ==> ' + m2.title"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // mapeador typed
        final var m = Movie.variable("m");
        final var m2 = Movie.variable("m2");
        final var coActors = Person("coActors");
        final var tom = Person("tom");
        final var cruise = Person("cruise");
        query = MATCH(tom, map(name, "Tom Hanks")).outgoing(ACTED_IN).to(m).incoming(ACTED_IN).from("coActors")
                                                .and("coActors").outgoing(ACTED_IN).to(m2).incoming(ACTED_IN).from(cruise, map(name, "Tom Cruise"))
                                                .RETURN(m.title.concat(" <== ").concat(coActors.name).concat(" ==> ").concat(m2.title));
        assertThat(query.toCypher()).isEqualTo(expected);
    }
}
