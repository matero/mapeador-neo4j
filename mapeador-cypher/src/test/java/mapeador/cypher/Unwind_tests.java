/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.$;
import static mapeador.cypher.Cypher.CASE;
import static mapeador.cypher.Cypher.L;
import static mapeador.cypher.Cypher.NULL;
import static mapeador.cypher.Cypher.T;
import static mapeador.cypher.Cypher.UNWIND;
import static mapeador.cypher.Cypher.WITH;
import static mapeador.cypher.Cypher.alias;
import static mapeador.cypher.Cypher.collect;
import static mapeador.cypher.Cypher.group;
import static mapeador.cypher.Cypher.l;
import static mapeador.cypher.Cypher.list;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.r;
import static org.assertj.core.api.Assertions.assertThat;

public class Unwind_tests
{
    @Test void can_unwind_a_list()
    {
        final var expected = "UNWIND [1, 2, 3, NULL] AS x RETURN x, 'val' AS y";

        final var query = UNWIND(list(1, 2, 3, null).AS("x")).RETURN(r("x"), l("val").AS("y"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_create_a_distinct_list()
    {
        final var expected = "WITH [1, 1, 2, 2] AS coll\n" +
                             "UNWIND coll AS x\n" +
                             "WITH DISTINCT x\n" +
                             "RETURN collect(x) AS setOfVals";
        final var coll = alias("coll");
        final var query = WITH(list(1, 1, 2, 2).AS(coll))
            .UNWIND(coll.AS("x"))
            .WITH_DISTINCT("x")
            .RETURN(collect("x").AS("setOfVals"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void any_expression_list_that_may_be_used_with_UNWIND()
    {
        final var expected = "WITH [1, 2] AS a, [3, 4] AS b\n" +
                             "UNWIND (a + b) AS x\n" +
                             "RETURN x";

        final var a = alias("a");
        final var b = alias("b");
        final var query = WITH(list(1, 2).AS(a), list(3, 4).AS(b))
            .UNWIND(group(a.plus(b)).AS("x"))
            .RETURN("x");

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_use_UNWIND_with_a_list_of_lists()
    {
        final var expected = "WITH [[1, 2], [3, 4], 5] AS nested\n" +
                             "UNWIND nested AS x\n" +
                             "UNWIND x AS y\n" +
                             "RETURN y";

        final var nested = alias("nested");
        final var x = alias("x");
        final var query = WITH(list(list(1, 2), list(3, 4), 5).AS(nested))
            .UNWIND(nested.AS(x))
            .UNWIND(x.AS("y"))
            .RETURN("y");

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_use_UNWIND_with_an_empty_list()
    {
        final var expected = "UNWIND [] AS empty RETURN empty, 'literal_that_is_not_returned'";

        final var empty = alias("empty");
        final var query = UNWIND(list().AS(empty))
            .RETURN(empty, l("literal_that_is_not_returned"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_use_CASE_to_replace_an_empty_list_with_a_null()
    {
        final var expected = "WITH [] AS list\n" +
                             "UNWIND CASE\n" +
                             "  WHEN list = [] THEN [NULL]\n" +
                             "  ELSE list\n" +
                             "END AS emptyList\n" +
                             "RETURN emptyList";

        final var list = alias("list");
        final var query = WITH(list().AS(list))
            .UNWIND(CASE.WHEN(list.EQ(list())).THEN(list(NULL))
                        .ELSE(list).END().AS("emptyList"))
            .RETURN("emptyList");

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_use_UNWIND_with_expression_that_isnt_a_list()
    {
        final var expected = "UNWIND NULL AS x RETURN x, 'some_literal'";

        final var query = UNWIND(NULL.AS("x")).RETURN("x", l("some_literal"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_create_nodes_and_relationships_from_list_parameters()
    {
        final var expected = "UNWIND $events AS event\n" +
                             "MERGE (y :Year {year: event.year})\n" +
                             "MERGE (y)<-[:`IN`]-(e :Event {id: event.id})\n" +
                             "RETURN e.id AS x\n" +
                             "ORDER BY x";

        final var query = UNWIND($("events").AS("event"))
            .MERGE("y", L("Year"), map("year", r("event.year")))
            .MERGE("y").incoming(T("IN")).from("e", L("Event"), map("id", r("event.id")))
            .RETURN(r("e.id").AS("x"))
            .ORDER_BY("x");

        assertThat(query.toCypher()).isEqualTo(expected);
    }
}
