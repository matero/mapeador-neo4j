/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Label;
import mapeador.Labels;
import mapeador.Type;
import mapeador.generated.labels;
import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.CREATE;
import static mapeador.cypher.Cypher.CREATE_path;
import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.r;
import static mapeador.cypher.Cypher.type;
import static mapeador.generated.nodes.Movie;
import static mapeador.generated.nodes.Person;
import static mapeador.generated.relationships.ACTED_IN;
import static mapeador.generated.relationships.PRODUCED;
import static mapeador.generated.relationships.RELTYPE;
import static mapeador.models.ACTED_IN.role;
import static mapeador.models.Movie.title;
import static mapeador.models.Person.born;
import static mapeador.models.Person.name;
import static org.assertj.core.api.Assertions.assertThat;

class CreateClause_tests
{
    @Test void can_create_single_node()
    {
        final var create = CREATE("n");

        assertThat(create.toCypher()).isEqualTo("CREATE (n)");
    }

    @Test void can_create_multiple_nodes()
    {
        final var create = CREATE("n").and("m");

        assertThat(create.toCypher()).isEqualTo("CREATE (n), (m)");
    }

    @Test void can_create_a_node_with_a_label()
    {
        //raw
        var create = CREATE("n", Label.of("Person"));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person)");

        // using Node definition
        create = CREATE("n", Person);

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person)");

        // using node
        final var n = Person("n");
        create = CREATE(n);

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person)");
    }

    @Test void can_create_a_node_with_multiple_labels()
    {
        //raw
        var create = CREATE("n", Labels.of(Label.of("Person"), Label.of("Swedish")));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person:Swedish)");

        // using Node definition
        create = CREATE("n", Labels.of(labels.Person, labels.Movie));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person:Movie)");
    }

    @Test void can_create_a_node_with_label_and_properties()
    {
        //raw
        var create = CREATE("n", Label.of("Person"), map("name", "Keanu Reaves", "born", 1964));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964})");

        // using Node definition
        create = CREATE("n", Person, map(name, "Keanu Reaves", born, 1964));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964})");

        // using node
        final var n = Person("n");
        create = CREATE(n, map(name, "Keanu Reaves", born, 1964));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964})");
    }

    @Test void can_return_created_node()
    {
        //raw
        var create = CREATE("n", Label.of("Person"), map("name", "Keanu Reaves", "born", 1964)).RETURN(r("n"));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n");

        // using Node definition
        create = CREATE("n", Person, map(name, "Keanu Reaves", born, 1964)).RETURN(r("n"));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n");

        // using node
        final var n = Person("n");
        create = CREATE(n, map(name, "Keanu Reaves", born, 1964)).RETURN(n);

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n");
    }

    @Test void can_return_created_node_property()
    {
        //raw
        var create = CREATE("n", Label.of("Person"), map("name", "Keanu Reaves", "born", 1964)).RETURN(r("n.name"));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n.name");

        // using Node definition
        create = CREATE("n", Person, map(name, "Keanu Reaves", born, 1964)).RETURN(r("n.name"));

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n.name");

        // using node
        final var n = Person("n");
        create = CREATE(n, map(name, "Keanu Reaves", born, 1964)).RETURN(n.name);

        assertThat(create.toCypher()).isEqualTo("CREATE (n :Person {name: 'Keanu Reaves', born: 1964}) RETURN n.name");
    }

    @Test void cant_create_a_relationship_between_two_nodes()
    {
        final var expected = "MATCH (a :Person), (b :Person) WHERE a.name = 'A' AND b.name = 'B'\n" +
                             "CREATE (a)-[r :RELTYPE]->(b)\n" +
                             "RETURN type(r)";
        var query = MATCH("a", Label.of("Person")).and("b", Label.of("Person")).WHERE(r("a.name = 'A' AND b.name = 'B'"))
                                                  .CREATE("a").outgoing("r", Type.of("RELTYPE")).to("b")
                                                  .RETURN(r("type(r)"));

        assertThat(query.toCypher()).isEqualTo(expected);

        query = MATCH("a", Person).and("b", Person).WHERE(r("a.name = 'A' AND b.name = 'B'"))
                                  .CREATE("a").outgoing("r", Type.of("RELTYPE")).to("b")
                                  .RETURN(type("r"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        final var a = Person("a");
        final var b = Person("b");
        final var r = RELTYPE("r");
        query = MATCH(a).and(b).WHERE(a.name.EQ('A').AND(b.name.EQ('B')))
                        .CREATE(a.name()).outgoing(r).to(b.name())
                        .RETURN(type(r));
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_create_a_relationship_and_set_properties()
    {
        final var expected = "MATCH (keanu :Person {name: 'Keanu Reaves'}), (matrix :Movie {title: 'The Matrix'})\n" +
                             "CREATE (keanu)-[a :ACTED_IN {role: 'Neo'}]->(matrix)\n" +
                             "RETURN type(a), a.role";

        var query = MATCH("keanu", Label.of("Person"), map("name", "Keanu Reaves"))
            .and("matrix", Label.of("Movie"), map("title", "The Matrix"))
            .CREATE("keanu").outgoing("a", Type.of("ACTED_IN"), map("role", "Neo")).to("matrix")
            .RETURN(r("type(a), a.role"));

        assertThat(query.toCypher()).isEqualTo(expected);

        query = MATCH("keanu", Person, map("name", "Keanu Reaves")).and("matrix", Movie, map("title", "The Matrix"))
                                                                 .CREATE("keanu").outgoing("a", ACTED_IN, map("role", "Neo")).to("matrix")
                                                                 .RETURN(type("a"), RelationshipVariable.of("a").get("role"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        final var keanu = Person("keanu");
        final var matrix = Movie("matrix");
        final var a = ACTED_IN("a");
        query = MATCH(keanu, map("name", "Keanu Reaves")).and(matrix, map(title, "The Matrix"))
                                                       .CREATE(keanu.name()).outgoing(a, map("role", "Neo")).to(matrix.name())
                                                       .RETURN(type(a), a.role);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_create_a_full_path()
    {
        final var expected = "CREATE p = (:Person {name: 'Keanu Reaves'})-[:ACTED_IN {role: 'Neo'}]->(:Movie {title: 'The Matrix'})" +
                             "<-[:PRODUCED]-(:Person {name: 'Joel Silver', born: 1952}) RETURN p";


        var query = CREATE_path("p").node(Label.of("Person"), map("name", "Keanu Reaves"))
                                    .outgoing(Type.of("ACTED_IN"), map("role", "Neo")).to(Label.of("Movie"), map("title", "The Matrix"))
                                    .incoming(Type.of("PRODUCED")).from(Label.of("Person"), map("name", "Joel Silver", "born", 1952))
                                    .RETURN(r("p"));

        assertThat(query.toCypher()).isEqualTo(expected);

        query = CREATE_path("p").node(Person, map("name", "Keanu Reaves"))
                                .outgoing(ACTED_IN, map("role", "Neo")).to(Movie, map("title", "The Matrix"))
                                .incoming(PRODUCED).from(Person, map("name", "Joel Silver", "born", 1952))
                                .RETURN(r("p"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // typed
        query = CREATE_path("p").node(Person, map(name, "Keanu Reaves")) // type safe
                                .outgoing(ACTED_IN, map(role, "Neo")).to(Movie, map(title, "The Matrix"))
                                .incoming(PRODUCED).from(Person, map(name, "Joel Silver", born, 1952))
                                .RETURN(r("p"));
        assertThat(query.toCypher()).isEqualTo(expected);
    }
}
