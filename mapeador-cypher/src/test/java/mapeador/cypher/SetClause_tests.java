/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.$;
import static mapeador.cypher.Cypher.CASE;
import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.NULL;
import static mapeador.cypher.Cypher.TRUE;
import static mapeador.cypher.Cypher.group;
import static mapeador.cypher.Cypher.labels;
import static mapeador.cypher.Cypher.map;
import static mapeador.cypher.Cypher.r;
import static mapeador.generated.nodes.Person;
import static mapeador.models.Person.name;
import static mapeador.models.Person.worksIn;
import static org.assertj.core.api.Assertions.assertThat;

class SetClause_tests
{
    @Test void can_set_a_property()
    {
        final var expected = "MATCH (n {name: 'Andy'})\n" +
                             "SET n.surname = 'Taylor'\n" +
                             "RETURN n.name, n.surname";

        // raw expressions as SetItem
        var query = MATCH("n", map("name", "Andy"))
            .SET(r("n.surname = 'Taylor'"))
            .RETURN(r("n.name, n.surname"));
        assertThat(query.toCypher()).isEqualTo(expected);

        query = MATCH("n", map("name", "Andy"))
            .SET(r("n.surname").to("Taylor"))
            .RETURN(r("n.name, n.surname"));
        assertThat(query.toCypher()).isEqualTo(expected);

        // using abstractions
        {
            final var n = NodeVariable.of("n");
            query = MATCH(n, map("name", "Andy"))
                .SET(n.get("surname").to("Taylor"))
                .RETURN(n.get("name"), n.get("surname"));
            assertThat(query.toCypher()).isEqualTo(expected);
        }

        // using generated code
        {
            final var n = Person.variable("n");
            query = MATCH(n, map(name, "Andy"))
                .SET(n.surname.to("Taylor"))
                .RETURN(n.name, n.surname);
            assertThat(query.toCypher()).isEqualTo(expected);
        }
    }

    @Test void can_set_a_property_for_a_node_selected_by_an_expression()
    {
        final var expected = "MATCH (n :Person {name: 'Andy'})\n" +
                             "SET (CASE\n" +
                             "  WHEN n.age = 36 THEN n\n" +
                             "END).worksIn = 'Malmo'\n" +
                             "RETURN n.name, n.worksIn";

        // using generated code
        final var n = Person("n");
        final var query = MATCH(n, map(name, "Andy"))
            .SET(group(CASE.WHEN(n.age.EQ(36))
                           .THEN(n)
                           .END()).set(worksIn, "Malmo"))
            .RETURN(n.name, n.worksIn);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_remove_a_property()
    {
        final var expected = "MATCH (n :Person {name: 'Andy'})\n" +
                             "SET n.name = NULL\n" +
                             "RETURN n.name, n.age";

        // using generated code
        final var n = Person("n");
        final var query = MATCH(n, map(name, "Andy"))
            .SET(n.name.to(NULL))
            .RETURN(n.name, n.age);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_copy_properties_between_entities()
    {
        final var expected = "MATCH (andy {name: 'Andy'}), (peter :Person {name: 'Peter'})\n" +
                             "SET andy = peter\n" +
                             "RETURN andy.name, andy.age, peter.name, peter.age";

        // using generated code
        final var andy = Person.variable("andy");
        final var peter = Person("peter");
        final var query = MATCH(andy, map(name, "Andy")).and(peter, map(name, "Peter"))
                                                        .SET(andy.to(peter))
                                                        .RETURN(andy.name, andy.age, peter.name, peter.age);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_replace_properties_using_a_map()
    {
        final var expected = "MATCH (andy {name: 'Andy'})\n" +
                             "SET andy = {name: 'Peter Smith', position: 'Entrepreneur'}\n" +
                             "RETURN andy.name, andy.age, andy.position";

        // using generated code
        final var andy = Person.variable("andy");
        final var query = MATCH(andy, map(name, "Andy"))
            .SET(andy.to(map(name, "Peter Smith", "position", "Entrepreneur")))
            .RETURN(andy.name, andy.age, r("andy.position"));
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_remove_all_properties_using_an_empty_map()
    {
        final var expected = "MATCH (andy {name: 'Andy'})\n" +
                             "SET andy = {}\n" +
                             "RETURN andy.name, andy.age";

        // using generated code
        final var andy = Person.variable("andy");
        final var query = MATCH(andy, map(name, "Andy"))
            .SET(andy.to(map()))
            .RETURN(andy.name, andy.age);
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_mutate_properties_using_a_map()
    {
        final var expected = "MATCH (andy {name: 'Andy'})\n" +
                             "SET andy += {name: 'Peter Smith', hungry: TRUE, position: 'Entrepreneur'}\n" +
                             "RETURN andy.name, andy.age, andy.hungry, andy.position";

        // using generated code
        final var andy = Person.variable("andy");
        final var query = MATCH(andy, map(name, "Andy"))
            .SET(andy.put(map(name, "Peter Smith", "hungry", TRUE, "position", "Entrepreneur")))
            .RETURN(andy.name, andy.age, r("andy.hungry, andy.position"));
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_set_multiple_properties()
    {
        final var expected = "MATCH (n {name: 'Andy'})\n" +
                             "SET n.worksIn = 'Chasque', n.surname = 'Taylor'";
        final var n = Person.variable("n");
        final var query = MATCH(n, map(name, "Andy"))
            .SET(n.worksIn.to("Chasque"), n.surname.to("Taylor"));
        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_set_a_property_using_a_parameter()
    {
        final var expected = "MATCH (n {name: 'Andy'})\n" +
                             "SET n.surname = $surname\n" +
                             "RETURN n.name, n.surname";
        final var n = Person.variable("n");
        final var query = MATCH(n, map(name, "Andy"))
            .SET(n.surname.to($("surname")))
            .RETURN(n.name, n.surname);

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_set_all_properties_using_a_parameter()
    {
        final var expected = "MATCH (n {name: 'Andy'})\n" +
                             "SET n = $props\n" +
                             "RETURN n.name, n.age, n.hungry, n.position";
        final var n = Person.variable("n");
        final var query = MATCH(n, map(name, "Andy"))
            .SET(n.to($("props")))
            .RETURN(n.name, n.age, r("n.hungry, n.position"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_set_a_label_on_a_node()
    {
        final var expected = "MATCH (n {name: 'Stefan'})\n" +
                             "SET n:German\n" +
                             "RETURN n.name, labels(n) AS labels";

        final var n = Person.variable("n");
        final var query = MATCH(n, map(name, "Stefan"))
            .SET(n.label("German"))
            .RETURN(n.name, labels(n).AS("labels"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }

    @Test void can_set_multiple_labels_on_a_node()
    {
        final var expected = "MATCH (n {name: 'George'})\n" +
                             "SET n:Swedish:Bossman\n" +
                             "RETURN n.name, labels(n) AS labels";

        final var n = Person.variable("n");
        final var query = MATCH(n, map(name, "George"))
            .SET(n.labels("Swedish", "Bossman"))
            .RETURN(n.name, labels(n).AS("labels"));

        assertThat(query.toCypher()).isEqualTo(expected);
    }
}
