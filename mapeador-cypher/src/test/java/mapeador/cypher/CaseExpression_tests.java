/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import org.junit.jupiter.api.Test;

import static mapeador.cypher.Cypher.CASE;
import static mapeador.cypher.Cypher.MATCH;
import static mapeador.cypher.Cypher.r;
import static org.assertj.core.api.Assertions.assertThat;

class CaseExpression_tests
{
    @Test void can_compare_an_expression_to_multiple_values()
    {
        final var expected = "MATCH (n) RETURN CASE n.eyes\n" +
                             "  WHEN 'blue' THEN 1\n" +
                             "  WHEN 'brown' THEN 2\n" +
                             "  ELSE 3\n" +
                             "END AS result";

        assertThat(MATCH("n").RETURN(CASE(r("n.eyes"))
                              .WHEN("blue").THEN(1)
                              .WHEN("brown").THEN(2)
                              .ELSE(3)
                              .END().AS("result")).toCypher()).isEqualTo(expected);
    }

    @Test void can_express_mulitple_conditionals()
    {
        final var expected = "MATCH (n) RETURN CASE\n" +
                             "  WHEN n.eyes = 'blue' THEN 1\n" +
                             "  WHEN n.age < 40 THEN 2\n" +
                             "  ELSE 3\n" +
                             "END AS result";

        assertThat(MATCH("n").RETURN(CASE.WHEN(r("n.eyes = 'blue'")).THEN(1)
                                         .WHEN(r("n.age < 40")).THEN(2)
                                         .ELSE(3)
                                         .END().AS("result")).toCypher()).isEqualTo(expected);
    }
}
