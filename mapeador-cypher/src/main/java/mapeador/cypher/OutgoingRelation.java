/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;

import java.util.Objects;

public interface OutgoingRelation<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>>
{
    CONNECTOR to();

    CONNECTOR to(String name);

    CONNECTOR to(Labels labels);

    default CONNECTOR to(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(labels.getNodeLabels());
    }

    CONNECTOR to(PropertiesMap properties);

    CONNECTOR to(Parameter properties);

    CONNECTOR to(String name, Labels labels);

    default CONNECTOR to(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(name, labels.getNodeLabels());
    }

    CONNECTOR to(String name, PropertiesMap properties);

    CONNECTOR to(String name, Parameter properties);

    CONNECTOR to(Labels labels, PropertiesMap properties);

    CONNECTOR to(Labels labels, Parameter properties);

    default CONNECTOR to(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(labels.getNodeLabels(), properties);
    }

    default CONNECTOR to(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(labels.getNodeLabels(), properties);
    }

    CONNECTOR to(String name, Labels labels, PropertiesMap properties);

    CONNECTOR to(String name, Labels labels, Parameter properties);

    default CONNECTOR to(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR to(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return to(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR to(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return to(node.name(), node.getNodeLabels());
    }

    default CONNECTOR to(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return to(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR to(final NodeVariable node, final Parameter properties)
    {
        Objects.requireNonNull(node, "node is required");
        return to(node.name(), node.getNodeLabels(), properties);
    }
}
