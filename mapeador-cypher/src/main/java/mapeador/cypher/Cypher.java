/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.IntegerProperty;
import mapeador.Label;
import mapeador.Labels;
import mapeador.StringProperty;
import mapeador.Type;
import mapeador.Types;
import mapeador.Utility;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public final class Cypher extends mapeador.Utility
{
    private static final FunctionCall TIMESTAMP;

    static {
        TIMESTAMP = FunctionCall.of("timestamp");
    }

    private Cypher() {super(Cypher.class);}

    // ============================================================================================================================================ //
    // E X P R E S S I O N S   &   L I T E R A L S                                                                                                  //
    // ============================================================================================================================================ //
    public static Expression all = r("*");
    public static Literal NULL = Literal.NULL;
    public static BooleanLiteral TRUE = BooleanValue.TRUE;
    public static BooleanLiteral FALSE = BooleanValue.FALSE;

    public static Not NOT(final Predicate operand) {return new Not(Objects.requireNonNull(operand, "operand is required"));}

    public static Expression e(final Object o)
    {
        return Expression.of(o);
    }

    public static Literal l(final Object o) {return Literal.of(o);}

    // boolean literals
    public static BooleanLiteral l(final boolean value) {return BooleanValue.of(value);}

    public static BooleanLiteral l(final Boolean value) {return BooleanValue.of(value);}

    // byte literals
    public static ByteLiteral l(final byte value) {return ByteValue.of(value);}

    public static ByteLiteral l(final Byte value) {return ByteValue.of(value);}

    // short literals
    public static ShortLiteral l(final short value) {return ShortValue.of(value);}

    public static ShortLiteral l(final Short value) {return ShortValue.of(value);}

    // int literals
    public static IntegerLiteral l(final int value) {return IntegerValue.of(value);}

    public static IntegerLiteral l(final Integer value) {return IntegerValue.of(value);}

    // long literals
    public static LongLiteral l(final long value) {return LongValue.of(value);}

    public static LongLiteral l(final Long value) {return LongValue.of(value);}

    // float literals
    public static FloatLiteral l(final float value) {return FloatValue.of(value);}

    public static FloatLiteral l(final Float value) {return FloatValue.of(value);}

    // double literals
    public static DoubleLiteral l(final double value) {return DoubleValue.of(value);}

    public static DoubleLiteral l(final Double value) {return DoubleValue.of(value);}

    // String literals
    public static StringLiteral l(final char value) {return StringValue.of(value);}

    public static StringLiteral l(final Character value) {return StringValue.of(value);}

    public static StringLiteral l(final CharSequence value) {return StringValue.of(value);}

    public static StringLiteral l(final String value) {return StringValue.of(value);}

    // list literals
    public static ListLiteral booleans(final boolean first, final boolean... others) {return ListValue.of(first, others);}

    public static ListLiteral booleans(final boolean[] elements) {return ListValue.of(elements);}

    public static ListLiteral bytes(final byte first, final byte... others) {return ListValue.of(first, others);}

    public static ListLiteral bytes(final byte[] elements) {return ListValue.of(elements);}

    public static ListLiteral shorts(final short first, final short... others) {return ListValue.of(first, others);}

    public static ListLiteral shorts(final short[] elements) {return ListValue.of(elements);}

    public static ListLiteral ints(final int first, final int... others) {return ListValue.of(first, others);}

    public static ListLiteral ints(final int[] elements) {return ListValue.of(elements);}

    public static ListLiteral longs(final long first, final long... others) {return ListValue.of(first, others);}

    public static ListLiteral longs(final long[] elements) {return ListValue.of(elements);}

    public static ListLiteral floats(final float first, final float... others) {return ListValue.of(first, others);}

    public static ListLiteral floats(final float[] elements) {return ListValue.of(elements);}

    public static ListLiteral doubles(final double first, final double... others) {return ListValue.of(first, others);}

    public static ListLiteral doubles(final double[] elements) {return ListValue.of(elements);}

    public static ListLiteral strings(final char first, final char... others) {return ListValue.of(first, others);}

    public static ListLiteral strings(final CharSequence first, final CharSequence... others) {return ListValue.of(first, others);}

    public static ListLiteral strings(final String first, final String... others) {return ListValue.of(first, others);}

    public static ListLiteral strings(final char[] elements) {return ListValue.of(elements);}

    public static ListLiteral strings(final CharSequence[] elements) {return ListValue.of(elements);}

    public static ListLiteral strings(final String[] elements) {return ListValue.of(elements);}

    public static ListLiteral list() {return ListValue.of();}

    public static ListLiteral list(final Expression first, final Expression... others) {return ListValue.of(first, others);}

    public static ListLiteral list(final Expression[] elements) {return ListValue.of(elements);}

    public static ListLiteral list(final Object first, final Object... others) {return ListValue.of(first, others);}

    public static ListLiteral list(final Object[] elements) {return ListValue.of(elements);}

    public static ListLiteral list(final Collection<?> elements) {return ListValue.of(elements);}

    // map literals
    public static PropertiesMap map() {return PropertiesMap.of();}

    public static PropertiesMap map(final PropertyValue property) {return PropertiesMap.of(property);}

    public static PropertiesMap map(final PropertyValue first, final PropertyValue... others) {return PropertiesMap.of(first, others);}

    public static PropertiesMap map(final PropertyValue first, final List<PropertyValue> others) {return PropertiesMap.of(first, others);}

    public static PropertiesMap map(final List<PropertyValue> properties) {return PropertiesMap.of(properties);}

    public static PropertiesMap map(final PropertyValue[] properties) {return PropertiesMap.of(properties);}

    public static PropertiesMap map(final Map<?, ?> properties) {return PropertiesMap.of(properties);}

    public static PropertiesMap map(final Object property, final Object value, final Object... otherEntries)
    {
        return PropertiesMap.of(property, value, otherEntries);
    }

    // raw expressions
    public static RawExpression r(final String expr) {return RawExpression.of(expr);}

    // ============================================================================================================================================ //
    // R E A D I N G   C L A U S E S                                                                                                                //
    // ============================================================================================================================================ //
    public static NodeConnector.WithWhere MATCH() {return new RegularQuery().MATCH();}

    public static NodeConnector.WithWhere MATCH(final String name) {return new RegularQuery().MATCH(name);}

    public static NodeConnector.WithWhere MATCH(final String name, final PropertiesMap properties) {return new RegularQuery().MATCH(name, properties);}

    public static NodeConnector.WithWhere MATCH(final String name, final Parameter properties) {return new RegularQuery().MATCH(name, properties);}

    public static NodeConnector.WithWhere MATCH(final Labels labels) {return new RegularQuery().MATCH(labels);}

    public static NodeConnector.WithWhere MATCH(final Labels labels, final PropertiesMap properties) {return new RegularQuery().MATCH(labels, properties);}

    public static NodeConnector.WithWhere MATCH(final Labels labels, final Parameter properties) {return new RegularQuery().MATCH(labels, properties);}

    public static NodeConnector.WithWhere MATCH(final HasLabels labels) {return new RegularQuery().MATCH(labels);}

    public static NodeConnector.WithWhere MATCH(final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MATCH(labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().MATCH(labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final String name, final Labels labels) {return new RegularQuery().MATCH(name, labels);}

    public static NodeConnector.WithWhere MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return new RegularQuery().MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final String name, final HasLabels labels) {return new RegularQuery().MATCH(name, labels);}

    public static NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere MATCH(final NodeVariable node) {return new RegularQuery().MATCH(node);}

    public static NodeConnector.WithWhere MATCH(final NodeVariable node, final PropertiesMap properties) {return new RegularQuery().MATCH(node, properties);}

    public static NodeConnector.WithWhere MATCH(final NodeVariable node, final Parameter properties) {return new RegularQuery().MATCH(node, properties);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH() {return new RegularQuery().OPTIONAL_MATCH();}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name) {return new RegularQuery().OPTIONAL_MATCH(name);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name,
                                                 properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name,
                                                 properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels) {return new RegularQuery().OPTIONAL_MATCH(labels);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(labels,
                                                 properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(labels,
                                                 properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels) {return new RegularQuery().OPTIONAL_MATCH(labels);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels) {return new RegularQuery().OPTIONAL_MATCH(name, labels);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels) {return new RegularQuery().OPTIONAL_MATCH(name, labels);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(name, labels, properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node) {return new RegularQuery().OPTIONAL_MATCH(node);}

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node, final PropertiesMap properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(node,
                                                 properties);
    }

    public static NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node, final Parameter properties)
    {
        return new RegularQuery().OPTIONAL_MATCH(node,
                                                 properties);
    }

    public static PathVariable<NodeConnector.WithWhere> MATCH_path(final String name) {return new RegularQuery().MATCH_path(name);}

    public static PathVariable<NodeConnector.WithWhere> MATCH_shortestPath(final String name) {return MATCH_path(name, PathFunction.shortestPath);}

    public static PathVariable<NodeConnector.WithWhere> MATCH_allShortestPaths(final String name)
    {
        return MATCH_path(name, PathFunction.allShortestPaths);
    }

    public static PathVariable<NodeConnector.WithWhere> MATCH_path(final String name, final PathFunction function)
    {
        return new RegularQuery().MATCH_path(name, function);
    }

    public static PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name) {return new RegularQuery().OPTIONAL_MATCH_path(name);}

    public static PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_shortestPath(final String name)
    {
        return OPTIONAL_MATCH_path(name, PathFunction.shortestPath);
    }

    public static PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_allShortestPaths(final String name)
    {
        return OPTIONAL_MATCH_path(name, PathFunction.allShortestPaths);
    }

    public static PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name, final PathFunction function)
    {
        return new RegularQuery().OPTIONAL_MATCH_path(name, function);
    }

    public static UnwindClause UNWIND(final Alias definition) {return new RegularQuery().UNWIND(definition);}

    // ============================================================================================================================================ //
    // U P D A T I N G   C L A U S E S                                                                                                              //
    // ============================================================================================================================================ //
    public static NodeConnector.WithReturnAfterUpdateClause CREATE() {return new RegularQuery().CREATE();}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name) {return new RegularQuery().CREATE(name);}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(name, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Parameter properties)
    {
        return new RegularQuery().CREATE(name, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels) {return new RegularQuery().CREATE(labels);}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final Parameter properties)
    {
        return new RegularQuery().CREATE(labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels) {return new RegularQuery().CREATE(labels);}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().CREATE(labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels) {return new RegularQuery().CREATE(name, labels);}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(name, labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final Parameter properties)
    {
        return new RegularQuery().CREATE(name, labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels)
    {
        return new RegularQuery().CREATE(name,
                                         labels);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(name, labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().CREATE(name, labels, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node) {return new RegularQuery().CREATE(node);}

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final PropertiesMap properties)
    {
        return new RegularQuery().CREATE(node, properties);
    }

    public static NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final Parameter properties)
    {
        return new RegularQuery().CREATE(node, properties);
    }

    public static PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(final String pathName)
    {
        return new RegularQuery().CREATE_path(pathName);
    }

    public static HasMergeActions MERGE() {return new RegularQuery().MERGE();}

    public static HasMergeActions MERGE(final String name) {return new RegularQuery().MERGE(name);}

    public static HasMergeActions MERGE(final String name, final PropertiesMap properties) {return new RegularQuery().MERGE(name, properties);}

    public static HasMergeActions MERGE(final String name, final Parameter properties) {return new RegularQuery().MERGE(name, properties);}

    public static HasMergeActions MERGE(final Labels labels) {return new RegularQuery().MERGE(labels);}

    public static HasMergeActions MERGE(final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MERGE(labels, properties);
    }

    public static HasMergeActions MERGE(final Labels labels, final Parameter properties) {return new RegularQuery().MERGE(labels, properties);}

    public static HasMergeActions MERGE(final HasLabels labels) {return new RegularQuery().MERGE(labels);}

    public static HasMergeActions MERGE(final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MERGE(labels, properties);
    }

    public static HasMergeActions MERGE(final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().MERGE(labels, properties);
    }

    public static HasMergeActions MERGE(final String name, final Labels labels) {return new RegularQuery().MERGE(name, labels);}

    public static HasMergeActions MERGE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MERGE(name, labels, properties);
    }

    public static HasMergeActions MERGE(final String name, final Labels labels, final Parameter properties)
    {
        return new RegularQuery().MERGE(name, labels, properties);
    }

    public static HasMergeActions MERGE(final String name, final HasLabels labels) {return new RegularQuery().MERGE(name, labels);}

    public static HasMergeActions MERGE(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return new RegularQuery().MERGE(name, labels, properties);
    }

    public static HasMergeActions MERGE(final String name, final HasLabels labels, final Parameter properties)
    {
        return new RegularQuery().MERGE(name, labels, properties);
    }

    public static HasMergeActions MERGE(final NodeVariable node) {return new RegularQuery().MERGE(node);}

    public static HasMergeActions MERGE(final NodeVariable node, final PropertiesMap properties)
    {
        return new RegularQuery().MERGE(node, properties);
    }

    public static HasMergeActions MERGE(final NodeVariable node, final Parameter properties) {return new RegularQuery().MERGE(node, properties);}

    /* WITH *************************************************************************************************************************************** */
    public static WithClause WITH(final Expression item) {return new RegularQuery().WITH(item);}

    public static WithClause WITH(final Expression firstItem, final Expression... otherItems) {return new RegularQuery().WITH(firstItem, otherItems);}

    public static WithClause WITH(final List<Expression> items) {return new RegularQuery().WITH(items);}

    public static WithClause WITH_DISTINCT(final Expression item) {return new RegularQuery().WITH_DISTINCT(item);}

    public static WithClause WITH_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        return new RegularQuery().WITH_DISTINCT(firstItem, otherItems);
    }

    public static WithClause WITH_DISTINCT(final List<Expression> items) {return new RegularQuery().WITH_DISTINCT(items);}

    /* RETURN ************************************************************************************************************************************* */
    public static Projection RETURN(final Expression item)
    {
        return new RegularQuery().RETURN(item);
    }

    public static Projection RETURN(final Expression firstItem, final Expression... otherItems)
    {
        return new RegularQuery().RETURN(firstItem, otherItems);
    }

    public static Projection RETURN(final List<Expression> items)
    {
        return new RegularQuery().RETURN(items);
    }

    public static Projection RETURN_DISTINCT(final Expression item)
    {
        return new RegularQuery().RETURN_DISTINCT(item);
    }

    public static Projection RETURN_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        return new RegularQuery().RETURN(firstItem, otherItems);
    }

    public static Projection RETURN_DISTINCT(final List<Expression> items)
    {
        return new RegularQuery().RETURN(items);
    }

    /* Property values **************************************************************************************************************************** */

    // generic
    public static PropertyValue $(final String name, final Object value) { return PropertyValue.of(name, e(value));}

    public static PropertyValue $(final String name, final Expression value) { return PropertyValue.of(name, value);}

    public static PropertyValue $(final String name, final Literal value) { return PropertyValue.of(name, value);}

    public static PropertyValue $(final String name, final Parameter value) { return PropertyValue.of(name, value);}

    public static PropertyValue $(final StringProperty<?> property, final char propertyValue)
    {
        return PropertyValue.of(property.getProperty(), StringValue.of(propertyValue));
    }

    // string properties
    public static PropertyValue $(final StringProperty<?> property, final Character propertyValue)
    {
        return PropertyValue.of(property.getProperty(), StringValue.of(propertyValue));
    }

    public static PropertyValue $(final StringProperty<?> property, final CharSequence propertyValue)
    {
        return PropertyValue.of(property.getProperty(), StringValue.of(propertyValue));
    }

    public static PropertyValue $(final StringProperty<?> property, final String propertyValue)
    {
        return PropertyValue.of(property.getProperty(), StringValue.of(propertyValue));
    }

    public static PropertyValue $(final StringProperty<?> property, final StringLiteral propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    public static PropertyValue $(final StringProperty<?> property, final StringExpression propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    public static PropertyValue $(final StringProperty<?> property, final Parameter propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    // integer properties
    public static PropertyValue $(final IntegerProperty<?> property, final byte propertyValue)
    {
        return $(property, ByteValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final Byte propertyValue)
    {
        return $(property, ByteValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final ByteLiteral propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    public static PropertyValue $(final IntegerProperty<?> property, final short propertyValue)
    {
        return $(property, ShortValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final Short propertyValue)
    {
        return $(property, ShortValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final ShortLiteral propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    public static PropertyValue $(final IntegerProperty<?> property, final int propertyValue)
    {
        return $(property, IntegerValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final Integer propertyValue)
    {
        return $(property, IntegerValue.of(propertyValue));
    }

    public static PropertyValue $(final IntegerProperty<?> property, final IntegerLiteral propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    public static PropertyValue $(final IntegerProperty<?> property, final Parameter propertyValue)
    {
        return PropertyValue.of(property.getProperty(), propertyValue);
    }

    /* anonymous patterns ************************************************************************************************************************* */
    public static NodeConnector pattern() {return new AnonymousPattern().node();}

    public static NodeConnector pattern(final String name) {return new AnonymousPattern().node(name);}

    public static NodeConnector pattern(final String name, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(name, properties);
    }

    public static NodeConnector pattern(final String name, final Parameter properties) {return new AnonymousPattern().node(name, properties);}

    public static NodeConnector pattern(final Labels labels) {return new AnonymousPattern().node(labels);}

    public static NodeConnector pattern(final Labels labels, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(labels, properties);
    }

    public static NodeConnector pattern(final Labels labels, final Parameter properties) {return new AnonymousPattern().node(labels, properties);}

    public static NodeConnector pattern(final HasLabels labels) {return new AnonymousPattern().node(labels);}

    public static NodeConnector pattern(final HasLabels labels, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(labels, properties);
    }

    public static NodeConnector pattern(final HasLabels labels, final Parameter properties)
    {
        return new AnonymousPattern().node(labels, properties);
    }

    public static NodeConnector pattern(final String name, final Labels labels) {return new AnonymousPattern().node(name, labels);}

    public static NodeConnector pattern(final String name, final Labels labels, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(name, labels, properties);
    }

    public static NodeConnector pattern(final String name, final Labels labels, final Parameter properties)
    {
        return new AnonymousPattern().node(name, labels, properties);
    }

    public static NodeConnector pattern(final String name, final HasLabels labels) {return new AnonymousPattern().node(name, labels);}

    public static NodeConnector pattern(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(name, labels, properties);
    }

    public static NodeConnector pattern(final String name, final HasLabels labels, final Parameter properties)
    {
        return new AnonymousPattern().node(name, labels, properties);
    }

    public static NodeConnector pattern(final NodeVariable node) {return new AnonymousPattern().node(node);}

    public static NodeConnector pattern(final NodeVariable node, final PropertiesMap properties)
    {
        return new AnonymousPattern().node(node, properties);
    }

    public static NodeConnector pattern(final NodeVariable node, final Parameter properties) {return new AnonymousPattern().node(node, properties);}

    /* functions / expressions ******************************************************************************************************************** */
    public static FunctionCall type(final String relationshipName) {return FunctionCall.of("type", RelationshipVariable.of(relationshipName));}

    public static FunctionCall type(final RelationshipVariable relationship) {return FunctionCall.of("type", relationship);}

    public static FunctionCall id(final String name) {return FunctionCall.of("id", NodeVariable.of(name));}

    public static FunctionCall id(final EntityVariable entity) {return FunctionCall.of("id", entity);}

    public static FunctionCall countAll() {return count(all);}

    public static FunctionCall count(final String expr) {return count(r(expr));}

    public static FunctionCall count(final Expression expr) {return FunctionCall.of("count", expr);}

    public static FunctionCall labels(final String nodeName) {return FunctionCall.of("labels", NodeVariable.of(nodeName));}

    public static FunctionCall labels(final NodeVariable node) {return FunctionCall.of("labels", node);}

    public static FunctionCall timestamp() {return TIMESTAMP;}

    public static FunctionCall collect(final String alias) {return FunctionCall.of("collect", AliasName.of(alias));}

    public static FunctionCall collect(final Expression expression) {return FunctionCall.of("collect", expression);}

    public static FunctionCall range(final int start, final int end)
    {

        return FunctionCall.of("range", IntegerValue.of(start), IntegerValue.of(end));
    }

    public static FunctionCall range(final int start, final int end, final int step)
    {
        return FunctionCall.of("range", IntegerValue.of(start), IntegerValue.of(end), IntegerValue.of(step));
    }

    public static FunctionCall range(final IntegerExpression start, final IntegerExpression end)
    {
        return FunctionCall.of("range", start, end);
    }

    public static FunctionCall range(final IntegerExpression start, final IntegerExpression end, final IntegerExpression step)
    {
        return FunctionCall.of("range", start, end, step);
    }

    public static RelationshipDetails.Hops anyHops() { return RelationshipDetails.AnyHops.INSTANCE;}

    public static RelationshipDetails.Hops hops(final int i)
    {
        if (1 == i) {
            return RelationshipDetails.OneHop.INSTANCE;
        }
        return new RelationshipDetails.FixedHops(i);
    }

    public static RelationshipDetails.Hops minHops(final int i)
    {
        return new RelationshipDetails.MinHops(i);
    }

    public static RelationshipDetails.Hops maxHops(final int i)
    {
        return new RelationshipDetails.MaxHops(i);
    }

    public static RelationshipDetails.Hops hops(final int min, final int max)
    {
        if (min == max) {
            return hops(min);
        }
        return new RelationshipDetails.RangeHops(min, max);
    }

    public static CaseExpression CASE(final Object baseExpr) {return CASE(e(baseExpr));}

    public static CaseExpression CASE(final Expression base)
    {
        Objects.requireNonNull(base, "base is required");
        return new CaseExpression(base);
    }

    public static final class CASE extends Utility
    {
        private CASE() {super(CASE.class);}

        public static CaseExpressionWhen WHEN(final Object expr)
        {
            return new CaseExpression().WHEN(expr);
        }

        public static CaseExpressionWhen WHEN(final Expression expr)
        {
            return new CaseExpression().WHEN(expr);
        }
    }

    public static <G extends GroupableExpression> G group(final G groupable)
    {
        groupable.group();
        return groupable;
    }

    public static NamedParameter $(final String name) {return Parameter.of(name);}

    public static IndexedParameter $(final int index) {return Parameter.of(index);}

    public static AliasName alias(final String name) {return AliasName.of(name);}

    public static Label L(final String name)
    {
        return Label.of(name);
    }

    public static Labels L(final String first, final String... others)
    {
        if (null == others || 0 == others.length) {
            return L(first);
        }
        final var labels = new java.util.LinkedHashSet<Label>(others.length + 1);
        labels.add(Label.of(first));
        for (final var other : others) {
            labels.add(Label.of(other));
        }
        return Labels.of(labels);
    }

    public static Type T(final String name)
    {
        return Type.of(name);
    }

    public static Types T(final String first, final String... others)
    {
        if (null == others || 0 == others.length) {
            return T(first);
        }
        final var types = new java.util.LinkedHashSet<Type>(others.length + 1);
        types.add(Type.of(first));
        for (final var other : others) {
            types.add(Type.of(other));
        }
        return Types.of(types);
    }

}
