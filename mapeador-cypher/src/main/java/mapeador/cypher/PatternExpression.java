/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Labels;
import mapeador.Types;
import mapeador.cypher.RelationshipDetails.Hops;

import java.util.Collections;
import java.util.List;

abstract class PatternExpression<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>> implements Pattern<CONNECTOR>
{
    private NodeDetails firstNode;
    private final List<NodesConnection> connections = new java.util.LinkedList<>();
    private DetailedConnection currentDetailedConnection;
    private boolean grouped;
    private Pattern<CONNECTOR> next = null;

    PatternExpression() {/*nothing to do*/}

    @Override public final boolean hasNext() {return null != this.next;}

    @Override public final Pattern<CONNECTOR> next() {return this.next;}

    @Override public final NodeDetails firstNode() {return this.firstNode;}

    @Override public final boolean hasConnections() {return !this.connections.isEmpty();}

    @Override public final List<NodesConnection> connections()
    {
        return this.connections.isEmpty() ? List.of() : Collections.unmodifiableList(this.connections);
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // PathVariable IMPL
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    @Override public CONNECTOR node()
    {
        this.firstNode = NodeDetails.of();
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name)
    {
        this.firstNode = NodeDetails.of(name);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final Labels labels)
    {
        this.firstNode = NodeDetails.of(labels);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final PropertiesMap properties)
    {
        this.firstNode = NodeDetails.of(properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final Parameter properties)
    {
        this.firstNode = NodeDetails.of(properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name, final Labels labels)
    {
        this.firstNode = NodeDetails.of(name, labels);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name, final PropertiesMap properties)
    {
        this.firstNode = NodeDetails.of(name, properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name, final Parameter properties)
    {
        this.firstNode = NodeDetails.of(name, properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final Labels labels, final PropertiesMap properties)
    {
        this.firstNode = NodeDetails.of(labels, properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final Labels labels, final Parameter properties)
    {
        this.firstNode = NodeDetails.of(labels, properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.firstNode = NodeDetails.of(name, labels, properties);
        return asNodeConnector();
    }

    @Override public CONNECTOR node(final String name, final Labels labels, final Parameter properties)
    {
        this.firstNode = NodeDetails.of(name, labels, properties);
        return asNodeConnector();
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // PatternNode IMPL
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

    /* unnamed undirected relationships between nodes ********************************************************************************************* */
    public CONNECTOR relatedWith()
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of());
    }

    public CONNECTOR relatedWith(final String name)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name));
    }

    public CONNECTOR relatedWith(final Labels labels)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(labels));
    }

    public CONNECTOR relatedWith(final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(properties));
    }

    public CONNECTOR relatedWith(final Parameter properties) {return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(properties));}

    public CONNECTOR relatedWith(final String name, final Labels labels)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name, labels));
    }

    public CONNECTOR relatedWith(final String name, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name, properties));
    }

    public CONNECTOR relatedWith(final String name, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name, properties));
    }

    public CONNECTOR relatedWith(final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(labels, properties));
    }

    public CONNECTOR relatedWith(final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(labels, properties));
    }

    public CONNECTOR relatedWith(final String name, final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name, labels, properties));
    }

    public CONNECTOR relatedWith(final String name, final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.ANY, NodeDetails.of(name, labels, properties));
    }

    /* unnamed incoming relationships between nodes *********************************************************************************************** */
    public CONNECTOR incomingFrom()
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of());
    }

    public CONNECTOR incomingFrom(final String name)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name));
    }

    public CONNECTOR incomingFrom(final Labels labels)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(labels));
    }

    public CONNECTOR incomingFrom(final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(properties));
    }

    public CONNECTOR incomingFrom(final Parameter properties) {return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(properties));}

    public CONNECTOR incomingFrom(final String name, final Labels labels)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name, labels));
    }

    public CONNECTOR incomingFrom(final String name, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name, properties));
    }

    public CONNECTOR incomingFrom(final String name, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name, properties));
    }

    public CONNECTOR incomingFrom(final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(labels, properties));
    }

    public CONNECTOR incomingFrom(final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(labels, properties));
    }

    public CONNECTOR incomingFrom(final String name, final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name, labels, properties));
    }

    public CONNECTOR incomingFrom(final String name, final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.INCOMING, NodeDetails.of(name, labels, properties));
    }

    /* unnamed outgoing relationships between nodes *********************************************************************************************** */
    public CONNECTOR outgoingTo()
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of());
    }

    public CONNECTOR outgoingTo(final String name)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name));
    }

    public CONNECTOR outgoingTo(final Labels labels)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(labels));
    }

    public CONNECTOR outgoingTo(final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(properties));
    }

    public CONNECTOR outgoingTo(final Parameter properties) {return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(properties));}

    public CONNECTOR outgoingTo(final String name, final Labels labels)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name, labels));
    }

    public CONNECTOR outgoingTo(final String name, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name, properties));
    }

    public CONNECTOR outgoingTo(final String name, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name, properties));
    }

    public CONNECTOR outgoingTo(final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(labels, properties));
    }

    public CONNECTOR outgoingTo(final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(labels, properties));
    }

    public CONNECTOR outgoingTo(final String name, final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name, labels, properties));
    }

    public CONNECTOR outgoingTo(final String name, final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.OUTGOING, NodeDetails.of(name, labels, properties));
    }

    /* unnamed bi-directional relationships between nodes ***************************************************************************************** */
    public CONNECTOR knowsEachOther()
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of());
    }

    public CONNECTOR knowsEachOther(final String name)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name));
    }

    public CONNECTOR knowsEachOther(final Labels labels)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(labels));
    }

    public CONNECTOR knowsEachOther(final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(properties));
    }

    public CONNECTOR knowsEachOther(final Parameter properties) {return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(properties));}

    public CONNECTOR knowsEachOther(final String name, final Labels labels)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name, labels));
    }

    public CONNECTOR knowsEachOther(final String name, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name, properties));
    }

    public CONNECTOR knowsEachOther(final String name, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name, properties));
    }

    public CONNECTOR knowsEachOther(final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(labels, properties));
    }

    public CONNECTOR knowsEachOther(final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(labels, properties));
    }

    public CONNECTOR knowsEachOther(final String name, final Labels labels, final PropertiesMap properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name, labels, properties));
    }

    public CONNECTOR knowsEachOther(final String name, final Labels labels, final Parameter properties)
    {
        return addConnection(NodesConnection.Direction.BOTH, NodeDetails.of(name, labels, properties));
    }

    /* detailed undirected connections between nodes ********************************************************************************************** */
    public UndirectedRelation<CONNECTOR> related() {return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of());}

    public UndirectedRelation<CONNECTOR> related(final String name)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types));
    }

    public UndirectedRelation<CONNECTOR> related(final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(hops));
    }

    public UndirectedRelation<CONNECTOR> related(final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, hops));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types, hops));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types, hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, types, hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types, hops));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types, hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(types, hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, hops, properties));
    }

    public UndirectedRelation<CONNECTOR> related(final String name, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.ANY, RelationshipDetails.of(name, hops, properties));
    }
    /* Undirected Relation ************************************************************************************************************************ */
    @Override public CONNECTOR with()
    {
        return setDetailedConnectionNode(NodeDetails.of());
    }

    @Override public CONNECTOR with(final String name)
    {
        return setDetailedConnectionNode(NodeDetails.of(name));
    }

    @Override public CONNECTOR with(final Labels labels)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels));
    }

    @Override public CONNECTOR with(final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(properties));
    }

    @Override public CONNECTOR with(final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(properties));
    }

    @Override public CONNECTOR with(final String name, final Labels labels)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels));
    }

    @Override public CONNECTOR with(final String name, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR with(final String name, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR with(final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR with(final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR with(final String name, final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    @Override public CONNECTOR with(final String name, final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    /* detailed incoming connections between nodes ************************************************************************************************ */
    public IncomingRelation<CONNECTOR> incoming()
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of());
    }

    public IncomingRelation<CONNECTOR> incoming(final String name)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name));
    }

    public IncomingRelation<CONNECTOR> incoming(final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types));
    }

    public IncomingRelation<CONNECTOR> incoming(final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(hops));
    }

    public IncomingRelation<CONNECTOR> incoming(final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, hops));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types, hops));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types, hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, types, hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types, hops));
    }


    public IncomingRelation<CONNECTOR> incoming(final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types, hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(types, hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, hops, properties));
    }

    public IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.INCOMING, RelationshipDetails.of(name, hops, properties));
    }

    /* incoming Relation ************************************************************************************************************************ */
    @Override public CONNECTOR from() {return setDetailedConnectionNode(NodeDetails.of());}

    @Override public CONNECTOR from(final String name) {return setDetailedConnectionNode(NodeDetails.of(name));}

    @Override public CONNECTOR from(final Labels labels) {return setDetailedConnectionNode(NodeDetails.of(labels));}

    @Override public CONNECTOR from(final PropertiesMap properties) {return setDetailedConnectionNode(NodeDetails.of(properties));}

    @Override public CONNECTOR from(final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(properties));
    }

    @Override public CONNECTOR from(final String name, final Labels labels)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels));
    }

    @Override public CONNECTOR from(final String name, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR from(final String name, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR from(final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR from(final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR from(final String name, final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    @Override public CONNECTOR from(final String name, final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    /* detailed outgoing connections between nodes ************************************************************************************************ */
    public OutgoingRelation<CONNECTOR> outgoing()
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of());
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(hops));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, hops));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types, hops));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types, hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, types, hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types, hops));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types, hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(types, hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, hops, properties));
    }

    public OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.OUTGOING, RelationshipDetails.of(name, hops, properties));
    }

    /* outgoing Relation ************************************************************************************************************************ */
    @Override public CONNECTOR to() {return setDetailedConnectionNode(NodeDetails.of());}

    @Override public CONNECTOR to(final String name) {return setDetailedConnectionNode(NodeDetails.of(name));}

    @Override public CONNECTOR to(final Labels labels) {return setDetailedConnectionNode(NodeDetails.of(labels));}

    @Override public CONNECTOR to(final PropertiesMap properties) {return setDetailedConnectionNode(NodeDetails.of(properties));}

    @Override public CONNECTOR to(final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(properties));
    }

    @Override public CONNECTOR to(final String name, final Labels labels)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels));
    }

    @Override public CONNECTOR to(final String name, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR to(final String name, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR to(final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR to(final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR to(final String name, final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    @Override public CONNECTOR to(final String name, final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    /* explicit in-out connections from node ****************************************************************************************************** */
    public InOutRelation<CONNECTOR> in()
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of());
    }

    public InOutRelation<CONNECTOR> in(final String name)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name));
    }

    public InOutRelation<CONNECTOR> in(final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types));
    }

    public InOutRelation<CONNECTOR> in(final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(hops));
    }

    public InOutRelation<CONNECTOR> in(final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(properties));
    }

    public InOutRelation<CONNECTOR> in(final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, hops));
    }

    public InOutRelation<CONNECTOR> in(final String name, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types, hops));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types, hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, types, hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final Types types, final Hops hops)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types, hops));
    }

    public InOutRelation<CONNECTOR> in(final Types types, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types, properties));
    }

    public InOutRelation<CONNECTOR> in(final Types types, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types, properties));
    }

    public InOutRelation<CONNECTOR> in(final Types types, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types, hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final Types types, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(types, hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Hops hops, final PropertiesMap properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, hops, properties));
    }

    public InOutRelation<CONNECTOR> in(final String name, final Hops hops, final Parameter properties)
    {
        return newDetailedConnection(NodesConnection.Direction.BOTH, RelationshipDetails.of(name, hops, properties));
    }

    /* outgoing Relation ************************************************************************************************************************ */
    @Override public CONNECTOR out() {return setDetailedConnectionNode(NodeDetails.of());}

    @Override public CONNECTOR out(final String name) {return setDetailedConnectionNode(NodeDetails.of(name));}

    @Override public CONNECTOR out(final Labels labels) {return setDetailedConnectionNode(NodeDetails.of(labels));}

    @Override public CONNECTOR out(final PropertiesMap properties) {return setDetailedConnectionNode(NodeDetails.of(properties));}

    @Override public CONNECTOR out(final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(properties));
    }

    @Override public CONNECTOR out(final String name, final Labels labels)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels));
    }

    @Override public CONNECTOR out(final String name, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR out(final String name, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, properties));
    }

    @Override public CONNECTOR out(final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR out(final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(labels, properties));
    }

    @Override public CONNECTOR out(final String name, final Labels labels, final PropertiesMap properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    @Override public CONNECTOR out(final String name, final Labels labels, final Parameter properties)
    {
        return setDetailedConnectionNode(NodeDetails.of(name, labels, properties));
    }

    public CONNECTOR and() {return makeNextPattern().node();}

    public CONNECTOR and(final String name) {return makeNextPattern().node(name);}

    public CONNECTOR and(final Labels labels) {return makeNextPattern().node(labels);}

    public CONNECTOR and(final PropertiesMap properties) {return makeNextPattern().node(properties);}

    public CONNECTOR and(final Parameter properties) {return makeNextPattern().node(properties);}

    public CONNECTOR and(final String name, final Labels labels) {return makeNextPattern().node(name, labels);}

    public CONNECTOR and(final String name, final PropertiesMap properties) {return makeNextPattern().node(name, properties);}

    public CONNECTOR and(final String name, final Parameter properties) {return makeNextPattern().node(name, properties);}

    public CONNECTOR and(final Labels labels, final PropertiesMap properties) {return makeNextPattern().node(labels, properties);}

    public CONNECTOR and(final Labels labels, final Parameter properties) {return makeNextPattern().node(labels, properties);}

    public CONNECTOR and(final String name, final Labels labels, final PropertiesMap properties)
    {
        return makeNextPattern().node(name, labels, properties);
    }

    public CONNECTOR and(final String name, final Labels labels, final Parameter properties)
    {
        return makeNextPattern().node(name, labels, properties);
    }

    public PathVariable<CONNECTOR> andPath(final String name) {return makeNextPattern(name);}

    public PathVariable<CONNECTOR> andPath(final String name, final String function) {return makeNextPattern(name, function);}

    @Override public boolean grouped() {return this.grouped;}

    @Override public PatternExpression<CONNECTOR> group()
    {
        this.grouped = true;
        return this;
    }

    private CONNECTOR addConnection(final NodesConnection.Direction direction, final NodeDetails node)
    {
        this.connections.add(new UndetailedConnection(direction, node));
        return asNodeConnector();
    }

    private PatternExpression<CONNECTOR> newDetailedConnection(final NodesConnection.Direction direction, final RelationshipDetails relationship)
    {
        this.currentDetailedConnection = new DetailedConnection(direction, relationship);
        this.connections.add(this.currentDetailedConnection);
        return this;
    }

    private CONNECTOR setDetailedConnectionNode(final NodeDetails node)
    {
        this.currentDetailedConnection.setNode(node);
        this.currentDetailedConnection = null;
        return asNodeConnector();
    }

    abstract Pattern<CONNECTOR> makeNextPattern();

    abstract NamedPattern<CONNECTOR> makeNextPattern(String name);

    abstract NamedPattern<CONNECTOR> makeNextPattern(String name, String function);

    protected final <P extends Pattern<CONNECTOR>> P nextPattern(final P pattern)
    {
        this.next = pattern;
        return pattern;
    }

    @SuppressWarnings("unchecked") private CONNECTOR asNodeConnector() {return (CONNECTOR) this;}
}
