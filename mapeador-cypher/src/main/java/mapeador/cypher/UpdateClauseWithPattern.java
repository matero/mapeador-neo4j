/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Labels;
import mapeador.Types;
import mapeador.cypher.RelationshipDetails.Hops;

abstract class UpdateClauseWithPattern<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>, CLAUSE extends UpdateClauseWithPattern<CONNECTOR, CLAUSE>>
    extends UpdateClause
    implements PathVariable<CONNECTOR>,
               UndirectedRelation<CONNECTOR>,
               IncomingRelation<CONNECTOR>,
               OutgoingRelation<CONNECTOR>,
               InOutRelation<CONNECTOR>
{
    final ClausePattern<CONNECTOR, CLAUSE> pattern;

    @FunctionalInterface
    interface AnonymousPatternFactory<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>, CLAUSE extends UpdateClauseWithPattern<CONNECTOR, CLAUSE>>
    {
        ClausePattern<CONNECTOR, CLAUSE> with(CLAUSE clause);
    }

    @FunctionalInterface
    interface PathPatternFactory<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>, CLAUSE extends UpdateClauseWithPattern<CONNECTOR, CLAUSE>>
    {
        ClausePattern<CONNECTOR, CLAUSE> with(CLAUSE clause, String path);
    }

    @FunctionalInterface
    interface PathPatternWithFunctionFactory<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>, CLAUSE extends UpdateClauseWithPattern<CONNECTOR, CLAUSE>>
    {
        ClausePattern<CONNECTOR, CLAUSE> with(CLAUSE clause, String path, String function);
    }

    @SuppressWarnings("unchecked") UpdateClauseWithPattern(final RegularQuery parent, final AnonymousPatternFactory<CONNECTOR, CLAUSE> createPattern)
    {
        super(parent);
        this.pattern = createPattern.with((CLAUSE) this);
    }

    @SuppressWarnings("unchecked") UpdateClauseWithPattern(final RegularQuery parent, final String pathName, final PathPatternFactory<CONNECTOR, CLAUSE> createPattern)
    {
        super(parent);
        this.pattern = createPattern.with((CLAUSE) this, pathName);
    }

    @SuppressWarnings("unchecked") UpdateClauseWithPattern(
        final RegularQuery parent, final String pathName, final String function, final PathPatternWithFunctionFactory<CONNECTOR, CLAUSE> createPattern)
    {
        super(parent);
        this.pattern = createPattern.with((CLAUSE) this, pathName, function);
    }

    public final Pattern<CONNECTOR> pattern() {return this.pattern;}

    @Override public final CONNECTOR node()
    {
        this.pattern.node();
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final String variable)
    {
        this.pattern.node(variable);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final Labels labels)
    {
        this.pattern.node(labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final PropertiesMap properties)
    {
        this.pattern.node(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final Parameter properties)
    {
        this.pattern.node(properties);
        return asNodeConnector();
    }


    @Override public final CONNECTOR node(final String name, final Labels labels)
    {
        this.pattern.node(name, labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final String name, final PropertiesMap properties)
    {
        this.pattern.node(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final String name, final Parameter properties)
    {
        this.pattern.node(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.node(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final Labels labels, final Parameter properties)
    {
        this.pattern.node(labels, properties);
        return asNodeConnector();
    }


    @Override public final CONNECTOR node(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.node(name, labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR node(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.node(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith()
    {
        this.pattern.relatedWith();
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name)
    {
        this.pattern.relatedWith(name);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final Labels labels)
    {
        this.pattern.relatedWith(labels);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final PropertiesMap properties)
    {
        this.pattern.relatedWith(properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final Parameter properties)
    {
        this.pattern.relatedWith(properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name, final Labels labels)
    {
        this.pattern.relatedWith(name, labels);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name, final PropertiesMap properties)
    {
        this.pattern.relatedWith(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name, final Parameter properties)
    {
        this.pattern.relatedWith(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.relatedWith(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final Labels labels, final Parameter properties)
    {
        this.pattern.relatedWith(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.relatedWith(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR relatedWith(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.relatedWith(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom()
    {
        this.pattern.incomingFrom();
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name)
    {
        this.pattern.incomingFrom(name);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final Labels labels)
    {
        this.pattern.incomingFrom(labels);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final PropertiesMap properties)
    {
        this.pattern.incomingFrom(properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final Parameter properties)
    {
        this.pattern.incomingFrom(properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name, final Labels labels)
    {
        this.pattern.incomingFrom(name, labels);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name, final PropertiesMap properties)
    {
        this.pattern.incomingFrom(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name, final Parameter properties)
    {
        this.pattern.incomingFrom(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.incomingFrom(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final Labels labels, final Parameter properties)
    {
        this.pattern.incomingFrom(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.incomingFrom(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR incomingFrom(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.incomingFrom(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo()
    {
        this.pattern.outgoingTo();
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name)
    {
        this.pattern.outgoingTo(name);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final Labels labels)
    {
        this.pattern.outgoingTo(labels);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final PropertiesMap properties)
    {
        this.pattern.outgoingTo(properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final Parameter properties)
    {
        this.pattern.outgoingTo(properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name, final Labels labels)
    {
        this.pattern.outgoingTo(name, labels);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name, final PropertiesMap properties)
    {
        this.pattern.outgoingTo(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name, final Parameter properties)
    {
        this.pattern.outgoingTo(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.outgoingTo(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final Labels labels, final Parameter properties)
    {
        this.pattern.outgoingTo(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.outgoingTo(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR outgoingTo(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.outgoingTo(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther()
    {
        this.pattern.knowsEachOther();
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name)
    {
        this.pattern.knowsEachOther(name);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final Labels labels)
    {
        this.pattern.knowsEachOther(labels);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final PropertiesMap properties)
    {
        this.pattern.knowsEachOther(properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final Parameter properties)
    {
        this.pattern.knowsEachOther(properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name, final Labels labels)
    {
        this.pattern.knowsEachOther(name, labels);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name, final PropertiesMap properties)
    {
        this.pattern.knowsEachOther(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name, final Parameter properties)
    {
        this.pattern.knowsEachOther(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.knowsEachOther(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final Labels labels, final Parameter properties)
    {
        this.pattern.knowsEachOther(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.knowsEachOther(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR knowsEachOther(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.knowsEachOther(name, labels, properties);
        return asNodeConnector();
    }

    public final UndirectedRelation<CONNECTOR> related()
    {
        this.pattern.related();
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name)
    {
        this.pattern.related(name);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Types types)
    {
        this.pattern.related(types);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Hops hops)
    {
        this.pattern.related(hops);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final PropertiesMap properties)
    {
        this.pattern.related(properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Parameter properties)
    {
        this.pattern.related(properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Types types)
    {
        this.pattern.related(name, types);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Hops hops)
    {
        this.pattern.related(name, hops);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final PropertiesMap properties)
    {
        this.pattern.related(name, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Parameter properties)
    {
        this.pattern.related(name, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Hops hops)
    {
        this.pattern.related(name, types, hops);
        return this;
    }


    public final UndirectedRelation<CONNECTOR> related(final String name, final Types types, final PropertiesMap properties)
    {
        this.pattern.related(name, types, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Types types, final Parameter properties)
    {
        this.pattern.related(name, types, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(
        final String name,
        final Types types,
        final Hops hops,
        final PropertiesMap properties)
    {
        this.pattern.related(name, types, hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(
        final String name,
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.related(name, types, hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Types types, final Hops hops)
    {
        this.pattern.related(types, hops);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Types types, final PropertiesMap properties)
    {
        this.pattern.related(types, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Types types, final Parameter properties)
    {
        this.pattern.related(types, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Types types, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.related(types, hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.related(types, hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Hops hops, final PropertiesMap properties)
    {
        this.pattern.related(hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final Hops hops, final Parameter properties)
    {
        this.pattern.related(hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.related(name, hops, properties);
        return this;
    }

    public final UndirectedRelation<CONNECTOR> related(final String name, final Hops hops, final Parameter properties)
    {
        this.pattern.related(name, hops, properties);
        return this;
    }

    @Override public final CONNECTOR with()
    {
        this.pattern.with();
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name)
    {
        this.pattern.with(name);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final Labels labels)
    {
        this.pattern.with(labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final PropertiesMap properties)
    {
        this.pattern.with(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final Parameter properties)
    {
        this.pattern.with(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name, final Labels labels)
    {
        this.pattern.with(name, labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name, final PropertiesMap properties)
    {
        this.pattern.with(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name, final Parameter properties)
    {
        this.pattern.with(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.with(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final Labels labels, final Parameter properties)
    {
        this.pattern.with(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.with(name, labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR with(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.with(name, labels, properties);
        return asNodeConnector();
    }

    public final IncomingRelation<CONNECTOR> incoming()
    {
        this.pattern.incoming();
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name)
    {
        this.pattern.incoming(name);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Types types)
    {
        this.pattern.incoming(types);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Hops hops)
    {
        this.pattern.incoming(hops);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final PropertiesMap properties)
    {
        this.pattern.incoming(properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Parameter properties)
    {
        this.pattern.incoming(properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Types types)
    {
        this.pattern.incoming(name, types);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops)
    {
        this.pattern.incoming(name, hops);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final PropertiesMap properties)
    {
        this.pattern.incoming(name, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Parameter properties)
    {
        this.pattern.incoming(name, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Hops hops)
    {
        this.pattern.incoming(name, types, hops);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final PropertiesMap properties)
    {
        this.pattern.incoming(name, types, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Types types, final Parameter properties)
    {
        this.pattern.incoming(name, types, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(
        final String name,
        final Types types,
        final Hops hops,
        final PropertiesMap properties)
    {
        this.pattern.incoming(name, types, hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(
        final String name,
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.incoming(name, types, hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Types types, final Hops hops)
    {
        this.pattern.incoming(types, hops);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Types types, final PropertiesMap properties)
    {
        this.pattern.incoming(types, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Types types, final Parameter properties)
    {
        this.pattern.incoming(types, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Types types, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.incoming(types, hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.incoming(types, hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Hops hops, final PropertiesMap properties)
    {
        this.pattern.incoming(hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final Hops hops, final Parameter properties)
    {
        this.pattern.incoming(hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.incoming(name, hops, properties);
        return this;
    }

    public final IncomingRelation<CONNECTOR> incoming(final String name, final Hops hops, final Parameter properties)
    {
        this.pattern.incoming(name, hops, properties);
        return this;
    }

    @Override public final CONNECTOR from()
    {
        this.pattern.from();
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name)
    {
        this.pattern.from(name);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final Labels labels)
    {
        this.pattern.from(labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final PropertiesMap properties)
    {
        this.pattern.from(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final Parameter properties)
    {
        this.pattern.from(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name, final Labels labels)
    {
        this.pattern.from(name, labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name, final PropertiesMap properties)
    {
        this.pattern.from(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name, final Parameter properties)
    {
        this.pattern.from(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.from(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final Labels labels, final Parameter properties)
    {
        this.pattern.from(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.from(name, labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR from(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.from(name, labels, properties);
        return asNodeConnector();
    }

    public final OutgoingRelation<CONNECTOR> outgoing()
    {
        this.pattern.outgoing();
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name)
    {
        this.pattern.outgoing(name);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Types types)
    {
        this.pattern.outgoing(types);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Hops hops)
    {
        this.pattern.outgoing(hops);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final PropertiesMap properties)
    {
        this.pattern.outgoing(properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Parameter properties)
    {
        this.pattern.outgoing(properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types)
    {
        this.pattern.outgoing(name, types);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops)
    {
        this.pattern.outgoing(name, hops);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final PropertiesMap properties)
    {
        this.pattern.outgoing(name, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Parameter properties)
    {
        this.pattern.outgoing(name, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Hops hops)
    {
        this.pattern.outgoing(name, types, hops);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final PropertiesMap properties)
    {
        this.pattern.outgoing(name, types, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Types types, final Parameter properties)
    {
        this.pattern.outgoing(name, types, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(
        final String name,
        final Types types,
        final Hops hops,
        final PropertiesMap properties)
    {
        this.pattern.outgoing(name, types, hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(
        final String name,
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.outgoing(name, types, hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Types types, final Hops hops)
    {
        this.pattern.outgoing(types, hops);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Types types, final PropertiesMap properties)
    {
        this.pattern.outgoing(types, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Types types, final Parameter properties)
    {
        this.pattern.outgoing(types, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Types types, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.outgoing(types, hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.outgoing(types, hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Hops hops, final PropertiesMap properties)
    {
        this.pattern.outgoing(hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final Hops hops, final Parameter properties)
    {
        this.pattern.outgoing(hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.outgoing(name, hops, properties);
        return this;
    }

    public final OutgoingRelation<CONNECTOR> outgoing(final String name, final Hops hops, final Parameter properties)
    {
        this.pattern.outgoing(name, hops, properties);
        return this;
    }

    @Override public final CONNECTOR to()
    {
        this.pattern.to();
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name)
    {
        this.pattern.to(name);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final Labels labels)
    {
        this.pattern.to(labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final PropertiesMap properties)
    {
        this.pattern.to(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final Parameter properties)
    {
        this.pattern.to(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name, final Labels labels)
    {
        this.pattern.to(name, labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name, final PropertiesMap properties)
    {
        this.pattern.to(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name, final Parameter properties)
    {
        this.pattern.to(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.to(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final Labels labels, final Parameter properties)
    {
        this.pattern.to(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.to(name, labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR to(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.to(name, labels, properties);
        return asNodeConnector();
    }

    public final InOutRelation<CONNECTOR> in()
    {
        this.pattern.in();
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name)
    {
        this.pattern.in(name);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Types types)
    {
        this.pattern.in(types);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Hops hops)
    {
        this.pattern.in(hops);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final PropertiesMap properties)
    {
        this.pattern.in(properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Parameter properties)
    {
        this.pattern.in(properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Types types)
    {
        this.pattern.in(name, types);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Hops hops)
    {
        this.pattern.in(name, hops);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final PropertiesMap properties)
    {
        this.pattern.in(name, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Parameter properties)
    {
        this.pattern.in(name, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Types types, final Hops hops)
    {
        this.pattern.in(name, types, hops);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Types types, final PropertiesMap properties)
    {
        this.pattern.in(name, types, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Types types, final Parameter properties)
    {
        this.pattern.in(name, types, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(
        final String name,
        final Types types,
        final Hops hops,
        final PropertiesMap properties)
    {
        this.pattern.in(name, types, hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(
        final String name,
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.in(name, types, hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Types types, final Hops hops)
    {
        this.pattern.in(types, hops);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Types types, final PropertiesMap properties)
    {
        this.pattern.in(types, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Types types, final Parameter properties)
    {
        this.pattern.in(types, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Types types, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.in(types, hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(
        final Types types,
        final Hops hops,
        final Parameter properties)
    {
        this.pattern.in(types, hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Hops hops, final PropertiesMap properties)
    {
        this.pattern.in(hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final Hops hops, final Parameter properties)
    {
        this.pattern.in(hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Hops hops, final PropertiesMap properties)
    {
        this.pattern.in(name, hops, properties);
        return this;
    }

    public final InOutRelation<CONNECTOR> in(final String name, final Hops hops, final Parameter properties)
    {
        this.pattern.in(name, hops, properties);
        return this;
    }

    public final CONNECTOR and()
    {
        this.pattern.and();
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name)
    {
        this.pattern.and(name);
        return asNodeConnector();
    }

    public final CONNECTOR and(final Labels labels)
    {
        this.pattern.and(labels);
        return asNodeConnector();
    }

    public final CONNECTOR and(final PropertiesMap properties)
    {
        this.pattern.and(properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final Parameter properties)
    {
        this.pattern.and(properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name, final Labels labels)
    {
        this.pattern.and(name, labels);
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name, final PropertiesMap properties)
    {
        this.pattern.and(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name, final Parameter properties)
    {
        this.pattern.and(name, properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.and(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final Labels labels, final Parameter properties)
    {
        this.pattern.and(labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.and(name, labels, properties);
        return asNodeConnector();
    }

    public final CONNECTOR and(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.and(name, labels, properties);
        return asNodeConnector();
    }

    public final PathVariable<CONNECTOR> andPath(final String name)
    {
        this.pattern.andPath(name);
        return this;
    }

    public final PathVariable<CONNECTOR> andPath(final String name, final String function)
    {
        this.pattern.andPath(name, function);
        return this;
    }

    @Override public final CONNECTOR out()
    {
        this.pattern.out();
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name)
    {
        this.pattern.out(name);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final Labels labels)
    {
        this.pattern.out(labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final PropertiesMap properties)
    {
        this.pattern.out(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final Parameter properties)
    {
        this.pattern.out(properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name, final Labels labels)
    {
        this.pattern.out(name, labels);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name, final PropertiesMap properties)
    {
        this.pattern.out(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name, final Parameter properties)
    {
        this.pattern.out(name, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final Labels labels, final PropertiesMap properties)
    {
        this.pattern.out(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final Labels labels, final Parameter properties)
    {
        this.pattern.out(labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name, final Labels labels, final PropertiesMap properties)
    {
        this.pattern.out(name, labels, properties);
        return asNodeConnector();
    }

    @Override public final CONNECTOR out(final String name, final Labels labels, final Parameter properties)
    {
        this.pattern.out(name, labels, properties);
        return asNodeConnector();
    }

    abstract CONNECTOR asNodeConnector();
}
