/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Name;

import java.util.List;

public class QueryValidator extends QueriesVisitor
{

    private int indexedParameters;
    private List<String> namedParameters;
    private List<String> pathVariables;

    public QueryValidator() {this(NO_INDEXED_PARAMETERS, null, null);}

    QueryValidator(final int indexedParameters, final List<String> namedParameters, final List<String> pathVariables)
    {
        this.indexedParameters = indexedParameters;
        this.namedParameters = namedParameters;
        this.pathVariables = pathVariables;
    }

    @Override public void visit(final Alias alias) {/* nothing to do*/}

    @Override public void visit(final boolean value) {/* nothing to do*/}

    @Override public void visit(final byte value) {/* nothing to do*/}

    @Override public void visit(final short value) {/* nothing to do*/}

    @Override public void visit(final int value) {/* nothing to do*/}

    @Override public void visit(final long value) {/* nothing to do*/}

    @Override public void visit(final float value) {/* nothing to do*/}

    @Override public void visit(final double value) {/* nothing to do*/}

    @Override public void visit(final String value) {/* nothing to do*/}

    @Override public void visit(final ListLiteral value)
    {
        if (!value.isEmpty()) {
            value.head().accept(this);
            for (final var element : value.tail()) {
                element.accept(this);
            }
        }
    }

    @Override public void visit(final UnaryOperation.Prefix<?> operation) {operation.operand().accept(this);}

    @Override public void visit(final UnaryOperation.Postfix<?> operation) {operation.operand().accept(this);}

    @Override public void visit(final BinaryOperation<?> operation)
    {
        operation.lhs().accept(this);
        operation.rhs().accept(this);
    }

    @Override public void visit(final Union union) {union.query().accept(this);}

    @Override public void visit(final FunctionCall call)
    {
        if (call.hasArguments()) {
            call.headArgument().accept(this);
            for (final var argument : call.tailArguments()) {
                argument.accept(this);
            }
        }
    }

    @Override public void visit(final SortPredicate.Explicit sortPredicate) {sortPredicate.sortOperand().accept(this);}

    @Override public void visit(final CaseExpression expr)
    {
        if (null != expr.base()) {
            expr.base().accept(this);
        }
        for (final var alternative : expr.alternatives()) {
            alternative.accept(this);
        }
        if (null != expr.elseAlternative()) {
            expr.elseAlternative().accept(this);
        }
    }

    @Override public void visit(final CaseExpression.Alternative alternative)
    {
        alternative.when().accept(this);
        alternative.then().accept(this);
    }

    @Override public void visit(final PropertyExpression propertyExpression) {propertyExpression.atom().accept(this);}

    @Override public void visit(final RawExpression expr) {/* nothing to do*/}

    @Override public void visit(final PropertyAccess.Static propertyAccess) {/* nothing to do*/}

    @Override public void visit(final PropertyAccess.Dynamic propertyAccess) {propertyAccess.operand().accept(this);}

    @Override public void visit(final NodeVariable n) {/* nothing to do*/}

    @Override public void visit(final RelationshipVariable r) {/* nothing to do*/}

    @Override public void visit(final NamedParameter parameter) {registerNamedParameter(parameter.name());}

    @Override public void visit(final IndexedParameter parameter) {registerIndexedParameter(parameter.index());}

    @Override public void visit(final GetElement expr)
    {
        expr.list().accept(this);
        expr.index().accept(this);
    }

    @Override public void visit(final GetSubList expr)
    {
        expr.list().accept(this);
        expr.start().accept(this);
        expr.end().accept(this);
    }

    @Override public void visit(final PropertyValue.WithExpression propertyValue) {/*nothing to do*/}

    @Override public void visit(final PropertyValue.WithParameter propertyValue) {propertyValue.value().accept(this);}

    @Override public void visit(final RegularQuery query)
    {
        if (noStatementVisited()) {
            if (null != query.statement()) {
                query.statement().accept(this);
            }
        }
        if (null == query.returnClause()) {
            throw new CypherException("RETURN must be defined on all queries!");
        }
        for (final var clause : query.clauses()) {
            clause.accept(this);
        }
    }

    @Override public void visit(final CypherProjection projection)
    {
        if (projection.doesntHaveItems()) {
            if (projection.distinct()) {
                throw new CypherException("RETURN DISTINCT should have at least one item defined");
            } else {
                throw new CypherException("RETURN should have at least one item defined");
            }
        }

        projection.itemsHead().accept(this);
        for (final var item : projection.itemsTail()) {
            item.accept(this);
        }
        if (projection.isSorted()) {
            projection.sortingHead().accept(this);
            for (final var sorPredicate : projection.sortingTail()) {
                sorPredicate.accept(this);
            }
        }
        if (projection.hasSkip()) {
            projection.skip().accept(this);
        }
        if (projection.hasLimit()) {
            projection.limit().accept(this);
        }
    }

    @Override public void visit(final WithClause clause)
    {
        clause.projection().accept(this);
        if (null != clause.where()) {
            clause.where().accept(this);
        }
    }

    @Override public void visit(final MatchClause match)
    {
        if (null == match.pattern()) {
            if (match.optional()) {
                throw new CypherException("MATCH OPTIONAL requires a pattern to be defined");
            } else {
                throw new CypherException("MATCH requires a pattern to be defined");
            }
        }
        match.pattern().accept(this);
        if (match.hasWhere()) {
            match.where().accept(this);
        }
    }

    @Override public void visit(final UnwindClause clause)
    {
        clause.definition().accept(this);
    }

    @Override public void visit(final InQueryCallClause clause)
    {
        if (null != clause.argumentsHead()) {
            clause.argumentsHead().accept(this);
            for (final var arg : clause.argumentsTail()) {
                arg.accept(this);
            }
        }
        if (null != clause.where()) {
            clause.where().accept(this);
        }
    }

    @Override public void visit(final CreateClause clause) {clause.pattern().accept(this);}

    @Override public void visit(final MergeClause clause)
    {
        clause.pattern().accept(this);
        for (final var item : clause.items()) {
            item.accept(this);
        }
    }

    @Override public void visit(final DeleteClause clause)
    {
        clause.head().accept(this);
        for (final var expr : clause.tail()) {
            expr.accept(this);
        }
    }

    @Override public void visit(final SetClause clause)
    {
        clause.head().accept(this);
        for (final var item : clause.tail()) {
            item.accept(this);
        }
    }

    @Override public void visit(final RemoveClause clause)
    {
        clause.head().accept(this);
        for (final var item : clause.tail()) {
            item.accept(this);
        }
    }

    @Override public void visit(final Pattern<?> pattern)
    {
        if (null == pattern.firstNode()) {
            throw new CypherException("Patterns should have at least one node  defined");
        }
        pattern.firstNode().accept(this);
        if (pattern.hasConnections()) {
            for (final var connection : pattern.connections()) {
                connection.accept(this);
            }
        }
        if (pattern.hasNext()) {
            pattern.next().accept(this);
        }
    }

    @Override public void visit(final NamedPattern<?> pattern)
    {
        registerPathVariable(pattern.path());
        visit((Pattern<?>) pattern);
    }

    @Override public void visit(final UndetailedConnection connection)
    {
        connection.node().accept(this);
    }

    @Override public void visit(final DetailedConnection connection)
    {
        if (connection.doesntHaveNode()) {
            throw new CypherException("Connections should define its nodes");
        }
        connection.relationship().accept(this);
        connection.node().accept(this);
    }

    @Override public void visit(final PropertiesMap map)
    {
        if (!map.isEmpty()) {
            map.head().accept(this);
            for (final var entry : map.tail()) {
                entry.accept(this);
            }
        }
    }

    @Override public void visit(final RelationshipDetails.OneHop hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.AnyHops hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.FixedHops hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.MinHops hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.MaxHops hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.RangeHops hops) {/*nothing to to*/}

    @Override public void visit(final MergeClause.Item item) {item.action().accept(this);}

    @Override public void visit(final AliasName aliasName) {/*FIXME: should validate that no alias conflict exists*/}

    @Override public void visit(final SetItem.Reset item)
    {
        item.lhs().accept(this);
        item.rhs().accept(this);
    }

    @Override public void visit(final SetItem.Append item)
    {
        item.lhs().accept(this);
        item.rhs().accept(this);
    }

    @Override public void visit(final UpdateLabels item)
    {
        if (item.labels().isEmpty()) {
            throw new CypherException("at least one label must be defined");
        }
        item.target().accept(this);
    }

    @Override public void visit(final NodeWithoutProperties node) {/*nothing to do*/}

    @Override public void visit(final NodeWithProperties node) {node.properties().accept(this);}

    @Override public void visit(final RelationshipDetails.Empty relationship) {/*nothing to do*/}

    @Override public void visit(final RelationshipWithoutProperties relationship) {/*nothing to do*/}

    @Override public void visit(final RelationshipWithProperties relationship) {relationship.properties().accept(this);}

    // common visits
    boolean hasIndexedParameters() {return NO_INDEXED_PARAMETERS != this.indexedParameters;}

    boolean hasNamedParameters() {return null != this.namedParameters && !this.namedParameters.isEmpty();}

    void registerIndexedParameter(final int index)
    {
        if (hasNamedParameters()) {
            throw new CypherException("indexed and named params can not be mixed");
        }
        this.indexedParameters = Math.max(this.indexedParameters, index);
    }

    void registerNamedParameter(final String name)
    {
        if (hasIndexedParameters()) {
            throw new CypherException("indexed and named params can not be mixed");
        }
        if (null == this.namedParameters) {
            this.namedParameters = new java.util.ArrayList<>(5);
        }
        if (!this.namedParameters.contains(name)) {
            this.namedParameters.add(name);
        }
    }

    void registerPathVariable(final String name)
    {
        if (null == this.pathVariables) {
            this.pathVariables = new java.util.LinkedList<>();
        }
        final var pathName = Name.at(name);
        if (this.pathVariables.contains(pathName)) {
            throw new CypherException("path variable '" + name + "' already defined.");
        }
        this.pathVariables.add(pathName);
    }
}
