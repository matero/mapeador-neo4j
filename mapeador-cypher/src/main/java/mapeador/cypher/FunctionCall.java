/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Name;

import java.util.List;
import java.util.Objects;

public final class FunctionCall implements CypherElement, Predicate, Alias.Target
{
    private final String name;
    private final Expression headArgument;
    private final List<Expression> tailArguments;

    FunctionCall(final String name, final Expression headArgument, final List<Expression> tailArguments)
    {
        this.name = name;
        this.headArgument = headArgument;
        this.tailArguments = tailArguments;
    }

    public String name() {return this.name;}

    public Expression headArgument() {return this.headArgument;}

    public List<Expression> tailArguments() {return this.tailArguments;}

    public boolean hasArguments() {return null != this.headArgument;}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    @Override public boolean grouped() {return false;}

    @Override public FunctionCall group() {return this;}

    @Override public FunctionCall sortOperand() {return this;}

    public static FunctionCall of(final String name)
    {
        checkName(name);
        return new FunctionCall(name, null, List.of());
    }

    public static FunctionCall of(final String name, final Expression argument)
    {
        checkName(name);
        Objects.requireNonNull(argument, "argument is required");
        return new FunctionCall(name, argument, List.of());
    }

    public static FunctionCall of(final String name, final Expression firstArtument, final Expression... tailArguments)
    {
        checkName(name);
        Objects.requireNonNull(firstArtument, "firstArtument is required");
        if (null == tailArguments || 0 == tailArguments.length) {
            return new FunctionCall(name, firstArtument, List.of());
        } else {
            return new FunctionCall(name, firstArtument, List.of(tailArguments));
        }
    }

    public static FunctionCall of(final String name, final List<Expression> arguments)
    {
        checkName(name);
        Objects.requireNonNull(arguments);
        if (arguments.isEmpty()) {
            return new FunctionCall(name, null, List.of());
        }
        final var firstArtument = arguments.get(0);
        Objects.requireNonNull(firstArtument, "firstArtument is required");

        if (1 == arguments.size()) {
            return new FunctionCall(name, firstArtument, List.copyOf(arguments));
        } else {
            return new FunctionCall(name, firstArtument, List.copyOf(arguments.subList(1, arguments.size())));
        }
    }

    private static String checkName(final String name)
    {
        Objects.requireNonNull(name, "name is required");
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name can't be empty");
        }
        if (name.isBlank()) {
            throw new IllegalArgumentException("name can't be blank");
        }
        if (Name.requireBackticks(name)) {
            throw new IllegalArgumentException("name shouldn't require backticks");
        }
        return name;
    }
}
