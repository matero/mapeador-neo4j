/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;

abstract class UpdateClause extends Clause implements HasReturn.AfterUpdateClause
{
    UpdateClause(final RegularQuery parent) {super(parent);}

    @Override public WithClause WITH(final Expression item) {return this.parent.WITH(item);}

    @Override public WithClause WITH(final Expression firstItem, final Expression... otherItems) {return this.parent.WITH(firstItem, otherItems);}

    @Override public WithClause WITH_DISTINCT(final Expression item) {return this.parent.WITH_DISTINCT(item);}

    @Override public WithClause WITH_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        return this.parent.WITH_DISTINCT(firstItem, otherItems);
    }

    // ======================================================================================================================================== //
    // U P D A T I N G   C L A U S E S                                                                                                          //
    // ======================================================================================================================================== //
    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE() {return this.parent.CREATE();}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name) {return this.parent.CREATE(name);}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final PropertiesMap properties)
    {
        return this.parent.CREATE(name, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Parameter properties)
    {
        return this.parent.CREATE(name, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels) {return this.parent.CREATE(labels);}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final Parameter properties)
    {
        return this.parent.CREATE(labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels)
    {
        return this.parent.CREATE(name, labels);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }

    @Override public PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(final String pathName)
    {
        return this.parent.CREATE_path(pathName);
    }

    @Override public final NodeConnector.WithMergeActions MERGE() {return this.parent.MERGE();}

    @Override public final NodeConnector.WithMergeActions MERGE(final String name) {return this.parent.MERGE(name);}

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final Parameter properties)
    {
        return this.parent.MERGE(name, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final Labels labels) {return this.parent.MERGE(labels);}

    @Override public final NodeConnector.WithMergeActions MERGE(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final Labels labels, final Parameter properties)
    {
        return this.parent.MERGE(labels,
                                 properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final HasLabels labels) {return this.parent.MERGE(labels);}

    @Override public final NodeConnector.WithMergeActions MERGE(final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final HasLabels labels, final Parameter properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels) {return this.parent.MERGE(name, labels);}

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels)
    {
        return this.parent.MERGE(name, labels);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final Parameter properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final NodeVariable node) {return this.parent.MERGE(node);}

    @Override public final NodeConnector.WithMergeActions MERGE(final NodeVariable node, final PropertiesMap properties)
    {
        return this.parent.MERGE(node, properties);
    }

    @Override public final NodeConnector.WithMergeActions MERGE(final NodeVariable node, final Parameter properties)
    {
        return this.parent.MERGE(node,
                                 properties);
    }

    @Override public final AfterUpdateClause DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        return this.parent.DELETE(deletion, otherDeletions);
    }

    @Override public final AfterUpdateClause DETACH_DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        return this.parent.DETACH_DELETE(deletion, otherDeletions);
    }

    @Override public final AfterUpdateClause SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return this.parent.SET(firstItem, otherItems);
    }

    @Override public final AfterUpdateClause REMOVE(final RemoveItem firstItem, final RemoveItem... otherItems)
    {
        return this.parent.REMOVE(firstItem, otherItems);
    }
}
