/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Labels;

import java.util.Collections;
import java.util.List;

public final class Statement implements CypherElement
{
    private final Query first;
    private List<Union> unions;
    private UnionSpec union;
    private UnionSpec unionAll;

    Statement(final Query first) {this.first = first;}

    public Query first() {return this.first;}

    public List<Union> unions() {return null == this.unions ? List.of() : Collections.unmodifiableList(this.unions);}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    UnionSpec union()
    {
        if (null == this.unions) {
            this.unions = new java.util.LinkedList<>();
        }
        if (null == this.union) {
            this.union = new UnionSpec(false);
        }
        return this.union;
    }

    UnionSpec unionAll()
    {
        if (null == this.unions) {
            this.unions = new java.util.LinkedList<>();
        }
        if (null == this.unionAll) {
            this.unionAll = new UnionSpec(true);
        }
        return this.unionAll;
    }

    public class UnionSpec implements HasReturn.AfterReadClause {
        private final boolean all;

        UnionSpec(final boolean all) {this.all = all;}

        RegularQuery registerUnion() {
            final var query = new RegularQuery(Statement.this);
            Statement.this.unions.add(new Union(this.all, query));
            return query;
        }

        @Override public NodeConnector.WithWhere MATCH() {return registerUnion().MATCH();}

        @Override public NodeConnector.WithWhere MATCH(final String name) {return registerUnion().MATCH(name);}

        @Override public NodeConnector.WithWhere MATCH(final String name, final PropertiesMap properties)
        {
            return registerUnion().MATCH(name, properties);
        }

        @Override public NodeConnector.WithWhere MATCH(final String name, final Parameter properties)
        {
            return registerUnion().MATCH(name, properties);
        }

        @Override public NodeConnector.WithWhere MATCH(final Labels labels) {return registerUnion().MATCH(labels);}

        @Override public NodeConnector.WithWhere MATCH(final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().MATCH(labels, properties);
        }

        @Override public NodeConnector.WithWhere MATCH(final Labels labels, final Parameter properties)
        {
            return registerUnion().MATCH(labels, properties);
        }

        @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels)
        {
            return registerUnion().MATCH(name, labels);
        }

        @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().MATCH(name, labels, properties);
        }

        @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels, final Parameter properties)
        {
            return registerUnion().MATCH(name, labels, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH() {return registerUnion().OPTIONAL_MATCH();}

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name) {return registerUnion().OPTIONAL_MATCH(name);}

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final PropertiesMap properties)
        {
            return registerUnion().OPTIONAL_MATCH(name, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Parameter properties)
        {
            return registerUnion().OPTIONAL_MATCH(name, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels)
        {
            return registerUnion().OPTIONAL_MATCH(labels);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().OPTIONAL_MATCH(labels, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final Parameter properties)
        {
            return registerUnion().OPTIONAL_MATCH(labels, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels)
        {
            return registerUnion().OPTIONAL_MATCH(name, labels);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().OPTIONAL_MATCH(name, labels, properties);
        }

        @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final Parameter properties)
        {
            return registerUnion().OPTIONAL_MATCH(name, labels, properties);
        }

        @Override public PathVariable<NodeConnector.WithWhere> MATCH_path(final String name)
        {
            return registerUnion().MATCH_path(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> MATCH_shortestPath(final String name)
        {
            return registerUnion().MATCH_shortestPath(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> MATCH_allShortestPaths(final String name)
        {
            return registerUnion().MATCH_allShortestPaths(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> MATCH_path(final String name, final PathFunction function)
        {
            return registerUnion().MATCH_path(name, function);
        }

        @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name)
        {
            return registerUnion().OPTIONAL_MATCH_path(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_shortestPath(final String name)
        {
            return registerUnion().OPTIONAL_MATCH_shortestPath(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_allShortestPaths(final String name)
        {
            return registerUnion().OPTIONAL_MATCH_allShortestPaths(name);
        }

        @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name, final PathFunction function)
        {
            return registerUnion().OPTIONAL_MATCH_path(name, function);
        }

        @Override public UnwindClause UNWIND(final Alias definition)
        {
            return registerUnion().UNWIND(definition);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE()
        {
            return registerUnion().CREATE();
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name)
        {
            return registerUnion().CREATE(name);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final PropertiesMap properties)
        {
            return registerUnion().CREATE(name, properties);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Parameter properties)
        {
            return registerUnion().CREATE(name, properties);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels)
        {
            return registerUnion().CREATE(labels);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().CREATE(labels, properties);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final Parameter properties)
        {
            return registerUnion().CREATE(labels, properties);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels)
        {
            return registerUnion().CREATE(name, labels);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(
            final String name,
            final Labels labels,
            final PropertiesMap properties)
        {
            return registerUnion().CREATE(name, labels, properties);
        }

        @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final Parameter properties)
        {
            return registerUnion().CREATE(name, labels, properties);
        }

        @Override public PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(final String pathName)
        {
            return registerUnion().CREATE_path(pathName);
        }

        @Override public NodeConnector.WithMergeActions MERGE()
        {
            return registerUnion().MERGE();
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name)
        {
            return registerUnion().MERGE(name);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name, final PropertiesMap properties)
        {
            return registerUnion().MERGE(name, properties);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name, final Parameter properties)
        {
            return registerUnion().MERGE(name, properties);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final Labels labels)
        {
            return registerUnion().MERGE(labels);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().MERGE(labels, properties);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final Labels labels, final Parameter properties)
        {
            return registerUnion().MERGE(labels, properties);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels)
        {
            return registerUnion().MERGE(name, labels);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final PropertiesMap properties)
        {
            return registerUnion().MERGE(name, labels, properties);
        }

        @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final Parameter properties)
        {
            return registerUnion().MERGE(name, labels, properties);
        }

        @Override public RegularQuery DELETE(final Expression deletion, final Expression... otherDeletions)
        {
            return registerUnion().DELETE(deletion, otherDeletions);
        }

        @Override public RegularQuery DETACH_DELETE(final Expression deletion, final Expression... otherDeletions)
        {
            return registerUnion().DETACH_DELETE(deletion, otherDeletions);
        }

        @Override public RegularQuery SET(final SetItem firstItem, final SetItem... otherItems)
        {
            return registerUnion().SET(firstItem, otherItems);
        }

        @Override public RegularQuery REMOVE(final RemoveItem firstItem, final RemoveItem... otherItems)
        {
            return registerUnion().REMOVE(firstItem, otherItems);
        }

        @Override public Projection RETURN(final Expression item)
        {
            return registerUnion().RETURN(item);
        }

        @Override public Projection RETURN(final Expression firstItem, final Expression... otherItems)
        {
            return registerUnion().RETURN(firstItem, otherItems);
        }

        @Override public Projection RETURN_DISTINCT(final Expression item)
        {
            return registerUnion().RETURN_DISTINCT(item);
        }

        @Override public Projection RETURN_DISTINCT(final Expression firstItem, final Expression... otherItems)
        {
            return registerUnion().RETURN_DISTINCT(firstItem, otherItems);
        }

        @Override public WithClause WITH(final Expression item)
        {
            return registerUnion().WITH(item);
        }

        @Override public WithClause WITH(final Expression firstItem, final Expression... otherItems)
        {
            return registerUnion().WITH(firstItem, otherItems);
        }

        @Override public WithClause WITH_DISTINCT(final Expression item)
        {
            return registerUnion().WITH_DISTINCT(item);
        }

        @Override public WithClause WITH_DISTINCT(final Expression firstItem, final Expression... otherItems)
        {
            return registerUnion().WITH_DISTINCT(firstItem, otherItems);
        }

        @Override public void accept(final CypherVisitor visitor) {throw new UnsupportedOperationException("this should never happend :-/");}
    }
}
