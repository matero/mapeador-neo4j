/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Name;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class InQueryCallClause extends ReadClause implements HasYield
{
    private final String procedure;
    private final Expression argumentsHead;
    private final List<Expression> argumentsTail;
    private String yieldItemsHead;
    private List<String> yieldItemsTail;
    private Predicate where;

    InQueryCallClause(final RegularQuery parent, final String procedure, final Expression argumentsHead, final List<Expression> argumentsTail)
    {
        super(parent);
        this.procedure = procedure;
        this.argumentsHead = argumentsHead;
        this.argumentsTail = argumentsTail;
    }

    public String procedure() {return this.procedure;}

    public Expression argumentsHead() {return this.argumentsHead;}

    public List<Expression> argumentsTail() {return this.argumentsTail;}

    public String yieldItemsHead() {return this.yieldItemsHead;}

    public List<String> yieldItemsTail() {return this.yieldItemsTail;}

    public Predicate where() {return this.where;}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    static InQueryCallClause of(final RegularQuery parent, final String canonicalName)
    {
        Objects.requireNonNull(canonicalName, "canonicalName is required");
        final var parts = canonicalName.split("."); // FIXME: use Pattern, after tests are dfined
        if (2 > parts.length) {
            throw new CypherException("namespace AND procedure names are required, only one of them was defined (received: '" + canonicalName + "')");
        }
        final var procedure = new StringBuilder(canonicalName.length() + parts.length * 3);
        procedure.append(Name.at(parts[0]));
        for (int i = 1; i < parts.length; i++) {
            procedure.append('.').append(Name.at(parts[i]));
        }
        return new InQueryCallClause(parent, procedure.toString(), null, List.of());
    }

    static InQueryCallClause of(final RegularQuery parent, final String packageName, final String procedureName)
    {
        Objects.requireNonNull(packageName, "packageName is required");
        Objects.requireNonNull(procedureName, "procedureName is required");

        final var procedure = Name.at(procedureName);

        final var parts = packageName.split("."); // FIXME: use Pattern, after tests are dfined

        final var procedureCanonicalName = new StringBuilder(packageName.length() + parts.length * 3 + procedure.length());
        for (final var part : parts) {
            procedureCanonicalName.append(Name.at(part)).append('.');
        }
        return new InQueryCallClause(parent, procedureCanonicalName.append(procedure).toString(), null, List.of());
    }

    static InQueryCallClause of(final RegularQuery parent, final String canonicalName, final Expression firstArgument, final Expression[] otherArguments)
    {
        Objects.requireNonNull(canonicalName, "canonicalName is required");
        Objects.requireNonNull(firstArgument, "firstArgument is required");
        final var parts = canonicalName.split("."); // FIXME: use Pattern, after tests are dfined
        if (2 > parts.length) {
            throw new CypherException("namespace AND procedure names are required, only one of them was defined (received: '" + canonicalName + "')");
        }
        final var procedure = new StringBuilder(canonicalName.length() + parts.length * 3);
        procedure.append(Name.at(parts[0]));
        for (int i = 1; i < parts.length; i++) {
            procedure.append('.').append(Name.at(parts[i]));
        }
        if (null == otherArguments || 1 == otherArguments.length) {
            return new InQueryCallClause(parent, procedure.toString(), firstArgument, List.of());
        } else {
            return new InQueryCallClause(parent, procedure.toString(), firstArgument, List.of(otherArguments));
        }
    }

    static InQueryCallClause of(
        final RegularQuery parent,
        final String packageName,
        final String procedureName,
        final Expression firstArgument,
        final Expression[] otherArguments)
    {
        Objects.requireNonNull(packageName, "packageName is required");
        Objects.requireNonNull(procedureName, "procedureName is required");
        Objects.requireNonNull(firstArgument, "firstArgument is required");

        final var procedure = Name.at(procedureName);
        final var packageParts = packageName.split("."); // FIXME: use Pattern, after tests are dfined

        final var procedureCanonicalName = new StringBuilder(packageName.length() + packageParts.length * 3 + procedure.length());
        for (final var part : packageParts) {
            procedureCanonicalName.append(Name.at(part)).append('.');
        }
        if (null == otherArguments || 1 == otherArguments.length) {
            return new InQueryCallClause(parent, procedureCanonicalName.append(procedure).toString(), firstArgument, List.of());
        } else {
            return new InQueryCallClause(parent, procedureCanonicalName.append(procedure).toString(), firstArgument, List.of(otherArguments));
        }
    }

    @Override public HasWhere YIELD(final String procedureResult, final String... otherProcedureResults)
    {
        Objects.requireNonNull(procedureResult, "procedureResult is required");
        this.yieldItemsHead = Name.at(procedureResult);
        if (null == otherProcedureResults || 0 == otherProcedureResults.length) {
            this.yieldItemsTail = List.of();
        } else {
            this.yieldItemsTail = Arrays.stream(otherProcedureResults).map(Name::at).collect(Collectors.toUnmodifiableList());
        }
        return this;
    }

    @Override public HasReturn.AfterReadClause WHERE(final Predicate predicate)
    {
        Objects.requireNonNull(predicate, "predicate is required");
        this.where = predicate;
        return this;
    }
}
