/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

public interface Projection extends ProjectionWithOrderBy
{
    ProjectionWithOrderBy ORDER_BY(final SortPredicate item);

    ProjectionWithOrderBy ORDER_BY(final SortPredicate first, final SortPredicate... others);

    ProjectionWithOrderBy ORDER_BY(final Object item);

    ProjectionWithOrderBy ORDER_BY(final Object first, final Object... others);
}

enum get
{
    projection;

    Expression[] tailAt(final Object[] items)
    {
        final var tail = new Expression[items.length];
        for (int i = 0; i < items.length; i++) {
            tail[i] = itemAt(items[i]);
        }
        return tail;
    }

    Expression itemAt(final Object item)
    {
        if (item instanceof String) {
            return AliasName.of((String) item);
        }
        if (item instanceof Expression) {
            return (Expression) item;
        }
        throw new IllegalArgumentException("only Expressions and strings (using alias known names) can be items of projections");
    }
}
