/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.List;
import java.util.Objects;

public final class SetClause extends UpdateClause
{
    private final SetItem head;
    private final List<SetItem> tail;

    SetClause(final RegularQuery parent, final SetItem head, final List<SetItem> tail)
    {
        super(parent);
        this.head = head;
        this.tail = tail;
    }

    public SetItem head() {return this.head;}

    public List<SetItem> tail() {return this.tail;}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    static SetClause of(final RegularQuery query, final SetItem firstItem, final SetItem[] otherItems)
    {
        Objects.requireNonNull(firstItem, "firstItem is required");
        final List<SetItem> tail;
        if (null != otherItems && 0 != otherItems.length) {
            try {
                tail = List.of(otherItems);
            } catch (final NullPointerException npe) {
                throw new CypherException("otherItems can't contain null elements", npe);
            }
        } else {
            tail = List.of();
        }
        return new SetClause(query, firstItem, tail);
    }
}
