/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Labels;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class RegularQuery implements Query, HasReturn.AfterReadClause, Projection
{
    private List<Clause> clauses;
    private CypherProjection returnClause;
    private Statement statement;

    RegularQuery() {this(null, null);}

    RegularQuery(final Statement statement) {this(statement, null);}

    RegularQuery(final Statement statement, final List<Clause> clauses)
    {
        this.statement = statement;
        this.clauses = clauses;
    }

    public Statement.UnionSpec UNION() {
        if (null == this.statement) {
            this.statement = new Statement(this);
        }
        return this.statement.union();
    }

    public Statement.UnionSpec UNION_ALL() {
        if (null == this.statement) {
            this.statement = new Statement(this);
        }
        return this.statement.unionAll();
    }

    public Statement statement() {return this.statement;}

    public List<Clause> clauses() {return null == this.clauses ? List.of() : Collections.unmodifiableList(this.clauses);}

    public CypherProjection returnClause() {return this.returnClause;}

    // ============================================================================================================================================ //
    // R E A D I N G   C L A U S E S                                                                                                                //
    // ============================================================================================================================================ //
    @Override public NodeConnector.WithWhere MATCH() {return makeMatchClause(false).node();}

    @Override public NodeConnector.WithWhere MATCH(final String name) {return makeMatchClause(false).node(name);}

    @Override public NodeConnector.WithWhere MATCH(final String name, final PropertiesMap properties)
    {
        return makeMatchClause(false).node(name, properties);
    }

    @Override public NodeConnector.WithWhere MATCH(final String name, final Parameter properties)
    {
        return makeMatchClause(false).node(name, properties);
    }

    @Override public NodeConnector.WithWhere MATCH(final Labels labels) {return makeMatchClause(false).node(labels);}

    @Override public NodeConnector.WithWhere MATCH(final Labels labels, final PropertiesMap properties)
    {
        return makeMatchClause(false).node(labels, properties);
    }

    @Override public NodeConnector.WithWhere MATCH(final Labels labels, final Parameter properties)
    {
        return makeMatchClause(false).node(labels, properties);
    }

    @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels) {return makeMatchClause(false).node(name, labels);}

    @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return makeMatchClause(false).node(name, labels, properties);
    }

    @Override public NodeConnector.WithWhere MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return makeMatchClause(false).node(name, labels, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH() {return makeMatchClause(true).node();}

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name) {return makeMatchClause(true).node(name);}

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final PropertiesMap properties)
    {
        return makeMatchClause(true).node(name, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Parameter properties)
    {
        return makeMatchClause(true).node(name, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels) {return makeMatchClause(true).node(labels);}

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final PropertiesMap properties)
    {
        return makeMatchClause(true).node(labels, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final Parameter properties)
    {
        return makeMatchClause(true).node(labels, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels)
    {
        return makeMatchClause(true).node(name, labels);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return makeMatchClause(true).node(name, labels, properties);
    }

    @Override public NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return makeMatchClause(true).node(name, labels, properties);
    }

    @Override public PathVariable<NodeConnector.WithWhere> MATCH_path(final String name) {return makeMatchClause(false, name);}

    @Override public PathVariable<NodeConnector.WithWhere> MATCH_shortestPath(final String name)
    {
        return makeMatchClause(false, name, PathFunction.shortestPath.name());
    }

    @Override public PathVariable<NodeConnector.WithWhere> MATCH_allShortestPaths(final String name)
    {
        return makeMatchClause(false, name, PathFunction.allShortestPaths.name());
    }

    @Override public PathVariable<NodeConnector.WithWhere> MATCH_path(final String name, final PathFunction function)
    {
        return makeMatchClause(false, name, function.name());
    }

    @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name) {return makeMatchClause(true, name);}

    @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_shortestPath(final String name)
    {
        return makeMatchClause(true, name, PathFunction.shortestPath.name());
    }

    @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_allShortestPaths(final String name)
    {
        return makeMatchClause(true, name, PathFunction.allShortestPaths.name());
    }

    @Override public PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name, final PathFunction function)
    {
        return makeMatchClause(true, name, function.name());
    }

    @Override public UnwindClause UNWIND(final Alias definition)
    {
        Objects.requireNonNull(definition, "definition is required");
        return registerClause(new UnwindClause(this, definition));
    }

    // ============================================================================================================================================ //
    // U P D A T I N G   C L A U S E S                                                                                                              //
    // ============================================================================================================================================ //
    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE() {return makeCreateClause().node();}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name) {return makeCreateClause().node(name);}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final PropertiesMap properties)
    {
        return makeCreateClause().node(name, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Parameter properties)
    {
        return makeCreateClause().node(name, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels) {return makeCreateClause().node(labels);}

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final PropertiesMap properties)
    {
        return makeCreateClause().node(labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final Parameter properties)
    {
        return makeCreateClause().node(labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels)
    {
        return makeCreateClause().node(name,
                                       labels);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return makeCreateClause().node(name, labels, properties);
    }

    @Override public NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final Parameter properties)
    {
        return makeCreateClause().node(name, labels, properties);
    }

    @Override public PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(final String pathName)
    {
        return makeCreateClause(pathName);
    }

    @Override public NodeConnector.WithMergeActions MERGE() {return makeMergeClause().node();}

    @Override public NodeConnector.WithMergeActions MERGE(final String name) {return makeMergeClause().node(name);}

    @Override public NodeConnector.WithMergeActions MERGE(final String name, final PropertiesMap properties)
    {
        return makeMergeClause().node(name, properties);
    }

    @Override public NodeConnector.WithMergeActions MERGE(final String name, final Parameter properties)
    {
        return makeMergeClause().node(name, properties);
    }

    @Override public NodeConnector.WithMergeActions MERGE(final Labels labels) {return makeMergeClause().node(labels);}

    @Override public NodeConnector.WithMergeActions MERGE(final Labels labels, final PropertiesMap properties)
    {
        return makeMergeClause().node(labels, properties);
    }

    @Override public NodeConnector.WithMergeActions MERGE(final Labels labels, final Parameter properties)
    {
        return makeMergeClause().node(labels, properties);
    }

    @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels) {return makeMergeClause().node(name, labels);}

    @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return makeMergeClause().node(name, labels, properties);
    }

    @Override public NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final Parameter properties)
    {
        return makeMergeClause().node(name, labels, properties);
    }

    @Override public RegularQuery DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        makeDeleteClause(false, deletion, otherDeletions);
        return this;
    }

    @Override public RegularQuery DETACH_DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        makeDeleteClause(true, deletion, otherDeletions);
        return this;
    }

    @Override public RegularQuery SET(final SetItem firstItem, final SetItem... otherItems)
    {
        registerClause(SetClause.of(this, firstItem, otherItems));
        return this;
    }

    @Override public RegularQuery REMOVE(final RemoveItem firstItem, final RemoveItem... otherItems)
    {
        Objects.requireNonNull(firstItem, "firstItem is required");
        final List<RemoveItem> tail;
        if (null != otherItems && 0 != otherItems.length) {
            try {
                tail = List.of(otherItems);
            } catch (final NullPointerException npe) {
                throw new CypherException("otherItems can't contain null elements", npe);
            }
        } else {
            tail = List.of();
        }
        registerClause(new RemoveClause(this, firstItem, tail));
        return this;
    }

    /* RETURN ************************************************************************************************************************************* */
    @Override public Projection RETURN(final Expression item)
    {
        if (null != this.returnClause) {
            throw new CypherException("RETURN Clause already defined");
        }
        Objects.requireNonNull(item, "item is required");
        this.returnClause = new CypherProjection(this, false, item);
        return this;
    }

    @Override public Projection RETURN(final Expression firstItem, final Expression... otherItems)
    {
        if (null != this.returnClause) {
            throw new CypherException("RETURN Clause already defined");
        }
        Objects.requireNonNull(firstItem, "firstColumn is required");
        this.returnClause = new CypherProjection(this, false, firstItem, otherItems);
        return this;
    }

    @Override public Projection RETURN_DISTINCT(final Expression item)
    {
        if (null != this.returnClause) {
            throw new CypherException("RETURN Clause already defined");
        }
        Objects.requireNonNull(item, "firstColumn is required");
        this.returnClause = new CypherProjection(this, true, item);
        return this;
    }

    @Override public Projection RETURN_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        if (null != this.returnClause) {
            throw new CypherException("RETURN Clause already defined");
        }
        Objects.requireNonNull(firstItem, "firstColumn is required");
        this.returnClause = new CypherProjection(this, true, firstItem, otherItems);
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final SortPredicate sortPredicate)
    {
        this.returnClause.ORDER_BY(sortPredicate);
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final Object sortPredicate)
    {
        this.returnClause.ORDER_BY(sortPredicate);
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final Object first, final Object... others)
    {
        this.returnClause.ORDER_BY(first, others);
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final SortPredicate first, final SortPredicate... others)
    {
        this.returnClause.ORDER_BY(first, others);
        return this;
    }

    @Override public ProjectionWithSkip SKIP(final Expression skip)
    {
        this.returnClause.SKIP(skip);
        return this;
    }

    @Override public ProjectionWithLimit LIMIT(final Expression limit)
    {
        this.returnClause.LIMIT(limit);
        return this;
    }

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    /* factory methods **************************************************************************************************************************** */
    private MatchClause makeMatchClause(final boolean optional) {return registerClause(new MatchClause(this, optional));}

    private MatchClause makeMatchClause(final boolean optional, final String name) {return registerClause(new MatchClause(this, optional, name));}

    private MatchClause makeMatchClause(final boolean optional, final String name, final String function)
    {
        return registerClause(new MatchClause(this, optional, name, function));
    }

    private CreateClause makeCreateClause() {return registerClause(new CreateClause(this));}

    private CreateClause makeCreateClause(final String name) {return registerClause(new CreateClause(this, name));}

    private MergeClause makeMergeClause() {return registerClause(new MergeClause(this));}

    private MergeClause makeMergeClause(final String name) {return registerClause(new MergeClause(this, name));}

    private void makeDeleteClause(final boolean detached, final Expression deletion, final Expression[] otherDeletions)
    {
        Objects.requireNonNull(deletion, "deletion is required");
        final List<Expression> tail;
        if (null != otherDeletions && 0 != otherDeletions.length) {
            try {
                tail = List.of(otherDeletions);
            } catch (final NullPointerException npe) {
                throw new CypherException("otherDeletions can't contain null elements", npe);
            }
        } else {
            tail = List.of();
        }
        registerClause(new DeleteClause(this, detached, deletion, tail));
    }

    <CLAUSE extends Clause> CLAUSE registerClause(final CLAUSE clause)
    {
        if (null == this.clauses) {
            this.clauses = new java.util.LinkedList<>();
        }
        this.clauses.add(clause);
        return clause;
    }

    @Override public WithClause WITH(final Expression item)
    {
        Objects.requireNonNull(item, "item is required");
        return registerClause(new WithClause(this, false, item));
    }

    @Override public WithClause WITH(final Expression firstItem, final Expression... otherItems)
    {
        Objects.requireNonNull(firstItem, "firstItem is required");
        return registerClause(new WithClause(this, false, firstItem, otherItems));
    }

    @Override public WithClause WITH_DISTINCT(final Expression item)
    {
        Objects.requireNonNull(item, "item is required");
        return registerClause(new WithClause(this, true, item));
    }

    @Override public WithClause WITH_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        Objects.requireNonNull(firstItem, "firstItem is required");
        return registerClause(new WithClause(this, true, firstItem, otherItems));
    }
}
