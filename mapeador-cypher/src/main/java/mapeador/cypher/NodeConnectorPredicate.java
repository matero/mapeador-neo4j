/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.HasTypes;
import mapeador.Labels;
import mapeador.Types;
import mapeador.cypher.RelationshipDetails.Hops;

import java.util.Objects;

public interface NodeConnectorPredicate<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>> extends Predicate, CypherElement
{
    /* unnamed undirected relationships between nodes ********************************************************************************************* */
    CONNECTOR relatedWith();

    CONNECTOR relatedWith(String name);

    CONNECTOR relatedWith(Labels labels);

    default CONNECTOR relatedWith(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(labels.getNodeLabels());
    }

    CONNECTOR relatedWith(PropertiesMap properties);

    CONNECTOR relatedWith(Parameter properties);

    CONNECTOR relatedWith(String name, Labels labels);

    default CONNECTOR relatedWith(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(name, labels.getNodeLabels());
    }

    CONNECTOR relatedWith(String name, PropertiesMap properties);

    CONNECTOR relatedWith(String name, Parameter properties);

    CONNECTOR relatedWith(Labels labels, PropertiesMap properties);

    CONNECTOR relatedWith(Labels labels, Parameter properties);

    default CONNECTOR relatedWith(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(labels.getNodeLabels(), properties);
    }

    default CONNECTOR relatedWith(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(labels.getNodeLabels(), properties);
    }

    CONNECTOR relatedWith(String name, Labels labels, PropertiesMap properties);

    CONNECTOR relatedWith(String name, Labels labels, Parameter properties);

    default CONNECTOR relatedWith(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR relatedWith(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return relatedWith(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR relatedWith(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return relatedWith(node.name(), node.getNodeLabels());
    }

    default CONNECTOR relatedWith(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return relatedWith(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR relatedWith(final NodeVariable node, final Parameter properties)
    {
        return relatedWith(node.name(), node.getNodeLabels(), properties);
    }

    /* unnamed incoming relationships between nodes *********************************************************************************************** */
    CONNECTOR incomingFrom();

    CONNECTOR incomingFrom(String name);

    CONNECTOR incomingFrom(Labels labels);

    default CONNECTOR incomingFrom(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(labels.getNodeLabels());
    }

    CONNECTOR incomingFrom(PropertiesMap properties);

    CONNECTOR incomingFrom(Parameter properties);

    CONNECTOR incomingFrom(String name, Labels labels);

    default CONNECTOR incomingFrom(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(name, labels.getNodeLabels());
    }

    CONNECTOR incomingFrom(String name, PropertiesMap properties);

    CONNECTOR incomingFrom(String name, Parameter properties);

    CONNECTOR incomingFrom(Labels labels, PropertiesMap properties);

    CONNECTOR incomingFrom(Labels labels, Parameter properties);

    default CONNECTOR incomingFrom(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(labels.getNodeLabels(), properties);
    }

    default CONNECTOR incomingFrom(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(labels.getNodeLabels(), properties);
    }

    CONNECTOR incomingFrom(String name, Labels labels, PropertiesMap properties);

    CONNECTOR incomingFrom(String name, Labels labels, Parameter properties);

    default CONNECTOR incomingFrom(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR incomingFrom(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return incomingFrom(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR incomingFrom(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return incomingFrom(node.name(), node.getNodeLabels());
    }

    default CONNECTOR incomingFrom(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return incomingFrom(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR incomingFrom(final NodeVariable node, final Parameter properties)
    {
        return incomingFrom(node.name(), node.getNodeLabels(), properties);
    }

    /* unnamed outgoing relationships between nodes *********************************************************************************************** */
    CONNECTOR outgoingTo();

    CONNECTOR outgoingTo(String name);

    CONNECTOR outgoingTo(Labels labels);

    default CONNECTOR outgoingTo(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(labels.getNodeLabels());
    }

    CONNECTOR outgoingTo(PropertiesMap properties);

    CONNECTOR outgoingTo(Parameter properties);

    CONNECTOR outgoingTo(String name, Labels labels);

    default CONNECTOR outgoingTo(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(name, labels.getNodeLabels());
    }

    CONNECTOR outgoingTo(String name, PropertiesMap properties);

    CONNECTOR outgoingTo(String name, Parameter properties);

    CONNECTOR outgoingTo(Labels labels, PropertiesMap properties);

    CONNECTOR outgoingTo(Labels labels, Parameter properties);

    default CONNECTOR outgoingTo(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(labels.getNodeLabels(), properties);
    }

    default CONNECTOR outgoingTo(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(labels.getNodeLabels(), properties);
    }

    CONNECTOR outgoingTo(String name, Labels labels, PropertiesMap properties);

    CONNECTOR outgoingTo(String name, Labels labels, Parameter properties);

    default CONNECTOR outgoingTo(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR outgoingTo(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return outgoingTo(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR outgoingTo(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return outgoingTo(node.name(), node.getNodeLabels());
    }

    default CONNECTOR outgoingTo(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return outgoingTo(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR outgoingTo(final NodeVariable node, final Parameter properties)
    {
        return outgoingTo(node.name(), node.getNodeLabels(), properties);
    }

    /* unnamed bi-directional relationships between nodes ***************************************************************************************** */
    CONNECTOR knowsEachOther();

    CONNECTOR knowsEachOther(String name);

    CONNECTOR knowsEachOther(Labels labels);

    default CONNECTOR knowsEachOther(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(labels.getNodeLabels());
    }

    CONNECTOR knowsEachOther(PropertiesMap properties);

    CONNECTOR knowsEachOther(Parameter properties);

    CONNECTOR knowsEachOther(String name, Labels labels);

    default CONNECTOR knowsEachOther(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(name, labels.getNodeLabels());
    }

    CONNECTOR knowsEachOther(String name, PropertiesMap properties);

    CONNECTOR knowsEachOther(String name, Parameter properties);

    CONNECTOR knowsEachOther(Labels labels, PropertiesMap properties);

    CONNECTOR knowsEachOther(Labels labels, Parameter properties);

    default CONNECTOR knowsEachOther(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(labels.getNodeLabels(), properties);
    }

    default CONNECTOR knowsEachOther(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(labels.getNodeLabels(), properties);
    }

    CONNECTOR knowsEachOther(String name, Labels labels, PropertiesMap properties);

    CONNECTOR knowsEachOther(String name, Labels labels, Parameter properties);

    default CONNECTOR knowsEachOther(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR knowsEachOther(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return knowsEachOther(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR knowsEachOther(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return knowsEachOther(node.name(), node.getNodeLabels());
    }

    default CONNECTOR knowsEachOther(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return knowsEachOther(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR knowsEachOther(final NodeVariable node, final Parameter properties)
    {
        return knowsEachOther(node.name(), node.getNodeLabels(), properties);
    }

    /* explicit undirected connections between nodes ********************************************************************************************** */
    UndirectedRelation<CONNECTOR> related();

    UndirectedRelation<CONNECTOR> related(String name);

    UndirectedRelation<CONNECTOR> related(Types types);

    default UndirectedRelation<CONNECTOR> related(final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes());
    }

    UndirectedRelation<CONNECTOR> related(Hops hops);

    UndirectedRelation<CONNECTOR> related(PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(Parameter properties);

    UndirectedRelation<CONNECTOR> related(String name, Types types);

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes());
    }

    UndirectedRelation<CONNECTOR> related(String name, Hops hops);

    UndirectedRelation<CONNECTOR> related(String name, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(String name, Parameter properties);

    UndirectedRelation<CONNECTOR> related(String name, Types types, Hops hops);

    UndirectedRelation<CONNECTOR> related(String name, Types types, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(String name, Types types, Parameter properties);

    UndirectedRelation<CONNECTOR> related(String name, Types types, Hops hops, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(String name, Types types, Hops hops, Parameter properties);

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes(), hops);
    }

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes(), hops, properties);
    }

    default UndirectedRelation<CONNECTOR> related(final String name, final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(name, types.getRelationTypes(), hops, properties);
    }

    UndirectedRelation<CONNECTOR> related(Types types, Hops hops);

    UndirectedRelation<CONNECTOR> related(Types types, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(Types types, Parameter properties);

    UndirectedRelation<CONNECTOR> related(Types types, Hops hops, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(Types types, Hops hops, Parameter properties);

    default UndirectedRelation<CONNECTOR> related(final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes(), hops);
    }

    default UndirectedRelation<CONNECTOR> related(final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes(), hops, properties);
    }

    default UndirectedRelation<CONNECTOR> related(final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return related(types.getRelationTypes(), hops, properties);
    }

    UndirectedRelation<CONNECTOR> related(Hops hops, PropertiesMap properties);

    UndirectedRelation<CONNECTOR> related(Hops hops, Parameter properties);

    UndirectedRelation<CONNECTOR> related(String name, Hops hops, PropertiesMap property);

    UndirectedRelation<CONNECTOR> related(String name, Hops hops, Parameter properties);

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes());
    }

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship, final Hops hops)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes(), hops);
    }

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    default UndirectedRelation<CONNECTOR> related(final RelationshipVariable relationship, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return related(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    /* explicit Incoming connections between nodes ********************************************************************************************** */
    IncomingRelation<CONNECTOR> incoming();

    IncomingRelation<CONNECTOR> incoming(String name);

    IncomingRelation<CONNECTOR> incoming(Types types);

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes());
    }

    IncomingRelation<CONNECTOR> incoming(Hops hops);

    IncomingRelation<CONNECTOR> incoming(PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(Parameter properties);

    IncomingRelation<CONNECTOR> incoming(String name, Types types);

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes());
    }

    IncomingRelation<CONNECTOR> incoming(String name, Hops hops);

    IncomingRelation<CONNECTOR> incoming(String name, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(String name, Parameter properties);

    IncomingRelation<CONNECTOR> incoming(String name, Types types, Hops hops);

    IncomingRelation<CONNECTOR> incoming(String name, Types types, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(String name, Types types, Parameter properties);

    IncomingRelation<CONNECTOR> incoming(String name, Types types, Hops hops, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(String name, Types types, Hops hops, Parameter properties);

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes(), hops);
    }

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes(), hops, properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final String name, final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(name, types.getRelationTypes(), hops, properties);
    }

    IncomingRelation<CONNECTOR> incoming(Types types, Hops hops);

    IncomingRelation<CONNECTOR> incoming(Types types, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(Types types, Parameter properties);

    IncomingRelation<CONNECTOR> incoming(Types types, Hops hops, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(Types types, Hops hops, Parameter properties);

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes(), hops);
    }

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes(), hops, properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return incoming(types.getRelationTypes(), hops, properties);
    }

    IncomingRelation<CONNECTOR> incoming(Hops hops, PropertiesMap properties);

    IncomingRelation<CONNECTOR> incoming(Hops hops, Parameter properties);

    IncomingRelation<CONNECTOR> incoming(String name, Hops hops, PropertiesMap property);

    IncomingRelation<CONNECTOR> incoming(String name, Hops hops, Parameter properties);

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes());
    }

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship, final Hops hops)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes(), hops);
    }

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    default IncomingRelation<CONNECTOR> incoming(final RelationshipVariable relationship, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return incoming(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    /* explicit Outgoing connections between nodes ************************************************************************************************ */
    OutgoingRelation<CONNECTOR> outgoing();

    OutgoingRelation<CONNECTOR> outgoing(String name);

    OutgoingRelation<CONNECTOR> outgoing(Types types);

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes());
    }

    OutgoingRelation<CONNECTOR> outgoing(Hops hops);

    OutgoingRelation<CONNECTOR> outgoing(PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(Parameter properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types);

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes());
    }

    OutgoingRelation<CONNECTOR> outgoing(String name, Hops hops);

    OutgoingRelation<CONNECTOR> outgoing(String name, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Parameter properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types, Hops hops);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types, Parameter properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types, Hops hops, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Types types, Hops hops, Parameter properties);

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes(), hops);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes(), hops, properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final String name, final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(name, types.getRelationTypes(), hops, properties);
    }

    OutgoingRelation<CONNECTOR> outgoing(Types types, Hops hops);

    OutgoingRelation<CONNECTOR> outgoing(Types types, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(Types types, Parameter properties);

    OutgoingRelation<CONNECTOR> outgoing(Types types, Hops hops, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(Types types, Hops hops, Parameter properties);

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes(), hops);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes(), hops, properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return outgoing(types.getRelationTypes(), hops, properties);
    }

    OutgoingRelation<CONNECTOR> outgoing(Hops hops, PropertiesMap properties);

    OutgoingRelation<CONNECTOR> outgoing(Hops hops, Parameter properties);

    OutgoingRelation<CONNECTOR> outgoing(String name, Hops hops, PropertiesMap property);

    OutgoingRelation<CONNECTOR> outgoing(String name, Hops hops, Parameter properties);

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes());
    }

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship, final Hops hops)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes(), hops);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    default OutgoingRelation<CONNECTOR> outgoing(final RelationshipVariable relationship, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return outgoing(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    /* explicit in-out connections between nodes ********************************************************************************************** */
    InOutRelation<CONNECTOR> in();

    InOutRelation<CONNECTOR> in(String name);

    InOutRelation<CONNECTOR> in(Types types);

    default InOutRelation<CONNECTOR> in(final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes());
    }

    InOutRelation<CONNECTOR> in(Hops hops);

    InOutRelation<CONNECTOR> in(PropertiesMap properties);

    InOutRelation<CONNECTOR> in(Parameter properties);

    InOutRelation<CONNECTOR> in(String name, Types types);

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes());
    }

    InOutRelation<CONNECTOR> in(String name, Hops hops);

    InOutRelation<CONNECTOR> in(String name, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(String name, Parameter properties);

    InOutRelation<CONNECTOR> in(String name, Types types, Hops hops);

    InOutRelation<CONNECTOR> in(String name, Types types, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(String name, Types types, Parameter properties);

    InOutRelation<CONNECTOR> in(String name, Types types, Hops hops, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(String name, Types types, Hops hops, Parameter properties);

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes(), hops);
    }

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes(), hops, properties);
    }

    default InOutRelation<CONNECTOR> in(final String name, final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(name, types.getRelationTypes(), hops, properties);
    }

    InOutRelation<CONNECTOR> in(Types types, Hops hops);

    InOutRelation<CONNECTOR> in(Types types, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(Types types, Parameter properties);

    InOutRelation<CONNECTOR> in(Types types, Hops hops, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(Types types, Hops hops, Parameter properties);

    default InOutRelation<CONNECTOR> in(final HasTypes types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes(), hops);
    }

    default InOutRelation<CONNECTOR> in(final HasTypes types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final HasTypes types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final HasTypes types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes(), hops, properties);
    }

    default InOutRelation<CONNECTOR> in(final HasTypes types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        return in(types.getRelationTypes(), hops, properties);
    }

    InOutRelation<CONNECTOR> in(Hops hops, PropertiesMap properties);

    InOutRelation<CONNECTOR> in(Hops hops, Parameter properties);

    InOutRelation<CONNECTOR> in(String name, Hops hops, PropertiesMap property);

    InOutRelation<CONNECTOR> in(String name, Hops hops, Parameter properties);

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes());
    }

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship, final Hops hops)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes(), hops);
    }

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes(), properties);
    }

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    default InOutRelation<CONNECTOR> in(final RelationshipVariable relationship, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(relationship, "relationship is required");
        return in(relationship.name(), relationship.getRelationTypes(), hops, properties);
    }

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // comma separated patterns IMPL                                                                                                                //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    CONNECTOR and();

    CONNECTOR and(String name);

    CONNECTOR and(Labels labels);

    default CONNECTOR and(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(labels.getNodeLabels());
    }

    CONNECTOR and(PropertiesMap properties);

    CONNECTOR and(Parameter properties);

    CONNECTOR and(String name, Labels labels);

    default CONNECTOR and(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(name, labels.getNodeLabels());
    }

    CONNECTOR and(String name, PropertiesMap properties);

    CONNECTOR and(String name, Parameter properties);

    CONNECTOR and(Labels labels, PropertiesMap properties);

    CONNECTOR and(Labels labels, Parameter properties);

    default CONNECTOR and(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(labels.getNodeLabels(), properties);
    }

    default CONNECTOR and(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(labels.getNodeLabels(), properties);
    }

    CONNECTOR and(String name, Labels labels, PropertiesMap properties);

    CONNECTOR and(String name, Labels labels, Parameter properties);

    default CONNECTOR and(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR and(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return and(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR and(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return and(node.name(), node.getNodeLabels());
    }

    default CONNECTOR and(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return and(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR and(final NodeVariable node, final Parameter properties)
    {
        Objects.requireNonNull(node, "node is required");
        return and(node.name(), node.getNodeLabels(), properties);
    }

    PathVariable<CONNECTOR> andPath(String name);

    default PathVariable<CONNECTOR> andPath(final String name, final PathFunction function)
    {
        return andPath(name, Objects.requireNonNull(function, "funciton is required").name());
    }

    PathVariable<CONNECTOR> andPath(String name, String function);

    default PathVariable<CONNECTOR> andShortestPath(final String name) {return andPath(name, PathFunction.shortestPath.name());}

    default PathVariable<CONNECTOR> andAllShortestPaths(final String name) {return andPath(name, PathFunction.allShortestPaths.name());}
}
