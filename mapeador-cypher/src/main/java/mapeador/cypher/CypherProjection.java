/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class CypherProjection implements Projection
{
    final RegularQuery parent;
    private final boolean distinct;

    private final Expression itemsHead;
    private final List<Expression> itemsTail;

    private Expression sortingHead;
    private List<Expression> sortingTail = List.of();

    private Expression limit;
    private Expression skip;

    CypherProjection(final RegularQuery parent, final boolean distinct, final Expression itemsHead)
    {
        this.parent = Objects.requireNonNull(parent, "parent is required");
        this.distinct = distinct;
        this.itemsHead = Objects.requireNonNull(itemsHead, "itemsHead is required");
        this.itemsTail = List.of();
    }

    CypherProjection(final RegularQuery parent, final boolean distinct, final Expression itemsHead, final Expression... itemsTail)
    {
        this.parent = Objects.requireNonNull(parent, "parent is required");
        this.distinct = distinct;
        this.itemsHead = Objects.requireNonNull(itemsHead, "itemsHead is required");
        try {
            this.itemsTail = List.of(itemsTail);
        } catch (final NullPointerException e) {
            throw new CypherException("projection items cant be null", e);
        }
    }

    CypherProjection(final RegularQuery parent, final boolean distinct, final Expression itemsHead, final List<Expression> itemsTail)
    {
        this.parent = Objects.requireNonNull(parent, "parent is required");
        this.distinct = distinct;
        this.itemsHead = Objects.requireNonNull(itemsHead, "itemsHead is required");
        try {
            this.itemsTail = List.copyOf(itemsTail);
        } catch (final NullPointerException e) {
            throw new CypherException("projection items cant be null", e);
        }
    }

    CypherProjection(final RegularQuery parent, final boolean distinct, final List<Expression> items)
    {
        this.parent = Objects.requireNonNull(parent, "parent is required");
        this.distinct = distinct;
        if (items.isEmpty()) {
            throw new CypherException("at least one item entries must be defined");
        }

        if (null == items.get(0)) {
            throw new CypherException("projection items cant be null");
        }
        this.itemsHead = items.get(0);

        if (1 == items.size()) {
            this.itemsTail = List.of();
        } else {
            try {
                this.itemsTail = List.copyOf(items.subList(1, items.size()));
            } catch (final NullPointerException e) {
                throw new CypherException("projection items cant be null", e);
            }
        }
    }

    public final Expression itemsHead() {return this.itemsHead;}

    public final List<Expression> itemsTail() {return this.itemsTail;}

    public Expression sortingHead() {return this.sortingHead;}

    public final List<Expression> sortingTail() {return this.sortingTail;}

    public boolean distinct() {return this.distinct;}

    public Expression limit() {return this.limit;}

    public Expression skip() {return this.skip;}

    public boolean isSorted() {return null != this.sortingHead;}

    public boolean hasSkip() {return null != this.skip;}

    public boolean hasLimit() {return null != this.limit;}

    public boolean doesntHaveItems() {return null != this.itemsHead;}

    @Override public ProjectionWithOrderBy ORDER_BY(final SortPredicate item)
    {
        Objects.requireNonNull(item, "sortPredicate is required");
        if (null != this.sortingHead) {
            throw new CypherException("sort predicates already defined");
        }
        this.sortingHead = item;
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final SortPredicate first, final SortPredicate... others)
    {
        Objects.requireNonNull(first, "first is required");
        if (null != this.sortingHead) {
            throw new CypherException("sort predicates already defined");
        }
        if (null != others && 0 < others.length) {
            try {
                this.sortingTail = List.of(others);
            } catch (final NullPointerException e) {
                throw new CypherException("ORDER BY predicates cant be null", e);
            }
        }
        this.sortingHead = first;
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final Object item)
    {
        Objects.requireNonNull(item, "sortPredicate is required");
        if (null != this.sortingHead) {
            throw new CypherException("sort predicates already defined");
        }
        this.sortingHead = toSortingItem(item);
        return this;
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final Object first, final Object... others)
    {
        Objects.requireNonNull(first, "first is required");
        if (null != this.sortingHead) {
            throw new CypherException("sort predicates already defined");
        }
        this.sortingHead = toSortingItem(first);
        if (null != others && 0 < others.length) {
            try {
                this.sortingTail = Arrays.stream(others).map(this::toSortingItem).collect(Collectors.toUnmodifiableList());
            } catch (final NullPointerException e) {
                throw new CypherException("ORDER BY predicates cant be null", e);
            }
        }
        return this;
    }

    private Expression toSortingItem(final Object item)
    {
        if (item instanceof SortPredicate) {
            return (SortPredicate) item;
        } else if (item instanceof String) {
            return AliasName.of((String) item);
        }
        throw new CypherException("ORDER BY predicates cn be instances of SortPredicate or String (referencing an Alias in the context)");
    }

    @Override public ProjectionWithSkip SKIP(final Expression skip)
    {
        Objects.requireNonNull(skip, "e is required");
        this.skip = skip;
        return this;
    }

    @Override public RegularQuery LIMIT(final Expression limit)
    {
        Objects.requireNonNull(limit, "e is required");
        this.limit = limit;
        return this.parent;
    }

    @Override public Statement.UnionSpec UNION() {return this.parent.UNION();}

    @Override public Statement.UnionSpec UNION_ALL() {return this.parent.UNION_ALL();}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
}
