/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.Labels;

import java.util.Objects;

public final class MatchClause extends ReadClause implements PathVariable<NodeConnector.WithWhere>, HasWhere
{
    private final boolean optional;
    private final Pattern<NodeConnector.WithWhere> pattern;

    private Predicate where;

    MatchClause(final RegularQuery parent, final boolean optional)
    {
        super(parent);
        this.optional = optional;
        this.pattern = new AnonymousPatternWithWhere(this);
    }

    MatchClause(final RegularQuery parent, final boolean optional, final String pathName)
    {
        super(parent);
        this.optional = optional;
        this.pattern = new PathPatternWithWhere(this, pathName);
    }

    MatchClause(final RegularQuery parent, final boolean optional, final String pathName, final String function)
    {
        super(parent);
        this.optional = optional;
        this.pattern = new PathPatternWithWhere(this, pathName, function);
    }

    public boolean optional() {return this.optional;}

    public Pattern<NodeConnector.WithWhere> pattern() {return this.pattern;}

    public Predicate where() {return this.where;}

    @Override public NodeConnector.WithWhere node() {return this.pattern.node();}

    @Override public NodeConnector.WithWhere node(final String variable) {return this.pattern.node(variable);}

    @Override public NodeConnector.WithWhere node(final Labels labels) {return this.pattern.node(labels);}

    @Override public NodeConnector.WithWhere node(final PropertiesMap properties) {return this.pattern.node(properties);}

    @Override public NodeConnector.WithWhere node(final Parameter properties) {return this.pattern.node(properties);}

    @Override public NodeConnector.WithWhere node(final String name, final Labels labels) {return this.pattern.node(name, labels);}

    @Override public NodeConnector.WithWhere node(final String name, final PropertiesMap properties) {return this.pattern.node(name, properties);}

    @Override public NodeConnector.WithWhere node(final String name, final Parameter properties) {return this.pattern.node(name, properties);}

    @Override public NodeConnector.WithWhere node(final Labels labels, final PropertiesMap properties)
    {
        return this.pattern.node(labels, properties);
    }

    @Override public NodeConnector.WithWhere node(final Labels labels, final Parameter properties) {return this.pattern.node(labels, properties);}


    @Override public NodeConnector.WithWhere node(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.pattern.node(name, labels, properties);
    }

    @Override public NodeConnector.WithWhere node(final String name, final Labels labels, final Parameter properties)
    {
        return this.pattern.node(name, labels, properties);
    }

    @Override public MatchClause WHERE(final Predicate predicate)
    {
        Objects.requireNonNull(predicate, "predicate is required");
        this.where = predicate;
        return this;
    }

    public boolean hasWhere() {return null != this.where;}

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
}
