/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Name;
import mapeador.Types;

import java.util.Objects;

public interface RelationshipDetails extends VariableDetails, CypherElement
{
    enum Empty implements RelationshipDetails {
        INSTANCE;

        @Override public Types types() {return Types.empty();}

        @Override public Hops hops() {return OneHop.INSTANCE;}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }
    interface Hops extends CypherElement {}

    enum AnyHops implements Hops
    {
        INSTANCE;

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    enum OneHop implements Hops
    {
        INSTANCE;

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    class FixedHops implements Hops
    {
        private final int value;

        FixedHops(final int value)
        {
            if (0 > value) {
                throw new CypherException("value must be >= 0");
            }
            this.value = value;
        }

        public final int value() {return this.value;}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    final class MinHops extends FixedHops
    {

        MinHops(final int value) {super(value);}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    final class MaxHops extends FixedHops
    {

        MaxHops(final int value) {super(value);}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    final class RangeHops implements Hops
    {
        private final int min;
        private final int max;

        RangeHops(final int min, final int max)
        {
            if (0 > min) {
                throw new CypherException("min must be >= 0");
            }
            if (0 > max) {
                throw new CypherException("max must be > 0");
            }
            if (min >= max) {
                throw new CypherException("max must be > min");
            }
            this.min = min;
            this.max = max;
        }

        public int min() {return this.min;}

        public int max() {return this.max;}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }

    Types types();

    default boolean hasTypes() {return types().isNotEmpty();}

    Hops hops();

    static RelationshipDetails of() {return Empty.INSTANCE;}

    static RelationshipDetails of(final String variable) {return new RelationshipWithoutProperties(Name.at(variable), Types.empty());}

    static RelationshipDetails of(final Types types)
    {
        Objects.requireNonNull(types, "types is required");
        if (types.isEmpty()) {
            return Empty.INSTANCE;
        } else {
            return new RelationshipWithoutProperties(null, types);
        }
    }

    static RelationshipDetails of(final Hops hops)
    {
        Objects.requireNonNull(hops, "hols is required");
        if (OneHop.INSTANCE == hops) {
            return Empty.INSTANCE;
        } else {
            return new RelationshipWithoutProperties(null, Types.empty(), hops);
        }
    }

    static RelationshipDetails of(final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return Empty.INSTANCE;
        }
        return new RelationshipWithProperties(null, Types.empty(), OneHop.INSTANCE, properties);
    }

    static RelationshipDetails of(final Parameter properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(null, Types.empty(), OneHop.INSTANCE, properties);
    }

    static RelationshipDetails of(final String variable, final Types types)
    {
        Objects.requireNonNull(types, "types is required");
        return new RelationshipWithoutProperties(Name.at(variable), types);
    }

    static RelationshipDetails of(final String variable, final Hops hops)
    {
        Objects.requireNonNull(hops, "hops is required");
        return new RelationshipWithoutProperties(Name.at(variable), Types.empty(), hops);
    }

    static RelationshipDetails of(final String variable, final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(variable);
        } else {
            return new RelationshipWithProperties(Name.at(variable), Types.empty(), OneHop.INSTANCE, properties);
        }
    }

    static RelationshipDetails of(final String variable, final Parameter properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(Name.at(variable), Types.empty(), OneHop.INSTANCE, properties);
    }

    static RelationshipDetails of(final String variable, final Types types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        return new RelationshipWithoutProperties(Name.at(variable), types, hops);
    }

    static RelationshipDetails of(final String variable, final Types types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(variable, types);
        } else {
            return new RelationshipWithProperties(Name.at(variable), types, OneHop.INSTANCE, properties);
        }
    }

    static RelationshipDetails of(final String variable, final Types types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(Name.at(variable), types, OneHop.INSTANCE, properties);
    }

    static RelationshipDetails of(final String variable, final Types types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(variable, types, hops);
        } else {
            return new RelationshipWithProperties(Name.at(variable), types, hops, properties);
        }
    }

    static RelationshipDetails of(final String variable, final Types types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(Name.at(variable), types, hops, properties);
    }

    static RelationshipDetails of(final Types types, final Hops hops)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        return new RelationshipWithoutProperties(null, types, hops);
    }

    static RelationshipDetails of(final Types types, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(types);
        } else {
            return new RelationshipWithProperties(null, types, OneHop.INSTANCE, properties);
        }
    }

    static RelationshipDetails of(final Types types, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(null, types, OneHop.INSTANCE, properties);
    }

    static RelationshipDetails of(final Types types, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(types, hops);
        } else {
            return new RelationshipWithProperties(null, types, hops, properties);
        }
    }

    static RelationshipDetails of(final Types types, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(types, "types is required");
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(null, types, hops, properties);
    }

    static RelationshipDetails of(final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(hops);
        } else {
            return new RelationshipWithProperties(null, Types.empty(), hops, properties);
        }
    }

    static RelationshipDetails of(final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(null, Types.empty(), hops, properties);
    }

    static RelationshipDetails of(final String variable, final Hops hops, final PropertiesMap properties)
    {
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return of(variable, hops);
        } else {
            return new RelationshipWithProperties(Name.at(variable), Types.empty(), hops, properties);
        }
    }

    static RelationshipDetails of(final String variable, final Hops hops, final Parameter properties)
    {
        Objects.requireNonNull(hops, "hops is required");
        Objects.requireNonNull(properties, "properties is required");
        return new RelationshipWithProperties(Name.at(variable), Types.empty(), hops, properties);
    }
}
