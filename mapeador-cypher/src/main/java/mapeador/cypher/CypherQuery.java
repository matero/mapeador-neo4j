/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import org.neo4j.driver.Result;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.internal.value.NodeValue;
import org.neo4j.driver.internal.value.PathValue;
import org.neo4j.driver.internal.value.RelationshipValue;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Path;
import org.neo4j.driver.types.Relationship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public final class CypherQuery
{
    private final String query;
    private final List<String> userRequiredParameters;
    private final Map<String, Object> internalParameters;

    CypherQuery(final String query, final List<String> userRequiredParameters, final Map<String, Object> internalParameters)
    {
        this.query = query;
        this.internalParameters = internalParameters;
        this.userRequiredParameters = userRequiredParameters;
    }

    public String getQuery() {return this.query;}

    public Map<String, Object> getInternalParameters() {return this.internalParameters;}

    public Result run(final Transaction tx)
    {
        return tx.run(this.query, this.internalParameters);
    }

    public Result run(final Transaction tx, final String p, final Object v)
    {
        validateAllRequiredParametersAreProvided(p);
        assertParameter(v);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p, v);
        return tx.run(this.query, params);
    }

    public Result run(final Transaction tx, final String p1, final Object v1, final String p2, final Object v2)
    {
        validateAllRequiredParametersAreProvided(p1, p2);
        assertParameters(v1, v2);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        return tx.run(this.query, params);
    }

    public Result run(final Transaction tx, final String p1, final Object v1, final String p2, final Object v2, final String p3, final Object v3)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3);
        assertParameters(v1, v2, v3);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4);
        assertParameters(v1, v2, v3, v4);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4,
        final String p5, final Object v5)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4, p5);
        assertParameters(v1, v2, v3, v4, v5);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        params.put(p5, v5);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4,
        final String p5, final Object v5,
        final String p6, final Object v6)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4, p5, p6);
        assertParameters(v1, v2, v3, v4, v5, v6);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        params.put(p5, v5);
        params.put(p6, v6);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4,
        final String p5, final Object v5,
        final String p6, final Object v6,
        final String p7, final Object v7)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4, p5, p6, p7);
        assertParameters(v1, v2, v3, v4, v5, v6, v7);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        params.put(p5, v5);
        params.put(p6, v6);
        params.put(p7, v7);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4,
        final String p5, final Object v5,
        final String p6, final Object v6,
        final String p7, final Object v7,
        final String p8, final Object v8)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4, p5, p6, p7, p8);
        assertParameters(v1, v2, v3, v4, v5, v6, v7, v8);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        params.put(p5, v5);
        params.put(p6, v6);
        params.put(p7, v7);
        params.put(p8, v8);
        return tx.run(this.query, params);
    }

    public Result run(
        final Transaction tx,
        final String p1, final Object v1,
        final String p2, final Object v2,
        final String p3, final Object v3,
        final String p4, final Object v4,
        final String p5, final Object v5,
        final String p6, final Object v6,
        final String p7, final Object v7,
        final String p8, final Object v8,
        final String p9, final Object v9)
    {
        validateAllRequiredParametersAreProvided(p1, p2, p3, p4, p5, p6, p7, p8, p9);
        assertParameters(v1, v2, v3, v4, v5, v6, v7, v8, v9);
        final var params = new HashMap<>(this.internalParameters);
        params.put(p1, v1);
        params.put(p2, v2);
        params.put(p3, v3);
        params.put(p4, v4);
        params.put(p5, v5);
        params.put(p6, v6);
        params.put(p7, v7);
        params.put(p8, v8);
        params.put(p9, v9);
        return tx.run(this.query, params);
    }

    public Result run(final Transaction tx, final Map<String, Object> userParameters)
    {
        validateAllRequiredParametersAreProvided(userParameters.keySet());
        assertParameters(userParameters.values());
        final var params = new HashMap<>(this.internalParameters);
        params.putAll(userParameters);
        return tx.run(this.query, params);
    }

    private void validateAllRequiredParametersAreProvided(final String... userParameters)
    {
        validateAllRequiredParametersAreProvided(Arrays.asList(userParameters));
    }

    private void validateAllRequiredParametersAreProvided(final Collection<String> userParameters)
    {
        if (this.userRequiredParameters.isEmpty()) {
            return;
        }

        final var params = new ArrayList<>(this.userRequiredParameters);
        params.removeAll(userParameters);
        if (params.isEmpty()) {
            return;
        }
        if (1 == params.size()) {
            throw new IllegalArgumentException("parameter '" + params.get(0) + "' is required.");
        } else {
            throw new IllegalArgumentException("parameters " + params + "' are required.");
        }
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof CypherQuery) {
            final CypherQuery other = (CypherQuery) o;
            return this.query.equals(other.query) && this.internalParameters.equals(other.internalParameters);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.query.hashCode();
        result = 31 * result + this.internalParameters.hashCode();
        return result;
    }

    @Override public String toString() {return "CypherQuery(query='" + this.query + "', parameters=" + this.internalParameters + ')';}

    public static CypherQuery of(final CharSequence query)
    {
        return new CypherQuery(cypherAt(query), List.of(), Map.of());
    }

    public static CypherQuery of(final CharSequence query, final Set<String> userRequiredParameters)
    {
        Objects.requireNonNull(userRequiredParameters, "userRequiredParameters is required");
        if (userRequiredParameters.isEmpty()) {
            return of(query);
        }
        return new CypherQuery(cypherAt(query), List.copyOf(userRequiredParameters), Map.of());
    }

    public static CypherQuery of(final CharSequence query, final Set<String> userRequiredParameters, final Map<String, Object> internalParameters)
    {
        Objects.requireNonNull(internalParameters, "internalParameters is required");
        if (internalParameters.isEmpty()) {
            return of(query, userRequiredParameters);
        }
        return new CypherQuery(cypherAt(query), List.copyOf(userRequiredParameters), Map.copyOf(assertParameters(internalParameters)));
    }

    static String cypherAt(final CharSequence query)
    {
        Objects.requireNonNull(query, "query is required");
        final var q = query.toString();
        if (q.isEmpty()) {
            throw new IllegalArgumentException("query can not be empty");
        }
        if (q.isBlank()) {
            throw new IllegalArgumentException("query can not be blank");
        }
        return q.trim();
    }

    private static Map<String, Object> assertParameters(final Map<String, Object> parameters)
    {
        assertParameters(parameters.values());
        return parameters;
    }

    static void assertParameters(final Iterable<Object> parameters)
    {
        for (final var p : parameters) {
            assertParameter(p);
        }
    }

    static void assertParameters(final Object... parameters)
    {
        for (final var p : parameters) {
            assertParameter(p);
        }
    }

    static void assertParameter(final Object value)
    {
        if (value instanceof PathPattern) {
            throw new IllegalArgumentException("Patterns can't be used as parameters.");
        }
        if (value instanceof Expression) {
            throw new IllegalArgumentException("Expressions can't be used as parameters.");
        }
        if (value instanceof Node || value instanceof NodeValue) {
            throw new IllegalArgumentException("Nodes can't be used as parameters.");
        }
        if (value instanceof Relationship || value instanceof RelationshipValue) {
            throw new IllegalArgumentException("Relationships can't be used as parameters.");
        }
        if (value instanceof Path || value instanceof PathValue) {
            throw new IllegalArgumentException("Paths can't be used as parameters.");
        }
    }
}
