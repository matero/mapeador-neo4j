/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

public interface CypherVisitor extends Visitor
{
    void visit(Statement statement);

    void visit(Union union);

    void visit(FunctionCall call);

    void visit(RegularQuery query);

    void visit(CypherProjection projection);

    // reading clauses
    void visit(WithClause clause);

    void visit(MatchClause clause);

    void visit(UnwindClause clause);

    void visit(InQueryCallClause clause);

    // updating clauses
    void visit(CreateClause clause);

    void visit(MergeClause clause);

    void visit(DeleteClause clause);

    void visit(SetClause clause);

    void visit(RemoveClause clause);

    void visit(Pattern<?> pattern);

    void visit(NamedPattern<?> pattern);

    // node details
    void visit(NodeWithoutProperties node);

    void visit(NodeWithProperties node);

    // relationship details
    void visit(RelationshipDetails.Empty relationship);

    void visit(RelationshipWithoutProperties relationship);

    void visit(RelationshipWithProperties relationship);

    // node connections
    void visit(UndetailedConnection connection);

    void visit(DetailedConnection connection);

    void visit(RelationshipDetails.OneHop hops);

    void visit(RelationshipDetails.AnyHops hops);

    void visit(RelationshipDetails.FixedHops hops);

    void visit(RelationshipDetails.MinHops hops);

    void visit(RelationshipDetails.MaxHops hops);

    void visit(RelationshipDetails.RangeHops hops);

    void visit(MergeClause.Item item);
}
