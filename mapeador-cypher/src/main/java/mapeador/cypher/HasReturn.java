/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;

import java.util.Objects;

public interface HasReturn extends CypherElement
{
    Projection RETURN(Expression item);

    Projection RETURN(Expression firstItem, Expression... otherItems);

    Projection RETURN_DISTINCT(Expression item);

    Projection RETURN_DISTINCT(Expression firstItem, Expression... otherItems);

    default Projection RETURN(final Object item) {return RETURN(get.projection.itemAt(item));}

    default Projection RETURN(final Object firstItem, final Object... otherItems)
    {
        final var head = get.projection.itemAt(firstItem);
        if (null == otherItems || 0 == otherItems.length) {
            return RETURN(head);
        }
        return RETURN(head, get.projection.tailAt(otherItems));
    }

    default Projection RETURN_DISTINCT(final Object item) {return RETURN_DISTINCT(get.projection.itemAt(item));}

    default Projection RETURN_DISTINCT(final Object firstItem, final Object... otherItems)
    {
        final var head = get.projection.itemAt(firstItem);
        if (null == otherItems || 0 == otherItems.length) {
            return RETURN_DISTINCT(head);
        }
        return RETURN_DISTINCT(head, get.projection.tailAt(otherItems));
    }


    interface AfterUpdateClauseWITH extends HasReturn
    {
        // ============================================================================================================================================ //
        // U P D A T I N G   C L A U S E S                                                                                                              //
        // ============================================================================================================================================ //
        NodeConnector.WithReturnAfterUpdateClause CREATE();

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name);

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name, PropertiesMap properties);

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name, Parameter properties);

        NodeConnector.WithReturnAfterUpdateClause CREATE(Labels labels);

        NodeConnector.WithReturnAfterUpdateClause CREATE(Labels labels, PropertiesMap properties);

        NodeConnector.WithReturnAfterUpdateClause CREATE(Labels labels, Parameter properties);

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name, Labels labels);

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name, Labels labels, PropertiesMap properties);

        NodeConnector.WithReturnAfterUpdateClause CREATE(String name, Labels labels, Parameter properties);

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(labels.getNodeLabels());
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(name, labels.getNodeLabels());
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return CREATE(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node)
        {
            Objects.requireNonNull(node, "node is required");
            return CREATE(node.name(), node.getNodeLabels());
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final PropertiesMap properties)
        {
            Objects.requireNonNull(node, "node is required");
            return CREATE(node.name(), node.getNodeLabels(), properties);
        }

        default NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final Parameter properties)
        {
            Objects.requireNonNull(node, "node is required");
            return CREATE(node.name(), node.getNodeLabels(), properties);
        }

        PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(String pathName);

        NodeConnector.WithMergeActions MERGE();

        NodeConnector.WithMergeActions MERGE(String name);

        NodeConnector.WithMergeActions MERGE(String name, PropertiesMap properties);

        NodeConnector.WithMergeActions MERGE(String name, Parameter properties);

        NodeConnector.WithMergeActions MERGE(Labels labels);

        NodeConnector.WithMergeActions MERGE(Labels labels, PropertiesMap properties);

        NodeConnector.WithMergeActions MERGE(Labels labels, Parameter properties);

        NodeConnector.WithMergeActions MERGE(String name, Labels labels);

        NodeConnector.WithMergeActions MERGE(String name, Labels labels, PropertiesMap properties);

        NodeConnector.WithMergeActions MERGE(String name, Labels labels, Parameter properties);

        default NodeConnector.WithMergeActions MERGE(final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(labels.getNodeLabels());
        }

        default NodeConnector.WithMergeActions MERGE(final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithMergeActions MERGE(final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(name, labels.getNodeLabels());
        }

        default NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MERGE(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithMergeActions MERGE(final NodeVariable node)
        {
            Objects.requireNonNull(node, "node is required");
            return MERGE(node.name(), node.getNodeLabels());
        }

        default NodeConnector.WithMergeActions MERGE(final NodeVariable node, final PropertiesMap properties)
        {
            Objects.requireNonNull(node, "node is required");
            return MERGE(node.name(), node.getNodeLabels(), properties);
        }

        default NodeConnector.WithMergeActions MERGE(final NodeVariable node, final Parameter properties)
        {
            Objects.requireNonNull(node, "node is required");
            return MERGE(node.name(), node.getNodeLabels(), properties);
        }

        AfterUpdateClause DELETE(Expression deletion, Expression... otherDeletions);

        AfterUpdateClause DETACH_DELETE(Expression deletion, Expression... otherDeletions);

        AfterUpdateClause SET(SetItem firstItem, SetItem... otherItems);

        AfterUpdateClause REMOVE(RemoveItem firstItem, RemoveItem... otherItems);
    }

    interface AfterReadClauseWITH extends  AfterUpdateClauseWITH
    {
        // ============================================================================================================================================ //
        // R E A D I N G   C L A U S E S                                                                                                                //
        // ============================================================================================================================================ //
        NodeConnector.WithWhere MATCH();

        NodeConnector.WithWhere MATCH(String name);

        NodeConnector.WithWhere MATCH(String name, PropertiesMap properties);

        NodeConnector.WithWhere MATCH(String name, Parameter properties);

        NodeConnector.WithWhere MATCH(Labels labels);

        NodeConnector.WithWhere MATCH(Labels labels, PropertiesMap properties);

        NodeConnector.WithWhere MATCH(Labels labels, Parameter properties);

        default NodeConnector.WithWhere MATCH(final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(labels.getNodeLabels());
        }

        default NodeConnector.WithWhere MATCH(final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere MATCH(final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(labels.getNodeLabels(), properties);
        }

        NodeConnector.WithWhere MATCH(String name, Labels labels);

        NodeConnector.WithWhere MATCH(String name, Labels labels, PropertiesMap properties);

        NodeConnector.WithWhere MATCH(String name, Labels labels, Parameter properties);

        default NodeConnector.WithWhere MATCH(final String name, final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(name, labels.getNodeLabels());
        }

        default NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return MATCH(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere MATCH(final NodeVariable node)
        {
            Objects.requireNonNull(node, "node is required");
            return MATCH(node.name(), node.getNodeLabels());
        }

        default NodeConnector.WithWhere MATCH(final NodeVariable node, final PropertiesMap properties)
        {
            Objects.requireNonNull(node, "node is required");
            return MATCH(node.name(), node.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere MATCH(final NodeVariable node, final Parameter properties)
        {
            Objects.requireNonNull(node, "node is required");
            return MATCH(node.name(), node.getNodeLabels(), properties);
        }

        NodeConnector.WithWhere OPTIONAL_MATCH();

        NodeConnector.WithWhere OPTIONAL_MATCH(String name);

        NodeConnector.WithWhere OPTIONAL_MATCH(String name, PropertiesMap properties);

        NodeConnector.WithWhere OPTIONAL_MATCH(String name, Parameter properties);

        NodeConnector.WithWhere OPTIONAL_MATCH(Labels labels);

        NodeConnector.WithWhere OPTIONAL_MATCH(Labels labels, PropertiesMap properties);

        NodeConnector.WithWhere OPTIONAL_MATCH(Labels labels, Parameter properties);

        default NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(labels.getNodeLabels());
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(labels.getNodeLabels(), properties);
        }

        NodeConnector.WithWhere OPTIONAL_MATCH(String name, Labels labels);

        NodeConnector.WithWhere OPTIONAL_MATCH(String name, Labels labels, PropertiesMap properties);

        NodeConnector.WithWhere OPTIONAL_MATCH(String name, Labels labels, Parameter properties);

        default NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(name, labels.getNodeLabels());
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final Parameter properties)
        {
            Objects.requireNonNull(labels, "labels is required");
            return OPTIONAL_MATCH(name, labels.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node)
        {
            Objects.requireNonNull(node, "node is required");
            return OPTIONAL_MATCH(node.name(), node.getNodeLabels());
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node, final PropertiesMap properties)
        {
            Objects.requireNonNull(node, "node is required");
            return OPTIONAL_MATCH(node.name(), node.getNodeLabels(), properties);
        }

        default NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node, final Parameter properties)
        {
            Objects.requireNonNull(node, "node is required");
            return OPTIONAL_MATCH(node.name(), node.getNodeLabels(), properties);
        }

        PathVariable<NodeConnector.WithWhere> MATCH_path(String name);

        PathVariable<NodeConnector.WithWhere> MATCH_shortestPath(String name);

        PathVariable<NodeConnector.WithWhere> MATCH_allShortestPaths(String name);

        PathVariable<NodeConnector.WithWhere> MATCH_path(String name, PathFunction function);

        PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(String name);

        PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_shortestPath(String name);

        PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_allShortestPaths(String name);

        PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(String name, PathFunction function);

        UnwindClause UNWIND(Alias definition);
    }

    interface AfterUpdateClause extends AfterUpdateClauseWITH, HasWith
    {
    }

    interface AfterReadClause extends AfterReadClauseWITH, AfterUpdateClause
    {
    }
}
