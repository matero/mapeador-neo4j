/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.function.Consumer;

public class QueryBuilder extends QueriesVisitor
{
    private int clauses;

    private final StringBuilder cypher;
    private TanslateAlias aliasTranslator = TanslateAlias.AS_REFERENCE;

    public QueryBuilder() {this(new StringBuilder());}

    public String getCypher() {return this.cypher.toString();}

    QueryBuilder(final StringBuilder cypher) {this.cypher = cypher;}

    @Override public void visit(final Alias alias) {this.aliasTranslator.translate(this, alias);}

    @Override public void visit(final AliasName aliasName) {this.cypher.append(aliasName.alias());}

    @Override public void visit(final boolean value) {this.cypher.append(value);}

    @Override public void visit(final byte value) {this.cypher.append(value);}

    @Override public void visit(final short value) {this.cypher.append(value);}

    @Override public void visit(final int value) {this.cypher.append(value);}

    @Override public void visit(final long value) {this.cypher.append(value);}

    @Override public void visit(final float value) {this.cypher.append(value);}

    @Override public void visit(final double value) {this.cypher.append(value);}

    @Override public void visit(final String value) {this.cypher.append(value);}

    @Override public void visit(final ListLiteral value)
    {
        if (value.isEmpty()) {
            this.cypher.append("[]");
        } else {
            this.cypher.append('[');
            value.head().accept(this);
            for (final var element : value.tail()) {
                this.cypher.append(", ");
                element.accept(this);
            }
            this.cypher.append(']');
        }
    }


    @Override public void visit(final UnaryOperation.Prefix<?> operation)
    {
        visitGroupable(operation, it -> {
            this.cypher.append(it.operator());
            it.operand().accept(this);
        });
    }

    @Override public void visit(final UnaryOperation.Postfix<?> operation)
    {
        visitGroupable(operation, it -> {
            it.operand().accept(this);
            this.cypher.append(it.operator());
        });
    }

    @Override public void visit(final BinaryOperation<?> expr)
    {
        visitGroupable(expr, it -> {
            it.lhs().accept(this);
            this.cypher.append(it.operator());
            it.rhs().accept(this);
        });
    }

    @Override public void visit(final Union union)
    {
        if (union.all()) {
            this.cypher.append("\nUNION ALL\n");
        } else {
            this.cypher.append("\nUNION\n");
        }
        resetClauses();
        union.query().accept(this);
    }

    @Override public void visit(final FunctionCall functionCall)
    {
        this.cypher.append(functionCall.name());
        if (functionCall.hasArguments()) {
            this.cypher.append('(');
            functionCall.headArgument().accept(this);
            for (final var argument : functionCall.tailArguments()) {
                this.cypher.append(", ");
                argument.accept(this);
            }
            this.cypher.append(')');
        } else {
            this.cypher.append("()");
        }
    }

    @Override public void visit(final SortPredicate.Explicit sortPredicate)
    {
        sortPredicate.sortOperand().accept(this);
        this.cypher.append(sortPredicate.sortOperator().cypher());
    }

    @Override public void visit(final CaseExpression expr)
    {
        visitGroupable(expr, it -> {
            if (null != expr.base()) {
                this.cypher.append("CASE ");
                expr.base().accept(this);
            } else {
                this.cypher.append("CASE");
            }
            for (final var alternative : expr.alternatives()) {
                alternative.accept(this);
            }
            if (null != expr.elseAlternative()) {
                this.cypher.append("\n  ELSE ");
                expr.elseAlternative().accept(this);
            }
            this.cypher.append("\nEND");
        });
    }

    @Override public void visit(final CaseExpression.Alternative alternative)
    {
        this.cypher.append("\n  WHEN ");
        alternative.when().accept(this);
        this.cypher.append(" THEN ");
        alternative.then().accept(this);
    }

    @Override public void visit(final PropertyExpression propertyExpression)
    {
        propertyExpression.atom().accept(this);
        this.cypher.append('.').append(propertyExpression.lookupsHead());
        for (final var propertyLookup : propertyExpression.lookupsTail()) {
            this.cypher.append('.').append(propertyLookup);
        }
    }

    @Override public void visit(final RawExpression expr)
    {
        visitGroupable(expr, raw -> this.cypher.append(raw.content()));
    }

    @Override public void visit(final PropertyAccess.Static propertyAccess)
    {
        propertyAccess.atom().accept(this);
        this.cypher.append('.').append(propertyAccess.name());
    }

    @Override public void visit(final PropertyAccess.Dynamic propertyAccess)
    {
        propertyAccess.atom().accept(this);
        this.cypher.append('[');
        propertyAccess.operand().accept(this);
        this.cypher.append(']');
    }

    @Override public void visit(final NodeVariable n) {this.cypher.append(n.name());}

    @Override public void visit(final RelationshipVariable r) {this.cypher.append(r.name());}

    @Override public void visit(final NamedParameter parameter) {this.cypher.append('$').append(parameter.name());}

    @Override public void visit(final IndexedParameter parameter) {this.cypher.append('$').append(parameter.index());}

    @Override public void visit(final GetElement expr)
    {
        expr.list().accept(this);
        this.cypher.append('[');
        expr.index().accept(this);
        this.cypher.append(']');
    }

    @Override public void visit(final GetSubList expr)
    {
        expr.list().accept(this);
        this.cypher.append('[');
        expr.start().accept(this);
        this.cypher.append("..");
        expr.end().accept(this);
        this.cypher.append(']');
    }

    @Override public void visit(final PropertyValue.WithExpression propertyValue)
    {
        this.cypher.append(propertyValue.name()).append(": ");
        propertyValue.value().accept(this);
    }

    @Override public void visit(final PropertyValue.WithParameter propertyValue)
    {
        this.cypher.append(propertyValue.name()).append(": ");
        propertyValue.value().accept(this);
    }

    @Override public void visit(final RegularQuery query)
    {
        if (null != query.statement() && noStatementVisited()) {
            query.statement().accept(this);
        } else {
            for (final var clause : query.clauses()) {
                clause.accept(this);
            }
            if (null != query.returnClause()) {
                appendNewLineIfQueryHasManyClausesOrElseSpace();
                this.cypher.append("RETURN ");
                query.returnClause().accept(this);
            }
        }
    }

    @Override public void visit(final CypherProjection returnProjection)
    {
        if (returnProjection.distinct()) {
            this.cypher.append("DISTINCT ");
        }

        this.aliasTranslator = TanslateAlias.AS_DEFINITION;
        returnProjection.itemsHead().accept(this);
        for (final var item : returnProjection.itemsTail()) {
            this.cypher.append(", ");
            item.accept(this);
        }
        this.aliasTranslator = TanslateAlias.AS_REFERENCE;
        if (returnProjection.isSorted()) {
            appendNewLineIfQueryHasManyClausesOrElseSpace();
            this.cypher.append("ORDER BY ");
            returnProjection.sortingHead().accept(this);
            for (final var sortPredicate : returnProjection.sortingTail()) {
                this.cypher.append(", ");
                sortPredicate.accept(this);
            }
        }
        if (returnProjection.hasSkip()) {
            appendNewLineIfQueryHasManyClausesOrElseSpace();
            this.cypher.append("SKIP ");
            returnProjection.skip().accept(this);
        }
        if (returnProjection.hasLimit()) {
            appendNewLineIfQueryHasManyClausesOrElseSpace();
            this.cypher.append("LIMIT ");
            returnProjection.limit().accept(this);
        }
    }

    @Override public void visit(final WithClause clause)
    {
        newClause();
        this.cypher.append("WITH ");
        clause.projection().accept(this);

        if (null != clause.where()) {
            this.cypher.append(" WHERE ");
            clause.where().accept(this);
        }
    }

    @Override public void visit(final MatchClause clause)
    {
        newClause();
        if (clause.optional()) {
            this.cypher.append("OPTIONAL MATCH ");
        } else {
            this.cypher.append("MATCH ");
        }
        clause.pattern().accept(this);
        if (clause.hasWhere()) {
            this.cypher.append(" WHERE ");
            clause.where().accept(this);
        }
    }

    @Override public void visit(final UnwindClause clause)
    {
        newClause();
        this.cypher.append("UNWIND ");
        TanslateAlias.AS_DEFINITION.translate(this, clause.definition());
    }

    @Override public void visit(final InQueryCallClause clause)
    {
        newClause();
        this.cypher.append("CALL ").append(clause.procedure()).append('(');
        if (null != clause.argumentsHead()) {
            clause.argumentsHead().accept(this);
            for (final var arg : clause.argumentsTail()) {
                this.cypher.append(", ");
                arg.accept(this);
            }
        }
        this.cypher.append(')');
        if (null != clause.argumentsHead()) {
            clause.argumentsHead().accept(this);
            for (final var arg : clause.argumentsTail()) {
                this.cypher.append(", ");
                arg.accept(this);
            }
            if (null != clause.where()) {
                clause.where().accept(this);
            }
        }
    }

    @Override public void visit(final CreateClause clause)
    {
        newClause();
        this.cypher.append("CREATE ");
        clause.pattern().accept(this);
    }

    @Override public void visit(final MergeClause clause)
    {
        newClause();
        this.cypher.append("MERGE ");
        clause.pattern().accept(this);
        for (final var item : clause.items()) {
            item.accept(this);
        }
    }

    @Override public void visit(final DeleteClause clause)
    {
        newClause();
        if (clause.detached()) {
            this.cypher.append("DETACH DELETE ");
        } else {
            this.cypher.append("DELETE ");
        }
        clause.head().accept(this);
        for (final var expr : clause.tail()) {
            this.cypher.append(", ");
            expr.accept(this);
        }
    }

    @Override public void visit(final SetClause clause)
    {
        newClause();
        this.cypher.append("SET ");
        clause.head().accept(this);
        for (final var item : clause.tail()) {
            this.cypher.append(", ");
            item.accept(this);
        }
    }

    @Override public void visit(final RemoveClause clause)
    {
        newClause();
        this.cypher.append("REMOVE ");
        clause.head().accept(this);
        for (final var item : clause.tail()) {
            this.cypher.append(", ");
            item.accept(this);
        }
    }

    @Override public void visit(final Pattern<?> pattern)
    {
        visitGroupable(pattern, it -> {
            it.firstNode().accept(this);
            for (final var connection : it.connections()) {
                connection.accept(this);
            }
            if (pattern.hasNext()) {
                this.cypher.append(", ");
                pattern.next().accept(this);
            }
        });
    }

    @Override public void visit(final NamedPattern<?> pattern)
    {
        this.cypher.append(pattern.path()).append(" = ");
        if (pattern.hasFunction()) {
            this.cypher.append(pattern.function()).append('(');
        }

        pattern.firstNode().accept(this);
        for (final var connection : pattern.connections()) {
            connection.accept(this);
        }
        if (pattern.hasFunction()) {
            this.cypher.append(')');
        }
        if (pattern.hasNext()) {
            this.cypher.append(", ");
            pattern.next().accept(this);
        }
    }

    @Override public void visit(final UndetailedConnection connection)
    {
        switch (connection.direction()) {
            case INCOMING:
                this.cypher.append("<--");
                break;
            case OUTGOING:
                this.cypher.append("-->");
                break;
            case BOTH:
                this.cypher.append("<-->");
                break;
            case ANY:
                this.cypher.append("--");
                break;
        }
        connection.node().accept(this);
    }

    @Override public void visit(final DetailedConnection connection)
    {
        switch (connection.direction()) {
            case INCOMING:
            case BOTH:
                this.cypher.append("<-");
                break;
            case OUTGOING:
            case ANY:
                this.cypher.append("-");
                break;
        }
        connection.relationship().accept(this);
        switch (connection.direction()) {
            case INCOMING:
            case ANY:
                this.cypher.append("-");
                break;
            case OUTGOING:
            case BOTH:
                this.cypher.append("->");
                break;
        }
        connection.node().accept(this);
    }

    @Override public void visit(final PropertiesMap map)
    {
        if (map.isEmpty()) {
            this.cypher.append("{}");
        } else {
            this.cypher.append('{');
            map.head().accept(this);
            for (final var entry : map.tail()) {
                this.cypher.append(", ");
                entry.accept(this);
            }
            this.cypher.append('}');
        }
    }

    @Override public void visit(final RelationshipDetails.OneHop hops) {/*nothing to to*/}

    @Override public void visit(final RelationshipDetails.AnyHops hops) {this.cypher.append('*');}

    @Override public void visit(final RelationshipDetails.FixedHops hops) {this.cypher.append("*").append(hops.value());}

    @Override public void visit(final RelationshipDetails.MinHops hops) {this.cypher.append("*").append(hops.value()).append("..");}

    @Override public void visit(final RelationshipDetails.MaxHops hops) {this.cypher.append("*..").append(hops.value());}

    @Override public void visit(final RelationshipDetails.RangeHops hops)
    {
        this.cypher.append("*").append(hops.min()).append("..").append(hops.max());
    }

    @Override public void visit(final MergeClause.Item item)
    {
        this.cypher.append(item.type());
        item.action().accept(this);
    }

    @Override public void visit(final SetItem.Reset item)
    {
        item.lhs().accept(this);
        this.cypher.append(" = ");
        item.rhs().accept(this);
    }

    @Override public void visit(final SetItem.Append item)
    {
        item.lhs().accept(this);
        this.cypher.append(" += ");
        item.rhs().accept(this);
    }

    @Override public void visit(final UpdateLabels item)
    {
        item.target().accept(this);
        this.cypher.append(item.labels().cypher());
    }

    @Override public void visit(final NodeWithoutProperties node)
    {
        if (node.isEmpty()) {
            this.cypher.append("()");
        } else {
            this.cypher.append('(');
            if (null != node.name()) {
                this.cypher.append(node.name());
            }
            if (node.hasLabels()) {
                if (null != node.name()) {
                    this.cypher.append(' ');
                }
                this.cypher.append(node.labels().cypher());
            }
            this.cypher.append(')');
        }
    }

    @Override public void visit(final NodeWithProperties node)
    {
        this.cypher.append('(');
        if (null != node.name()) {
            this.cypher.append(node.name()).append(' ');
        }
        if (node.hasLabels()) {
            this.cypher.append(node.labels().cypher()).append(' ');
        }

        node.properties().accept(this);
        this.cypher.append(')');
    }

    @Override public void visit(final RelationshipDetails.Empty relationship)
    {
        this.cypher.append("[]");
    }

    @Override public void visit(final RelationshipWithoutProperties relationship)
    {
        this.cypher.append('[');
        var separate = false;
        if (null != relationship.name()) {
            this.cypher.append(relationship.name());
            separate = true;
        }
        if (relationship.hasTypes()) {
            if (separate) {
                this.cypher.append(' ');
            }
            this.cypher.append(relationship.types().cypher());
        }

        relationship.hops().accept(this);

        this.cypher.append(']');
    }

    @Override public void visit(final RelationshipWithProperties relationship)
    {
        boolean separate = false;
        this.cypher.append('[');
        if (null != relationship.name()) {
            this.cypher.append(relationship.name());
            separate = true;
        }
        if (relationship.hasTypes()) {
            if (separate) {
                this.cypher.append(' ');
            } else {
                separate = true;
            }
            this.cypher.append(relationship.types().cypher());
        }
        relationship.hops().accept(this);
        if (separate) {
            this.cypher.append(' ');
        }
        relationship.properties().accept(this);
        this.cypher.append(']');
    }

    // common visits
    private <G extends GroupableExpression> void visitGroupable(final G groupable, final Consumer<G> visit)
    {
        if (groupable.grouped()) {
            this.cypher.append('(');
            visit.accept(groupable);
            this.cypher.append(')');
        } else {
            visit.accept(groupable);
        }
    }

    /* strategy to use when processing an Alias. */
    enum TanslateAlias
    {
        AS_DEFINITION {
            @Override void translate(final QueryBuilder self, final Alias alias)
            {
                alias.expr().accept(self);
                self.cypher.append(" AS ").append(alias.name());
            }
        },
        AS_REFERENCE {
            @Override void translate(final QueryBuilder self, final Alias alias) {self.cypher.append(alias.name());}
        };

        abstract void translate(QueryBuilder self, Alias alias);
    }

    private void newClause()
    {
        appendNewLineIfQueryHasManyClauses();
        this.clauses++;
    }

    private void resetClauses()
    {
        this.clauses = 0;
    }

    private void appendNewLineIfQueryHasManyClauses()
    {
        if (0 < this.clauses) {
            this.cypher.append('\n');
        }
    }

    private void appendNewLineIfQueryHasManyClausesOrElseSpace()
    {
        if (1 < this.clauses) {
            this.cypher.append('\n');
        } else if (1 == this.clauses) {
            this.cypher.append(' ');
        }
    }
}
