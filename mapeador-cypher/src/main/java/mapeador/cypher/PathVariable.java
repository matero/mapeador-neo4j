/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;

import java.util.Objects;

public interface PathVariable<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>>
{
    CONNECTOR node();

    CONNECTOR node(String name);

    CONNECTOR node(Labels labels);

    default CONNECTOR node(final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(labels.getNodeLabels());
    }

    CONNECTOR node(PropertiesMap properties);

    CONNECTOR node(Parameter properties);

    CONNECTOR node(String name, Labels labels);

    default CONNECTOR node(final String name, final HasLabels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(name, labels.getNodeLabels());
    }

    CONNECTOR node(String name, PropertiesMap properties);

    CONNECTOR node(String name, Parameter properties);

    CONNECTOR node(Labels labels, PropertiesMap properties);

    CONNECTOR node(Labels labels, Parameter properties);

    default CONNECTOR node(final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(labels.getNodeLabels(), properties);
    }

    default CONNECTOR node(final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(labels.getNodeLabels(), properties);
    }

    CONNECTOR node(String name, Labels labels, PropertiesMap properties);

    CONNECTOR node(String name, Labels labels, Parameter properties);

    default CONNECTOR node(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR node(final String name, final HasLabels labels, final Parameter properties)
    {
        Objects.requireNonNull(labels, "labels is required");
        return node(name, labels.getNodeLabels(), properties);
    }

    default CONNECTOR node(final NodeVariable node)
    {
        Objects.requireNonNull(node, "node is required");
        return node(node.name(), node.getNodeLabels());
    }

    default CONNECTOR node(final NodeVariable node, final PropertiesMap properties)
    {
        Objects.requireNonNull(node, "node is required");
        return node(node.name(), node.getNodeLabels(), properties);
    }

    default CONNECTOR node(final NodeVariable node, final Parameter properties)
    {
        Objects.requireNonNull(node, "node is required");
        return node(node.name(), node.getNodeLabels(), properties);
    }
}
