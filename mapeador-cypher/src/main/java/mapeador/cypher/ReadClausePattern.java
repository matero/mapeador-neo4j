/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;

import java.util.List;

abstract class ReadClausePattern<CONNECTOR extends NodeConnectorPredicate<CONNECTOR>, PARENT extends ReadClause>
    extends ClausePattern<CONNECTOR, PARENT>
{
    ReadClausePattern(final PARENT parent) {super(parent);}

    public WithClause WITH(final Expression item) {return this.parent.WITH(item);}

    public WithClause WITH(final Expression firstItem, final Expression... otherItems) {return this.parent.WITH(firstItem, otherItems);}

    public WithClause WITH(final List<Expression> items) {return this.parent.WITH(items);}

    public WithClause WITH_DISTINCT(final Expression item) {return this.parent.WITH_DISTINCT(item);}

    public WithClause WITH_DISTINCT(final Expression firstItem, final Expression... otherItems)
    {
        return this.parent.WITH_DISTINCT(firstItem, otherItems);
    }

    public WithClause WITH_DISTINCT(final List<Expression> items) {return this.parent.WITH_DISTINCT(items);}

    // ============================================================================================================================================ //
    // R E A D I N G   C L A U S E S                                                                                                                //
    // ============================================================================================================================================ //
    public final NodeConnector.WithWhere MATCH() {return this.parent.MATCH();}

    public final NodeConnector.WithWhere MATCH(final String name) {return this.parent.MATCH(name);}

    public final NodeConnector.WithWhere MATCH(final String name, final PropertiesMap properties)
    {
        return this.parent.MATCH(name, properties);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final Parameter properties) {return this.parent.MATCH(name, properties);}

    public final NodeConnector.WithWhere MATCH(final Labels labels) {return this.parent.MATCH(labels);}

    public final NodeConnector.WithWhere MATCH(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final Labels labels, final Parameter properties) {return this.parent.MATCH(labels, properties);}

    public final NodeConnector.WithWhere MATCH(final HasLabels labels) {return this.parent.MATCH(labels);}

    public final NodeConnector.WithWhere MATCH(final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final HasLabels labels, final Parameter properties)
    {
        return this.parent.MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final Labels labels)
    {
        return this.parent.MATCH(name, labels);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final HasLabels labels)
    {
        return this.parent.MATCH(name, labels);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final String name, final HasLabels labels, final Parameter properties)
    {
        return this.parent.MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere MATCH(final NodeVariable node)
    {
        return this.parent.MATCH(node);
    }

    public final NodeConnector.WithWhere MATCH(final NodeVariable node, final PropertiesMap properties)
    {
        return this.parent.MATCH(node, properties);
    }

    public final NodeConnector.WithWhere MATCH(final NodeVariable node, final Parameter properties)
    {
        return this.parent.MATCH(node, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH() {return this.parent.OPTIONAL_MATCH();}

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name) {return this.parent.OPTIONAL_MATCH(name);}

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(name, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Parameter properties)
    {
        return this.parent.OPTIONAL_MATCH(name, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels) {return this.parent.OPTIONAL_MATCH(labels);}

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final Labels labels, final Parameter properties)
    {
        return this.parent.OPTIONAL_MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels)
    {
        return this.parent.OPTIONAL_MATCH(labels);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final HasLabels labels, final Parameter properties)
    {
        return this.parent.OPTIONAL_MATCH(labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels)
    {
        return this.parent.OPTIONAL_MATCH(name, labels);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.OPTIONAL_MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels)
    {
        return this.parent.OPTIONAL_MATCH(name, labels);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final String name, final HasLabels labels, final Parameter properties)
    {
        return this.parent.OPTIONAL_MATCH(name, labels, properties);
    }

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node) {return this.parent.OPTIONAL_MATCH(node);}

    public final NodeConnector.WithWhere OPTIONAL_MATCH(final NodeVariable node, final PropertiesMap properties)
    {
        return this.parent.OPTIONAL_MATCH(node, properties);
    }

    public final PathVariable<NodeConnector.WithWhere> MATCH_path(final String name) {return this.parent.MATCH_path(name);}

    public final PathVariable<NodeConnector.WithWhere> MATCH_shortestPath(final String name)
    {
        return this.parent.MATCH_shortestPath(name);
    }

    public final PathVariable<NodeConnector.WithWhere> MATCH_allShortestPaths(final String name)
    {
        return this.parent.MATCH_allShortestPaths(name);
    }

    public final PathVariable<NodeConnector.WithWhere> MATCH_path(final String name, final PathFunction function)
    {
        return this.parent.MATCH_path(name, function);
    }

    public final PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name) {return this.parent.OPTIONAL_MATCH_path(name);}

    public final PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_shortestPath(final String name)
    {
        return this.parent.OPTIONAL_MATCH_shortestPath(name);
    }

    public final PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_allShortestPaths(final String name)
    {
        return this.parent.OPTIONAL_MATCH_allShortestPaths(name);
    }

    public final PathVariable<NodeConnector.WithWhere> OPTIONAL_MATCH_path(final String name, final PathFunction function)
    {
        return this.parent.OPTIONAL_MATCH_path(name, function);
    }

    public final UnwindClause UNWIND(final Alias definition) {return this.parent.UNWIND(definition);}

    // ======================================================================================================================================== //
    // U P D A T I N G   C L A U S E S                                                                                                          //
    // ======================================================================================================================================== //
    public final NodeConnector.WithReturnAfterUpdateClause CREATE() {return this.parent.CREATE();}

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name) {return this.parent.CREATE(name);}

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final PropertiesMap properties)
    {
        return this.parent.CREATE(name, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Parameter properties)
    {
        return this.parent.CREATE(name, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels) {return this.parent.CREATE(labels);}

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final Labels labels, final Parameter properties)
    {
        return this.parent.CREATE(labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels) {return this.parent.CREATE(labels);}

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final HasLabels labels, final Parameter properties)
    {
        return this.parent.CREATE(labels, properties);
    }


    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels) {return this.parent.CREATE(name, labels);}


    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels)
    {
        return this.parent.CREATE(name, labels);
    }


    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }


    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final String name, final HasLabels labels, final Parameter properties)
    {
        return this.parent.CREATE(name, labels, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node) {return this.parent.CREATE(node);}

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final PropertiesMap properties)
    {
        return this.parent.CREATE(node, properties);
    }

    public final NodeConnector.WithReturnAfterUpdateClause CREATE(final NodeVariable node, final Parameter properties)
    {
        return this.parent.CREATE(node, properties);
    }

    public final PathVariable<NodeConnector.WithReturnAfterUpdateClause> CREATE_path(final String pathName)
    {
        return this.parent.CREATE_path(pathName);
    }

    public final NodeConnector.WithMergeActions MERGE() {return this.parent.MERGE();}

    public final NodeConnector.WithMergeActions MERGE(final String name) {return this.parent.MERGE(name);}

    public final NodeConnector.WithMergeActions MERGE(final String name, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final Parameter properties)
    {
        return this.parent.MERGE(name, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final Labels labels) {return this.parent.MERGE(labels);}

    public final NodeConnector.WithMergeActions MERGE(final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final Labels labels, final Parameter properties)
    {
        return this.parent.MERGE(labels,
                                 properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final HasLabels labels) {return this.parent.MERGE(labels);}

    public final NodeConnector.WithMergeActions MERGE(final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final HasLabels labels, final Parameter properties)
    {
        return this.parent.MERGE(labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels) {return this.parent.MERGE(name, labels);}

    public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final Labels labels, final Parameter properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels)
    {
        return this.parent.MERGE(name, labels);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final PropertiesMap properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final String name, final HasLabels labels, final Parameter properties)
    {
        return this.parent.MERGE(name, labels, properties);
    }

    public final NodeConnector.WithMergeActions MERGE(final NodeVariable node) {return this.parent.MERGE(node);}

    public final NodeConnector.WithMergeActions MERGE(final NodeVariable node, final PropertiesMap properties)
    {
        return this.parent.MERGE(node, properties);
    }


    public final NodeConnector.WithMergeActions MERGE(final NodeVariable node, final Parameter properties)
    {
        return this.parent.MERGE(node,
                                 properties);
    }

    public final HasReturn.AfterUpdateClause DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        return this.parent.DELETE(deletion, otherDeletions);
    }

    public final HasReturn.AfterUpdateClause DETACH_DELETE(final Expression deletion, final Expression... otherDeletions)
    {
        return this.parent.DETACH_DELETE(deletion, otherDeletions);
    }

    public final HasReturn.AfterUpdateClause SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return this.parent.SET(firstItem, otherItems);
    }

    public final HasReturn.AfterUpdateClause REMOVE(final RemoveItem firstItem, final RemoveItem... otherItems)
    {
        return this.parent.REMOVE(firstItem, otherItems);
    }
}
