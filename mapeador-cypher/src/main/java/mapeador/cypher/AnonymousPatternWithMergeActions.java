/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

public class AnonymousPatternWithMergeActions
    extends UpdateClausePattern<NodeConnector.WithMergeActions, MergeClause>
    implements NodeConnector.WithMergeActions
{
    AnonymousPatternWithMergeActions(final MergeClause parent) {super(parent);}

    @Override public AnonymousPatternWithMergeActions sortOperand() {return this;}

    @Override AnonymousPatternWithMergeActions makeNextPattern() {return nextPattern(new AnonymousPatternWithMergeActions(this.parent));}

    @Override NamedPattern<NodeConnector.WithMergeActions> makeNextPattern(final String name)
    {
        return nextPattern(new PathPatternWithMergeActions(this.parent, name));
    }

    @Override NamedPattern<NodeConnector.WithMergeActions> makeNextPattern(final String name, final String function)
    {
        return nextPattern(new PathPatternWithMergeActions(this.parent, name, function));
    }

    @Override public NodeConnector.WithMergeActions ON_MATCH_SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return this.parent.ON_MATCH_SET(firstItem, otherItems);
    }

    @Override public NodeConnector.WithMergeActions ON_CREATE_SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return this.parent.ON_MATCH_SET(firstItem, otherItems);
    }
}
