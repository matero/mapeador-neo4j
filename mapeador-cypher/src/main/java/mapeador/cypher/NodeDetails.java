/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Labels;
import mapeador.Name;

import java.util.Objects;


public interface NodeDetails extends VariableDetails, CypherElement
{
    Labels labels();

    default boolean hasLabels() {return labels().isNotEmpty();}

    static NodeDetails of() {return NodeWithoutProperties.Empty.INSTANCE;}

    static NodeDetails of(final String variable) {return new NodeWithoutProperties(Name.at(variable), Labels.empty());}

    static NodeDetails of(final Labels labels)
    {
        Objects.requireNonNull(labels, "labels is required");
        if (labels.isEmpty()) {
            return NodeWithoutProperties.Empty.INSTANCE;
        }
        return new NodeWithoutProperties(null, labels);
    }

    static NodeDetails of(final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return NodeWithoutProperties.Empty.INSTANCE;
        }
        return new NodeWithProperties(null, Labels.empty(), properties);
    }

    static NodeDetails of(final String variable, final Labels labels) {return new NodeWithoutProperties(Name.at(variable), labels);}

    static NodeDetails of(final String variable, final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return new NodeWithoutProperties(Name.at(variable), Labels.empty());
        }
        return new NodeWithProperties(Name.at(variable), Labels.empty(), properties);
    }

    static NodeDetails of(final Labels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return new NodeWithoutProperties(null, labels);
        }
        return new NodeWithProperties(null, labels, properties);
    }

    static NodeDetails of(final String name, final Labels labels, final PropertiesMap properties)
    {
        Objects.requireNonNull(properties, "properties is required");
        if (properties.isEmpty()) {
            return new NodeWithoutProperties(name, labels);
        }
        return new NodeWithProperties(name, labels, properties);
    }

    static NodeDetails of(final Parameter properties)
    {
        return new NodeWithProperties(null, Labels.empty(), properties);
    }

    static NodeDetails of(final String variable, final Parameter properties)
    {
        return new NodeWithProperties(Name.at(variable), Labels.empty(), properties);
    }

    static NodeDetails of(final Labels labels, final Parameter property)
    {
        return new NodeWithProperties(null, labels, property);
    }

    static NodeDetails of(final String name, final Labels labels, final Parameter property)
    {
        return new NodeWithProperties(name, labels, property);
    }
}
