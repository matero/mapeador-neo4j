/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.Collections;
import java.util.List;

public final class MergeClause
        extends UpdateClauseWithPattern<NodeConnector.WithMergeActions, MergeClause>
    implements HasMergeActions, NodeConnector.WithMergeActions
{
    private static final String ON_MATCH = "\nON MATCH";
    private static final String ON_CREATE = "\nON CREATE";

    private List<Item> items;

    MergeClause(final RegularQuery parent) {super(parent, AnonymousPatternWithMergeActions::new);}

    MergeClause(final RegularQuery parent, final String pathName) {super(parent, pathName, PathPatternWithMergeActions::new);}

    MergeClause(final RegularQuery parent, final String pathName, final String function)
    {
        super(parent, pathName, function, PathPatternWithMergeActions::new);
    }

    public List<Item> items() {return null == this.items? List.of() : Collections.unmodifiableList(this.items);}

    @Override public NodeConnector.WithMergeActions ON_MATCH_SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return addAction(ON_MATCH, SetClause.of(this.parent, firstItem, otherItems));
    }

    @Override public NodeConnector.WithMergeActions ON_CREATE_SET(final SetItem firstItem, final SetItem... otherItems)
    {
        return addAction(ON_CREATE, SetClause.of(this.parent, firstItem, otherItems));
    }

    @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}

    @Override public boolean grouped() {throw new CypherException("MERGE clauses can not be grouped.");}

    @Override public MergeClause group() {throw new CypherException("MERGE clauses can not be grouped.");}

    @Override public Operand sortOperand() {throw new CypherException("MERGE clauses doesn't have a sort operand.");}

    @Override NodeConnector.WithMergeActions asNodeConnector() {return this;}

    private MergeClause addAction(final String type, final SetClause action)
    {
        if (null == this.items) {
            this.items = new java.util.LinkedList<>();
        }
        this.items.add(new Item(type, action));
        return this;
    }

    public static final class Item implements CypherElement
    {
        private final String type;
        private final SetClause action;

        Item(final String type, final SetClause action)
        {
            this.type = type;
            this.action = action;
        }

        public String type() {return this.type;}

        public SetClause action() {return this.action;}

        @Override public void accept(final CypherVisitor visitor) {visitor.visit(this);}
    }
}
