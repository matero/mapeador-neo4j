/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.Expression;
import mapeador.cypher.Projection;
import mapeador.cypher.RawExpression;
import mapeador.cypher.SortPredicate;

import java.util.Objects;


public final class ProjectionFetchOptions extends OrderByFetchOptions
{
    private final boolean projectDistinct;
    private final Expression firstProjectionColumn;
    private final Expression[] otherProjectionColumns;

    ProjectionFetchOptions(final boolean projectDistinct, final Expression firstProjectionColumn, final Expression[] otherProjectionColumns)
    {
        this.projectDistinct = projectDistinct;
        this.firstProjectionColumn = firstProjectionColumn;
        this.otherProjectionColumns = otherProjectionColumns;
    }

    @Override public boolean projectDistinct() {return this.projectDistinct;}

    @Override public Expression firstProjectionColumn() {return this.firstProjectionColumn;}

    @Override public Expression[] otherProjectionColumns() {return this.otherProjectionColumns;}

    @Override public OrderByFetchOptions orderBy(final String sortPredicates)
    {
        return super.orderBy(sortPredicates);
    }

    @Override public OrderByFetchOptions orderBy(final SortPredicate firstSortPredicate, final SortPredicate... otherSortPredicates)
    {
        return super.orderBy(firstSortPredicate, otherSortPredicates);
    }

    static ProjectionFetchOptions of(final boolean distinct, final String columns)
    {
        Objects.requireNonNull(columns, "columns is required");
        if (columns.isEmpty()) {
            throw new IllegalArgumentException("rawPredicate can't be empty");
        }
        if (columns.isBlank()) {
            throw new IllegalArgumentException("rawPredicate can't be blank");
        }
        return new ProjectionFetchOptions(distinct, new RawExpression(columns.trim()), Projection.NO_COLUMNS);
    }

    static ProjectionFetchOptions of(final boolean distinct, final Expression firstProjectionColumn, final Expression... otherProjectionColumns)
    {
        Objects.requireNonNull(firstProjectionColumn, "firstProjectionColumn is required");
        return new ProjectionFetchOptions(distinct, firstProjectionColumn, otherProjectionColumns);
    }
}

