/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.CypherDSL;
import mapeador.cypher.Expression;
import mapeador.cypher.Projection;
import mapeador.cypher.RawExpression;
import mapeador.cypher.SortPredicate;

import java.util.Map;
import java.util.Objects;

public interface FetchOptions
{
    FetchOptions DEFAULT = DefaultFetchOptions.INSTANCE;
    boolean projectDistinct();

    Expression firstProjectionColumn();

    Expression[] otherProjectionColumns();

    SortPredicate firstSortPredicate();

    SortPredicate[] otherSortPredicates();

    Expression skip();

    Expression limit();

    Map<String, Object> parameters();

    static ProjectionFetchOptions returning(final String rawColumns)
    {
        return ProjectionFetchOptions.of(false, rawColumns);
    }

    static ProjectionFetchOptions returning(final Expression firstProjectionColumn, final Expression... otherProjectionColumns)
    {
        return ProjectionFetchOptions.of(false, firstProjectionColumn, otherProjectionColumns);
    }

    static ProjectionFetchOptions returningDistinct(final String rawColumns)
    {
        return ProjectionFetchOptions.of(true, rawColumns);
    }

    static ProjectionFetchOptions returningDistinct(final Expression firstProjectionColumn, final Expression... otherProjectionColumns)
    {
        return ProjectionFetchOptions.of(true, firstProjectionColumn, otherProjectionColumns);
    }

    static OrderByFetchOptions orderBy(final String sortPredicates)
    {
        Objects.requireNonNull(sortPredicates, "sortPredicates is required");
        if (sortPredicates.isEmpty()) {
            throw new IllegalArgumentException("sortPredicates can't be empty");
        }
        if (sortPredicates.isBlank()) {
            throw new IllegalArgumentException("sortPredicates can't be blank");
        }
        return new OrderByFetchOptions(new RawExpression(sortPredicates.trim()), SortPredicate.NONE);
    }

    static OrderByFetchOptions orderBy(final SortPredicate firstSortPredicate, final SortPredicate... otherSortPredicates)
    {
        Objects.requireNonNull(firstSortPredicate, "firstSortPredicate is required");
        return new OrderByFetchOptions(firstSortPredicate, otherSortPredicates);
    }

    static SkipFetchOptions skip(final int value)
    {
        return new SkipFetchOptions(CypherDSL.$(value));
    }

    static SkipFetchOptions skip(final String skip)
    {
        Objects.requireNonNull(skip, "skip is required");
        if (skip.isEmpty()) {
            throw new IllegalArgumentException("skip can't be empty");
        }
        if (skip.isBlank()) {
            throw new IllegalArgumentException("skip can't be blank");
        }
        return new SkipFetchOptions(new RawExpression(skip.trim()));
    }

    static SkipFetchOptions skip(final Expression skipExpr)
    {
        Objects.requireNonNull(skipExpr, "skipExpr is required");
        return new SkipFetchOptions(skipExpr);
    }

    static LimitFetchOptions limit(final int value)
    {
        return new LimitFetchOptions(CypherDSL.$(value));
    }

    static LimitFetchOptions limit(final String limit)
    {
        Objects.requireNonNull(limit, "limit is required");
        if (limit.isEmpty()) {
            throw new IllegalArgumentException("limit can't be empty");
        }
        if (limit.isBlank()) {
            throw new IllegalArgumentException("limit can't be blank");
        }
        return new LimitFetchOptions(new RawExpression(limit.trim()));
    }

    static LimitFetchOptions limit(final Expression limitExpr)
    {
        Objects.requireNonNull(limitExpr, "limitExpr is required");
        return new LimitFetchOptions(limitExpr);
    }
}

enum DefaultFetchOptions implements FetchOptions {
    INSTANCE;

    @Override public boolean projectDistinct() {return false;}

    @Override public Expression firstProjectionColumn() {return Projection.ALL;}

    @Override public Expression[] otherProjectionColumns() {return Projection.NO_COLUMNS;}

    @Override public SortPredicate firstSortPredicate() {return null;}

    @Override public SortPredicate[] otherSortPredicates() {return SortPredicate.NONE;}

    @Override public Expression skip() {return null;}

    @Override public Expression limit() {return null;}

    @Override public Map<String, Object> parameters() {return Map.of();}
}
