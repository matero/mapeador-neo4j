/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.CypherDSL;
import mapeador.cypher.Expression;
import mapeador.cypher.RawExpression;

import java.util.Objects;

public class SkipFetchOptions extends LimitFetchOptions
{
    private Expression skip;

    SkipFetchOptions() {this(null);}

    SkipFetchOptions(final Expression skip) {this.skip = skip;}

    @Override public Expression skip() {return this.skip;}

    SkipFetchOptions skip(final int value) {return skip(CypherDSL.$(value));}

    SkipFetchOptions skip(final String skip)
    {
        Objects.requireNonNull(skip, "skip is required");
        if (skip.isEmpty()) {
            throw new IllegalArgumentException("skip can't be empty");
        }
        if (skip.isBlank()) {
            throw new IllegalArgumentException("skip can't be blank");
        }
        return skip(new RawExpression(skip));
    }

    SkipFetchOptions skip(final Expression skipExpr)
    {
        this.skip = Objects.requireNonNull(skipExpr, "skipExpr is required");
        return this;
    }

    @Override public LimitFetchOptions limit(final int value) {return super.limit(value);}

    @Override public LimitFetchOptions limit(final String limit) {return super.limit(limit);}

    @Override public LimitFetchOptions limit(final Expression limitExpr) {return super.limit(limitExpr);}
}
