/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.Expression;
import mapeador.cypher.SortPredicate;
import org.neo4j.driver.Record;
import org.neo4j.driver.Value;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public class ParametersFetchOptions implements FetchOptions
{
    private Map<String, Object> parameters;

    ParametersFetchOptions() {this(Map.of());}

    ParametersFetchOptions(final Map<String, Object> parameters) {this.parameters = parameters;}

    @Override public boolean projectDistinct() {return FetchOptions.DEFAULT.projectDistinct();}

    @Override public Expression firstProjectionColumn() {return FetchOptions.DEFAULT.firstProjectionColumn();}

    @Override public Expression[] otherProjectionColumns() {return FetchOptions.DEFAULT.otherProjectionColumns();}

    @Override public SortPredicate firstSortPredicate() {return FetchOptions.DEFAULT.firstSortPredicate();}

    @Override public SortPredicate[] otherSortPredicates() {return FetchOptions.DEFAULT.otherSortPredicates();}

    @Override public Expression skip() {return FetchOptions.DEFAULT.skip();}

    @Override public Expression limit() {return FetchOptions.DEFAULT.limit();}

    @Override public Map<String, Object> parameters() {return this.parameters;}

    ParametersFetchOptions parameters(final Map<String, Object> parameters)
    {
        Objects.requireNonNull(parameters, "parameters is required.");
        this.parameters = parameters;
        return this;
    }

    ParametersFetchOptions parameters(final Value parameters)
    {
        Objects.requireNonNull(parameters, "parameters is required.");
        this.parameters = parameters.asMap();
        return this;
    }

    ParametersFetchOptions parameters(final Record parameters)
    {
        Objects.requireNonNull(parameters, "parameters is required.");
        this.parameters = parameters.asMap();
        return this;
    }

    ParametersFetchOptions parameters(final Object... parameters)
    {
        Objects.requireNonNull(parameters, "parameters is required.");
        if (0 != parameters.length % 2) {
            throw new IllegalArgumentException("Even number of arguments required, alternating key and value. But 'parameters' was " +
                                               Arrays.toString(parameters) + '.');
        }
        final var map = new java.util.HashMap<String, Object>(parameters.length / 2);
        for (int i = 0; i < parameters.length; i += 2) {
            final Object value = parameters[i + 1];
            map.put(parameters[i].toString(), value);
        }
        this.parameters = map;
        return this;
    }
}

