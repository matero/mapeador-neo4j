/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software fromout restriction, including fromout limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.NodeVariable;
import mapeador.cypher.Pattern;
import mapeador.cypher.PropertyValue;
import mapeador.model.HasNodeLabels;
import mapeador.model.HasParameterClasses;

import java.util.Map;
import java.util.Set;

public class IncomingRelationCriteria extends ResultCriteria
{
    protected IncomingRelationCriteria(final Pattern pattern, final Map<String, Object> actualParameters, final Set<String> requiredParameters)
    {
        super(pattern, actualParameters, requiredParameters);
    }

    public ResultCriteria from()
    {
        this.pattern.from();
        return this;
    }

    public ResultCriteria from(final NodeVariable n)
    {
        this.pattern.from(n);
        return this;
    }

    public ResultCriteria from(final String name)
    {
        this.pattern.from(name);
        return this;
    }

    public ResultCriteria from(final HasNodeLabels labels)
    {
        this.pattern.from(labels);
        return this;
    }

    public ResultCriteria from(final String name, final HasNodeLabels labels)
    {
        this.pattern.from(name, labels);
        return this;
    }

    public ResultCriteria from(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        useParameters(n, value, otherValues);
        this.pattern.from(n, value, otherValues);
        return this;
    }

    public ResultCriteria from(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        useParameters(name, (String) null, value, otherValues);
        this.pattern.from(name, value, otherValues);
        return this;
    }

    public ResultCriteria from(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        useParameters(null, labels, value, otherValues);
        this.pattern.from(labels, value, otherValues);
        return this;
    }

    public ResultCriteria from(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        useParameters(name, labels, value, otherValues);
        this.pattern.from(name, labels, value, otherValues);
        return this;
    }

    private void useParameters(final NodeVariable n, final PropertyValue value, final PropertyValue[] otherValues)
    {
        useParameters(n.getName(), n.getParameterClasses(), value, otherValues);
    }

    private void useParameters(final String name, final HasParameterClasses parameterClasses, final PropertyValue value, final PropertyValue[] otherValues)
    {
        useParameters(name, parameterClasses.getParameterClasses(), value, otherValues);
    }

    private void useParameters(final String name, final String parameterClasses, final PropertyValue value, final PropertyValue[] otherValues)
    {
        value.useParameter(this, name, parameterClasses);
        if (null != otherValues && 0 != otherValues.length) {
            for (final var otherValue : otherValues) {
                otherValue.useParameter(this, name, parameterClasses);
            }
        }
    }


}
