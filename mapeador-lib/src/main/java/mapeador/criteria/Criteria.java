/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.CurrentSession;
import mapeador.cypher.Expression;
import mapeador.cypher.Hops;
import mapeador.cypher.NodeVariable;
import mapeador.cypher.Parameter;
import mapeador.cypher.PathFunction;
import mapeador.cypher.Pattern;
import mapeador.cypher.Projection;
import mapeador.cypher.PropertyValue;
import mapeador.cypher.RelationVariable;
import mapeador.model.HasNodeLabels;
import mapeador.model.HasRelationTypes;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public abstract class Criteria<T, C extends Criteria<T, C>> extends PatternBuilder
{
    private final Function<Result, T> interpreter;

    private Map<String, Object> actualParameters;
    private Set<String> requiredParameters;

    protected Criteria(final Pattern pattern, final Function<Result, T> interpreter)
    {
        super(pattern);
        this.interpreter = interpreter;
    }

    public Criteria(
        final Pattern pattern,
        final Function<Result, T> interpreter,
        final Map<String, Object> actualParameters,
        final Set<String> requiredParameters)
    {
        super(pattern);
        this.interpreter = interpreter;
        this.actualParameters = actualParameters;
        this.requiredParameters = requiredParameters;
    }

    protected final Map<String, Object> getActualParameters() {return this.actualParameters;}

    protected final Set<String> getRequiredParameters() {return this.requiredParameters;}

    public T fetch()
    {
        final var session = CurrentSession.get();
        Objects.requireNonNull(session, "CurrentSession.get() is not initialized.");
        return session.readTransaction(tx -> doFetch(tx, FetchOptions.DEFAULT));
    }

    public T fetch(final FetchOptions fetchOptions)
    {
        Objects.requireNonNull(fetchOptions, "fetchOptions is required.");
        final var session = CurrentSession.get();
        Objects.requireNonNull(session, "CurrentSession.get() is not initialized.");
        return session.readTransaction(tx -> doFetch(tx, fetchOptions));
    }

    public T fetch(final Session session)
    {
        Objects.requireNonNull(session, "session is required.");
        return session.readTransaction(tx -> doFetch(tx, FetchOptions.DEFAULT));
    }

    public T fetch(final Session session, final FetchOptions fetchOptions)
    {
        Objects.requireNonNull(session, "session is required.");
        Objects.requireNonNull(fetchOptions, "parameters is required.");
        return session.readTransaction(tx -> doFetch(tx, fetchOptions));
    }

    public T fetch(final Transaction tx)
    {
        Objects.requireNonNull(tx, "tx is required.");
        return doFetch(tx, FetchOptions.DEFAULT);
    }

    public T fetch(final Transaction tx, final FetchOptions fetchOptions)
    {
        Objects.requireNonNull(tx, "tx is required.");
        Objects.requireNonNull(fetchOptions, "parameters is required.");
        return doFetch(tx, fetchOptions);
    }

    protected T doFetch(final Transaction tx, final FetchOptions fetchOptions)
    {
        prepareQuery(fetchOptions);
        final var query = cypherQuery();
        final var parameters = getPatternParameters(fetchOptions.parameters());
        final var rawResult = tx.run(query, parameters);
        return this.interpreter.apply(rawResult);
    }

    protected abstract void prepareQuery(final FetchOptions fetchOptions);

    protected final Map<String, Object> getPatternParameters(final Map<String, Object> userDefinedParameters)
    {
        return patternParameters(parametersPlus(userDefinedParameters), requiredParameters());
    }

    protected final Map<String, Object> parametersPlus(final Map<String, Object> userDefinedParameters)
    {
        if (null == this.actualParameters || this.actualParameters.isEmpty()) {
            if (null == userDefinedParameters || userDefinedParameters.isEmpty()) {
                return Map.of();
            } else {
                return userDefinedParameters;
            }
        } else {
            if (null == userDefinedParameters || userDefinedParameters.isEmpty()) {
                return this.actualParameters;
            } else {
                final var result = new HashMap<String, Object>(this.actualParameters.size() + userDefinedParameters.size());
                result.putAll(this.actualParameters);
                result.putAll(userDefinedParameters);
                return result;
            }
        }
    }

    protected final List<String> requiredParameters()
    {
        if (null == this.requiredParameters || this.requiredParameters.isEmpty()) {
            return List.of();
        }
        return new ArrayList<>(this.requiredParameters);
    }

    protected final Map<String, Object> patternParameters(final Map<String, Object> parameters, final List<String> requiredParameters)
    {
        if (null == parameters || parameters.isEmpty()) {
            if (!requiredParameters.isEmpty()) {
                throw new IllegalStateException("missing required parameters " + requiredParameters);
            }
            return Map.of();
        } else {
            if (!requiredParameters.isEmpty()) {
                requiredParameters.removeIf(parameters::containsKey);
                if (!requiredParameters.isEmpty()) {
                    throw new IllegalStateException("missing required parameters " + requiredParameters);
                }
            }
        }
        return parameters;
    }

    protected String cypherQuery()
    {
        if (this.pattern.hasReturn()) {
            return this.pattern.getCypher();
        } else {
            return this.pattern.RETURN(Projection.ALL).getCypher();
        }
    }

    protected C where(final String rawPredicate)
    {
        this.pattern.WHERE(rawPredicate);
        return self();
    }

    protected C where(final Predicate predicate)
    {
        this.pattern.WHERE(predicate);
        return self();
    }

    @SuppressWarnings("unchecked") protected final C self() {return (C) this;}

    public void addParameter(final String name, final Object value)
    {
        if (value instanceof Parameter) {
            throw new IllegalArgumentException("parameter '" + name + "' is defined with parameter '" + value + "'.");
        }
        if (null == this.actualParameters) {
            this.actualParameters = new HashMap<>();
        } else {
            if (this.actualParameters.containsKey(name)) {
                throw new IllegalStateException("parameter '" + name + "' is already defined.");
            }
        }
        this.actualParameters.put(name, value);
    }


    public void addRequiredParameter(final Parameter value) {addRequiredParameter(value.getName());}

    public void addRequiredParameter(final String name)
    {
        if (null == this.requiredParameters) {
            this.requiredParameters = new HashSet<>();
        }
        this.requiredParameters.add(name);
    }

    /* unnamed relationships ************************************************************************************************************************/
    public ResultCriteria relatedWith()
    {
        this.pattern.relatedWith();
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final NodeVariable n)
    {
        this.pattern.relatedWith(n);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final String name)
    {
        this.pattern.relatedWith(name);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final HasNodeLabels labels)
    {
        this.pattern.relatedWith(labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final String name, final HasNodeLabels labels)
    {
        this.pattern.relatedWith(name, labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.relatedWith(value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.relatedWith(name, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.relatedWith(labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.relatedWith(n, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria relatedWith(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.relatedWith(name, labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom()
    {
        this.pattern.incomingFrom();
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final NodeVariable n)
    {
        this.pattern.incomingFrom(n);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final String name)
    {
        this.pattern.incomingFrom(name);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final HasNodeLabels labels)
    {
        this.pattern.incomingFrom(labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final String name, final HasNodeLabels labels)
    {
        this.pattern.incomingFrom(name, labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incomingFrom(value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incomingFrom(name, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incomingFrom(labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incomingFrom(n, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria incomingFrom(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incomingFrom(name, labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo()
    {
        this.pattern.outgoingTo();
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final NodeVariable n)
    {
        this.pattern.outgoingTo(n);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final String name)
    {
        this.pattern.outgoingTo(name);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final HasNodeLabels labels)
    {
        this.pattern.outgoingTo(labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final String name, final HasNodeLabels labels)
    {
        this.pattern.outgoingTo(name, labels);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoingTo(value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoingTo(name, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoingTo(labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoingTo(n, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public ResultCriteria outgoingTo(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoingTo(name, labels, value, otherValues);
        return new ResultCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    /* relation variable definition *****************************************************************************************************************/
    public UndirectedRelationCriteria related(final RelationVariable r)
    {
        this.pattern.related(r);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name)
    {
        this.pattern.related(name);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final HasRelationTypes types)
    {
        this.pattern.related(types);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name, final HasRelationTypes types)
    {
        this.pattern.related(name, types);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(name, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(types, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(r, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.related(name, types, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name, final Hops hops)
    {
        this.pattern.related(name, hops);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final HasRelationTypes types, final Hops hops)
    {
        this.pattern.related(types, hops);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final RelationVariable r, final Hops hops)
    {
        this.pattern.related(r, hops);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name, final HasRelationTypes types, final Hops hops)
    {
        this.pattern.related(name, types, hops);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(hops, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.related(name, hops, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.related(types, hops, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(
        final RelationVariable r,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.related(r, hops, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.related(name, types, hops, value, otherValues);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public UndirectedRelationCriteria related(final Hops hops)
    {
        this.pattern.related(hops);
        return new UndirectedRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final RelationVariable r)
    {
        this.pattern.incoming(r);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name)
    {
        this.pattern.incoming(name);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final HasRelationTypes types)
    {
        this.pattern.incoming(types);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name, final HasRelationTypes types)
    {
        this.pattern.incoming(name, types);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(name, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(types, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(r, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.incoming(name, types, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name, final Hops hops)
    {
        this.pattern.incoming(name, hops);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final HasRelationTypes types, final Hops hops)
    {
        this.pattern.incoming(types, hops);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final RelationVariable r, final Hops hops)
    {
        this.pattern.incoming(r, hops);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name, final HasRelationTypes types, final Hops hops)
    {
        this.pattern.incoming(name, types, hops);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(hops, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.incoming(name, hops, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.incoming(types, hops, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(
        final RelationVariable r,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.incoming(r, hops, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.incoming(name, types, hops, value, otherValues);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public IncomingRelationCriteria incoming(final Hops hops)
    {
        this.pattern.incoming(hops);
        return new IncomingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final RelationVariable r)
    {
        this.pattern.outgoing(r);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name)
    {
        this.pattern.outgoing(name);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final HasRelationTypes types)
    {
        this.pattern.outgoing(types);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name, final HasRelationTypes types)
    {
        this.pattern.outgoing(name, types);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(name, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(types, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(r, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.outgoing(name, types, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name, final Hops hops)
    {
        this.pattern.outgoing(name, hops);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final HasRelationTypes types, final Hops hops)
    {
        this.pattern.outgoing(types, hops);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final RelationVariable r, final Hops hops)
    {
        this.pattern.outgoing(r, hops);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name, final HasRelationTypes types, final Hops hops)
    {
        this.pattern.outgoing(name, types, hops);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(hops, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        this.pattern.outgoing(name, hops, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.outgoing(types, hops, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(
        final RelationVariable r,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.outgoing(r, hops, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.outgoing(name, types, hops, value, otherValues);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    public OutgoingRelationCriteria outgoing(final Hops hops)
    {
        this.pattern.outgoing(hops);
        return new OutgoingRelationCriteria(this.pattern, this.actualParameters, this.requiredParameters);
    }

    /* criteria sequences ***************************************************************************************************************************/
    public C plus(final NodeVariable n)
    {
        this.pattern.plus(n);
        return self();
    }

    public C plus(final String name)
    {
        this.pattern.plus(name);
        return self();
    }

    public C plus(
        final NodeVariable n,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.plus(n, value, otherValues);
        return self();
    }

    public C plus(
        final String name,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.plus(name, value, otherValues);
        return self();
    }

    public C plus(final HasNodeLabels labels)
    {
        this.pattern.plus(labels);
        return self();
    }

    public C plus(
        final HasNodeLabels labels,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.plus(labels, value, otherValues);
        return self();
    }

    public C plus(final String name, final HasNodeLabels labels)
    {
        this.pattern.plus(name, labels);
        return self();
    }

    public C plus(
        final String name,
        final HasNodeLabels labels,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        this.pattern.plus(name, labels, value, otherValues);
        return self();
    }

    public C plusPath(final String name)
    {
        this.pattern.plusPath(name);
        return self();
    }

    public C plusPath(final String name, final PathFunction function)
    {
        this.pattern.plusPath(name, function);
        return self();
    }

    public C plusPath(final String name, final String function)
    {
        this.pattern.plusPath(name, function);
        return self();
    }
}

