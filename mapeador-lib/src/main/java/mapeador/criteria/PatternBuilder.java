/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.CypherDSL;
import mapeador.cypher.Expression;
import mapeador.cypher.Pattern;
import mapeador.cypher.Projection;
import mapeador.cypher.SortPredicate;

public abstract class PatternBuilder
{
    protected final Pattern pattern;

    protected PatternBuilder(final Pattern pattern) {this.pattern = pattern;}

    protected void returning(final FetchOptions fetchOptions)
    {
        returning(fetchOptions.projectDistinct(), fetchOptions.firstProjectionColumn(), fetchOptions.otherProjectionColumns());
    }

    protected void returning(final boolean projectDistinct, final Expression firstProjectionColumn, final Expression[] otherProjectionColumns)
    {
        if (!this.pattern.hasReturn()) {
            if (projectDistinct) {
                if (null != firstProjectionColumn) {
                    this.pattern.RETURN_DISTINCT(firstProjectionColumn, otherProjectionColumns);
                } else {
                    this.pattern.RETURN_DISTINCT(Projection.ALL);
                }
            } else {
                if (null != firstProjectionColumn) {
                    this.pattern.RETURN(firstProjectionColumn, otherProjectionColumns);
                } else {
                    this.pattern.RETURN(Projection.ALL);
                }
            }
        }
    }

    protected void orderBy(final FetchOptions fetchOptions) {orderBy(fetchOptions.firstSortPredicate(), fetchOptions.otherSortPredicates());}

    protected void orderBy(final SortPredicate firstSortPredicate, final SortPredicate[] otherSortPredicates)
    {
        if (null != firstSortPredicate) {
            if (this.pattern.hasReturn()) {
                if (!this.pattern.hasOrderBy()) {
                    this.pattern.ORDER_BY(firstSortPredicate, otherSortPredicates);
                }
            } else {
                this.pattern.RETURN(Projection.ALL).ORDER_BY(firstSortPredicate, otherSortPredicates);
            }
        }
    }

    protected void skip(final FetchOptions fetchOptions) {skip(fetchOptions.skip());}

    protected void skip(final int value)
    {
        if (this.pattern.hasReturn()) {
            if (!this.pattern.hasSkip()) {
                this.pattern.SKIP(CypherDSL.$(value));
            }
        } else {
            this.pattern.RETURN(Projection.ALL).SKIP(CypherDSL.$(value));
        }
    }

    protected void skip(final Expression value)
    {
        if (null != value) {
            if (this.pattern.hasReturn()) {
                if (!this.pattern.hasSkip()) {
                    this.pattern.SKIP(value);
                }
            } else {
                this.pattern.RETURN(Projection.ALL).SKIP(value);
            }
        }
    }

    protected void limit(final FetchOptions fetchOptions) {limit(fetchOptions.limit());}

    protected void limit(final int value)
    {
        if (this.pattern.hasReturn()) {
            if (!this.pattern.hasSkip()) {
                this.pattern.SKIP(CypherDSL.$(value));
            }
        } else {
            this.pattern.RETURN(Projection.ALL).SKIP(CypherDSL.$(value));
        }
    }

    protected void limit(final Expression value)
    {
        if (null != value) {
            if (this.pattern.hasReturn()) {
                if (!this.pattern.hasLimit()) {
                    this.pattern.LIMIT(value);
                }
            } else {
                this.pattern.RETURN(Projection.ALL).LIMIT(value);
            }
        }
    }
}
