/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.CypherDSL;
import mapeador.cypher.Expression;
import mapeador.cypher.Pattern;
import org.neo4j.driver.Result;
import org.neo4j.driver.Value;
import org.neo4j.driver.exceptions.NoSuchRecordException;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

public class ValueCriteria<T> extends Criteria<T, ValueCriteria<T>>
{
    private static final Expression ONE = CypherDSL.$(1);

    public ValueCriteria(final Pattern pattern, final Function<Result, Value> extractor, final Function<Value, T> interpreter)
    {
        super(pattern, extractor.andThen(value -> null == value ? null : interpreter.apply(value)));
    }

    protected ValueCriteria(
        final Pattern pattern,
        final Function<Result, Value> extractor,
        final Function<Value, T> interpreter,
        final Map<String, Object> actualParameters,
        final Set<String> requiredParameters)
    {
        super(pattern, extractor.andThen(value -> null == value ? null : interpreter.apply(value)), actualParameters, requiredParameters);
    }

    public static <T> ValueCriteria<T> required(final Pattern pattern, final Function<Value, T> interpreter)
    {
        Objects.requireNonNull(pattern, "pattern is required");
        Objects.requireNonNull(interpreter, "interpreter is required");
        return new ValueCriteria<>(pattern, ValueCriteria::getFirstAt, interpreter);
    }

    public static <T> ValueCriteria<T> notRequired(final Pattern pattern, final Function<Value, T> interpreter)
    {
        Objects.requireNonNull(pattern, "pattern is required");
        Objects.requireNonNull(interpreter, "interpreter is required");
        return new ValueCriteria<>(pattern, ValueCriteria::findFirstAt, interpreter);
    }

    @Override protected void prepareQuery(final FetchOptions fetchOptions)
    {
        returning(fetchOptions);
        orderBy(fetchOptions);
        skip(fetchOptions);
        limit(ONE);
    }

    protected static Value findFirstAt(final Result result)
    {
        if (result.hasNext()) {
            return result.next().get(0);
        } else {
            return null;
        }
    }

    protected static Value getFirstAt(final Result result)
    {
        if (!result.hasNext()) {
            throw new NoSuchRecordException("Cannot retrieve a single record, because this result is empty."); // FIXME: create specific Exception for this
        }
        return result.next().get(0);
    }
}
