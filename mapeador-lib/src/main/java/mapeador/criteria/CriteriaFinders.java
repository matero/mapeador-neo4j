/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.NodeVariable;
import mapeador.cypher.Pattern;
import mapeador.model.GraphNode;
import mapeador.model.HasNodeLabels;

import java.util.Map;
import java.util.Set;

public abstract class CriteriaFinders<V extends NodeVariable, N extends GraphNode<V, N>>
{
    protected CriteriaFinders() {}

    protected abstract GraphNode.Metadata<V, N> metadata();

    public void find(final Criteria<?, ?> criteria, final HasNodeLabels labels) {criteria.pattern.by(labels);}

    public void find(final Criteria<?, ?> criteria, final String nodeName) {criteria.pattern.by(nodeName, metadata());}

    public void find(final Criteria<?, ?> criteria, final String nodeName, final EntityFieldValue<?, ?> value)
    {
        criteria.pattern.by(nodeName, metadata(), value.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2)
    {
        criteria.pattern.by(nodeName, metadata(), v1.useParameter(criteria, metadata(), nodeName), v2.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName),
                            v5.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName),
                            v5.useParameter(criteria, metadata(), nodeName),
                            v6.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName),
                            v5.useParameter(criteria, metadata(), nodeName),
                            v6.useParameter(criteria, metadata(), nodeName),
                            v7.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7,
        final EntityFieldValue<?, ?> v8)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName),
                            v5.useParameter(criteria, metadata(), nodeName),
                            v6.useParameter(criteria, metadata(), nodeName),
                            v7.useParameter(criteria, metadata(), nodeName),
                            v8.useParameter(criteria, metadata(), nodeName));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final String nodeName,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7,
        final EntityFieldValue<?, ?> v8,
        final EntityFieldValue<?, ?> v9)
    {
        criteria.pattern.by(nodeName,
                            metadata(),
                            v1.useParameter(criteria, metadata(), nodeName),
                            v2.useParameter(criteria, metadata(), nodeName),
                            v3.useParameter(criteria, metadata(), nodeName),
                            v4.useParameter(criteria, metadata(), nodeName),
                            v5.useParameter(criteria, metadata(), nodeName),
                            v6.useParameter(criteria, metadata(), nodeName),
                            v7.useParameter(criteria, metadata(), nodeName),
                            v8.useParameter(criteria, metadata(), nodeName),
                            v9.useParameter(criteria, metadata(), nodeName));
    }

    public void find(final Criteria<?, ?> criteria, final NodeVariable node)
    {
        criteria.pattern.by(node);
    }

    public void find(final Criteria<?, ?> criteria, final NodeVariable node, final EntityFieldValue<?, ?> value)
    {
        criteria.pattern.by(node, value.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2)
    {
        criteria.pattern.by(node, v1.useParameter(criteria, metadata(), node), v2.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node),
                            v5.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node),
                            v5.useParameter(criteria, metadata(), node),
                            v6.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node),
                            v5.useParameter(criteria, metadata(), node),
                            v6.useParameter(criteria, metadata(), node),
                            v7.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7,
        final EntityFieldValue<?, ?> v8)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node),
                            v5.useParameter(criteria, metadata(), node),
                            v6.useParameter(criteria, metadata(), node),
                            v7.useParameter(criteria, metadata(), node),
                            v8.useParameter(criteria, metadata(), node));
    }

    public void find(
        final Criteria<?, ?> criteria,
        final NodeVariable node,
        final EntityFieldValue<?, ?> v1,
        final EntityFieldValue<?, ?> v2,
        final EntityFieldValue<?, ?> v3,
        final EntityFieldValue<?, ?> v4,
        final EntityFieldValue<?, ?> v5,
        final EntityFieldValue<?, ?> v6,
        final EntityFieldValue<?, ?> v7,
        final EntityFieldValue<?, ?> v8,
        final EntityFieldValue<?, ?> v9)
    {
        criteria.pattern.by(node,
                            v1.useParameter(criteria, metadata(), node),
                            v2.useParameter(criteria, metadata(), node),
                            v3.useParameter(criteria, metadata(), node),
                            v4.useParameter(criteria, metadata(), node),
                            v5.useParameter(criteria, metadata(), node),
                            v6.useParameter(criteria, metadata(), node),
                            v7.useParameter(criteria, metadata(), node),
                            v8.useParameter(criteria, metadata(), node),
                            v9.useParameter(criteria, metadata(), node));
    }

    protected final Pattern patternOf(final Criteria<?, ?> criteria) {return criteria.pattern;}

    protected final Set<String> requiredParametersOf(final Criteria<?, ?> criteria) {return criteria.getRequiredParameters();}

    protected final Map<String, Object> actualParametersOf(final Criteria<?, ?> criteria) {return criteria.getActualParameters();}
}
