/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.Expression;
import mapeador.cypher.Pattern;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

public class ResultCriteria extends Criteria<Result, ResultCriteria>
{
    private enum Get implements Function<Result, Result>
    {
        RESULT;

        @Override public Result apply(final Result result) {return result;}
    }

    public ResultCriteria(final Pattern pattern) {super(pattern, Get.RESULT);}

    public ResultCriteria(final Pattern pattern, final Map<String, Object> actualParameters, final Set<String> requiredParameters)
    {
        super(pattern, Get.RESULT, actualParameters, requiredParameters);
    }

    public Stream<Record> asStream() {return fetch().stream();}

    public Stream<Record> asStream(final FetchOptions fetchOptions) {return fetch(fetchOptions).stream();}

    public Stream<Record> asStream(final Session session) {return fetch(session).stream();}

    public Stream<Record> asStream(final Session session, final FetchOptions fetchOptions) {return fetch(session, fetchOptions).stream();}

    public Stream<Record> asStream(final Transaction tx) {return fetch(tx).stream();}

    public Stream<Record> asStream(final Transaction tx, final FetchOptions fetchOptions) {return fetch(tx, fetchOptions).stream();}

    public List<Record> asList() {return fetch().list();}

    public List<Record> asList(final FetchOptions fetchOptions) {return fetch(fetchOptions).list();}

    public List<Record> asList(final Session session) {return fetch(session).list();}

    public List<Record> asList(final Session session, final FetchOptions fetchOptions) {return fetch(session, fetchOptions).list();}

    public List<Record> asList(final Transaction tx) {return fetch(tx).list();}

    public List<Record> asList(final Transaction tx, final FetchOptions fetchOptions) {return fetch(tx, fetchOptions).list();}

    public <T> List<T> asList(final Function<Record, T> mapFunction) {return fetch().list(mapFunction);}

    public <T> List<T> asList(final FetchOptions fetchOptions, final Function<Record, T> mapFunction) {return fetch(fetchOptions).list(mapFunction);}

    public <T> List<T> asList(final Session session, final Function<Record, T> mapFunction) {return fetch(session).list(mapFunction);}

    public <T> List<T> asList(final Session session, final FetchOptions fetchOptions, final Function<Record, T> mapFunction)
    {
        return fetch(session, fetchOptions).list(mapFunction);
    }

    public <T> List<T> asList(final Transaction tx, final Function<Record, T> mapFunction) {return fetch(tx).list(mapFunction);}

    public <T> List<T> asList(final Transaction tx, final FetchOptions fetchOptions, final Function<Record, T> mapFunction)
    {
        return fetch(tx, fetchOptions).list(mapFunction);
    }

    @Override public ResultCriteria where(final String rawPredicate)
    {
        return super.where(rawPredicate);
    }

    @Override public ResultCriteria where(final Predicate predicate)
    {
        return super.where(predicate);
    }

    @Override protected void prepareQuery(final FetchOptions fetchOptions)
    {
        returning(fetchOptions);
        orderBy(fetchOptions);
        skip(fetchOptions);
        limit(fetchOptions);
    }
}
