/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;


import mapeador.cypher.Expression;
import mapeador.cypher.RawExpression;
import mapeador.cypher.SortPredicate;

import java.util.Objects;

public class OrderByFetchOptions extends SkipFetchOptions
{
    private SortPredicate firstSortPredicate;
    private SortPredicate[] otherSortPredicates;

    OrderByFetchOptions() {/*nothing to do*/}

    public OrderByFetchOptions(final SortPredicate firstSortPredicate, final SortPredicate[] otherSortPredicates)
    {
        this.firstSortPredicate = firstSortPredicate;
        this.otherSortPredicates = otherSortPredicates;
    }

    @Override public SortPredicate firstSortPredicate() {return this.firstSortPredicate;}

    @Override public SortPredicate[] otherSortPredicates() {return this.otherSortPredicates;}

    @Override public SkipFetchOptions skip(final int value) {return super.skip(value);}

    @Override public SkipFetchOptions skip(final String skip) {return super.skip(skip);}

    @Override public SkipFetchOptions skip(final Expression skipExpr) {return super.skip(skipExpr);}

    OrderByFetchOptions orderBy(final String sortPredicates)
    {
        Objects.requireNonNull(sortPredicates, "sortPredicates is required");
        if (sortPredicates.isEmpty()) {
            throw new IllegalArgumentException("sortPredicates can't be empty");
        }
        if (sortPredicates.isBlank()) {
            throw new IllegalArgumentException("sortPredicates can't be blank");
        }
        return orderBy(new RawExpression(sortPredicates.trim()));
    }

    OrderByFetchOptions orderBy(final SortPredicate firstSortPredicate, final SortPredicate... otherSortPredicates)
    {
        this.firstSortPredicate = Objects.requireNonNull(firstSortPredicate, "firstSortPredicate is required");
        this.otherSortPredicates = otherSortPredicates;
        return this;
    }
}
