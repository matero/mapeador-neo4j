/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.criteria;

import mapeador.cypher.CypherDSL;
import mapeador.cypher.Expression;
import mapeador.cypher.Projection;
import mapeador.cypher.RawExpression;
import mapeador.cypher.SortPredicate;
import org.neo4j.driver.Record;
import org.neo4j.driver.Value;

import java.util.Map;
import java.util.Objects;

public class LimitFetchOptions extends ParametersFetchOptions
{
    private Expression limit;

    LimitFetchOptions() {this(null);}

    LimitFetchOptions(final Expression limit) {this.limit = limit;}

    @Override public boolean projectDistinct() {return false;}

    @Override public Expression firstProjectionColumn() {return Projection.ALL;}

    @Override public Expression[] otherProjectionColumns() {return Projection.NO_COLUMNS;}

    @Override public SortPredicate firstSortPredicate() {return null;}

    @Override public SortPredicate[] otherSortPredicates() {return SortPredicate.NONE;}

    @Override public Expression skip() {return null;}

    @Override public Expression limit() {return this.limit;}

    @Override public ParametersFetchOptions parameters(final Map<String, Object> parameters) {return super.parameters(parameters);}

    @Override public ParametersFetchOptions parameters(final Value parameters) {return super.parameters(parameters);}

    @Override public ParametersFetchOptions parameters(final Record parameters) {return super.parameters(parameters);}

    @Override public ParametersFetchOptions parameters(final Object... parameters) {return super.parameters(parameters);}

    LimitFetchOptions limit(final int value) {return limit(CypherDSL.$(value));}

    LimitFetchOptions limit(final String limit)
    {
        Objects.requireNonNull(limit, "limit is required");
        if (limit.isEmpty()) {
            throw new IllegalArgumentException("limit can't be empty");
        }
        if (limit.isBlank()) {
            throw new IllegalArgumentException("limit can't be blank");
        }
        return limit(new RawExpression(limit));
    }

    LimitFetchOptions limit(final Expression limitExpr)
    {
        this.limit = Objects.requireNonNull(limitExpr, "limitExpr is required");
        return this;
    }
}

