/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

final class DefaultTypes implements Types
{
    private final List<Type> types;
    private final String cypher;
    private final String parameterClsses;

    DefaultTypes(final Collection<Type> types)
    {
        this.types = List.copyOf(types);
        this.cypher = makeCypherFor(this.types);
        this.parameterClsses = makePrameterClassesFor(this.types);
    }

    private static String makeCypherFor(final List<Type> types)
    {
        final var cypher = new StringBuilder(getRequiredCapacityFor(types));
        final var i = types.iterator();
        cypher.append(i.next().getCypher());
        while (i.hasNext()) {
            cypher.append('|');
            cypher.append(i.next().name());
        }
        return cypher.toString();
    }

    private static String makePrameterClassesFor(final List<Type> types)
    {
        final var parameterClasses = new StringBuilder(getRequiredCapacityFor(types));
        for (final var type:types) {
            parameterClasses.append(type.getParameterClasses());
        }
        return parameterClasses.toString();
    }

    private static int getRequiredCapacityFor(final List<Type> types)
    {
        int requiredCapacity = 0;
        for (final var label : types) {
            requiredCapacity += label.getCypher().length();
        }
        return requiredCapacity;
    }

    @Override public List<Type> getTypes() {return this.types;}

    @Override public Iterator<Type> iterator()
    {
        return this.types.iterator();
    }

    @Override public boolean contains(final Type type) {return this.types.contains(type);}

    @Override public String getCypher() {return this.cypher;}

    @Override public boolean isEmpty() {return this.types.isEmpty();}

    @Override public boolean isNotEmpty() {return !this.types.isEmpty();}

    @Override public int size()
    {
        return this.types.size();
    }

    @Override public DefaultTypes plus(final Types other)
    {
        if (null == other || other.isEmpty()) {
            return this;
        }
        final var types = new java.util.LinkedHashSet<Type>(size() + other.size());
        types.addAll(this.types);
        for (final var type : other) {
            types.add(type);
        }
        return new DefaultTypes(types);
    }

    @Override public String getParameterClasses() {return this.parameterClsses;}

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof DefaultTypes) {
            final var other = (DefaultTypes) o;

            return this.cypher.equals(other.cypher);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.cypher.hashCode();
    }

    @Override public String toString() {return this.cypher;}

}
