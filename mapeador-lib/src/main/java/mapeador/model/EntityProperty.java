/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import mapeador.criteria.EntityFieldValue;
import mapeador.cypher.Parameter;
import org.neo4j.driver.Record;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.Node;

import java.util.Objects;

public interface EntityProperty<E extends GraphEntity, T>
{
    Class<? extends E> getEntityClass();

    Class<T> getType();

    String getName();

    String getProperty();

    String getCanonicalName();

    T interpret(Value value);

    T of(Record record);

    T of(Record record, String alias);

    T of(Record record, int index);

    T of(Node node);

    default EntityFieldValue<E, T> eq(final T value)
    {
        Objects.requireNonNull(value, "value is required");
        return new EntityFieldValue<>(this, value);
    }

    default EntityFieldValue<E, T> eq(final Parameter parameter)
    {
        Objects.requireNonNull(parameter, "parameter is required");
        return new EntityFieldValue<>(this, parameter);
    }
}
