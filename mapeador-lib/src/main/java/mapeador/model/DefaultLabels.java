/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class DefaultLabels implements Labels
{
    private final List<Label> labels;
    private final String cypher;
    private final String parameterClasses;

    DefaultLabels(final Collection<Label> labels)
    {
        this.labels = List.copyOf(labels);
        this.cypher = makeCypherFor(this.labels);
        this.parameterClasses = makeParmeterClassesFor(this.labels);
    }

    private static String makeCypherFor(final List<Label> labels)
    {
        final var cypher = new StringBuilder(getRequiredCapacityFor(labels));
        for (final var label : labels) {
            cypher.append(label.getCypher());
        }
        return cypher.toString();
    }

    private String makeParmeterClassesFor(final List<Label> labels)
    {
        final var cypher = new StringBuilder(getRequiredCapacityFor(labels));
        for (final var label : labels) {
            cypher.append(label.getParameterClasses());
        }
        return cypher.toString();
    }

    private static int getRequiredCapacityFor(final List<Label> labels)
    {
        int requiredCapacity = 0;
        for (final var label : labels) {
            requiredCapacity += label.getCypher().length();
        }
        return requiredCapacity;
    }

    @Override public List<Label> getLabels() {return this.labels;}

    @Override public Iterator<Label> iterator()
    {
        return this.labels.iterator();
    }

    @Override public boolean contains(final Label label) {return this.labels.contains(label);}

    @Override public String getCypher() {return this.cypher;}

    @Override public boolean isEmpty() {return this.labels.isEmpty();}

    @Override public boolean isNotEmpty() {return !this.labels.isEmpty();}

    @Override public Labels plus(final Labels other)
    {
        if (null == other || other.isEmpty()) {
            return this;
        }
        final var labels = new java.util.LinkedHashSet<Label>(size() + other.size());
        labels.addAll(this.labels);
        for (final var label : other) {
            labels.add(label);
        }
        return new DefaultLabels(labels);
    }

    @Override public String getParameterClasses()
    {
        return this.parameterClasses;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof DefaultLabels) {
            final var other = (DefaultLabels) o;
            return this.cypher.equals(other.cypher);
        }
        return false;
    }

    @Override public int hashCode()
    {
        return this.cypher.hashCode();
    }

    @Override public String toString() {return this.cypher;}
}
