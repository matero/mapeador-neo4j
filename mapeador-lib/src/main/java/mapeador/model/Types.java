/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

public interface Types extends HasRelationTypes, Iterable<Type>
{
    @Override default Types getRelationTypes() {return this;}

    List<Type> getTypes();

    int size();

    @Override default Iterator<Type> iterator() {return getTypes().iterator();}

    boolean contains(Type type);

    String getCypher();

    boolean isEmpty();

    boolean isNotEmpty();

    Types plus(Types other);

    static Types empty() {return EmptyTypes.INSTANCE;}

    static Types of(final Type type)
    {
        Objects.requireNonNull(type, "type");

        // discard repeated type
        return type;
    }

    static Types of(final Type first, final Type... types)
    {
        if (null == types || 0 == types.length) {
            return of(first);
        }
        // discard repeated types
        final var resultTypes = new LinkedHashSet<Type>(types.length + 1);
        resultTypes.add(first);
        resultTypes.addAll(List.of(types));
        if (1 == resultTypes.size()) {
            return first;
        } else {
            return new DefaultTypes(resultTypes);
        }
    }

    static Types of(final Collection<Type> types)
    {
        Objects.requireNonNull(types, "types");

        if (types.isEmpty()) {
            return empty();
        }
        if (1 == types.size()) {
            return of(types.iterator().next());
        }
        // discard repeated types
        final var resultTypes = new LinkedHashSet<>(types);
        if (1 == resultTypes.size()) {
            return of(resultTypes.iterator().next());
        } else {
            return new DefaultTypes(resultTypes);
        }
    }
}

enum EmptyTypes implements Types
{
    INSTANCE;

    @Override public List<Type> getTypes()
    {
        return List.of();
    }

    @Override public Iterator<Type> iterator()
    {
        return getTypes().iterator();
    }

    @Override public boolean contains(final Type type)
    {
        return false;
    }

    @Override public String getCypher()
    {
        return "";
    }

    @Override public boolean isEmpty()
    {
        return true;
    }

    @Override public boolean isNotEmpty()
    {
        return false;
    }

    @Override public int size()
    {
        return 0;
    }

    @Override public Types plus(final Types types)
    {
        return types;
    }

    @Override public String getParameterClasses() {return null;}
}
