/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import java.util.List;

public interface Label extends Labels
{
    String name();

    default String getName() {return name();}

    @Override default List<Label> getLabels() {return List.of(this);}

    @Override default int size() {return 1;}

    @Override default boolean contains(final Label label) {return this.equals(label);}

    @Override default boolean isEmpty() {return false;}

    @Override default boolean isNotEmpty() {return true;}

    @Override default Labels plus(final Labels other)
    {
        if (null == other || other.isEmpty()) {
            return this;
        }
        if (other.contains(this)) {
            return other;
        }
        final var labels = new java.util.ArrayList<Label>(1 + other.size());
        labels.add(this);
        for (final var label : other) {
            labels.add(label);
        }
        return new DefaultLabels(labels);

    }
}
