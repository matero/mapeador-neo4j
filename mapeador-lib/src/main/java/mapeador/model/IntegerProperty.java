/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.model;

import mapeador.criteria.EntityFieldValue;
import org.neo4j.driver.Value;

import java.util.Objects;

public final class IntegerProperty<E extends GraphEntity> extends Property<E, Integer>
{
    public IntegerProperty(final Class<? extends E> entityClass, final String identifier, final String name, final String canonicalName)
    {
        super(entityClass, identifier, name, canonicalName);
    }

    @Override public final Class<Integer> getType() {return Integer.class;}

    @Override public final Integer interpretNonNull(final Value value) {return value.asInt();}

    public final EntityFieldValue<E, Integer> to(final byte value) {return to((int) value);}

    public final EntityFieldValue<E, Integer> to(final Byte value)
    {
        Objects.requireNonNull(value, "value is required");
        return to(value.intValue());
    }

    public final EntityFieldValue<E, Integer> to(final short value) {return to((int) value);}

    public final EntityFieldValue<E, Integer> to(final Short value)
    {
        Objects.requireNonNull(value, "value is required");
        return to(value.intValue());
    }

    public final EntityFieldValue<E, Integer> to(final int value) {return new EntityFieldValue<>(this, value);}
}

