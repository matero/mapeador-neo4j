 /*
  * Copyright 2020-2020 matero@gmail.com
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy of
  * this software and associated documentation files (the "Software"), to deal in
  * the Software without restriction, including without limitation the rights to
  * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
  * of the Software, and to permit persons to whom the Software is furnished to do
  * so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */
 package mapeador.model;

 import mapeador.cypher.Name;
 import mapeador.cypher.NodeVariable;

 import java.security.NoSuchAlgorithmException;
 import java.security.SecureRandom;

 public abstract class GraphNode<V extends NodeVariable, N extends GraphNode<V, N>> implements GraphEntity, GraphNodeSpec
 {
     protected GraphNode() {/*nothing to do*/}

     public abstract GraphNode.Metadata<V, N> getMetadata();

     @Override public final Labels getNodeLabels() {return getMetadata().getNodeLabels();}

     @Override public String getParameterClasses()
     {
         return getMetadata().getParameterClasses();
     }
     /*
      * finder templates
      */

     public interface Metadata<V extends NodeVariable, N extends GraphNode<V, N>> extends HasNodeLabels, EntityMetadata<N>
     {
         Property<N, ?> getIdProperty();

         Class<N> getModelClass();

         Class<V> getVariableClass();

         @Override default String getParameterClasses() {return Name.withoutBackticksNecessity(getNodeLabels().getCypher());}
     }

     protected enum VariableNameGenerator
     {
         INSTANCE;

         private static final String DEFAULT_NODE_VARIABLE_NAME_PREFIX = "n_";
         private static final int DEFAULT_VARIABLE_NAME_SUFFIX_BOUND = 9999;

         private final SecureRandom idGenerator;

         VariableNameGenerator()
         {
             try {
                 //Initialize SecureRandom
                 //This is a lengthy operation, to be done only upon initialization of the application
                 this.idGenerator = SecureRandom.getInstance("SHA1PRNG");
             } catch (final NoSuchAlgorithmException e) {
                 throw new IllegalStateException("Could not initialize NodeNameGenerator", e);
             }
         }

         public String nextName() {return nextName(DEFAULT_NODE_VARIABLE_NAME_PREFIX, DEFAULT_VARIABLE_NAME_SUFFIX_BOUND);}

         public String nextName(final int suffixBound) {return nextName(DEFAULT_NODE_VARIABLE_NAME_PREFIX, suffixBound);}

         public String nextName(final String prefix) {return nextName(prefix, DEFAULT_VARIABLE_NAME_SUFFIX_BOUND);}

         public String nextName(final String prefix, final int suffixBound)
         {
             return DEFAULT_NODE_VARIABLE_NAME_PREFIX + this.idGenerator.nextInt(suffixBound);
         }
     }
 }

