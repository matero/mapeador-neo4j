/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

public interface Labels extends HasNodeLabels, Iterable<Label>
{
    @Override default Labels getNodeLabels() {return this;}

    List<Label> getLabels();

    default int size() {return getLabels().size();}

    @Override default Iterator<Label> iterator() {return getLabels().iterator();}

    boolean contains(Label label);

    String getCypher();

    boolean isEmpty();

    boolean isNotEmpty();

    Labels plus(Labels other);

    static Labels empty() {return EmptyLabels.INSTANCE;}

    static Labels of(final Label label)
    {
        Objects.requireNonNull(label, "label");

        // discard repeated label
        return label;
    }

    static Labels of(final Label first, final Label... labels)
    {
        if (null == labels || 0 == labels.length) {
            return of(first);
        }
        // discard repeated labels
        final var resultLabels = new LinkedHashSet<Label>(labels.length + 1);
        resultLabels.add(first);
        resultLabels.addAll(List.of(labels));
        if (1 == resultLabels.size()) {
            return first;
        } else {
            return new DefaultLabels(resultLabels);
        }
    }

    static Labels of(final Collection<Label> labels)
    {
        Objects.requireNonNull(labels, "labels");

        if (labels.isEmpty()) {
            return empty();
        }
        if (1 == labels.size()) {
            return of(labels.iterator().next());
        }
        // discard repeated labels
        final var resultLabels = new LinkedHashSet<>(labels);
        if (1 == resultLabels.size()) {
            return of(resultLabels.iterator().next());
        } else {
            return new DefaultLabels(resultLabels);
        }
    }
}

enum EmptyLabels implements Labels
{
    INSTANCE;

    @Override public List<Label> getLabels() {return List.of();}

    @Override public int size() {return 0;}

    @Override public boolean contains(final Label label) {return false;}

    @Override public String getCypher() {return "";}

    @Override public boolean isEmpty() {return true;}

    @Override public boolean isNotEmpty() {return false;}

    @Override public Labels plus(final Labels other) {return null == other ? this : other;}

    @Override public String getParameterClasses() {return null;}
}
