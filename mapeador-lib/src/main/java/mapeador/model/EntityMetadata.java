/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.model;

import mapeador.cypher.Name;

import java.util.List;
import java.util.Objects;

public interface EntityMetadata<E extends GraphEntity> extends HasParameterClasses
{
    List<Property<E, ?>> getProperties();

    boolean hasPropertyNamed(String name);

    Property<E, ?> getProperty(String name);

    default String buildParameterName(final String propertyName)
    {
        Objects.requireNonNull(propertyName, "propertyName is required");
        if (propertyName.isEmpty()) {
            throw new IllegalArgumentException("propertyName can not be empty");
        }
        if (propertyName.isBlank()) {
            throw new IllegalArgumentException("propertyName can not be blank");
        }
        return getParameterClasses() + '_' + Name.withoutBackticksNecessity(propertyName);
    }

    default String buildParameterName(final String entityName, final String propertyName)
    {
        Objects.requireNonNull(entityName, "nodeName is required");
        Objects.requireNonNull(propertyName, "propertyName is required");
        if (entityName.isEmpty()) {
            throw new IllegalArgumentException("nodeName can not be empty");
        }
        if (entityName.isBlank()) {
            throw new IllegalArgumentException("nodeName can not be blank");
        }
        if (propertyName.isEmpty()) {
            throw new IllegalArgumentException("propertyName can not be empty");
        }
        if (propertyName.isBlank()) {
            throw new IllegalArgumentException("propertyName can not be blank");
        }
        return Name.withoutBackticksNecessity(entityName) + '_' + getParameterClasses() + '_' + Name.withoutBackticksNecessity(propertyName);
    }
}
