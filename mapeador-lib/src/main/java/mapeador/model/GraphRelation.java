/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.model;

import mapeador.cypher.Name;
import mapeador.cypher.NodeVariable;
import mapeador.cypher.RelationVariable;

public abstract class GraphRelation<
    FV extends NodeVariable, FN extends GraphNode<FV, FN>,
    TV extends NodeVariable, TN extends GraphNode<TV, TN>,
    V extends RelationVariable, R extends GraphRelation<FV, FN, TV, TN, V, R>> implements GraphEntity, GraphRelationSpec
{
    @Override public final Types getRelationTypes() {return getMetadata().getRelationTypes();}

    @Override public String getParameterClasses()
    {
        return getMetadata().getParameterClasses();
    }

    public abstract GraphRelation.Metadata<FV, FN, TV, TN, V, R> getMetadata();

    public interface Metadata<
        FV extends NodeVariable, FN extends GraphNode<FV, FN>,
        TV extends NodeVariable, TN extends GraphNode<TV, TN>,
        V extends RelationVariable, R extends GraphRelation<FV, FN, TV, TN, V, R>> extends HasRelationTypes, EntityMetadata<R>
    {

        Class<R> getRelationshipClass();

        Class<V> getVariableClass();

        @Override default String getParameterClasses() {return Name.withoutBackticksNecessity(getRelationTypes().getCypher());}
    }
}
