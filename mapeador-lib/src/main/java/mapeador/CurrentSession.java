/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador;

import org.neo4j.driver.Driver;
import org.neo4j.driver.Session;
import org.neo4j.driver.SessionConfig;

public final class CurrentSession
{
    private static Driver neo4j;
    private static SessionConfig sessionConfig;

    private static ThreadLocal<Session> session = new ThreadLocal<>()
    {
        @Override protected Session initialValue()
        {
            return neo4j.session(sessionConfig);
        }

        @Override public void remove()
        {
            get().close();
            set(null);
        }
    };

    private CurrentSession() {throw new UnsupportedOperationException();}

    public static Session get() {return session.get();}

    public static synchronized void init(final Driver db)
    {
        init(db, SessionConfig.defaultConfig());
    }

    public static synchronized void init(final Driver db, final SessionConfig config)
    {
        neo4j = db;
        sessionConfig = config;
    }
}
