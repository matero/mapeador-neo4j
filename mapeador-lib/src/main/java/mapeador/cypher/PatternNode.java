/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.model.HasNodeLabels;
import mapeador.model.HasRelationTypes;

public interface PatternNode extends CypherBuilder, WithReturn, WithWhere, Predicate
{
    //
    // unnamed relationships between nodes
    //
    PatternNode relatedWith();

    PatternNode relatedWith(NodeVariable n);

    PatternNode relatedWith(String name);

    PatternNode relatedWith(HasNodeLabels labels);

    PatternNode relatedWith(String name, HasNodeLabels labels);

    PatternNode relatedWith(PropertyValue value, PropertyValue... otherValues);

    PatternNode relatedWith(String name, PropertyValue value, PropertyValue... otherValues);

    PatternNode relatedWith(HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode relatedWith(NodeVariable n, PropertyValue value, PropertyValue... otherValues);

    PatternNode relatedWith(String name, HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode incomingFrom();

    PatternNode incomingFrom(NodeVariable n);

    PatternNode incomingFrom(String name);

    PatternNode incomingFrom(HasNodeLabels labels);

    PatternNode incomingFrom(String name, HasNodeLabels labels);

    PatternNode incomingFrom(PropertyValue value, PropertyValue... otherValues);

    PatternNode incomingFrom(String name, PropertyValue value, PropertyValue... otherValues);

    PatternNode incomingFrom(HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode incomingFrom(NodeVariable n, PropertyValue value, PropertyValue... otherValues);

    PatternNode incomingFrom(String name, HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode outgoingTo();

    PatternNode outgoingTo(NodeVariable n);

    PatternNode outgoingTo(String name);

    PatternNode outgoingTo(HasNodeLabels labels);

    PatternNode outgoingTo(String name, HasNodeLabels labels);

    PatternNode outgoingTo(PropertyValue value, PropertyValue... otherValues);

    PatternNode outgoingTo(String name, PropertyValue value, PropertyValue... otherValues);

    PatternNode outgoingTo(HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode outgoingTo(NodeVariable n, PropertyValue value, PropertyValue... otherValues);

    PatternNode outgoingTo(String name, HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    //
    // relation variable definition
    //
    UndirectedRelation related(RelationVariable r);

    UndirectedRelation related(String name);

    UndirectedRelation related(HasRelationTypes types);

    UndirectedRelation related(String name, HasRelationTypes types);

    UndirectedRelation related(PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(String name, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(RelationVariable r, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(String name, HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(String name, Hops hops);

    UndirectedRelation related(HasRelationTypes types, Hops hops);

    UndirectedRelation related(RelationVariable r, Hops hops);

    UndirectedRelation related(String name, HasRelationTypes types, Hops hops);

    UndirectedRelation related(Hops hops, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(String name, Hops hops, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(RelationVariable r, Hops hops, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(String name, HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    UndirectedRelation related(Hops hops);

    IncomingRelation incoming(RelationVariable r);

    IncomingRelation incoming(String name);

    IncomingRelation incoming(HasRelationTypes types);

    IncomingRelation incoming(String name, HasRelationTypes types);

    IncomingRelation incoming(PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(String name, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(RelationVariable r, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(String name, HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(String name, Hops hops);

    IncomingRelation incoming(HasRelationTypes types, Hops hops);

    IncomingRelation incoming(RelationVariable r, Hops hops);

    IncomingRelation incoming(String name, HasRelationTypes types, Hops hops);

    IncomingRelation incoming(Hops hops, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(String name, Hops hops, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(RelationVariable r, Hops hops, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(String name, HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    IncomingRelation incoming(Hops hops);

    OutgoingRelation outgoing(RelationVariable r);

    OutgoingRelation outgoing(String name);

    OutgoingRelation outgoing(HasRelationTypes types);

    OutgoingRelation outgoing(String name, HasRelationTypes types);

    OutgoingRelation outgoing(PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(String name, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(RelationVariable r, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(String name, HasRelationTypes types, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(String name, Hops hops);

    OutgoingRelation outgoing(HasRelationTypes types, Hops hops);

    OutgoingRelation outgoing(RelationVariable r, Hops hops);

    OutgoingRelation outgoing(String name, HasRelationTypes types, Hops hops);

    OutgoingRelation outgoing(Hops hops, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(String name, Hops hops, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(RelationVariable r, Hops hops, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(String name, HasRelationTypes types, Hops hops, PropertyValue value, PropertyValue... otherValues);

    OutgoingRelation outgoing(Hops hops);

    //
    // comma separated patterns
    //
    PatternNode plus(NodeVariable n);

    PatternNode plus(String name);

    PatternNode plus(String name, PropertyValue value, PropertyValue... otherValues);

    PatternNode plus(HasNodeLabels labels);

    PatternNode plus(HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PatternNode plus(String name, HasNodeLabels labels);

    PatternNode plus(NodeVariable n, PropertyValue value, PropertyValue... otherValues);

    PatternNode plus(String name, HasNodeLabels labels, PropertyValue value, PropertyValue... otherValues);

    PathVariable plusPath(String name);

    PathVariable plusPath(String name, PathFunction function);

    PathVariable plusPath(String name, String function);
}
