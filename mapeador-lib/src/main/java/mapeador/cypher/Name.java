/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;
import java.util.regex.Pattern;

public final class Name
{
    private Name() {throw new UnsupportedOperationException("Name should not be instantiated.");}

    public static final char BACKTICK = '`';

    public static String at(final String name)
    {
        Objects.requireNonNull(name, "name is required");
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name can not be empty");
        }
        if (name.isBlank()) {
            throw new IllegalArgumentException("name can not be blank");
        }
        return normalize(name);
    }

    static String normalize(final String id)
    {
        if (requireBackticks(id)) {
            return BACKTICK + id + BACKTICK;
        }
        return id;
    }

    public static boolean requireBackticks(final String s)
    {
        if (isCypherReserverKeyword(s)) {
            return true;
        }

        final var chars = s.toCharArray();

        final int size = chars.length;
        if (BACKTICK == chars[0] && BACKTICK == chars[size - 1]) {
            return false;
        }
        if (!Character.isLetter(chars[0])) {
            return true;
        }
        for (int i = 1; i < size; i++) {
            if (requireBackticks(chars[i])) {
                return true;
            }
        }

        return false;
    }

    // based on definitions at: https://neo4j.com/docs/cypher-manual/current/syntax/reserved/
    private static boolean isCypherReserverKeyword(final String s)
    {
        switch (s.toUpperCase()) {
            //
            // Clauses
            //
            case "CALL":
            case "CREATE":
            case "DELETE":
            case "DETACH":
            case "EXISTS":
            case "FOREACH":
            case "LOAD":
            case "MATCH":
            case "MERGE":
            case "OPTIONAL":
            case "REMOVE":
            case "RETURN":
            case "SET":
            case "START":
            case "UNION":
            case "UNWIND":
            case "WITH":

                //
                // Subclauses
                //
            case "LIMIT":
            case "ORDER":
            case "SKIP":
            case "WHERE":
            case "YIELD":

                //
                // Modifiers
                //
            case "ASC":
            case "ASCENDING":
            case "ASSERT":
            case "BY":
            case "CSV":
            case "DESC":
            case "DESCENDING":
            case "ON":

                //
                // Expressions
                //
            case "ALL":
            case "CASE":
            case "ELSE":
            case "END":
            case "THEN":
            case "WHEN":

                //
                // Operators
                //
            case "AND":
            case "AS":
            case "CONTAINS":
            case "DISTINCT":
            case "ENDS":
            case "IN":
            case "IS":
            case "NOT":
            case "OR":
            case "STARTS":
            case "XOR":

                //
                // Schema
                //
            case "CONSTRAINT":
                //case "CREATE":
            case "DROP":
                //case "EXISTS":
            case "INDEX":
            case "NODE":
            case "KEY":
            case "UNIQUE":

                //
                // Hints
                //
                //case "INDEX":
            case "JOIN":
            case "PERIODIC":
            case "COMMIT":
            case "SCAN":
            case "USING":

                //
                // Literals
                //
            case "FALSE":
            case "NULL":
            case "TRUE":

                //
                // Reserved for future use
                //
            case "ADD":
            case "DO":
            case "FOR":
            case "MANDATORY":
            case "OF":
            case "REQUIRE":
            case "SCALAR":
                return true;

            default:
                return false;
        }
    }

    private static boolean requireBackticks(final char c) {return !(Character.isLetterOrDigit(c) || '_' == c);}

    private static final Pattern NO_IDENTIFIER_CHARS = Pattern.compile("[\\W]|_");

    public static String withoutBackticksNecessity(final String name)
    {
        if (null == name || !Character.isLetter(name.charAt(0))) {
            return name;
        }
        return NO_IDENTIFIER_CHARS.matcher(name).replaceAll("");
    }
}
