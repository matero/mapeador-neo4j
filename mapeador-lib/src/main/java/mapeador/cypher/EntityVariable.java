/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.model.HasParameterClasses;

import java.util.Objects;

public abstract class EntityVariable implements Expression.WithAlias, HasParameterClasses, Expression.Comparable
{
    private final String name;

    EntityVariable(final String name) {this.name = name;}

    public final String getName() {return this.name;}

    @Override public abstract String getParameterClasses();

    @Override public void appendTo(final StringBuilder builder)
    {
        if (null != this.name) {
            builder.append(this.name);
        }
    }

    public Property get(final String name) {return new StaticProperty(withName(name));}


    public Property at(final Expression expr) {return new DynamicProperty(Objects.requireNonNull(expr, "expr is required"));}

    public interface Property extends Predicate, Expression.WithAlias, StringExpression {}

    protected static String withName(final String name) {return Name.at(name);}

    protected final class StaticProperty implements Property
    {
        private final String name;

        public StaticProperty(final String name) {this.name = name;}

        @Override public void appendTo(final StringBuilder builder)
        {
            builder.append(EntityVariable.this.name);
            builder.append('.').append(this.name);
        }

        @Override public Groupable group() {return this;}
    }

    protected final class DynamicProperty implements Property
    {
        private final Expression expr;

        DynamicProperty(final Expression expr) {this.expr = expr;}

        @Override public void appendTo(final StringBuilder builder)
        {
            builder.append(EntityVariable.this.name);
            builder.append('[');
            this.expr.appendTo(builder);
            builder.append(']');
        }

        @Override public Groupable group() {return this;}
    }
}
