/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.CurrentSession;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public interface CypherBuilder
{
    String getCypher();

    default Result run() {return run(CurrentSession.get());}

    default Result run(final Session s) {return s.readTransaction(tx -> run(tx));}

    default Result run(final Transaction tx) {return tx.run(getCypher());}

    default Record single() {return single(CurrentSession.get());}

    default Record single(final Session s) {return s.readTransaction(tx -> single(tx));}

    default Record single(final Transaction tx) {return run(tx).single();}

    default Stream<Record> stream() {return stream(CurrentSession.get());}

    default Stream<Record> stream(final Session s) {return s.readTransaction(tx -> stream(tx));}

    default Stream<Record> stream(final Transaction tx) {return run(tx).stream();}

    default List<Record> list() {return list(CurrentSession.get());}

    default List<Record> list(final Session s) {return s.readTransaction(tx -> list(tx));}

    default List<Record> list(final Transaction tx) {return run(tx).list();}

    default <T> List<T> list(final Function<Record, T> mapFunction) {return list(CurrentSession.get(), mapFunction);}

    default <T> List<T> list(final Session s, final Function<Record, T> mapFunction) {return s.readTransaction(tx -> list(tx, mapFunction));}

    default <T> List<T> list(final Transaction tx, final Function<Record, T> mapFunction) {return run(tx).list(mapFunction);}
}
