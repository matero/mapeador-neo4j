/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.model.HasNodeLabels;
import mapeador.model.Types;

import java.util.Collection;
import java.util.Objects;

import static mapeador.cypher.PredefinedFunctions.COUNT;
import static mapeador.cypher.PredefinedFunctions.COUNT_ALL;
import static mapeador.cypher.PredefinedFunctions.ID;
import static mapeador.cypher.PredefinedFunctions.TYPE;
import static mapeador.cypher.PredefinedFunctions.call;

public final class CypherDSL
{
    private CypherDSL() {throw new UnsupportedOperationException("CypherDSL can not be instantiated."); }

    public static NodeVariable node(final String name) {return NodeVariable.of(name);}

    public static NodeVariable node(final String name, final HasNodeLabels labels) {return NodeVariable.of(name, labels);}

    public static RelationVariable rel(final String name) {return RelationVariable.of(name);}

    public static RelationVariable rel(final String name, final Types types) {return RelationVariable.of(name, types);}

    public static Parameter p(final String name) {return Parameter.of(name);}

    public static Parameter p(final int index) {return Parameter.of(index);}

    /* n Patterns *******************************************************************************************************************************/
    public static PatternNode node() {return Pattern.node().by();}

    public static PatternNode n(final NodeVariable n) {return Pattern.node().by(n);}

    public static PatternNode n(final String name) {return Pattern.node().by(name);}

    public static PatternNode n(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.node().by(n, value, otherValues);
    }

    public static PatternNode n(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.node().by(name, value, otherValues);
    }

    public static PatternNode n(final HasNodeLabels labels) {return Pattern.node().by(labels);}

    public static PatternNode n(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.node().by(labels, value, otherValues);
    }

    public static PatternNode n(final String name, final HasNodeLabels labels) {return Pattern.node().by(name, labels);}

    public static PatternNode n(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.node().by(name, labels, value, otherValues);
    }

    /* MATCH Patterns *******************************************************************************************************************************/
    public static PatternNode MATCH() {return Pattern.MATCH().by();}

    public static PatternNode MATCH(final NodeVariable n) {return Pattern.MATCH().by(n);}

    public static PatternNode MATCH(final String name) {return Pattern.MATCH().by(name);}

    public static PatternNode MATCH(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.MATCH().by(n, value, otherValues);
    }

    public static PatternNode MATCH(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.MATCH().by(name, value, otherValues);
    }

    public static PatternNode MATCH(final HasNodeLabels labels) {return Pattern.MATCH().by(labels);}

    public static PatternNode MATCH(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.MATCH().by(labels, value, otherValues);
    }

    public static PatternNode MATCH(final String name, final HasNodeLabels labels) {return Pattern.MATCH().by(name, labels);}

    public static PatternNode MATCH(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.MATCH().by(name, labels, value, otherValues);
    }

    public static final class MATCH
    {
        public static PathVariable path(final String name) {return Pattern.MATCH().path(name);}

        public static PathVariable shortestPath(final String name) {return path(name, PathFunction.shortestPath);}

        public static PathVariable allShortestPaths(final String name) {return path(name, PathFunction.allShortestPaths);}

        public static PathVariable path(final String name, final PathFunction function) {return Pattern.MATCH().path(name, function);}
    }

    /* OPTIONAL MATCH Patterns **********************************************************************************************************************/
    public static PatternNode OPTIONAL_MATCH() {return Pattern.OPTIONAL_MATCH();}

    public static PatternNode OPTIONAL_MATCH(final NodeVariable n) {return Pattern.OPTIONAL_MATCH().by(n);}

    public static PatternNode OPTIONAL_MATCH(final String name) {return Pattern.OPTIONAL_MATCH().by(name);}

    public static PatternNode OPTIONAL_MATCH(
        final NodeVariable n,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {return Pattern.OPTIONAL_MATCH().by(n, value, otherValues);}

    public static PatternNode OPTIONAL_MATCH(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.OPTIONAL_MATCH().by(name, value, otherValues);
    }

    public static PatternNode OPTIONAL_MATCH(final HasNodeLabels labels) {return Pattern.OPTIONAL_MATCH().by(labels);}

    public static PatternNode OPTIONAL_MATCH(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return Pattern.OPTIONAL_MATCH().by(labels, value, otherValues);
    }

    public static PatternNode OPTIONAL_MATCH(final String name, final HasNodeLabels labels) {return Pattern.OPTIONAL_MATCH().by(name, labels);}

    public static PatternNode OPTIONAL_MATCH(
        final String name,
        final HasNodeLabels labels,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return Pattern.OPTIONAL_MATCH().by(name, labels, value, otherValues);
    }

    public static final class OPTIONAL_MATCH
    {
        public static PathVariable path(final String name) {return Pattern.OPTIONAL_MATCH().path(name);}
    }

    /* Relation variable hops ***********************************************************************************************************************/
    public static Hops hops(final int min, final int max)
    {
        if (0 > min) {
            throw new IllegalArgumentException("min must be >= 0");
        }
        if (0 > max) {
            throw new IllegalArgumentException("max must be >= 0");
        }
        return new Hops(min, max);
    }

    public static Hops hops(final int value)
    {
        if (0 > value) {
            throw new IllegalArgumentException("value must be >= 0");
        }
        return new Hops(value, value);
    }

    public static Hops minHops(final int value)
    {
        if (0 > value) {
            throw new IllegalArgumentException("value must be >= 0");
        }
        return new Hops(value, Hops.ANY);
    }

    public static Hops maxHops(final int value)
    {
        if (0 > value) {
            throw new IllegalArgumentException("value must be >= 0");
        }
        return new Hops(Hops.ANY, value);
    }

    public static Hops anyHops() {return Hops.Unique.FROM_ZERO_TO_INFINITE;}

    /* FUNCTIONS ************************************************************************************************************************************/
    public static FunctionCall countAll() {return COUNT_ALL;}

    public static FunctionCall count(final Expression arg) {return call(COUNT, arg);}

    public static FunctionCall count(final String variableName) {return call(COUNT, variableName);}

    public static FunctionCall count(final EntityVariable variable) {return call(ID, variable);}

    public static FunctionCall id(final Expression expression) {return call(ID, expression);}

    public static FunctionCall id(final String variableName) {return call(ID, variableName);}

    public static FunctionCall id(final EntityVariable entityVariable) {return call(ID, entityVariable);}

    public static FunctionCall type(final Expression expression) {return call(TYPE, expression);}

    public static FunctionCall type(final String variableName) {return call(TYPE, variableName);}

    public static FunctionCall type(final RelationVariable relation) {return call(TYPE, relation);}

    /* Expressions **********************************************************************************************************************************/
    public static RawExpression r(final String content)
    {
        Objects.requireNonNull(content, "content is required.");
        if (content.isEmpty()) {
            throw new IllegalArgumentException("content can't be empty.");
        }
        if (content.isBlank()) {
            throw new IllegalArgumentException("content can't be blank.");
        }
        return new RawExpression(content.trim());
    }

    /* Predicates ***********************************************************************************************************************************/
    public static Predicate NOT(final Predicate operand)
    {
        return NotPredicate.of(Objects.requireNonNull(operand, "operand is required"));
    }

    static Predicate AND(final Predicate first, final Predicate second)
    {
        Objects.requireNonNull(first, "first is required");
        return first.AND(second);
    }

    static Predicate AND(
        final Predicate first,
        final Predicate second,
        final Predicate... otherPredicates)
    {
        Objects.requireNonNull(first, "first is required");
        return first.AND(second, otherPredicates);
    }

    static Predicate OR(final Predicate first, final Predicate second)
    {
        Objects.requireNonNull(first, "first is required");
        return first.OR(second);
    }

    static Predicate OR(final Predicate first, final Predicate second, final Predicate... otherPredicates)
    {
        Objects.requireNonNull(first, "first is required");
        return first.OR(second, otherPredicates);
    }

    static Predicate XOR(final Predicate first, final Predicate second)
    {
        Objects.requireNonNull(first, "first is required");
        return first.XOR(second);
    }

    static Predicate XOR(
        final Predicate first,
        final Predicate second,
        final Predicate... otherPredicates)
    {
        Objects.requireNonNull(first, "first is required");
        return first.XOR(second, otherPredicates);
    }

    /* PropertyValue ********************************************************************************************************************************/
    public static PropertyValue value(final String name, final Object value)
    {
        Objects.requireNonNull(value, "value is required");
        if (value instanceof Expression) {
            if (!(value instanceof Parameter || value instanceof Literal)) {
                throw new IllegalArgumentException("value can be a java object, literals or a parameters, NOT an expression, received: " + value);
            }
        }
        return new PropertyValue(Name.at(name), value);
    }

    /* Literals *************************************************************************************************************************************/
    public static Literal literal(final Object value)
    {
        if (null == value) {
            return Literal.NULL;
        }
        if (value instanceof Literal) {
            return (Literal) value;
        }
        if (value instanceof Boolean) {
            return $((boolean) value);
        }
        if (value instanceof String) {
            return Literal.StringLiteral.ofNoNull(value.toString());
        }
        if (value instanceof Character) {
            return $((char) value);
        }
        if (value instanceof Long) {
            return $((long) value);
        }
        if (value instanceof Short) {
            return $((short) value);
        }
        if (value instanceof Byte) {
            return $((byte) value);
        }
        if (value instanceof Integer) {
            return $((int) value);
        }
        if (value instanceof Double) {
            return $((double) value);
        }
        if (value instanceof Float) {
            return $((float) value);
        }
        if (value instanceof Collection<?>) {
            return Literal.ObjectListLiteral.of((Collection<?>) value);
        }
        if (value instanceof Expression[]) {
            return $((Expression[]) value);
        }
        if (value instanceof byte[]) {
            return $((byte[]) value);
        }
        if (value instanceof Byte[]) {
            return $((Byte[]) value);
        }
        if (value instanceof boolean[]) {
            return $((boolean[]) value);
        }
        if (value instanceof Boolean[]) {
            return $((Boolean[]) value);
        }
        if (value instanceof String[]) {
            return $((String[]) value);
        }
        if (value instanceof long[]) {
            return $((long[]) value);
        }
        if (value instanceof Long[]) {
            return $((Long[]) value);
        }
        if (value instanceof int[]) {
            return $((int[]) value);
        }
        if (value instanceof Integer[]) {
            return $((Integer[]) value);
        }
        if (value instanceof double[]) {
            return $((double[]) value);
        }
        if (value instanceof Double[]) {
            return $((Double[]) value);
        }
        if (value instanceof float[]) {
            return $((float[]) value);
        }
        if (value instanceof Float[]) {
            return $((Float[]) value);
        }
        if (value instanceof Object[]) {
            return $((Object[]) value);
        }

        // FIXME: add values supported by neo4j Values class, like Map, and java Time classes

        throw new IllegalArgumentException("Unable to convert " + value.getClass().getName() + " to Neo4j Value.");
    }

    public static Literal.BooleanLiteral $(final boolean value) {return Literal.BooleanLiteral.of(value);}

    public static Literal.BooleanLiteral $(final Boolean value) {return Literal.BooleanLiteral.of(value);}

    public static Literal.StringLiteral $(final String value) {return Literal.StringLiteral.of(value);}

    public static Literal.StringLiteral $(final char value) {return Literal.StringLiteral.of(value);}

    public static Literal.StringLiteral $(final Character value) {return Literal.StringLiteral.of(value);}

    public static Literal.ByteLiteral $(final byte value) {return Literal.ByteLiteral.of(value);}

    public static Literal.ByteLiteral $(final Byte value) {return Literal.ByteLiteral.of(value);}

    public static Literal.ShortLiteral $(final short value) {return Literal.ShortLiteral.of(value);}

    public static Literal.ShortLiteral $(final Short value) {return Literal.ShortLiteral.of(value);}

    public static Literal.IntegerLiteral $(final int value) {return Literal.IntegerLiteral.of(value);}

    public static Literal.IntegerLiteral $(final Integer value) {return Literal.IntegerLiteral.of(value);}

    public static Literal.LongLiteral $(final long value) {return Literal.LongLiteral.of(value);}

    public static Literal.LongLiteral $(final Long value) {return Literal.LongLiteral.of(value);}

    public static Literal.FloatLiteral $(final float value) {return Literal.FloatLiteral.of(value);}

    public static Literal.FloatLiteral $(final Float value) {return Literal.FloatLiteral.of(value);}

    public static Literal.DoubleLiteral $(final double value) {return Literal.DoubleLiteral.of(value);}

    public static Literal.DoubleLiteral $(final Double value) {return Literal.DoubleLiteral.of(value);}

    public static Literal.ObjectListLiteral $(final Object... values) {return Literal.ObjectListLiteral.of(values);}

    public static Literal.ObjectListLiteral $(final Collection<?> values)
    {
        return Literal.ObjectListLiteral.of(Objects.requireNonNull(values, "values is required"));
    }

    public static Literal.ExpressionListLiteral $(final Expression... values) {return Literal.ExpressionListLiteral.of(values);}

    public static Literal.BooleanListLiteral $(final boolean... values) {return Literal.BooleanListLiteral.of(values);}

    public static Literal.BooleanListLiteral $(final Boolean... values) {return Literal.BooleanListLiteral.of(values);}

    public static Literal.ByteListLiteral $(final byte... values) {return Literal.ByteListLiteral.of(values);}

    public static Literal.ByteListLiteral $(final Byte... values) {return Literal.ByteListLiteral.of(values);}

    public static Literal.ShortListLiteral $(final short... values) {return Literal.ShortListLiteral.of(values);}

    public static Literal.ShortListLiteral $(final Short... values) {return Literal.ShortListLiteral.of(values);}

    public static Literal.IntegerListLiteral $(final int... values) {return Literal.IntegerListLiteral.of(values);}

    public static Literal.IntegerListLiteral $(final Integer... values) {return Literal.IntegerListLiteral.of(values);}

    public static Literal.LongListLiteral $(final long... values) {return Literal.LongListLiteral.of(values);}

    public static Literal.LongListLiteral $(final Long... values) {return Literal.LongListLiteral.of(values);}

    public static Literal.FloatListLiteral $(final float... values) {return Literal.FloatListLiteral.of(values);}

    public static Literal.FloatListLiteral $(final Float... values) {return Literal.FloatListLiteral.of(values);}

    public static Literal.DoubleListLiteral $(final double... values) {return Literal.DoubleListLiteral.of(values);}

    public static Literal.DoubleListLiteral $(final Double... values) {return Literal.DoubleListLiteral.of(values);}

    public static Literal.StringListLiteral $(final String... values) {return Literal.StringListLiteral.of(values);}
}

final class PredefinedFunctions
{
    static final String COUNT = "count";
    static final String ID = "id";
    static final String TYPE = "type";

    static final FunctionCall COUNT_ALL = call(COUNT, new RawExpression("*"));

    static final FunctionCall call(final String function) {return new FunctionCall.WithoutArgs(function);}

    static FunctionCall call(final String function, final Expression arg)
    {
        return new FunctionCall.WithArgument(function, Objects.requireNonNull(arg, "arg is required"));
    }

    static FunctionCall call(final String function, final EntityVariable variable)
    {
        Objects.requireNonNull(variable, "variable is required");
        return new FunctionCall.WithArgument(function, variable.getName());
    }

    static FunctionCall call(final String function, final String variableName)
    {
        return new FunctionCall.WithArgument(function, Name.at(variableName));
    }
}
