/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Set;

abstract class Builder implements CypherBuilder
{
    private static final Logger LOG = LoggerFactory.getLogger(Builder.class);

    private List<String> nodeIds;
    private List<String> pathIds;
    private List<String> relationshipIds;
    private Set<String> parameters;

    final StringBuilder cypher;

    Builder() {this(null, null, null, null, new StringBuilder());}

    Builder(final StringBuilder cypher) {this(null, null, null, null, cypher);}

    Builder(final Builder other) {this(other.nodeIds, other.pathIds, other.relationshipIds, other.parameters, other.cypher);}

    Builder(
        final List<String> nodeIds,
        final List<String> pathIds,
        final List<String> relationshipIds,
        final Set<String> parameters,
        final StringBuilder cypher)
    {
        this.nodeIds = nodeIds;
        this.pathIds = pathIds;
        this.relationshipIds = relationshipIds;
        this.parameters = parameters;
        this.cypher = cypher;
    }

    @Override public String getCypher() {return this.cypher.toString();}

    protected final boolean knowsPath(final String id) {return null != this.pathIds && this.pathIds.contains(id);}

    protected final boolean knowsRelationship(final String id) {return null != this.relationshipIds && this.relationshipIds.contains(id);}

    protected final boolean knowsNode(final String id) {return null != this.nodeIds && this.nodeIds.contains(id);}

    protected final boolean knowsParameter(final String id) {return null != this.parameters && this.parameters.contains(id);}

    protected final String registerPath(final String name)
    {
        final var id = Name.at(name);
        if (knowsRelationship(id)) {
            throw new IllegalArgumentException("path '" + id + "' has the same name of a previously defined relationship.");
        }
        if (knowsNode(id)) {
            throw new IllegalArgumentException("path '" + id + "' has the same name of a previously defined node.");
        }
        if (null == this.pathIds) {
            this.pathIds = new java.util.ArrayList<>(2);
        }
        this.pathIds.add(id);
        return id;
    }

    protected final String registerNode(final String name)
    {
        final var id = Name.at(name);
        if (knowsPath(id)) {
            throw new IllegalArgumentException("node '" + id + "' has the same name of a previously defined path.");
        }
        if (knowsRelationship(id)) {
            throw new IllegalArgumentException("node '" + id + "' has the same name of a previously defined relationship.");
        }
        if (null == this.nodeIds) {
            this.nodeIds = new java.util.ArrayList<>();
        }
        this.nodeIds.add(id);
        return id;
    }

    protected final String registerRelationship(final String name)
    {
        final var id = Name.at(name);
        if (knowsPath(id)) {
            throw new IllegalArgumentException("relationship '" + id + "' has the same name of a previously defined path.");
        }
        if (knowsNode(id)) {
            throw new IllegalArgumentException("relationship '" + id + "' has the same name of a previously defined node.");
        }
        if (null == this.relationshipIds) {
            this.relationshipIds = new java.util.ArrayList<>(2);
        }
        this.relationshipIds.add(id);
        return id;
    }

    protected final void registerParameter(final String name)
    {
        Objects.requireNonNull(name, "name is required");
        if (knowsParameter(name)) {
            LOG.warn("name '{}' is already defined.", name);
        }
        if (null == this.parameters) {
            this.parameters = new java.util.HashSet<>();
        }
        this.parameters.add(name);
    }

    protected final void append(final CharSequence cs) {this.cypher.append(cs);}

    protected final void append(final String s) {this.cypher.append(s);}

    protected final void append(final char c) {this.cypher.append(c);}

    protected final void append(final boolean value) {this.cypher.append(value);}

    protected final void append(final int value) {this.cypher.append(value);}

    protected final void append(final long value) {this.cypher.append(value);}

    protected final void append(final float value) {this.cypher.append(value);}

    protected final void append(final double value) {this.cypher.append(value);}

    protected final void append(final CypherElement expr) {expr.appendTo(this.cypher);}
}
