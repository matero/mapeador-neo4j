/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.model.HasNodeLabels;
import mapeador.model.HasRelationTypes;

import java.util.Objects;

public class Pattern
    extends Builder
    implements Expression.Groupable, PathVariable, PatternNode, UndirectedRelation, IncomingRelation, OutgoingRelation, ReturnClause,
               WithReturn
{
    private int parens;
    private boolean grouped;
    private boolean hasReturn;
    private boolean hasLimit;
    private boolean hasSkip;
    private boolean hasOrderBy;

    Pattern(final StringBuilder cypher) {super(cypher);}

    public Pattern(final Pattern other)
    {
        super(other);
        this.parens = other.parens;
        this.grouped = other.grouped;
        this.hasLimit = other.hasLimit;
        this.hasReturn = other.hasReturn;
        this.hasSkip = other.hasSkip;
        this.hasOrderBy = other.hasOrderBy;
    }

    public static Pattern node()
    {
        return new Pattern(new StringBuilder());
    }

    public static Pattern MATCH()
    {
        return new Pattern(new StringBuilder().append("MATCH "));
    }

    public static Pattern OPTIONAL_MATCH()
    {
        return new Pattern(new StringBuilder().append("OPTIONAL MATCH "));
    }

    //
    // Expression operations
    //
    @Override public void appendTo(final StringBuilder builder)
    {
        if (this.grouped) {
            builder.append('(');
        }
        builder.append(this.cypher);

        if (this.grouped) {
            builder.append(')');
        }
    }

    @Override public Pattern group()
    {
        this.grouped = true;
        return this;
    }

    //
    // PATH definition
    //
    public final PathVariable path(final String name)
    {
        final var path = registerPath(name);
        append(path);
        append(" = ");
        return this;
    }

    public final PathVariable path(final String name, final PathFunction function) {return path(name, function.name());}

    public final PathVariable path(final String name, final String function)
    {
        final var path = registerPath(name);
        append(path);
        append(" = ");
        append(function);
        incParens();
        return this;
    }

    @Override public final PatternNode by() {return add.node.to(this);}

    @Override public final PatternNode by(final NodeVariable n) {return add.node.to(this, n.getName(), n.labels);}

    @Override public final PatternNode by(final String name) {return add.node.to(this, name);}

    @Override public final PatternNode by(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.node.to(this, n.getName(), n.labels, value, otherValues);
    }

    @Override public final PatternNode by(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.node.to(this,
                           name,
                           value,
                           otherValues);
    }

    @Override public final PatternNode by(final HasNodeLabels labels) {return add.node.to(this, labels);}

    @Override public final PatternNode by(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.node.to(this,
                           labels,
                           value,
                           otherValues);
    }

    @Override public final PatternNode by(final String name, final HasNodeLabels labels) {return add.node.to(this, name, labels);}

    @Override
    public final PatternNode by(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.node.to(this, name, labels, value, otherValues);
    }

    //
    // comma separated MATCH / OPTIONAL MATCH
    //
    @Override public final PatternNode plus(final NodeVariable n) {return appendComma().by(n);}

    @Override public final PatternNode plus(final String name) {return appendComma().by(name);}

    @Override public final PatternNode plus(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return appendComma().by(n, value, otherValues);
    }

    @Override public final PatternNode plus(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return appendComma().by(name,
                                value,
                                otherValues);
    }

    @Override public final PatternNode plus(final HasNodeLabels labels) {return appendComma().by(labels);}

    @Override public final PatternNode plus(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return appendComma().by(labels,
                                value,
                                otherValues);
    }

    @Override public final PatternNode plus(final String name, final HasNodeLabels labels) {return appendComma().by(name, labels);}

    @Override
    public final PatternNode plus(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return appendComma().by(name, labels, value, otherValues);
    }

    @Override public final PathVariable plusPath(final String name) {return appendComma().path(name);}

    @Override public final PathVariable plusPath(final String name, final PathFunction function) {return appendComma().path(name, function);}

    @Override public final PathVariable plusPath(final String name, final String function) {return appendComma().path(name, function);}

    private Pattern appendComma()
    {
        decParens();
        append(", ");
        return this;
    }

    //
    // unnamed relationships between nodes
    //
    @Override public final PatternNode relatedWith() {return add.related_node.to(this);}

    @Override public final PatternNode relatedWith(final NodeVariable n) {return add.related_node.to(this, n.getName());}

    @Override public final PatternNode relatedWith(final String name) {return add.related_node.to(this, name);}

    @Override public final PatternNode relatedWith(final HasNodeLabels labels) {return add.related_node.to(this, labels);}

    @Override public final PatternNode relatedWith(final String name, final HasNodeLabels labels)
    {
        return add.related_node.to(this, name, labels);
    }

    @Override public final PatternNode relatedWith(final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.related_node.to(this,
                                   value,
                                   otherValues);
    }

    @Override public final PatternNode relatedWith(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.related_node.to(this, n.getName(), value, otherValues);
    }

    @Override public final PatternNode relatedWith(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.related_node.to(this, name, value, otherValues);
    }

    @Override public final PatternNode relatedWith(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.related_node.to(this, labels, value, otherValues);
    }

    @Override
    public final PatternNode relatedWith(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.related_node.to(this, name, labels, value, otherValues);
    }

    @Override public final PatternNode incomingFrom() {return add.incoming_node.to(this);}

    @Override public final PatternNode incomingFrom(final NodeVariable n) {return add.incoming_node.to(this, n.getName());}

    @Override public final PatternNode incomingFrom(final String name) {return add.incoming_node.to(this, name);}

    @Override public final PatternNode incomingFrom(final HasNodeLabels labels) {return add.incoming_node.to(this, labels);}

    @Override public final PatternNode incomingFrom(final String name, final HasNodeLabels labels)
    {
        return add.incoming_node.to(this, name, labels);
    }

    @Override public final PatternNode incomingFrom(final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.incoming_node.to(this,
                                    value,
                                    otherValues);
    }

    @Override public final PatternNode incomingFrom(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.incoming_node.to(this, n.getName(), value, otherValues);
    }

    @Override public final PatternNode incomingFrom(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.incoming_node.to(this, name, value, otherValues);
    }

    @Override public final PatternNode incomingFrom(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.incoming_node.to(this, labels, value, otherValues);
    }

    @Override public final PatternNode incomingFrom(
        final String name,
        final HasNodeLabels labels,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return add.incoming_node.to(this, name, labels, value, otherValues);
    }

    @Override public final PatternNode outgoingTo() {return add.outgoing_node.to(this);}

    @Override public final PatternNode outgoingTo(final NodeVariable n) {return add.outgoing_node.to(this, n.getName());}

    @Override public final PatternNode outgoingTo(final String name) {return add.outgoing_node.to(this, name);}

    @Override public final PatternNode outgoingTo(final HasNodeLabels labels) {return add.outgoing_node.to(this, labels);}

    @Override public final PatternNode outgoingTo(final String name, final HasNodeLabels labels)
    {
        return add.outgoing_node.to(this,
                                    name,
                                    labels);
    }

    @Override public final PatternNode outgoingTo(final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.outgoing_node.to(this,
                                    value,
                                    otherValues);
    }

    @Override public final PatternNode outgoingTo(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.outgoing_node.to(this, n.getName(), value, otherValues);
    }

    @Override public final PatternNode outgoingTo(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.outgoing_node.to(this, name, value, otherValues);
    }

    @Override public final PatternNode outgoingTo(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.outgoing_node.to(this, labels, value, otherValues);
    }

    @Override
    public final PatternNode outgoingTo(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return add.outgoing_node.to(this, name, labels, value, otherValues);
    }

    //
    // undirected relation variable
    //
    @Override public final UndirectedRelation related(final RelationVariable r) {return start.relation.at(this, r.getName(), r.types);}

    @Override public final UndirectedRelation related(final String name) {return start.relation.at(this, name);}

    @Override public final UndirectedRelation related(final HasRelationTypes types) {return start.relation.at(this, types);}

    @Override public final UndirectedRelation related(final String name, final HasRelationTypes types)
    {
        return start.relation.at(this, name, types);
    }

    @Override public final UndirectedRelation related(final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this,
                                 value,
                                 otherValues);
    }

    @Override public final UndirectedRelation related(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, r.getName(), value, otherValues);
    }

    @Override public final UndirectedRelation related(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, name, value, otherValues);
    }

    @Override public final UndirectedRelation related(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, types, value, otherValues);
    }

    @Override public final UndirectedRelation related(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.relation.at(this, name, types, value, otherValues);
    }

    @Override public final UndirectedRelation related(final RelationVariable r, final Hops hops) {return start.relation.at(this, r.getName(), hops);}

    @Override public final UndirectedRelation related(final String name, final Hops hops) {return start.relation.at(this, name, hops);}

    @Override public final UndirectedRelation related(final HasRelationTypes types, final Hops hops) {return start.relation.at(this, types, hops);}

    @Override public final UndirectedRelation related(final String name, final HasRelationTypes types, final Hops hops)
    {
        return start.relation.at(this, name, types, hops);
    }

    @Override public final UndirectedRelation related(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, hops, value, otherValues);
    }

    @Override
    public final UndirectedRelation related(final RelationVariable r, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, r.getName(), hops, value, otherValues);
    }

    @Override
    public final UndirectedRelation related(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.relation.at(this, name, hops, value, otherValues);
    }

    @Override public final UndirectedRelation related(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.relation.at(this, types, hops, value, otherValues);
    }

    @Override
    public final UndirectedRelation related(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.relation.at(this, name, types, hops, value, otherValues);
    }

    @Override public final UndirectedRelation related(final Hops hops) {return start.relation.at(this, hops);}

    //
    // Undirected Relationships finalization (UndirectedRelationSpec impl)
    //
    @Override public PatternNode with() {return end.relation.at(this);}

    @Override public PatternNode with(final NodeVariable n) {return with(n.getName(), n.getNodeLabels());}

    @Override public PatternNode with(final String name) {return end.relation.at(this, name);}

    @Override public PatternNode with(final HasNodeLabels labels) {return end.relation.at(this, labels);}

    @Override public PatternNode with(final String name, final HasNodeLabels labels) {return end.relation.at(this, name, labels);}

    @Override public PatternNode with(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return with(n.getName(), n.getNodeLabels(), value, otherValues);
    }

    @Override public PatternNode with(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.relation.at(this, name, value, otherValues);
    }

    @Override public PatternNode with(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.relation.at(this, labels, value, otherValues);
    }

    @Override public PatternNode with(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.relation.at(this, name, labels, value, otherValues);
    }

    //
    // incoming relationship variable
    //

    @Override public final IncomingRelation incoming(final RelationVariable r) {return start.incoming_relation.at(this, r.getName());}

    @Override public final IncomingRelation incoming(final String name) {return start.incoming_relation.at(this, name);}

    @Override public final IncomingRelation incoming(final HasRelationTypes types) {return start.incoming_relation.at(this, types);}

    @Override public final IncomingRelation incoming(final String name, final HasRelationTypes types)
    {
        return start.incoming_relation.at(this, name, types);
    }

    @Override public final IncomingRelation incoming(final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this,
                                          value,
                                          otherValues);
    }

    @Override public final IncomingRelation incoming(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, r.getName(), value, otherValues);
    }

    @Override public final IncomingRelation incoming(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, name, value, otherValues);
    }

    @Override public final IncomingRelation incoming(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, types, value, otherValues);
    }

    @Override public final IncomingRelation incoming(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, name, types, value, otherValues);
    }

    @Override public final IncomingRelation incoming(final RelationVariable r, final Hops hops)
    {
        return start.incoming_relation.at(this, r.getName(), hops);
    }

    @Override public final IncomingRelation incoming(final String name, final Hops hops)
    {
        return start.incoming_relation.at(this, name, hops);
    }

    @Override public final IncomingRelation incoming(final HasRelationTypes types, final Hops hops)
    {
        return start.incoming_relation.at(this,
                                          types,
                                          hops);
    }

    @Override public final IncomingRelation incoming(final String name, final HasRelationTypes types, final Hops hops)
    {
        return start.incoming_relation.at(this, name, types, hops);
    }

    @Override public final IncomingRelation incoming(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, hops, value, otherValues);
    }

    @Override
    public final IncomingRelation incoming(final RelationVariable r, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, r.getName(), hops, value, otherValues);
    }

    @Override
    public final IncomingRelation incoming(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, name, hops, value, otherValues);
    }

    @Override public final IncomingRelation incoming(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, types, hops, value, otherValues);
    }

    @Override
    public final IncomingRelation incoming(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.incoming_relation.at(this, name, types, hops, value, otherValues);
    }

    @Override public final IncomingRelation incoming(final Hops hops) {return start.incoming_relation.at(this, hops);}

    //
    // Incoming Relationships finalization (IncomingRelation impl)
    //
    @Override public PatternNode from() {return end.incoming_relation.at(this);}

    @Override public PatternNode from(final NodeVariable n) {return end.incoming_relation.at(this, n.getName(), n.getNodeLabels());}

    @Override public PatternNode from(final String name) {return end.incoming_relation.at(this, name);}

    @Override public PatternNode from(final HasNodeLabels labels) {return end.incoming_relation.at(this, labels);}

    @Override public PatternNode from(final String name, final HasNodeLabels labels) {return end.incoming_relation.at(this, name, labels);}

    @Override public PatternNode from(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.incoming_relation.at(this, n.getName(), n.getNodeLabels(), value, otherValues);
    }

    @Override public PatternNode from(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.incoming_relation.at(this, name, value, otherValues);
    }

    @Override public PatternNode from(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.incoming_relation.at(this,
                                        labels,
                                        value, otherValues);
    }

    @Override public PatternNode from(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.incoming_relation.at(this, name, labels, value, otherValues);
    }


    //
    // outgoing relationship variable
    //
    @Override public final OutgoingRelation outgoing(final RelationVariable r) {return start.outgoing_relation.at(this, r.getName(), r.types);}

    @Override public final OutgoingRelation outgoing(final String name) {return start.outgoing_relation.at(this, name);}

    @Override public final OutgoingRelation outgoing(final HasRelationTypes types) {return start.outgoing_relation.at(this, types);}

    @Override public final OutgoingRelation outgoing(final String name, final HasRelationTypes types)
    {
        return start.outgoing_relation.at(this, name, types);
    }

    @Override public final OutgoingRelation outgoing(final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this,
                                          value,
                                          otherValues);
    }

    @Override public final OutgoingRelation outgoing(final RelationVariable r, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, r.getName(), value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, name, value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, types, value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(
        final String name,
        final HasRelationTypes types,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, name, types, value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(final RelationVariable r, final Hops hops)
    {
        return start.outgoing_relation.at(this, r.getName(), hops);
    }

    @Override public final OutgoingRelation outgoing(final String name, final Hops hops)
    {
        return start.outgoing_relation.at(this, name, hops);
    }

    @Override public final OutgoingRelation outgoing(final HasRelationTypes types, final Hops hops)
    {
        return start.outgoing_relation.at(this,
                                          types,
                                          hops);
    }

    @Override public final OutgoingRelation outgoing(final String name, final HasRelationTypes types, final Hops hops)
    {
        return start.outgoing_relation.at(this, name, types, hops);
    }

    @Override public final OutgoingRelation outgoing(final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, hops, value, otherValues);
    }

    @Override
    public final OutgoingRelation outgoing(final RelationVariable r, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, r.getName(), hops, value, otherValues);
    }

    @Override
    public final OutgoingRelation outgoing(final String name, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, name, hops, value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, types, hops, value, otherValues);
    }

    @Override
    public final OutgoingRelation outgoing(
        final String name,
        final HasRelationTypes types,
        final Hops hops,
        final PropertyValue value,
        final PropertyValue... otherValues)
    {
        return start.outgoing_relation.at(this, name, types, hops, value, otherValues);
    }

    @Override public final OutgoingRelation outgoing(final Hops hops) {return start.outgoing_relation.at(this, hops);}

    //
    // Outgoing Relationships finalization (OutgoingRelationSpec impl)
    //
    @Override public PatternNode to() {return end.outgoing_relation.at(this);}

    @Override public PatternNode to(final NodeVariable n) {return end.outgoing_relation.at(this, n.getName(), n.labels);}

    @Override public PatternNode to(final String name) {return end.outgoing_relation.at(this, name);}

    @Override public PatternNode to(final HasNodeLabels labels) {return end.outgoing_relation.at(this, labels);}

    @Override public PatternNode to(final String name, final HasNodeLabels labels) {return end.outgoing_relation.at(this, name, labels);}

    @Override public PatternNode to(final NodeVariable n, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.outgoing_relation.at(this,
                                        n.getName(),
                                        value, otherValues);
    }

    @Override public PatternNode to(final String name, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.outgoing_relation.at(this,
                                        name,
                                        value,
                                        otherValues);
    }

    @Override public PatternNode to(final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.outgoing_relation.at(this,
                                        labels,
                                        value, otherValues);
    }

    @Override public PatternNode to(final String name, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
    {
        return end.outgoing_relation.at(this, name, labels, value, otherValues);
    }

    @Override public final WithReturn WHERE(final Predicate predicate)
    {
        Objects.requireNonNull(predicate, "predicate is required");
        decParens();
        append(" WHERE ");
        append(predicate);
        return this;
    }

    //
    // RETURN clauses
    //

    @Override public ReturnClause RETURN(final String columns)
    {
        Objects.requireNonNull(columns, "rawColumns is required");
        return addReturnWithColumns(" RETURN ", new RawExpression(columns.toString()));
    }

    @Override public ReturnClause RETURN(final Expression firstColumn, final Expression... otherColumns)
    {
        return addReturnWithColumns(" RETURN ", firstColumn, otherColumns);
    }

    @Override public ReturnClause RETURN_DISTINCT(final Expression firstColumn, final Expression... otherColumns)
    {
        return addReturnWithColumns(" RETURN DISTINCT ", firstColumn, otherColumns);
    }

    @Override public ProjectionWithOrderBy ORDER_BY(final SortPredicate firstSortPredicate, final SortPredicate... otherSortPredicates)
    {
        append(" ORDER BY ");
        append(firstSortPredicate);
        if (null != otherSortPredicates && 0 != otherSortPredicates.length) {
            for (final var sortPredicate : otherSortPredicates) {
                append(", ");
                append(sortPredicate);
            }
        }
        this.hasOrderBy = true;
        return this;
    }

    @Override public ProjectionWithSkip SKIP(final Expression e)
    {
        append(" SKIP ");
        append(e);
        this.hasSkip = true;
        return this;
    }

    @Override public ProjectionWithLimit LIMIT(final Expression e)
    {
        append(" LIMIT ");
        append(e);
        this.hasLimit = true;
        return this;
    }

    private Pattern addReturnWithColumns(final String clause, final Expression first, final Expression... otherColumns)
    {
        prepareReturn(clause);
        append(first);
        for (final var column : otherColumns) {
            append(", ");
            append(column);
        }
        this.hasReturn = true;
        return this;
    }

    private void prepareReturn(final String clause)
    {
        close().append(clause);
    }

    public Pattern incParens()
    {
        this.parens++;
        append('(');
        return this;
    }

    Pattern decParens()
    {
        this.parens--;
        append(')');
        return this;
    }

    Pattern close()
    {
        while (hasUnbalancedParens()) {
            decParens();
        }
        return this;
    }

    private boolean hasUnbalancedParens()
    {
        return 0 < this.parens;
    }

    public boolean hasReturn() {return this.hasReturn;}

    public boolean hasLimit() {return this.hasLimit;}

    public boolean hasSkip() {return this.hasSkip;}

    public boolean hasOrderBy() {return this.hasOrderBy;}

    enum add
    {
        node(Connect::noop) {
            @Override Pattern connectTo(final Pattern pattern) {return super.connectTo(pattern.incParens());}
        },
        incoming_node(Connect::incomingNode),
        outgoing_node(Connect::outgoingNode),
        related_node(Connect::relatedNode);

        private final Connector connector;

        add(final Connector connector) {this.connector = connector;}


        Pattern to(final Pattern pattern)
        {
            return connectTo(pattern);
        }

        Pattern to(
            final Pattern pattern,
            final String node,
            final HasNodeLabels labels,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            to(pattern, node, labels);
            return Append.to(pattern, value, otherValues);
        }

        Pattern to(final Pattern pattern, final String node, final HasNodeLabels labels)
        {
            to(pattern, node).append(labels.getNodeLabels().getCypher());
            return pattern;
        }

        Pattern to(final Pattern pattern, final String node, final PropertyValue value, final PropertyValue... otherValues)
        {
            to(pattern, node);
            return Append.to(pattern, value, otherValues);
        }

        Pattern to(final Pattern pattern, final String id)
        {
            connectTo(pattern).append(pattern.registerNode(id));
            return pattern;
        }

        Pattern to(final Pattern pattern, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
        {
            to(pattern, labels);
            return Append.to(pattern, value, otherValues);
        }

        Pattern to(final Pattern pattern, final HasNodeLabels labels)
        {
            connectTo(pattern).append(labels.getNodeLabels().getCypher());
            return pattern;
        }

        Pattern to(final Pattern pattern, final PropertyValue value, final PropertyValue... otherValues)
        {
            connectTo(pattern);
            return Append.to(pattern, value, otherValues);
        }

        Pattern connectTo(final Pattern pattern) {return this.connector.connect(pattern);}
    }

    enum end
    {
        incoming_relation(Connect::nodeIncoming),
        outgoing_relation(Connect::outgoingRelation),
        relation(Connect::with);

        private final Connector connector;

        end(final Connector connector) {this.connector = connector;}

        Pattern at(final Pattern pattern)
        {
            this.connector.connect(pattern);
            return pattern;
        }

        Pattern at(final Pattern pattern, final String id, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, id, labels);
            return Append.to(pattern, value, otherValues);
        }

        Pattern at(final Pattern pattern, final String id, final HasNodeLabels labels)
        {
            at(pattern, id).append(labels.getNodeLabels().getCypher());
            return pattern;
        }

        Pattern at(final Pattern pattern, final String id, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, id);
            return Append.to(pattern, value, otherValues);
        }

        Pattern at(final Pattern pattern, final String id)
        {
            this.connector.connect(pattern).append(pattern.registerNode(id));
            return pattern;
        }

        Pattern at(final Pattern pattern, final HasNodeLabels labels, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, labels);
            return Append.to(pattern, value, otherValues);
        }

        Pattern at(final Pattern pattern, final HasNodeLabels labels)
        {
            this.connector.connect(pattern).append(labels.getNodeLabels().getCypher());
            return pattern;
        }

        Pattern at(final Pattern pattern, final PropertyValue value, final PropertyValue... otherValues)
        {
            this.connector.connect(pattern);
            return Append.to(pattern, value, otherValues);
        }
    }

    static final class start<R extends PatternRelationship>
    {
        static final start<UndirectedRelation> relation = new start<>(UndirectedRelation.class, Connect::using);
        static final start<IncomingRelation> incoming_relation = new start<>(IncomingRelation.class, Connect::incomingRelation);
        static final start<OutgoingRelation> outgoing_relation = new start<>(OutgoingRelation.class, Connect::using);

        private final Class<R> relationType;
        private final Connector connector;

        public start(final Class<R> relationType, final Connector connector)
        {
            this.relationType = relationType;
            this.connector = connector;
        }

        R at(
            final Pattern pattern,
            final String id,
            final HasRelationTypes types,
            final Hops hops,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            at(pattern, id, types, hops);
            return this.relationType.cast(Append.to(pattern, value, otherValues));
        }

        R at(final Pattern pattern, final String id, final HasRelationTypes types, final Hops hops)
        {
            at(pattern, id, types);
            pattern.append(hops.getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final String id, final HasRelationTypes types)
        {
            this.connector.connect(pattern).append(pattern.registerRelationship(id));
            pattern.append(types.getRelationTypes().getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final String id, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, id, hops);
            return this.relationType.cast(Append.to(pattern, value, otherValues));
        }

        R at(final Pattern pattern, final String id, final Hops hops)
        {
            this.connector.connect(pattern).append(pattern.registerRelationship(id));
            pattern.append(hops.getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final String id, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, id);
            return this.relationType.cast(Append.to(pattern, value, otherValues));
        }

        R at(final Pattern pattern, final String id)
        {
            this.connector.connect(pattern).append(pattern.registerRelationship(id));
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final HasRelationTypes types, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, types, hops);
            return this.relationType.cast(Append.to(pattern, value, otherValues));
        }

        R at(final Pattern pattern, final HasRelationTypes types, final Hops hops)
        {
            at(pattern, types);
            pattern.append(hops.getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final HasRelationTypes types)
        {
            this.connector.connect(pattern);
            pattern.append(types.getRelationTypes().getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final String id, final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, id);
            pattern.append(types.getRelationTypes().getCypher());
            Append.to(pattern, value, otherValues);
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final HasRelationTypes types, final PropertyValue value, final PropertyValue... otherValues)
        {
            this.connector.connect(pattern);
            pattern.append(types.getRelationTypes().getCypher());
            Append.to(pattern, value, otherValues);
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final Hops hops, final PropertyValue value, final PropertyValue... otherValues)
        {
            at(pattern, hops);
            return this.relationType.cast(Append.to(pattern, value, otherValues));
        }

        R at(final Pattern pattern, final Hops hops)
        {
            this.connector.connect(pattern).append(hops.getCypher());
            return this.relationType.cast(pattern);
        }

        R at(final Pattern pattern, final PropertyValue value, final PropertyValue... otherValues)
        {
            Connect.using(pattern);
            Append.to(pattern, value, otherValues);
            return this.relationType.cast(pattern);
        }
    }

    static final class Connect
    {
        private Connect() { throw new UnsupportedOperationException();}

        static Pattern noop(final Pattern pattern) {return pattern;}

        static Pattern relatedNode(final Pattern pattern) {return connect(pattern.decParens(), "--").incParens();}

        static Pattern using(final Pattern pattern) {return connect(pattern.decParens(), "-[");}

        static Pattern with(final Pattern pattern) {return connect(pattern, "]-").incParens();}

        static Pattern incomingNode(final Pattern pattern) {return connect(pattern.decParens(), "<-").incParens();}

        static Pattern incomingRelation(final Pattern pattern) {return connect(pattern.decParens(), "<-[");}

        static Pattern nodeIncoming(final Pattern pattern) {return connect(pattern, "]-").incParens();}

        static Pattern outgoingNode(final Pattern pattern) {return connect(pattern.decParens(), "->").incParens();}

        static Pattern nodeOutgoing(final Pattern pattern) {return connect(pattern.decParens(), "-[");}

        static Pattern outgoingRelation(final Pattern pattern)
        {
            return connect(pattern, "]->").incParens();
        }

        private static Pattern connect(final Pattern pattern, final String connector)
        {
            pattern.append(connector);
            return pattern;
        }
    }

    @FunctionalInterface
    interface Connector
    {
        Pattern connect(Pattern pattern);
    }
}

