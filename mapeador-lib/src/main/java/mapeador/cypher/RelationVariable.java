/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.model.Types;

import java.util.Objects;

public class RelationVariable extends EntityVariable
{
    final Types types;

    protected RelationVariable(final String name, final Types types)
    {
        super(name);
        this.types = types;
    }

    @Override public final String getParameterClasses() {return this.types.getParameterClasses();}

    @Override public void appendTo(final StringBuilder builder)
    {
        super.appendTo(builder);
        builder.append(this.types.getCypher());
    }

    @Override public RelationVariable group()
    {
        return this;
    }

    static RelationVariable of(final String name) {return new RelationVariable(withName(name), Types.empty());}

    static RelationVariable of(final String name, final Types types)
    {
        return new RelationVariable(withName(name), Objects.requireNonNull(types, "types is required"));
    }
}
