/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.criteria.Criteria;
import mapeador.model.EntityMetadata;

public class PropertyValue
{
    private final String name;
    private Object value;

    protected PropertyValue(final String name, final Object value)
    {
        this.name = name;
        this.value = value;
    }

    protected void setParameter(final Parameter parameter)
    {
        this.value = parameter;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof PropertyValue) {
            final var other = (PropertyValue) o;
            return this.name.equals(other.name) && this.value.equals(other.value);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.name.hashCode();
        result = 31 * result + this.value.hashCode();
        return result;
    }

    @Override public String toString()
    {
        return "PropertyValue(name='" + this.name + "', value=" + this.value + ')';
    }

    public boolean hasParameter()
    {
        return this.value instanceof Parameter;
    }

    public Parameter getParameter()
    {
        if (hasParameter()) {
            return (Parameter) this.value;
        } else {
            return null;
        }
    }

    void appendTo(final Pattern pattern)
    {
        pattern.append(this.name);
        pattern.append(": ");
        if (hasParameter()) {
            final var parameter = getParameter();
            pattern.registerParameter(parameter.getName());
            pattern.append(parameter);
        } else {
            final Literal lit = (this.value instanceof Literal)? (Literal) this.value :  CypherDSL.literal(this.value);
            pattern.append(lit);
        }
    }

    public PropertyValue useParameter(final Criteria<?, ?> criteria, final EntityMetadata<?> metadata, final NodeVariable node)
    {
        return useParameter(criteria, metadata, node.getName());
    }

    public PropertyValue useParameter(final Criteria<?, ?> criteria, final EntityMetadata<?> metadata, final String nodeName)
    {
        return useParameter(criteria, metadata.getParameterClasses(), nodeName);
    }

    public PropertyValue useParameter(final Criteria<?, ?> criteria, final String node, final String classes)
    {
        if (hasParameter()) {
            criteria.addRequiredParameter(getParameter());
        } else {
            final var parameterName = getParameterName(node, classes);
            final var parameter = Parameter.of(parameterName);
            criteria.addParameter(parameter.getName(), this.value);
            setParameter(parameter);
        }
        return this;
    }

    private String getParameterName(final String node, final String classes)
    {
        if (null != node) {
            if (null != classes) {
                return node + '_' + classes + '_' + this.name;
            } else {
                return node + '_' + this.name;
            }
        } else {
            if (null != classes) {
                return classes + '_' + this.name;
            } else {
                return this.name;
            }
        }
    }
}
