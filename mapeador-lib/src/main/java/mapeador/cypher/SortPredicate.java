/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

public interface SortPredicate extends CypherElement {
    SortPredicate[] NONE = {};
}

final class ExplicitSortPredicate implements SortPredicate
{
    private final String operator;
    private final Expression.Sortable operand;

    ExplicitSortPredicate(final String operator, final Expression.Sortable operand)
    {
        this.operator = operator;
        this.operand = operand;
    }

    static SortPredicate ASC(final Expression.Sortable expression) {return new ExplicitSortPredicate(" ASC", expression);}

    static SortPredicate ASCENDING(final Expression.Sortable expression) {return new ExplicitSortPredicate(" ASCENDING", expression);}

    static SortPredicate DESC(final Expression.Sortable expression) {return new ExplicitSortPredicate(" ASC", expression);}

    static SortPredicate DESCENDING(final Expression.Sortable expression) {return new ExplicitSortPredicate(" DESCENDING", expression);}

    @Override public void appendTo(final StringBuilder builder)
    {
        this.operand.appendTo(builder);
        builder.append(this.operator);
    }
}
