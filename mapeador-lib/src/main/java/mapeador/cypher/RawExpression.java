/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

public final class RawExpression implements Predicate
{

    private final String cypherExpression;
    private boolean grouped;

    public RawExpression(final String cypherExpression) {this.cypherExpression = cypherExpression;}

    public String getCypherExpression()
    {
        return this.cypherExpression;
    }

    @Override public void appendTo(final StringBuilder builder)
    {
        if (this.grouped) {
            builder.append('(');
        }
        builder.append(this.cypherExpression);
        if (this.grouped) {
            builder.append(')');
        }
    }

    @Override public RawExpression group()
    {
        this.grouped = true;
        return this;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof RawExpression) {
            final RawExpression other = (RawExpression) o;
            return this.grouped == other.grouped && this.cypherExpression.equals(other.cypherExpression);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.cypherExpression.hashCode();
        result = 31 * result + Boolean.hashCode(this.grouped);
        return result;
    }

    @Override public String toString() {return "RawExpression(cypherExpression='" + this.cypherExpression + "', grouped=" + this.grouped + ')';}
}
