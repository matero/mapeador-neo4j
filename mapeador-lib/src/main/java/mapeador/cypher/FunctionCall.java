/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.List;
import java.util.Objects;

public abstract class FunctionCall implements Predicate
{
    final String name;

    FunctionCall(final String name) {this.name = name;}

    @Override public Groupable group() {return this;}

    static FunctionCall of(final String name)
    {
        return new FunctionCall.WithoutArgs(checkName(name));
    }

    static FunctionCall of(final String name, final Object firstArg, final Object... otherArgs)
    {
        final var id = checkName(name);
        if (null == otherArgs || 0 == otherArgs.length) {
            return new FunctionCall.WithArgument(id, firstArg);
        } else {
            final var arguments = new Object[1 + otherArgs.length];
            arguments[0] = firstArg;
            System.arraycopy(otherArgs, 0, arguments, 1, otherArgs.length);
            return new FunctionCall.WithManyArguments(id, arguments);
        }
    }

    static FunctionCall of(final String name, final List<Object> arguments)
    {
        final var id = checkName(name);
        Objects.requireNonNull(arguments, "arguments is required");
        if (arguments.isEmpty()) {
            return new FunctionCall.WithoutArgs(id);
        } else {
            if (1 == arguments.size()) {
                return new WithArgument(id, arguments.get(0));
            } else {
                return new WithManyArguments(id, arguments.toArray());
            }
        }
    }

    private static String checkName(final String name)
    {
        Objects.requireNonNull(name, "name is required");
        if (name.isEmpty()) {
            throw new IllegalArgumentException("name can't be empty");
        }
        if (name.isBlank()) {
            throw new IllegalArgumentException("name can't be blank");
        }
        if (Name.requireBackticks(name)) {
            throw new IllegalArgumentException("name shouldn't require backticks");
        }
        return name;
    }

    static final class WithoutArgs extends FunctionCall
    {
        WithoutArgs(final String name) {super(name);}

        @Override public void appendTo(final StringBuilder builder) {builder.append(this.name).append("()");}
    }

    static final class WithArgument extends FunctionCall
    {
        private final Object argument;

        WithArgument(final String name, final Object argument)
        {
            super(name);
            this.argument = argument;
        }

        @Override public void appendTo(final StringBuilder builder)
        {
            builder.append(this.name).append('(');
            Append.to(builder, this.argument);
            builder.append(')');
        }
    }

    static final class WithManyArguments extends FunctionCall
    {
        private final Object[] arguments;

        WithManyArguments(final String name, final Object[] arguments)
        {
            super(name);
            this.arguments = arguments;
        }

        @Override public void appendTo(final StringBuilder builder)
        {
            builder.append(this.name).append('(');
            Append.to(builder, this.arguments[0]);
            for (int i = 1; i < this.arguments.length; i++){
                builder.append(", ");
                Append.to(builder, this.arguments[i]);
            }
            builder.append(')');
        }

    }
}
