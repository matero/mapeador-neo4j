/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface StringExpression extends Expression.Comparable
{
    default Predicate STARTS_WITH(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" STARTS WITH ", this, rhs);
    }

    default Predicate STARTS_WITH(final String rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" STARTS WITH ", this, Literal.StringLiteral.ofNoNull(rhs));
    }

    default Predicate ENDS_WITH(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" ENDS WITH ", this, rhs);
    }

    default Predicate ENDS_WITH(final String rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" ENDS WITH ", this, Literal.StringLiteral.ofNoNull(rhs));
    }

    default Predicate CONTAINS(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" CONTAINS ", this, rhs);
    }

    default Predicate CONTAINS(final String rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new BinaryComparableExpression(" CONTAINS ", this, Literal.StringLiteral.ofNoNull(rhs));
    }

    default Predicate MATCHES(final StringExpression regex)
    {
        Objects.requireNonNull(regex, "regex is required");
        return new BinaryComparableExpression(" ~= ", this, regex);
    }

    default Predicate MATCHES(final String regex)
    {
        Objects.requireNonNull(regex, "regex is required");
        return new BinaryComparableExpression(" ~= ", this, Literal.StringLiteral.ofNoNull(regex));
    }

    default Concatenation concat(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "regex is required");
        return new Concatenation(this, rhs);
    }

    default Concatenation concat(final String rhs)
    {
        return new Concatenation(this, Literal.StringLiteral.of(rhs));
    }

    final class Concatenation implements StringExpression
    {
        private final StringExpression lhs;
        private final StringExpression rhs;
        private boolean grouped;

        Concatenation(final StringExpression lhs, final StringExpression rhs)
        {
            this.lhs = lhs;
            this.rhs = Objects.requireNonNull(rhs, "rhs is required");
        }

        @Override public void appendTo(final StringBuilder builder)
        {
            if (this.grouped) {
                builder.append('(');
            }
            this.lhs.appendTo(builder);
            builder.append(" + ");
            this.rhs.appendTo(builder);
            if (this.grouped) {
                builder.append(')');
            }
        }

        @Override public Concatenation group()
        {
            this.grouped = true;
            return this;
        }
    }
}
