/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

final class Append
{
    private Append() {throw new UnsupportedOperationException("Append should not be instantiated.");}

    static void to(final StringBuilder builder, final Object o)
    {
        if (o instanceof CypherElement) {
            ((CypherElement) o).appendTo(builder);
        } else if (null == o) {
            Literal.NULL.appendTo(builder);
        } else {
            builder.append(o);
        }
    }

    static Pattern to(final Pattern pattern, final PropertyValue value, final PropertyValue[] otherValues)
    {
        pattern.append(" {");
        value.appendTo(pattern);
        if (null != otherValues && 0 < otherValues.length) {
            for (final var otherValue : otherValues) {
                pattern.append(", ");
                otherValue.appendTo(pattern);
            }
        }
        pattern.append('}');
        return pattern;
    }
}
