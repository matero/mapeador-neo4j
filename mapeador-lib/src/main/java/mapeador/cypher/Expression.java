/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface Expression extends CypherElement
{
    static Expression of(final Object value)
    {
        if (value instanceof Expression) {
            return (Expression) value;
        }
        return CypherDSL.literal(value);
    }

    interface Groupable extends Expression
    {
        Groupable group();
    }

    interface Sortable extends Expression
    {
        default SortPredicate ASC() {return ExplicitSortPredicate.ASC(this);}

        default SortPredicate ASCENDING() {return ExplicitSortPredicate.ASCENDING(this);}

        default SortPredicate DESC() {return ExplicitSortPredicate.DESC(this);}

        default SortPredicate DESCENDING() {return ExplicitSortPredicate.DESCENDING(this);}
    }

    interface Comparable extends Expression.Groupable, Expression.Sortable, SortPredicate
    {
        default Predicate EQ(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" = ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate NE(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" <> ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate GT(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" > ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate GE(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" >= ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate LT(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" < ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate LE(final Expression.Comparable rhs)
        {
            return new BinaryComparableExpression(" <= ", this, Objects.requireNonNull(rhs, "rhs is required"));
        }

        default Predicate IS_NULL() {return new UnaryComparableExpression(" IS NULL", this);}

        default Predicate IS_NOT_NULL() {return new UnaryComparableExpression(" IS NOT NULL", this);}
    }

    interface WithAlias extends Expression
    {
        default Alias as(final String alias)
        {
            Objects.requireNonNull(alias, "alias is required"); // neo4j allows empty and blank alias -> don't to check that
            return new Alias(this, Name.normalize(alias));
        }
    }

    interface Predicate extends Expression.Comparable, Expression.WithAlias
    {
        default Predicate AND(final String second)
        {
            return BinaryPredicate.and(this, CypherDSL.r(Objects.requireNonNull(second, "second is required")));
        }

        default Predicate AND(final Predicate second)
        {
            return BinaryPredicate.and(this, Objects.requireNonNull(second, "second is required"));
        }

        default Predicate AND(final Predicate second, final Predicate... otherOperands)
        {
            Objects.requireNonNull(second, "second is required");
            if (null == otherOperands || 0 == otherOperands.length) {
                return BinaryPredicate.and(this, second);
            } else {
                return MultiplePredicates.and(this, second, FailIf.hasNull(otherOperands, "otherOperands"));
            }
        }

        default Predicate OR(final Predicate second)
        {
            return BinaryPredicate.or(this, Objects.requireNonNull(second, "second is required"));
        }

        default Predicate OR(final Predicate second, final Predicate... otherOperands)
        {
            Objects.requireNonNull(second, "second is required");
            if (null == otherOperands || 0 == otherOperands.length) {
                return BinaryPredicate.or(this, second);
            } else {
                return MultiplePredicates.or(this, second, FailIf.hasNull(otherOperands, "otherOperands"));
            }
        }

        default Predicate XOR(final Predicate second)
        {
            return BinaryPredicate.xor(this, Objects.requireNonNull(second, "second is required"));
        }

        default Predicate XOR(final Predicate second, final Predicate... otherOperands)
        {
            Objects.requireNonNull(second, "second is required");
            if (null == otherOperands || 0 == otherOperands.length) {
                return BinaryPredicate.xor(this, second);
            } else {
                return MultiplePredicates.xor(this, second, FailIf.hasNull(otherOperands, "otherOperands"));
            }
        }
    }
}

final class UnaryComparableExpression implements Predicate
{
    private final String operator;
    private final Expression.Comparable operand;

    UnaryComparableExpression(final String operator, final Expression.Comparable operand)
    {
        this.operator = operator;
        this.operand = operand;
    }

    @Override public void appendTo(final StringBuilder builder)
    {
        this.operand.appendTo(builder);
        builder.append(this.operator);
    }

    @Override public Predicate group() {return this;}
}

class BinaryComparableExpression implements Predicate
{
    private final String operator;
    private final Expression.Comparable lhs;
    private final Expression.Comparable rhs;
    private boolean grouped;

    BinaryComparableExpression(final String operator, final Expression.Comparable lhs, final Expression.Comparable rhs)
    {
        this.operator = operator;
        this.lhs = lhs;
        this.rhs = Objects.requireNonNull(rhs, "rhs is required");
    }

    @Override public void appendTo(final StringBuilder builder)
    {
        if (this.grouped) {
            builder.append('(');
        }
        this.lhs.appendTo(builder);
        builder.append(this.operator);
        this.rhs.appendTo(builder);
        if (this.grouped) {
            builder.append(')');
        }
    }

    @Override public Predicate group()
    {
        this.grouped = true;
        return this;
    }
}

final class NotPredicate implements Predicate
{
    private final Predicate operand;

    public NotPredicate(final Predicate operand) {this.operand = operand;}

    @Override public NotPredicate group() {return this;}

    @Override public void appendTo(final StringBuilder builder)
    {
        builder.append("NOT ");
        this.operand.appendTo(builder);
    }

    static NotPredicate of(final Predicate p) {
        if (p instanceof Pattern) {
            final var pattern = (Pattern) p;
            pattern.close();
        }
        return new NotPredicate(p);
    }
}

class BinaryPredicate implements Predicate
{
    private final String operator;
    private final Predicate first;
    private final Predicate second;
    private boolean grouped;

    BinaryPredicate(final String operator, final Predicate first, final Predicate second)
    {
        this.operator = operator;
        this.first = first;
        this.second = second;
    }

    static Predicate and(final Predicate first, final Predicate second)
    {
        return new BinaryPredicate(" AND ", first, second);
    }

    static Predicate or(final Predicate first, final Predicate second)
    {
        return new BinaryPredicate(" OR ", first, second);
    }

    static Predicate xor(final Predicate first, final Predicate second)
    {
        return new BinaryPredicate(" XOR ", first, second);
    }

    @Override public void appendTo(final StringBuilder builder)
    {
        if (this.grouped) {
            builder.append('(');
        }
        appendPredicateTo(builder);
        if (this.grouped) {
            builder.append(')');
        }
    }

    void appendPredicateTo(final StringBuilder builder)
    {
        this.first.appendTo(builder);
        appendOperandTo(builder, this.second);
    }

    final void appendOperandTo(final StringBuilder builder, final Predicate operand)
    {
        builder.append(this.operator);
        operand.appendTo(builder);
    }

    @Override public BinaryPredicate group()
    {
        this.grouped = true;
        return this;
    }
}

final class MultiplePredicates extends BinaryPredicate
{
    private final Predicate[] otherOperands;

    MultiplePredicates(
        final String operator,
        final Predicate first,
        final Predicate second,
        final Predicate[] otherOperands)
    {
        super(operator, first, second);
        this.otherOperands = otherOperands;
    }

    static Predicate and(final Predicate first, final Predicate second, final Predicate[] otherOperands)
    {
        return new MultiplePredicates(" AND ", first, second, otherOperands);
    }

    static Predicate or(final Predicate first, final Predicate second, final Predicate[] otherOperands)
    {
        return new MultiplePredicates(" OR ", first, second, otherOperands);
    }

    static Predicate xor(final Predicate first, final Predicate second, final Predicate[] otherOperands)
    {
        return new MultiplePredicates(" XOR ", first, second, otherOperands);
    }

    @Override void appendPredicateTo(final StringBuilder builder)
    {
        super.appendPredicateTo(builder);
        for (final var operand : this.otherOperands) {
            appendOperandTo(builder, operand);
        }
    }
}
