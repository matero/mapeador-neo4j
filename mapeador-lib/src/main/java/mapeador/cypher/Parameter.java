/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public final class Parameter implements Predicate
{
    private final String name;

    Parameter(final String name) {this.name = name;}

    public String getName() {return this.name;}

    @Override public void appendTo(final StringBuilder builder) {builder.append('$').append(this.name);}

    @Override public Parameter group() {return this;}

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Parameter) {
            final var other = (Parameter) o;
            return this.name.equals(other.name);
        }
        return false;
    }

    @Override public int hashCode() {return this.name.hashCode();}

    @Override public String toString() {return '$' + this.name;}

    public static Parameter of(final String parameter)
    {
        Objects.requireNonNull(parameter, "parameter is required");
        if (parameter.isEmpty()) {
            throw new IllegalArgumentException("parameter can not be empty");
        }
        if (parameter.isBlank()) {
            throw new IllegalArgumentException("parameter can not be blank");
        }
        if (Name.requireBackticks(parameter)) {
            throw new IllegalArgumentException("parameters should not require backticks as '" + parameter + "'.");
        }

        return new Parameter(parameter);
    }

    public static Parameter of(final int index)
    {
        if (0 > index) {
            throw new IllegalArgumentException("indexed oparameters should be 0 or positive");
        }
        return new Parameter(Integer.toString(index));
    }
}

