/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

public class Hops
{
    static final class Unique
    {
        static final Hops DEFAULT = new Hops(1, 1);
        static final Hops FROM_ZERO_TO_INFINITE = new Hops(Hops.ANY, Hops.ANY);
    }

    static final int ANY = -1;

    final int min, max;

    Hops(final int min, final int max)
    {
        this.min = min;
        this.max = max;
    }

    public boolean isDefault()
    {
        return getDefault().equals(this);
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof Hops) {
            final var other = (Hops) o;

            return (this.min == other.min) && (this.max == other.max);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = Integer.hashCode(this.min);
        result = 31 * Integer.hashCode(result + this.max);
        return result;
    }

    @Override public String toString()
    {
        return "Hops(min=" + this.min + ", max=" + this.max + ')';
    }

    public static Hops getDefault() { return Unique.DEFAULT; }

    public String getCypher()
    {
        if (1 == this.min && 1 == this.max) {
            return "";
        }
        if (ANY == this.min && ANY == this.max) {
            return "*";
        }
        if (this.min == this.max) {
            return "*" + this.min;
        }
        if (ANY == this.min) {
            return "*.." + this.max;
        }
        if (ANY == this.max) {
            return "*" + this.min + "..";
        }
        return "*" + this.min + ".." + this.max;
    }
}
