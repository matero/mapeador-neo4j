/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

public interface Literal extends Expression.Comparable, Expression.WithAlias
{
    NullLiteral NULL = NullLiteral.INSTANCE;

    boolean isNull();

    enum NullLiteral implements Literal, Predicate
    {
        INSTANCE;

        @Override public void appendTo(final StringBuilder builder) {builder.append("NULL");}

        @Override public boolean isNull() {return true;}

        @Override public NullLiteral group() {return this;}
    }

    enum BooleanLiteral implements Literal, Predicate
    {
        TRUE, FALSE, NULL;

        public static BooleanLiteral of(final boolean value) {return value ? TRUE : FALSE;}
        public static BooleanLiteral of(final Boolean value) {return null == value ? NULL : value ? TRUE : FALSE;}

        @Override public void appendTo(final StringBuilder builder) {builder.append(name());}

        @Override public boolean isNull() {return NULL == this;}

        @Override public BooleanLiteral group() {return this;}
    }

    final class ByteLiteral implements Literal
    {
        private static final class Null
        {
            private static final ByteLiteral INSTANCE = new ByteLiteral((byte) 0);
        }

        private final byte value;

        ByteLiteral(final byte value) {this.value = value;}

        @Override public ByteLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static ByteLiteral of(final byte value) {return new ByteLiteral(value);}

        static ByteLiteral of(final Byte value) {return null == value ? Null.INSTANCE : ByteLiteral.of(value.byteValue());}
    }

    final class DoubleLiteral implements Literal
    {
        private static final class Null
        {
            private static final DoubleLiteral INSTANCE = new DoubleLiteral(0.0d);
        }

        private final double value;

        DoubleLiteral(final double value) {this.value = value;}

        @Override public DoubleLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static DoubleLiteral of(final double value) {return new DoubleLiteral(value);}

        static DoubleLiteral of(final Double value) {return null == value ? Null.INSTANCE : DoubleLiteral.of(value.doubleValue());}
    }

    final class FloatLiteral implements Literal
    {
        private static final class Null
        {
            private static final FloatLiteral INSTANCE = new FloatLiteral(0.0f);
        }

        private final float value;

        FloatLiteral(final float value) {this.value = value;}

        @Override public FloatLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static FloatLiteral of(final float value) {return new FloatLiteral(value);}

        static FloatLiteral of(final Float value) {return null == value ? Null.INSTANCE : FloatLiteral.of(value.floatValue());}
    }

    final class IntegerLiteral implements Literal
    {
        private static final class Null
        {
            private static final IntegerLiteral INSTANCE = new IntegerLiteral(0);
        }

        private final int value;

        IntegerLiteral(final int value) {this.value = value;}

        @Override public IntegerLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static IntegerLiteral of(final int value) {return new IntegerLiteral(value);}

        static IntegerLiteral of(final Integer value) {return null == value ? Null.INSTANCE : IntegerLiteral.of(value.intValue());}
    }

    final class LongLiteral implements Literal
    {
        private static final class Null
        {
            private static final LongLiteral INSTANCE = new LongLiteral(0);
        }

        private final long value;

        LongLiteral(final long value) {this.value = value;}

        @Override public LongLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static LongLiteral of(final long value) {return new LongLiteral(value);}

        static LongLiteral of(final Long value) {return null == value ? Null.INSTANCE : LongLiteral.of(value.longValue());}
    }

    final class ShortLiteral implements Literal
    {
        private static final class Null
        {
            private static final ShortLiteral INSTANCE = new ShortLiteral((short) 0);
        }

        private final short value;

        ShortLiteral(final short value) {this.value = value;}

        @Override public ShortLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return Null.INSTANCE == this;}

        static ShortLiteral of(final short value) {return new ShortLiteral(value);}

        static ShortLiteral of(final Short value) {return null == value ? Null.INSTANCE : ShortLiteral.of(value.shortValue());}
    }

    final class StringLiteral implements Literal, StringExpression
    {
        private static final class Null
        {
            private static final StringLiteral INSTANCE = new StringLiteral(null);
        }

        private final String value;

        StringLiteral(final String representation) {this.value = representation;}

        @Override public StringLiteral group() {return this;}

        @Override public void appendTo(final StringBuilder builder)
        {
            if (isNull()) {
                NULL.appendTo(builder);
            } else {
                builder.append(this.value);
            }
        }

        @Override public boolean isNull() {return null == this.value;}

        static StringLiteral of(final String value)
        {
            if (null == value) {
                return Null.INSTANCE;
            }
            return ofNoNull(value);
        }

        static StringLiteral ofNoNull(final String value)
        {
            if (value.isEmpty()) {
                return new StringLiteral("''");
            }
            if (value.isBlank()) {
                return new StringLiteral('\'' + value + '\'');
            }
            if (value.contains("'")) {
                if (value.contains("\"")) {
                    return new StringLiteral('\'' + value.replaceAll("'", "\\'") + '\'');
                } else {
                    return new StringLiteral('"' + value + '"');
                }
            } else {
                return new StringLiteral('\'' + value + '\'');
            }
        }

        static StringLiteral of(final char value)
        {
            if ('\'' == value) {
                return new StringLiteral("\"'\"");
            } else {
                return new StringLiteral("'" + value + '\'');
            }
        }

        static StringLiteral of(final Character value)
        {
            if (null == value) {
                return Null.INSTANCE;
            }
            if ('\'' == value) {
                return new StringLiteral("\"'\"");
            } else {
                return new StringLiteral("'" + value + '\'');
            }
        }
    }

    abstract class BooleanListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final BooleanListLiteral INSTANCE = new BooleanListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        BooleanListLiteral() {/*nothing to do*/}

        @Override public final BooleanListLiteral group() {return this;}

        static BooleanListLiteral of(final boolean[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static BooleanListLiteral of(final Boolean[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new boolean[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends BooleanListLiteral
        {
            private final boolean[] elements;

            OfJavaType(final boolean[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends BooleanListLiteral
        {
            private final Boolean[] elements;

            OfWrapper(final Boolean[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    abstract class ByteListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final ByteListLiteral INSTANCE = new ByteListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        ByteListLiteral() {/*nothing to do*/}

        @Override public final ByteListLiteral group() {return this;}

        static ByteListLiteral of(final byte[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static ByteListLiteral of(final Byte[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new byte[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends ByteListLiteral
        {
            private final byte[] elements;

            OfJavaType(final byte[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends ByteListLiteral
        {
            private final Byte[] elements;

            OfWrapper(final Byte[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    abstract class DoubleListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final DoubleListLiteral INSTANCE = new DoubleListLiteral()
            {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        DoubleListLiteral() {/*nothing to do*/}

        @Override public final DoubleListLiteral group() {return this;}

        static DoubleListLiteral of(final double[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static DoubleListLiteral of(final Double[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new double[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends DoubleListLiteral
        {
            private final double[] elements;

            OfJavaType(final double[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends DoubleListLiteral
        {
            private final Double[] elements;

            OfWrapper(final Double[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    final class ExpressionListLiteral extends GenericListLiteral<Expression>
    {
        private static final class Empty
        {
            private static final ExpressionListLiteral INSTANCE = new ExpressionListLiteral(null);
        }

        ExpressionListLiteral(final Expression[] elements) {super(elements);}

        @Override public ExpressionListLiteral group() {return this;}

        @Override void appendElementAt(final int i, final StringBuilder builder) {this.elements[i].appendTo(builder);}

        static ExpressionListLiteral of(final Expression[] elements)
        {
            if (null == elements || 0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new ExpressionListLiteral(FailIf.hasNull(elements, "elements"));
            }
        }
    }

    abstract class FloatListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final FloatListLiteral INSTANCE = new FloatListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        FloatListLiteral() {/*nothing to do*/}

        @Override public final FloatListLiteral group() {return this;}

        static FloatListLiteral of(final float[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static FloatListLiteral of(final Float[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new float[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends FloatListLiteral
        {
            private final float[] elements;

            OfJavaType(final float[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends FloatListLiteral
        {
            private final Float[] elements;

            OfWrapper(final Float[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    abstract class IntegerListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final IntegerListLiteral INSTANCE = new IntegerListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        IntegerListLiteral() {/*nothing to do*/}

        @Override public final IntegerListLiteral group() {return this;}

        static IntegerListLiteral of(final int[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static IntegerListLiteral of(final Integer[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new int[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends IntegerListLiteral
        {
            private final int[] elements;

            OfJavaType(final int[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends IntegerListLiteral
        {
            private final Integer[] elements;

            OfWrapper(final Integer[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    abstract class LongListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final LongListLiteral INSTANCE = new LongListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        LongListLiteral() {/*nothing to do*/}

        @Override public final LongListLiteral group() {return this;}

        static LongListLiteral of(final long[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static LongListLiteral of(final Long[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new long[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends LongListLiteral
        {
            private final long[] elements;

            OfJavaType(final long[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends LongListLiteral
        {
            private final Long[] elements;

            OfWrapper(final Long[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    final class ObjectListLiteral extends GenericListLiteral<Object>
    {
        private static final class Empty
        {
            private static final ObjectListLiteral INSTANCE = new ObjectListLiteral(null);
        }

        ObjectListLiteral(final Object[] elements) {super(elements);}

        @Override public ObjectListLiteral group() {return this;}

        @Override void appendElementAt(final int i, final StringBuilder builder) {Append.to(builder, this.elements[i]);}

        static ObjectListLiteral of(final Object[] elements)
        {
            if (null == elements || 0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new ObjectListLiteral(FailIf.hasNull(elements, "elements"));
            }
        }

        static ObjectListLiteral of(final java.util.Collection<?> elements)
        {
            if (elements.isEmpty()) {
                return Empty.INSTANCE;
            } else {
                return new ObjectListLiteral(elements.toArray());
            }
        }
    }

    abstract class ShortListLiteral extends ListLiteral
    {
        private static final class Empty
        {
            private static final ShortListLiteral INSTANCE = new ShortListLiteral() {
                @Override int size() {return 0;}

                @Override void appendElementAt(final int i, final StringBuilder builder) {/*nothing to do*/}
            };
        }

        ShortListLiteral() {/*nothing to do*/}

        @Override public final ShortListLiteral group() {return this;}

        static ShortListLiteral of(final short[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new OfJavaType(elements);
            }
        }

        static ShortListLiteral of(final Short[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                if (AreAll.notNull(elements)) {
                    final var data = new short[elements.length];
                    for (int i = 0; i < elements.length; i++) {
                        data[i] = elements[i];
                    }
                    return new OfJavaType(data);
                } else {
                    return new OfWrapper(elements);
                }
            }
        }

        private static final class OfJavaType extends ShortListLiteral
        {
            private final short[] elements;

            OfJavaType(final short[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }

        private static final class OfWrapper extends ShortListLiteral
        {
            private final Short[] elements;

            OfWrapper(final Short[] elements) {this.elements = elements;}

            @Override int size() {return this.elements.length;}

            @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}
        }
    }

    final class StringListLiteral extends GenericListLiteral<String>
    {
        private static final class Empty
        {
            private static final StringListLiteral INSTANCE = new StringListLiteral(null);
        }

        StringListLiteral(final String[] elements) {super(elements);}

        @Override public StringListLiteral group() {return this;}

        static StringListLiteral of(final String[] elements)
        {
            if (0 == elements.length) {
                return Empty.INSTANCE;
            } else {
                return new StringListLiteral(elements);
            }
        }
    }
}

abstract class ListLiteral implements Literal
{
    @Override public final void appendTo(final StringBuilder builder)
    {
        if (isEmpty()) {
            builder.append("[ ]");
        } else {
            builder.append('[');
            appendElementAt(0, builder);
            for (int i = 1; i < size(); i++) {
                builder.append(", ");
                appendElementAt(i, builder);
            }
            builder.append(']');
        }
    }

    @Override public final boolean isNull() {return false;}

    boolean isEmpty() { return 0 == size(); }

    abstract int size();

    abstract void appendElementAt(final int i, final StringBuilder builder);
}

abstract class GenericListLiteral<T> extends ListLiteral
{
    protected final T[] elements;

    GenericListLiteral(final T[] elements) {this.elements = elements;}

    @Override void appendElementAt(final int i, final StringBuilder builder) {builder.append(this.elements[i]);}

    @Override int size() {return this.elements.length;}
}

