/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.helpers;

import java.util.Objects;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isUpperCase;
import static java.lang.Character.toUpperCase;

public enum RelationshipName
{
    INSTANCE;

    public String getTypeFrom(final String identifier) {return ':' + toNeo4JSuggestedFormat(identifier);}

    public String toNeo4JSuggestedFormat(final String identifier)
    {
        Objects.requireNonNull(identifier, "identifier is required");

        final var content = identifier.toCharArray();
        final var result = new StringBuilder(content.length + 5);

        boolean lastUppercase = false;

        for (int i = 0; i < content.length; i++) {
            var ch = content[i];
            final var lastEntry = getLastEntry(result);

            if (isUndescore(ch)) {
                lastUppercase = false;

                if (isUndescore(lastEntry)) {
                    continue;
                } else {
                    ch = '_';
                }
            } else {
                if (isUpperCase(ch)) {
                    // is start?
                    if (!isEmpty(result)) {
                        if (lastUppercase) {
                            // test if end of acronym
                            if (i + 1 < content.length) {
                                final char next = content[i + 1];
                                if (!isUpperCase(next) && isAlphabetic(next)) {
                                    // end of acronym
                                    if (isAcronymEnd(lastEntry)) {
                                        result.append('_');
                                    }
                                }
                            }
                        } else {
                            if (wasLowercase(lastEntry)) {
                                result.append('_');
                            }
                        }
                    }
                    lastUppercase = true;
                } else {
                    ch = toUpperCase(ch);
                    lastUppercase = false;
                }
            }

            result.append(ch);
        }
        return result.toString();
    }

    private boolean wasLowercase(final char lastEntry)
    {
        return '_' != lastEntry;
    }

    private boolean isAcronymEnd(final char lastEntry)
    {
        return !isUndescore(lastEntry);
    }

    private boolean isUndescore(final char ch)
    {
        return '_' == ch;
    }

    private char getLastEntry(final StringBuilder builder)
    {
        return isEmpty(builder) ? 'X' : getLast(builder);
    }

    private boolean isEmpty(final StringBuilder builder)
    {
        return 0 == builder.length();
    }

    private char getLast(final StringBuilder builder)
    {
        return builder.charAt(builder.length() - 1);
    }
}
