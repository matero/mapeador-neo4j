/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.models;

import mapeador.criteria.Criteria;
import mapeador.criteria.CriteriaFinders;
import mapeador.criteria.EntityFieldValue;
import mapeador.criteria.ResultCriteria;
import mapeador.criteria.ValueCriteria;
import mapeador.cypher.Pattern;
import mapeador.cypher.PropertyValue;
import mapeador.generated.metadata.NodeLabel;
import mapeador.model.GraphNode;
import mapeador.model.IntegerProperty;
import mapeador.model.Labels;
import mapeador.model.Property;
import mapeador.model.StringProperty;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.Node;

import javax.annotation.processing.Generated;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

@Generated("mapeador-neo4j")
abstract class Person_Node extends GraphNode<PersonVariable, Person>
{
    Person_Node() {}

    public static final Metadata METADATA = Metadata.INSTANCE;
    public static final PersonVariable NODE = new PersonVariable("person");

    public static final IntegerProperty<Person> id = METADATA.id;
    public static final StringProperty<Person> name = METADATA.name;
    public static final IntegerProperty<Person> born = METADATA.born;

    @Override public final Metadata getMetadata() {return Metadata.INSTANCE;}

    public enum Metadata implements GraphNode.Metadata<PersonVariable, Person>
    {
        INSTANCE;

        final Labels __labels__;
        final List<Property<Person, ?>> __properties__;

        private final IntegerProperty<Person> id;
        private final StringProperty<Person> name;
        private final IntegerProperty<Person> born;


        Metadata()
        {
            this.id = new IntegerProperty<>(Person.class, "id", "id", "mapeador.models.Person.id");
            this.name = new StringProperty<>(Person.class, "name", "name", "mapeador.models.Person.name");
            this.born = new IntegerProperty<>(Person.class, "born", "born", "mapeador.models.Person.born");
            this.__labels__ = NodeLabel.Person;
            this.__properties__ = List.of(this.id, this.name, this.born);
        }

        @Override public IntegerProperty<Person> getIdProperty() {return this.id;}

        @Override public Labels getNodeLabels() {return this.__labels__;}

        @Override public Class<Person> getModelClass() {return Person.class;}

        @Override public Class<PersonVariable> getVariableClass() {return PersonVariable.class;}

        @Override public String getParameterClasses() {return "Person";}

        @Override public List<Property<Person, ?>> getProperties() {return this.__properties__;}

        @Override public boolean hasPropertyNamed(final String name)
        {
            Objects.requireNonNull(name, "name is required");
            switch (name) {
                case "id":
                case "name":
                case "born":
                    return true;
                default:
                    return false;
            }
        }

        @Override public Property<Person, ?> getProperty(final String name)
        {
            Objects.requireNonNull(name, "name is required");
            switch (name) {
                case "id":
                    return this.id;
                case "name":
                    return this.name;
                case "born":
                    return this.born;
                default:
                    throw new IllegalArgumentException("no property named '" + name + "'is known.");
            }
        }
    }

    public static Node find(final int id) {return NodeCriteria.finder().match(NODE, Person.id.to(id)).fetch();}

    public static Node get(final int id) {return NodeCriteria.getter().match(NODE, Person.id.to(id)).fetch();}

    public static Node find(final Session session, final int id) {return NodeCriteria.finder().match(NODE, Person.id.to(id)).fetch(session);}

    public static Node get(final Session session, final int id) {return NodeCriteria.getter().match(NODE, Person.id.to(id)).fetch(session);}

    public static Node find(final Transaction tx, final int id) {return NodeCriteria.finder().match(NODE, Person.id.to(id)).fetch(tx);}

    public static Node get(final Transaction tx, final int id) {return NodeCriteria.getter().match(NODE, Person.id.to(id)).fetch(tx);}

    public static MatchCriteria all() {return all(generateNodeName());}

    public static MatchCriteria all(final EntityFieldValue<Person, ?> value) {return all(generateNodeName(), value);}

    public static MatchCriteria all(
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return all(VariableNameGenerator.INSTANCE.nextName("p"), v1, v2);
    }

    public static MatchCriteria all(final String nodeName)
    {
        return new MatchCriteria().match(nodeName);
    }

    public static MatchCriteria all(final String nodeName, final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().match(nodeName, value);
    }

    public static MatchCriteria all(final String nodeName, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().match(nodeName, v1, v2);
    }

    public static MatchCriteria all(final PersonVariable variable) {return all(variable.getName());}

    public static MatchCriteria all(final PersonVariable variable, final EntityFieldValue<Person, ?> value) {return all(variable.getName(), value);}

    public static MatchCriteria all(final PersonVariable variable, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
    {
        return all(variable.getName(), v1, v2);
    }

    public static NodeCriteria findBy(final PersonVariable node, final EntityFieldValue<Person, ?> value)
    {
        return NodeCriteria.finder().match(node, value);
    }

    public static NodeCriteria findBy(final PersonVariable node, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
    {
        return NodeCriteria.finder().match(node, v1, v2);
    }

    public static NodeCriteria findBy(final String nodeName, final EntityFieldValue<Person, ?> value)
    {
        return NodeCriteria.finder().match(nodeName, value);
    }

    public static NodeCriteria findBy(final String nodeName, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
    {
        return NodeCriteria.finder().match(nodeName, v1, v2);
    }

    public static NodeCriteria findBy(final EntityFieldValue<Person, ?> value)
    {
        return NodeCriteria.finder().match(NODE, value);
    }

    public static NodeCriteria findBy(
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return NodeCriteria.finder().match(NODE, v1, v2);
    }

    public static NodeCriteria getBy(final EntityFieldValue<Person, ?> value)
    {
        return NodeCriteria.getter().match(NODE, value);
    }

    public static NodeCriteria getBy(
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return NodeCriteria.getter().match(NODE, v1, v2);
    }

    public static PersonVariable node(final String nodeName) {return new PersonVariable(nodeName);}

    public static Person get() {return Unique.INSTANCE;}

    private static final class Unique
    {
        private static final Person INSTANCE = new Person();
    }

    public static Movie.MatchCriteria actedIn()
    {
        return new MatchCriteria().actedIn();
    }

    public static Movie.MatchCriteria actedIn(final String nodeName)
    {
        return new MatchCriteria().actedIn(nodeName);
    }

    public static Movie.MatchCriteria actedIn(final MovieVariable node)
    {
        return new MatchCriteria().actedIn(node);
    }

    public static Movie.MatchCriteria actedIn(final EntityFieldValue<Movie, ?> value)
    {
        return new MatchCriteria().actedIn(value);
    }

    public static Movie.MatchCriteria actedIn(final String nodeName, final EntityFieldValue<Movie, ?> value)
    {
        return new MatchCriteria().actedIn(nodeName, value);
    }

    public static Movie.MatchCriteria actedIn(final MovieVariable node, final EntityFieldValue<Movie, ?> value)
    {
        return new MatchCriteria().actedIn(node, value);
    }

    public static Movie.MatchCriteria actedIn(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return new MatchCriteria().actedIn(v1, v2);
    }

    public static Movie.MatchCriteria actedIn(
        final String nodeName,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return new MatchCriteria().actedIn(nodeName, v1, v2);
    }

    public static Movie.MatchCriteria actedIn(
        final MovieVariable node,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return new MatchCriteria().actedIn(node, v1, v2);
    }

    public static Movie.MatchCriteria actedIn(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return new MatchCriteria().actedIn(v1, v2, v3);
    }

    public static Movie.MatchCriteria actedIn(
        final String nodeName,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return new MatchCriteria().actedIn(nodeName, v1, v2, v3);
    }

    public static Movie.MatchCriteria actedIn(
        final MovieVariable node,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return new MatchCriteria().actedIn(node, v1, v2, v3);
    }

    /* RELATION criteria methods */
    public static class NodeCriteria extends ValueCriteria<Node>
    {
        NodeCriteria(final Function<Result, Value> extractor) {this(Pattern.MATCH(), extractor);}

        NodeCriteria(final Pattern pattern, final Function<Result, Value> extractor) {super(pattern, extractor, Value::asNode);}

        public NodeCriteria(
            final Pattern pattern,
            final Function<Result, Value> extractor,
            final Map<String, Object> actualParameters,
            final Set<String> requiredParameters)
        {
            super(pattern, extractor, Value::asNode, actualParameters, requiredParameters);
        }

        public Movie.MatchCriteria actedIn()
        {
            return Finder.get().actedIn(this);
        }

        public Movie.MatchCriteria actedIn(final String nodeName)
        {
            return Finder.get().actedIn(this, nodeName);
        }

        public Movie.MatchCriteria actedIn(final MovieVariable node)
        {
            return Finder.get().actedIn(this, node);
        }

        public Movie.MatchCriteria actedIn(final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, value);
        }

        public Movie.MatchCriteria actedIn(final String nodeName, final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, nodeName, value);
        }

        public Movie.MatchCriteria actedIn(final MovieVariable node, final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, node, value);
        }

        public Movie.MatchCriteria actedIn(
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, nodeName, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, node, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, v1, v2, v3);
        }

        public Movie.MatchCriteria actedIn(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, nodeName, v1, v2, v3);
        }

        public Movie.MatchCriteria actedIn(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, node, v1, v2, v3);
        }

        NodeCriteria match(final String nodeName)
        {
            Finder.get().find(this, nodeName);
            return this;
        }

        NodeCriteria match(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            Finder.get().find(this, nodeName, value);
            return this;
        }

        NodeCriteria match(
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            Finder.get().find(this, nodeName, v1, v2);
            return this;
        }

        NodeCriteria match(final PersonVariable node)
        {
            Finder.get().find(this, node);
            return this;
        }

        NodeCriteria match(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            Finder.get().find(this, node, value);
            return this;
        }

        NodeCriteria match(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            Finder.get().find(this, node, v1, v2);
            return this;
        }

        public MatchCriteria plus(final PersonVariable n)
        {
            super.plus(n);
            return new MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public MatchCriteria plus(
            final PersonVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(final MovieVariable n)
        {
            super.plus(n);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(
            final MovieVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public static NodeCriteria finder() { return new NodeCriteria(ValueCriteria::findFirstAt);}

        public static NodeCriteria getter() { return new NodeCriteria(ValueCriteria::getFirstAt);}
    }

    public static class MatchCriteria extends ResultCriteria
    {
        MatchCriteria() {this(Pattern.MATCH());}

        MatchCriteria(final Pattern pattern) {super(pattern);}

        public MatchCriteria(final Pattern pattern, final Map<String, Object> actualParameters, final Set<String> requiredParameters)
        {
            super(pattern, actualParameters, requiredParameters);
        }

        public Movie.MatchCriteria actedIn()
        {
            return Finder.get().actedIn(this);
        }

        public Movie.MatchCriteria actedIn(final String nodeName)
        {
            return Finder.get().actedIn(this, nodeName);
        }

        public Movie.MatchCriteria actedIn(final MovieVariable node)
        {
            return Finder.get().actedIn(this, node);
        }

        public Movie.MatchCriteria actedIn(final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, value);
        }

        public Movie.MatchCriteria actedIn(final String nodeName, final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, nodeName, value);
        }

        public Movie.MatchCriteria actedIn(final MovieVariable node, final EntityFieldValue<Movie, ?> value)
        {
            return Finder.get().actedIn(this, node, value);
        }

        public Movie.MatchCriteria actedIn(
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, nodeName, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            return Finder.get().actedIn(this, node, v1, v2);
        }

        public Movie.MatchCriteria actedIn(
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, v1, v2, v3);
        }

        public Movie.MatchCriteria actedIn(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, nodeName, v1, v2, v3);
        }

        public Movie.MatchCriteria actedIn(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            return Finder.get().actedIn(this, node, v1, v2, v3);
        }

        MatchCriteria match(final String nodeName)
        {
            Finder.get().find(this, nodeName);
            return this;
        }

        MatchCriteria match(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            Finder.get().find(this, nodeName, value);
            return this;
        }

        MatchCriteria match(
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            Finder.get().find(this, nodeName, v1, v2);
            return this;
        }

        MatchCriteria match(final PersonVariable node)
        {
            Finder.get().find(this, node);
            return this;
        }

        MatchCriteria match(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            Finder.get().find(this, node, value);
            return this;
        }

        MatchCriteria match(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            Finder.get().find(this, node, v1, v2);
            return this;
        }

        public MatchCriteria plus(final PersonVariable n)
        {
            super.plus(n);
            return this;
        }

        public MatchCriteria plus(
            final PersonVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return this;
        }

        public Movie.MatchCriteria plus(final MovieVariable n)
        {
            super.plus(n);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(
            final MovieVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }
    }

    static class Finder extends CriteriaFinders<PersonVariable, Person>
    {
        Finder() {/*nothing to do*/}

        @Override protected GraphNode.Metadata<PersonVariable, Person> metadata()
        {
            return Person.METADATA;
        }

        Movie.MatchCriteria actedIn(final Criteria<?, ?> criteria)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(Movie.METADATA);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(final Criteria<?, ?> criteria, final String nodeName)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(nodeName, Movie.METADATA);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(final Criteria<?, ?> criteria, final MovieVariable node)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(node);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(final Criteria<?, ?> criteria, final EntityFieldValue<Movie, ?> value)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(Movie.METADATA, value);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(final Criteria<?, ?> criteria, final String nodeName, final EntityFieldValue<Movie, ?> value)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(nodeName, Movie.METADATA, value);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> value)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(node, value);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(Movie.METADATA, v1, v2);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(nodeName, Movie.METADATA, v1, v2);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(node, v1, v2);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(Movie.METADATA, v1, v2, v3);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(nodeName, Movie.METADATA, v1, v2, v3);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Movie.MatchCriteria actedIn(
            final Criteria<?, ?> criteria,
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            final var p = patternOf(criteria);
            p.outgoing(ACTED_IN.METADATA).to(node, v1, v2, v3);
            return new Movie.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        static Finder get() {return Unique.INSTANCE;}

        static final class Unique
        {
            static final Finder INSTANCE = new Finder();
        }
    }

    private static String generateNodeName() {return VariableNameGenerator.INSTANCE.nextName("person");}
}

