/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.models;

import mapeador.generated.metadata.RelationshipType;
import mapeador.model.GraphRelation;
import mapeador.model.Property;
import mapeador.model.StringProperty;
import mapeador.model.Types;

import javax.annotation.processing.Generated;
import java.util.List;
import java.util.Objects;

@Generated("mapeador-neo4j")
abstract class ACTED_IN_Relation extends GraphRelation<PersonVariable, Person, MovieVariable, Movie, ACTED_INVariable, ACTED_IN>
{
    ACTED_IN_Relation() {/*nothing to do*/}

    public static final Metadata METADATA = ACTED_IN_Relation.Metadata.INSTANCE;
    public static final ACTED_INVariable REL = new ACTED_INVariable("actedIn");
    public static final Types T = METADATA.getRelationTypes();

    @Override public final Metadata getMetadata() {return Metadata.INSTANCE;}

    enum Metadata implements GraphRelation.Metadata<PersonVariable, Person, MovieVariable, Movie, ACTED_INVariable, ACTED_IN>
    {
        INSTANCE;

        final Types __types__;
        final List<Property<ACTED_IN, ?>> __properties__;

        private final StringProperty<ACTED_IN> role;

        Metadata()
        {
            this.role = new StringProperty<>(ACTED_IN.class, "role", "role", "mapeador.models.Person.role");
            this.__types__ = RelationshipType.ACTED_IN;
            this.__properties__ = List.of(this.role);
        }

        @Override public Types getRelationTypes() {return this.__types__;}

        @Override public Class<ACTED_IN> getRelationshipClass() {return ACTED_IN.class;}

        @Override public Class<ACTED_INVariable> getVariableClass() {return ACTED_INVariable.class;}

        @Override public List<Property<ACTED_IN, ?>> getProperties() {return this.__properties__;}

        @Override public boolean hasPropertyNamed(final String name)
        {
            Objects.requireNonNull(name, "name is required");
            return "role".equals(name);
        }

        @Override public Property<ACTED_IN, ?> getProperty(final String name)
        {
            Objects.requireNonNull(name, "name is required");
            if (!"role".equals(name)) {
                throw new IllegalArgumentException("no property named '" + name + "'is known.");
            }
            return this.role;
        }
    }

    private static final class Unique {
        private static final ACTED_IN INSTANCE = new ACTED_IN();
    }

    public static ACTED_IN get() {return Unique.INSTANCE;}
}
