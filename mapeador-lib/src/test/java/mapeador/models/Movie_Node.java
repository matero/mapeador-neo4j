/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.models;

import mapeador.criteria.Criteria;
import mapeador.criteria.CriteriaFinders;
import mapeador.criteria.EntityFieldValue;
import mapeador.criteria.ResultCriteria;
import mapeador.criteria.ValueCriteria;
import mapeador.cypher.Pattern;
import mapeador.cypher.PropertyValue;
import mapeador.generated.metadata.NodeLabel;
import mapeador.model.GraphNode;
import mapeador.model.IntegerProperty;
import mapeador.model.Labels;
import mapeador.model.Property;
import mapeador.model.StringProperty;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.Node;

import javax.annotation.processing.Generated;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

@Generated("mapeador-neo4j")
abstract class Movie_Node extends GraphNode<MovieVariable, Movie>
{
    Movie_Node() {}

    public static final Metadata METADATA = Metadata.INSTANCE;
    public static final MovieVariable NODE = new MovieVariable("movie");

    public static final IntegerProperty<Movie> id = METADATA.id;
    public static final StringProperty<Movie> title = METADATA.title;
    public static final IntegerProperty<Movie> released = METADATA.released;
    public static final StringProperty<Movie> tagline = METADATA.tagline;

    @Override public final Metadata getMetadata() {return Metadata.INSTANCE;}

    public enum Metadata implements GraphNode.Metadata<MovieVariable, Movie>
    {
        INSTANCE;

        final Labels __labels__;
        final List<Property<Movie, ?>> __properties__;

        private final IntegerProperty<Movie> id;
        private final StringProperty<Movie> title;
        private final IntegerProperty<Movie> released;
        private final StringProperty<Movie> tagline;


        Metadata()
        {
            this.id = new IntegerProperty<>(Movie.class, "id", "id", "mapeador.models.Movie.id");
            this.title = new StringProperty<>(Movie.class, "title", "title", "mapeador.models.Movie.title");
            this.released = new IntegerProperty<>(Movie.class, "released", "released", "mapeador.models.Movie.released");
            this.tagline = new StringProperty<>(Movie.class, "tagline", "tagline", "mapeador.models.Movie.tagline");
            this.__labels__ = NodeLabel.Movie;
            this.__properties__ = List.of(this.id, this.title, this.released, this.tagline);
        }

        @Override public IntegerProperty<Movie> getIdProperty() {return this.id;}

        @Override public Labels getNodeLabels() {return this.__labels__;}

        @Override public Class<Movie> getModelClass() {return Movie.class;}

        @Override public Class<MovieVariable> getVariableClass() {return MovieVariable.class;}

        @Override public String getParameterClasses() {return "Movie";}

        @Override public List<Property<Movie, ?>> getProperties() {return this.__properties__;}

        @Override public boolean hasPropertyNamed(final String title)
        {
            Objects.requireNonNull(title, "title is required");
            switch (title) {
                case "id":
                case "title":
                case "released":
                case "tagline":
                    return true;
                default:
                    return false;
            }
        }

        @Override public Property<Movie, ?> getProperty(final String title)
        {
            Objects.requireNonNull(title, "title is required");
            switch (title) {
                case "id":
                    return this.id;
                case "title":
                    return this.title;
                case "released":
                    return this.released;
                case "tagline":
                    return this.tagline;
                default:
                    throw new IllegalArgumentException("no property named '" + title + "'is known.");
            }
        }
    }

    public static Node find(final int id) {return NodeCriteria.finder().match(NODE, Movie.id.to(id)).fetch();}

    public static Node get(final int id) {return NodeCriteria.getter().match(NODE, Movie.id.to(id)).fetch();}

    public static Node find(final Session session, final int id) {return NodeCriteria.finder().match(NODE, Movie.id.to(id)).fetch(session);}

    public static Node get(final Session session, final int id) {return NodeCriteria.getter().match(NODE, Movie.id.to(id)).fetch(session);}

    public static Node find(final Transaction tx, final int id) {return NodeCriteria.finder().match(NODE, Movie.id.to(id)).fetch(tx);}

    public static Node get(final Transaction tx, final int id) {return NodeCriteria.getter().match(NODE, Movie.id.to(id)).fetch(tx);}

    public static MatchCriteria all() {return all(generateNodeName());}

    public static MatchCriteria all(final EntityFieldValue<Movie, ?> value) {return all(generateNodeName(), value);}

    public static MatchCriteria all(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return all(VariableNameGenerator.INSTANCE.nextName("p"), v1, v2);
    }

    public static MatchCriteria all(final String nodeName)
    {
        return new MatchCriteria().match(nodeName);
    }

    public static MatchCriteria all(final String nodeName, final EntityFieldValue<Movie, ?> value)
    {
        return new MatchCriteria().match(nodeName, value);
    }

    public static MatchCriteria all(
        final String nodeName,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return new MatchCriteria().match(nodeName, v1, v2);
    }

    public static MatchCriteria all(
        final String nodeName,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return new MatchCriteria().match(nodeName, v1, v2, v3);
    }

    public static MatchCriteria all(final MovieVariable variable)
    {
        return all(variable.getName());
    }

    public static MatchCriteria all(final MovieVariable variable, final EntityFieldValue<Movie, ?> value)
    {
        return all(variable.getName(), value);
    }

    public static MatchCriteria all(
        final MovieVariable variable,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return all(variable.getName(), v1, v2);
    }

    public static MatchCriteria all(
        final MovieVariable variable,
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return all(variable.getName(), v1, v2, v3);
    }

    public static NodeCriteria findBy(final EntityFieldValue<Movie, ?> value)
    {
        return NodeCriteria.finder().match(NODE, value);
    }

    public static NodeCriteria findBy(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return NodeCriteria.finder().match(NODE, v1, v2);
    }

    public static NodeCriteria findBy(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return NodeCriteria.finder().match(NODE, v1, v2, v3);
    }

    public static NodeCriteria getBy(final EntityFieldValue<Movie, ?> value)
    {
        return NodeCriteria.getter().match(NODE, value);
    }

    public static NodeCriteria getBy(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2)
    {
        return NodeCriteria.getter().match(NODE, v1, v2);
    }

    public static NodeCriteria getBy(
        final EntityFieldValue<Movie, ?> v1,
        final EntityFieldValue<Movie, ?> v2,
        final EntityFieldValue<Movie, ?> v3)
    {
        return NodeCriteria.getter().match(NODE, v1, v2, v3);
    }

    /* actors */
    public static Person.MatchCriteria actors()
    {
        return new MatchCriteria().actors();
    }

    public static Person.MatchCriteria actors(final String nodeName)
    {
        return new MatchCriteria().actors(nodeName);
    }

    public static Person.MatchCriteria actors(final PersonVariable node)
    {
        return new MatchCriteria().actors(node);
    }

    public static Person.MatchCriteria actors(final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().actors(value);
    }

    public static Person.MatchCriteria actors(final String nodeName, final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().actors(nodeName, value);
    }

    public static Person.MatchCriteria actors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().actors(node, value);
    }

    public static Person.MatchCriteria actors(
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().actors(v1, v2);
    }

    public static Person.MatchCriteria actors(
        final String nodeName,
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().actors(nodeName, v1, v2);
    }

    public static Person.MatchCriteria actors(
        final PersonVariable node,
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().actors(node, v1, v2);
    }

    /* directors */
    public static Person.MatchCriteria directors()
    {
        return new MatchCriteria().directors();
    }

    public static Person.MatchCriteria directors(final String nodeName)
    {
        return new MatchCriteria().directors(nodeName);
    }

    public static Person.MatchCriteria directors(final PersonVariable node)
    {
        return new MatchCriteria().directors(node);
    }

    public static Person.MatchCriteria directors(final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().directors(value);
    }

    public static Person.MatchCriteria directors(final String nodeName, final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().directors(nodeName, value);
    }

    public static Person.MatchCriteria directors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
    {
        return new MatchCriteria().directors(node, value);
    }

    public static Person.MatchCriteria directors(
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().directors(v1, v2);
    }

    public static Person.MatchCriteria directors(
        final String nodeName,
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().directors(nodeName, v1, v2);
    }

    public static Person.MatchCriteria directors(
        final PersonVariable node,
        final EntityFieldValue<Person, ?> v1,
        final EntityFieldValue<Person, ?> v2)
    {
        return new MatchCriteria().directors(node, v1, v2);
    }

    public static MovieVariable node(final String nodeName) {return new MovieVariable(nodeName);}

    public static Movie get() {return Unique.INSTANCE;}

    private static final class Unique
    {
        private static final Movie INSTANCE = new Movie();
    }

    /* RELATION criteria methods */
    public static class NodeCriteria extends ValueCriteria<Node>
    {
        NodeCriteria(final Function<Result, Value> extractor) {this(Pattern.MATCH(), extractor);}

        NodeCriteria(final Pattern pattern, final Function<Result, Value> extractor) {super(pattern, extractor, Value::asNode);}

        public NodeCriteria(
            final Pattern pattern,
            final Function<Result, Value> extractor,
            final Map<String, Object> actualParameters,
            final Set<String> requiredParameters)
        {
            super(pattern, extractor, Value::asNode, actualParameters, requiredParameters);
        }

        public Person.MatchCriteria actors()
        {
            return Finder.get().actors(this);
        }

        public Person.MatchCriteria actors(final String nodeName)
        {
            return Finder.get().actors(this, nodeName);
        }

        public Person.MatchCriteria actors(final PersonVariable node)
        {
            return Finder.get().actors(this, node);
        }

        public Person.MatchCriteria actors(final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, value);
        }

        public Person.MatchCriteria actors(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, nodeName, value);
        }

        public Person.MatchCriteria actors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, node, value);
        }

        public Person.MatchCriteria actors(final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, v1, v2);
        }

        public Person.MatchCriteria actors(final String nodeName, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, nodeName, v1, v2);
        }

        public Person.MatchCriteria actors(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, node, v1, v2);
        }

        public Person.MatchCriteria directors()
        {
            return Finder.get().directors(this);
        }

        public Person.MatchCriteria directors(final String nodeName)
        {
            return Finder.get().directors(this, nodeName);
        }

        public Person.MatchCriteria directors(final PersonVariable node)
        {
            return Finder.get().directors(this, node);
        }

        public Person.MatchCriteria directors(final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, value);
        }

        public Person.MatchCriteria directors(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, nodeName, value);
        }

        public Person.MatchCriteria directors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, node, value);
        }

        public Person.MatchCriteria directors(
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, v1, v2);
        }

        public Person.MatchCriteria directors(
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, nodeName, v1, v2);
        }

        public Person.MatchCriteria directors(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, node, v1, v2);
        }

        NodeCriteria match(final String nodeName)
        {
            Finder.get().find(this, nodeName);
            return this;
        }

        NodeCriteria match(final String nodeName, final EntityFieldValue<Movie, ?> value)
        {
            Finder.get().find(this, nodeName, value);
            return this;
        }

        NodeCriteria match(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            Finder.get().find(this, nodeName, v1, v2);
            return this;
        }

        NodeCriteria match(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            Finder.get().find(this, nodeName, v1, v2, v3);
            return this;
        }

        NodeCriteria match(final MovieVariable node)
        {
            Finder.get().find(this, node);
            return this;
        }

        NodeCriteria match(final MovieVariable node, final EntityFieldValue<Movie, ?> value)
        {
            Finder.get().find(this, node, value);
            return this;
        }

        NodeCriteria match(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            Finder.get().find(this, node, v1, v2);
            return this;
        }

        NodeCriteria match(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            Finder.get().find(this, node, v1, v2, v3);
            return this;
        }

        public Person.MatchCriteria plus(final PersonVariable n)
        {
            super.plus(n);
            return new Person.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Person.MatchCriteria plus(
            final PersonVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new Person.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(final MovieVariable n)
        {
            super.plus(n);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(
            final MovieVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new Movie.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public static NodeCriteria finder() { return new NodeCriteria(ValueCriteria::findFirstAt);}

        public static NodeCriteria getter() { return new NodeCriteria(ValueCriteria::getFirstAt);}
    }

    public static class MatchCriteria extends ResultCriteria
    {
        MatchCriteria() {this(Pattern.MATCH());}

        MatchCriteria(final Pattern pattern) {super(pattern);}

        public MatchCriteria(final Pattern pattern, final Map<String, Object> actualParameters, final Set<String> requiredParameters)
        {
            super(pattern, actualParameters, requiredParameters);
        }

        public Person.MatchCriteria actors()
        {
            return Finder.get().actors(this);
        }

        public Person.MatchCriteria actors(final String nodeName)
        {
            return Finder.get().actors(this, nodeName);
        }

        public Person.MatchCriteria actors(final PersonVariable node)
        {
            return Finder.get().actors(this, node);
        }

        public Person.MatchCriteria actors(final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, value);
        }

        public Person.MatchCriteria actors(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, nodeName, value);
        }

        public Person.MatchCriteria actors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().actors(this, node, value);
        }

        public Person.MatchCriteria actors(
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, v1, v2);
        }

        public Person.MatchCriteria actors(
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, nodeName, v1, v2);
        }

        public Person.MatchCriteria actors(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().actors(this, node, v1, v2);
        }

        public Person.MatchCriteria directors()
        {
            return Finder.get().directors(this);
        }

        public Person.MatchCriteria directors(final String nodeName)
        {
            return Finder.get().directors(this, nodeName);
        }

        public Person.MatchCriteria directors(final PersonVariable node)
        {
            return Finder.get().directors(this, node);
        }

        public Person.MatchCriteria directors(final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, value);
        }

        public Person.MatchCriteria directors(final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, nodeName, value);
        }

        public Person.MatchCriteria directors(final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            return Finder.get().directors(this, node, value);
        }

        public Person.MatchCriteria directors(
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, v1, v2);
        }

        public Person.MatchCriteria directors(
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, nodeName, v1, v2);
        }

        public Person.MatchCriteria directors(
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            return Finder.get().directors(this, node, v1, v2);
        }

        MatchCriteria match(final String nodeName)
        {
            Finder.get().find(this, nodeName);
            return this;
        }

        MatchCriteria match(final String nodeName, final EntityFieldValue<Movie, ?> value)
        {
            Finder.get().find(this, nodeName, value);
            return this;
        }

        MatchCriteria match(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            Finder.get().find(this, nodeName, v1, v2);
            return this;
        }

        MatchCriteria match(
            final String nodeName,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            Finder.get().find(this, nodeName, v1, v2, v3);
            return this;
        }

        MatchCriteria match(final MovieVariable node)
        {
            Finder.get().find(this, node);
            return this;
        }

        MatchCriteria match(final MovieVariable node, final EntityFieldValue<Movie, ?> value)
        {
            Finder.get().find(this, node, value);
            return this;
        }

        MatchCriteria match(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2)
        {
            Finder.get().find(this, node, v1, v2);
            return this;
        }

        MatchCriteria match(
            final MovieVariable node,
            final EntityFieldValue<Movie, ?> v1,
            final EntityFieldValue<Movie, ?> v2,
            final EntityFieldValue<Movie, ?> v3)
        {
            Finder.get().find(this, node, v1, v2, v3);
            return this;
        }

        public Person.MatchCriteria plus(final PersonVariable n)
        {
            super.plus(n);
            return new Person.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Person.MatchCriteria plus(
            final PersonVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return new Person.MatchCriteria(this.pattern, getActualParameters(), getRequiredParameters());
        }

        public Movie.MatchCriteria plus(final MovieVariable n)
        {
            super.plus(n);
            return this;
        }

        public Movie.MatchCriteria plus(
            final MovieVariable n,
            final PropertyValue value,
            final PropertyValue... otherValues)
        {
            super.plus(n, value, otherValues);
            return this;
        }
    }

    static class Finder extends CriteriaFinders<MovieVariable, Movie>
    {
        Finder() {/*nothing to do*/}

        @Override protected GraphNode.Metadata<MovieVariable, Movie> metadata() { return Movie.METADATA; }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(Movie.METADATA);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final String nodeName)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(nodeName, Person.METADATA);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final PersonVariable node)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(node);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final EntityFieldValue<Person, ?> value)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(Person.METADATA, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(nodeName, Person.METADATA, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final PersonVariable node, final EntityFieldValue<Person, ?> value)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(node, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(final Criteria<?, ?> criteria, final EntityFieldValue<Person, ?> v1, final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(Person.METADATA, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(
            final Criteria<?, ?> criteria,
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(nodeName, Person.METADATA, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria actors(
            final Criteria<?, ?> criteria,
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(ACTED_IN.METADATA).from(node, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(Movie.METADATA);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria, final String nodeName)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(nodeName, Person.METADATA);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria, final PersonVariable node)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(node);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria, final EntityFieldValue<Person, ?> value)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(Person.METADATA, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria, final String nodeName, final EntityFieldValue<Person, ?> value)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(nodeName, Person.METADATA, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(final Criteria<?, ?> criteria, final PersonVariable node, final EntityFieldValue<Person, ?>  value)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(node, value);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(
            final Criteria<?, ?> criteria,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(Person.METADATA, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(
            final Criteria<?, ?> criteria,
            final String nodeName,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(nodeName, Person.METADATA, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        Person.MatchCriteria directors(
            final Criteria<?, ?> criteria,
            final PersonVariable node,
            final EntityFieldValue<Person, ?> v1,
            final EntityFieldValue<Person, ?> v2)
        {
            final var p = patternOf(criteria);
            p.incoming(DIRECTED.METADATA).from(node, v1, v2);
            return new Person.MatchCriteria(p, actualParametersOf(criteria), requiredParametersOf(criteria));
        }

        static Finder get() {return Finder.Unique.INSTANCE;}

        static final class Unique
        {
            static final Finder INSTANCE = new Finder();
        }
    }

    private static String generateNodeName() {return VariableNameGenerator.INSTANCE.nextName("movie");}
}

