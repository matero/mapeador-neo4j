/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.models;

import mapeador.generated.metadata.RelationshipType;
import mapeador.model.GraphRelation;
import mapeador.model.Property;
import mapeador.model.Types;

import javax.annotation.processing.Generated;
import java.util.List;
import java.util.Objects;

@Generated("mapeador-neo4j")
final public class DIRECTED extends GraphRelation<PersonVariable, Person, MovieVariable, Movie, DIRECTEDVariable, DIRECTED>
{
    DIRECTED() {/*nothing to do*/}

    public static final Metadata METADATA = DIRECTED.Metadata.INSTANCE;

    @Override public Metadata getMetadata() {return Metadata.INSTANCE;}

    enum Metadata implements GraphRelation.Metadata<PersonVariable, Person, MovieVariable, Movie, DIRECTEDVariable, DIRECTED>
    {
        INSTANCE;

        final Types __types__;

        Metadata()
        {
            this.__types__ = RelationshipType.DIRECTED;
        }

        @Override public Types getRelationTypes() {return this.__types__;}

        @Override public Class<DIRECTED> getRelationshipClass() {return DIRECTED.class;}

        @Override public Class<DIRECTEDVariable> getVariableClass() {return DIRECTEDVariable.class;}

        @Override public List<Property<DIRECTED, ?>> getProperties() {return List.of();}

        @Override public boolean hasPropertyNamed(final String name) {return false;}

        @Override public Property<DIRECTED, ?> getProperty(final String name)
        {
            Objects.requireNonNull(name, "name is required");
            throw new IllegalArgumentException("no property named '" + name + "'is known.");
        }
    }
}
