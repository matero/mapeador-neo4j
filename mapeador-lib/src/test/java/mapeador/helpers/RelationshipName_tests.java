/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.helpers;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class RelationshipName_tests
{
    @ParameterizedTest
    @MethodSource("singleWord")
    void toNeo4JSuggestedFormat_should_format_single_word_to_uppercase(final String word, final String expectedResult)
    {
        assertThat(RelationshipName.INSTANCE.toNeo4JSuggestedFormat(word)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("nWords")
    void toNeo4JSuggestedFormat_should_format_n_words_to_uppercase_separated_by_underscore(final String word, final String expectedResult)
    {
        assertThat(RelationshipName.INSTANCE.toNeo4JSuggestedFormat(word)).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @MethodSource("singleWord")
    void getTypeFrom_should_format_single_word_to_uppercase_with_a_colon(final String word, final String expectedResult)
    {
        assertThat(RelationshipName.INSTANCE.getTypeFrom(word)).isEqualTo(':' + expectedResult);
    }

    @ParameterizedTest
    @MethodSource("nWords")
    void getTypeFrom_should_format_n_words_to_uppercase_separated_by_underscore_with_a_colon(final String word, final String expectedResult)
    {
        assertThat(RelationshipName.INSTANCE.getTypeFrom(word)).isEqualTo(':' + expectedResult);
    }

    static Stream<Arguments> singleWord() {return Stream.of($("race", "RACE"), $("Radar", "RADAR"), $("PEPE", "PEPE"), $("_a", "_A"), $("Z", "Z"));}

    static Stream<Arguments> nWords() {return Stream.of($("fstRace", "FST_RACE"),$("_RaClass", "_RA_CLASS"),$("iPE", "I_PE"),$("aBvD", "A_BV_D"));}

    static Arguments $(final String input, final String expectedResult) {return Arguments.arguments(input, expectedResult);}
}
