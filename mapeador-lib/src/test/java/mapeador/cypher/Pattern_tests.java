/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import mapeador.cypher.CypherDSL.MATCH;
import mapeador.generated.metadata.NodeLabel;
import mapeador.models.Movie;
import org.junit.jupiter.api.Test;

import static mapeador.cypher.CypherDSL.$;
import static mapeador.cypher.CypherDSL.MATCH;
import static mapeador.cypher.CypherDSL.OPTIONAL_MATCH;
import static mapeador.cypher.CypherDSL.anyHops;
import static mapeador.cypher.CypherDSL.count;
import static mapeador.cypher.CypherDSL.countAll;
import static mapeador.cypher.CypherDSL.hops;
import static mapeador.cypher.CypherDSL.id;
import static mapeador.cypher.CypherDSL.maxHops;
import static mapeador.cypher.CypherDSL.node;
import static mapeador.cypher.CypherDSL.p;
import static mapeador.cypher.CypherDSL.r;
import static mapeador.cypher.CypherDSL.value;
import static mapeador.cypher.PathFunction.allShortestPaths;
import static mapeador.cypher.PathFunction.shortestPath;
import static mapeador.generated.metadata.NodeLabel.Person;
import static mapeador.generated.metadata.NodeLabel.Post;
import static mapeador.generated.metadata.Relations.ACTED_IN;
import static mapeador.generated.metadata.RelationshipType.DIRECTED;
import static org.assertj.core.api.Assertions.assertThat;

class Pattern_tests
{
    @Test void should_be_able_to_create_MATCH_clause()
    {
        // given
        final var cypher = MATCH("n").RETURN(r("n")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (n) RETURN n");
    }

    @Test void should_be_able_to_create_OPTIONAL_MATCH_clause()
    {
        // given
        final var cypher = OPTIONAL_MATCH("n").RETURN(r("n")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("OPTIONAL MATCH (n) RETURN n");
    }

    @Test void should_be_able_to_create_MATCH_node_relatedTo_another_node()
    {
        // given
        final var cypher = MATCH("a").relatedWith("b").RETURN(r("a, b")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (a)--(b) RETURN a, b");
    }

    @Test void should_be_able_to_create_OPTIONAL_MATCH_node_relatedTo_another_node()
    {
        // given
        final var cypher = OPTIONAL_MATCH("a").relatedWith("b").RETURN(r("a, b")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("OPTIONAL MATCH (a)--(b) RETURN a, b");
    }

    @Test void should_be_able_to_create_MATCH_node_with_a_label_relatedTo_another_node()
    {
        // given
        final var cypher = MATCH("a", Person).relatedWith("b").RETURN(r("a, b")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (a:Person)--(b) RETURN a, b");
    }

    @Test void should_be_able_to_create_MATCH_node_with_a_label_relatedTo_another_labelled_node()
    {
        // given
        final var cypher = MATCH("a", Person).relatedWith("b", Post).RETURN(r("a, b")).getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (a:Person)--(b:Post) RETURN a, b");
    }

    @Test void should_be_able_to_create_MATCH_node_with_a_label_and_properties_relatedTo_another_labelled_node()
    {
        // given
        final var cypher = MATCH("a", Person, value("name", "pepe"))
            .relatedWith("b", value("title", "Neo4j"), value("visitors", 10))
            .RETURN(r("a, b"))
            .getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (a:Person {name: 'pepe'})--(b {title: 'Neo4j', visitors: 10}) RETURN a, b");
    }

    @Test void should_be_able_to_Match_node_with_label_having_an_outgoing_relation_to_another_node()
    {
        // given
        final var cypher = MATCH(Person, value("name", "Oliver Stone"))
            .outgoingTo("post", Post)
            .RETURN(r("post.title")).getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (:Person {name: 'Oliver Stone'})->(post:Post) RETURN post.title");
    }

    @Test void should_be_able_to_Match_node_with_incoming_relation_to_another_node()
    {
        // given
        final var cypher = MATCH(Person, value("name", "Oliver Stone"))
            .incomingFrom("post", Post)
            .RETURN(r("post.content")).getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (:Person {name: 'Oliver Stone'})<-(post:Post) RETURN post.content");
    }

    @Test void should_manage_directed_relationships_as_variable()
    {

        // given
        final var cypher = MATCH(Movie.node("wallstreet"), value("title", p(0)))
            .incoming(ACTED_IN)
            .from("actor")
            .RETURN(r("actor.name"))
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (wallstreet:Movie {title: $0})<-[:ACTED_IN]-(actor) RETURN actor.name");
    }

    @Test void should_be_able_to_match_on_multiple_relationship_type()
    {
        // given
        final var cypher = MATCH(Movie.node("wallstreet"), value("title", "Wall Street"))
            .incoming(ACTED_IN.or(DIRECTED))
            .from("person")
            .RETURN(r("person.name"))
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (wallstreet:Movie {title: 'Wall Street'})<-[:ACTED_IN|DIRECTED]-(person) RETURN person.name");
    }

    @Test void relationships_can_be_strung_together()
    {
        // given
        final var cypher = MATCH("charlie", value("name", "Charlie Sheen"))
            .outgoing(ACTED_IN)
            .to("movie")
            .incoming(DIRECTED)
            .from("director")
            .RETURN(r("movie.title"), r("director.name"))
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (charlie {name: 'Charlie Sheen'})-[:ACTED_IN]->(movie)<-[:DIRECTED]-(director) RETURN movie.title, director.name");
    }

    @Test void should_be_able_to_describe_variable_length_relationships()
    {
        final var movie = Movie.node("movie");

        // given
        final var cypher = MATCH("charlie", value("name", "Charlie Sheen"))
            .outgoing(ACTED_IN, hops(1, 3))
            .to(movie)
            .WHERE(movie.title.STARTS_WITH("walls"))
            .RETURN(movie.title)
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (charlie {name: 'Charlie Sheen'})-[:ACTED_IN*1..3]->(movie:Movie) WHERE movie.title STARTS WITH 'walls' " +
                       "RETURN movie.title");
    }

    @Test void should_be_able_to_describe_variable_length_relationships_with_multiple_relationship_types()
    {
        // given
        final var cypher = MATCH("charlie", value("name", "Charlie Sheen"))
            .related(ACTED_IN.or(DIRECTED), hops(2))
            .with("person", Person)
            .RETURN(r("person.name"))
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (charlie {name: 'Charlie Sheen'})-[:ACTED_IN|DIRECTED*2]-(person:Person) RETURN person.name");
    }

    @Test void when_the_connection_between_2_nodes_is_of_variable_length_the_list_of_relationships_comprising_the_connection_can_be_returned()
    {
        // given
        final var cypher = MATCH.path("p")
                                .by("actor", value("name", "Charlie Sheen"))
                                .related(ACTED_IN, hops(2))
                                .with("co_actor")
                                .RETURN(r("relationships(p)"))
                                .getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH p = (actor {name: 'Charlie Sheen'})-[:ACTED_IN*2]-(co_actor) RETURN relationships(p)");
    }

    @Test void can_match_with_properties_on_a_variable_length_path()
    {
        // given
        final var charlie = node("charlie", Person);
        final var martin = node("martin", Person);

        final var cypher = MATCH.path("p")
                                .by(charlie)
                                .related(anyHops(), value("blocked", false))
                                .with(martin)
                                .WHERE(charlie.get("name").EQ($("Charlie Sheen")).AND(martin.at(p("property"))).EQ($("Martin Sheen")))
                                .RETURN(r("p"))
                                .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH p = (charlie:Person)-[* {blocked: FALSE}]-(martin:Person)" +
                       " WHERE charlie.name = 'Charlie Sheen' AND martin[$property] = 'Martin Sheen' " +
                       "RETURN p");

    }

    @Test void can_match_with_zero_length_path()
    {
        // given
        final var cypher = MATCH("wallstreet", NodeLabel.Movie, value("title", "Wall Street"))
            .related(hops(0, 1))
            .with("x")
            .RETURN(r("x"))
            .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (wallstreet:Movie {title: 'Wall Street'})-[*0..1]-(x) RETURN x");
    }

    @Test void can_match_named_paths()
    {
        // given
        final var cypher = MATCH.path("p").by("michael", value("name", "Michael D'ouglas"))
                                .outgoingTo()
                                .RETURN(r("p"))
                                .getCypher();

        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH p = (michael {name: \"Michael D'ouglas\"})->() RETURN p");
    }

    @Test void can_match_bound_relationships()
    {
        final var a = node("a");
        final var b = node("b");
        final var cypher = MATCH(a).related("r").with(b)
                                   .WHERE(id("r").EQ($(0)).AND(id(b).NE($(3)))) // the relation should be grouped, but the literal no
                                   .RETURN(a, b.as("the b"))
                                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (a)-[r]-(b) WHERE id(r) = 0 AND id(b) <> 3 RETURN a, b AS `the b`");
    }

    @Test void can_find_single_shortest_path_between_2_nodes()
    {
        // given
        final var cypher =
            MATCH("martin", Person, value("name", p(0)))
                .plus("oliver", Person, value("name", "Oliver Stone"))
                .plusPath("p", shortestPath).by("martin").related(maxHops(15)).with("oliver")
                .RETURN(r("p"))
                .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (martin:Person {name: $0})," +
                       " (oliver:Person {name: 'Oliver Stone'})," +
                       " p = shortestPath((martin)-[*..15]-(oliver)) " +
                       "RETURN p");
    }

    @Test void can_find_all_shortest_path_between_2_nodes()
    {
        // given
        final var cypher =
            MATCH("martin", Person, value("name", "Martin Sheen"))
                .plus("oliver", Person, value("name", p("oliver_name")))
                .plusPath("p", allShortestPaths).by("martin").related(maxHops(15)).with("oliver")
                .RETURN(r("p"))
                .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH (martin:Person {name: 'Martin Sheen'}), (oliver:Person {name: $oliver_name})," +
                       " p = allShortestPaths((martin)-[*..15]-(oliver)) " +
                       "RETURN p");
    }

    @Test void can_search_for_relationships_by_id()
    {
        // given
        final var cypher =
            MATCH().related("r").with()
                   .WHERE(id("r").EQ($(1)))
                   .RETURN(r("r"))
                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH ()-[r]-() WHERE id(r) = 1 RETURN r");
    }

    @Test void can_return_columns()
    {

        // given
        final var cypher =
            MATCH().related("r").with()
                   .WHERE(id("r").EQ($(1)))
                   .RETURN(count("r"))
                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH ()-[r]-() WHERE id(r) = 1 RETURN count(r)");
    }

    @Test void can_return_columns_with_alias()
    {
        // given
        var cypher =
            MATCH().related("r").with()
                   .WHERE(id("r").EQ($(1)))
                   .RETURN(count("r").as("Total"))
                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH ()-[r]-() WHERE id(r) = 1 RETURN count(r) AS Total");

        // and given
        cypher =
            MATCH().related("r").with()
                   .WHERE(id("r").EQ($(1)))
                   .RETURN(countAll().as("Total found"))
                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH ()-[r]-() WHERE id(r) = 1 RETURN count(*) AS `Total found`");

        // and given
        cypher =
            MATCH().related("r").with()
                   .WHERE(id("r").EQ($(1)))
                   .RETURN_DISTINCT(
                       count("r").as("@r"),
                       countAll().as("other_total"))
                   .getCypher();


        // expect
        assertThat(cypher)
            .isNotNull()
            .isEqualTo("MATCH ()-[r]-() WHERE id(r) = 1 RETURN DISTINCT count(r) AS `@r`, count(*) AS other_total");
    }
}
