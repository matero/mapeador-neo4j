/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.CurrentSession;
import mapeador.Neo4jClientTest;
import mapeador.cypher.CypherDSL.MATCH;
import mapeador.model.Type;
import mapeador.models.ACTED_IN;
import mapeador.models.Movie;
import mapeador.models.Person;
import migrador.Migrador;
import migrador.actions.Baseline;
import migrador.actions.Migrate;
import migrador.helpers.Classes;
import migrador.migrations.Version;
import migrador.migrations.resolvers.CypherRenderer;
import migrador.migrations.resolvers.MigrationResolver;
import migrador.resources.Location;
import migrador.resources.LocationScannerCache;
import migrador.resources.ResourceNameCache;
import migrador.resources.ResourceScanner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Record;
import org.neo4j.driver.Transaction;
import org.neo4j.driver.exceptions.NoSuchRecordException;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Relationship;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import static mapeador.criteria.FetchOptions.returning;
import static mapeador.criteria.FetchOptions.returningDistinct;
import static mapeador.cypher.CypherDSL.$;
import static mapeador.cypher.CypherDSL.NOT;
import static mapeador.cypher.CypherDSL.anyHops;
import static mapeador.cypher.CypherDSL.hops;
import static mapeador.cypher.CypherDSL.n;
import static mapeador.cypher.CypherDSL.rel;
import static mapeador.cypher.CypherDSL.type;
import static mapeador.models.Movie.title;
import static mapeador.models.Person.born;
import static mapeador.models.Person.name;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class Model_cypher_tests extends Neo4jClientTest.withSessionAndTransactionSupport
{
    final Classes classes = Classes.get();
    private MigrationResolver migrationResolver;

    Model_cypher_tests(final Driver driver)
    {
        super(driver);
        final var migrate = Migrador.Command.migrate;
        final var scanner = scan("classpath:db/migrations")
            .classes(this.classes)
            .encoding(StandardCharsets.UTF_8)
            .resourceNameCache(new ResourceNameCache())
            .locationScannerCache(new LocationScannerCache())
            .build();
        this.migrationResolver = MigrationResolver.composite()
                                                  .dontSkipDefaultResolvers()
                                                  .resourceProvider(migrate.resourceProvider(false, scanner))
                                                  .classProvider(migrate.classProvider(false, scanner))
                                                  .classes(this.classes)
                                                  .renderer(CypherRenderer.textRenderer())
                                                  .build();
    }

    @BeforeAll void setup_movies_db()
    {
        // ensure that we have Migrador installed in test DB
        withNewSession(Baseline.of(Version.latest()).executedBy("jj")::execute);

        CurrentSession.init(driver());

        // Migrate
        Migrate.of(Version.make("1.1"))
               .executedBy("jj")
               .baseline()
               .migrationResolver(this.migrationResolver)
               .allowOutOfOrderMigrations(true)
               .allowPendingMigrations(true)
               .allowMissingMigrations(true)
               .allowIgnoredMigrations(true)
               .allowFutureMigrations(true)
               .execute(CurrentSession.get());

        // ensure that db is correctly init
        final var result = CurrentSession.get().run("MATCH (a:Person {name:'Tom Hanks', born:1956})-[:ACTED_IN]->(m)<-[:DIRECTED]-(d) " +
                                                    "RETURN a.name,m.title,d.name ORDER BY m.title,d.name LIMIT 2")
                                         .list(r -> r.get(0).asString() + ", " + r.get(1).asString() + ", " + r.get(2).asString());

        assertThat(result).containsExactly("Tom Hanks, A League of Their Own, Penny Marshall", "Tom Hanks, Apollo 13, Ron Howard");
    }

    @Test void can_find_an_actor_by_name()
    {
        verify(tx -> {
            // raw java driver way
            final Record result = tx.run("MATCH (tom {name: 'Tom Hanks'}) RETURN tom").single();
            Node tom = result.get(0).asNode();

            assertThat(tom).isNotNull();
            assertThat(tom.get("name").asString()).isEqualTo("Tom Hanks");
            assertThat(tom.get("born").asInt()).isEqualTo(1956);

            // mapeador way, using explicit tx
            tom = Person.findBy(name.eq("Tom Hanks")).fetch(tx);

            assertThat(tom).isNotNull();
            assertThat(name.of(tom)).isEqualTo("Tom Hanks");
            assertThat(born.of(tom)).isEqualTo(1956);
        });
    }

    @Test void can_find_10_people()
    {
        final String[] expectedPeopleNames = {"Aaron Sorkin", "Al Pacino", "Angela Scope", "Annabella Sciorra", "Anthony Edwards", "Audrey Tautou",
                                              "Ben Miles", "Bill Paxton", "Bill Pullman", "Billy Crystal"};
        verify(tx -> {
            // raw java driver way
            List<String> peopleNames = tx.run("MATCH (people:Person) " +
                                              "RETURN people.name" +
                                              " ORDER BY people.name" +
                                              " LIMIT 10").list(r -> r.get("people.name").asString());
            assertThat(peopleNames).hasSize(10)
                                   .containsExactly(expectedPeopleNames);

            // mapeador way, using raw expressions (not type-safe) and explicit session
            peopleNames = Person.all("people") // we need to define a variable name, so we can work with raw expressions
                                .asList(tx, returning("people.name").orderBy("people.name").limit(10), r -> r.get("people.name").asString());

            assertThat(peopleNames).hasSize(10)
                                   .containsExactly(expectedPeopleNames);


            // mapeador way, using node-variable (type-safe) and explicit session
            final var people = Person.node("people");
            peopleNames = Person.all(people) // we need to define a variable name, so we can work with raw expressions
                                .asList(tx, returning(people.name).orderBy(people.name).limit(10), Person.name);

            assertThat(peopleNames).containsExactly(expectedPeopleNames);
        });
    }

    @Test void can_find_movies_released_in_the_1990s()
    {
        final String[] expectedPeopleNames = {"A Few Good Men", "A League of Their Own", "Apollo 13", "As Good as It Gets", "Bicentennial Man",
                                              "Hoffa", "Joe Versus the Volcano", "Johnny Mnemonic", "Sleepless in Seattle", "Snow Falling on Cedars",
                                              "That Thing You Do", "The Birdcage", "The Devil's Advocate", "The Green Mile", "The Matrix", "Twister",
                                              "Unforgiven", "What Dreams May Come", "When Harry Met Sally", "You've Got Mail"};
        verify(tx -> {
            // raw java driver way
            List<String> movies = tx.run("MATCH (nineties:Movie) WHERE nineties.released >= 1990 AND nineties.released < 2000 " +
                                         "RETURN nineties.title ORDER BY nineties.title").list(r -> r.get(0).asString());
            assertThat(movies).containsExactly(expectedPeopleNames);


            // mapeador way, using raw expressions (not type-safe) and explicit session
            movies = Movie.all("nineties") // we need to define a variable name, so we can work with raw expressions
                          .where("nineties.released >= 1990 AND nineties.released < 2000")
                          .asList(tx, returning("nineties.title").orderBy("nineties.title"), r -> r.get(0).asString());
            assertThat(movies).containsExactly(expectedPeopleNames);

            // mapeador way, using node-variable (type-safe) and explicit session
            final var nineties = Movie.node("nineties");
            movies = Movie.all(nineties) // we need to define a variable name, so we can work with raw expressions
                          .where(nineties.released.GE($(1990)).AND(nineties.released.LT($(2000))))
                          .asList(tx, returning(nineties.title).orderBy(nineties.title), Movie.title);
            assertThat(movies).containsExactly(expectedPeopleNames);
        });
    }

    @Test void should_be_able_to_list_all_Tom_Hanks_movies()
    {
        final String[] expectedMovies = {"Joe Versus the Volcano", "A League of Their Own", "Sleepless in Seattle", "Apollo 13",
                                         "That Thing You Do", "You've Got Mail", "The Green Mile", "Cast Away", "The Polar Express",
                                         "The Da Vinci Code", "Charlie Wilson's War", "Cloud Atlas"};
        verify(tx -> {
            // raw java driver way
            var tomHanksMovies = tx.run("MATCH (tom:Person {name: 'Tom Hanks'})-[:ACTED_IN]->(movie) " +
                                        "RETURN movie.title ORDER BY movie.released, movie.title")
                                   .list(r -> r.get(0).asString());
            assertThat(tomHanksMovies).containsExactly(expectedMovies);

            // mapeador, no-typed way
            tomHanksMovies = Person.findBy(name.eq("Tom Hanks")).actedIn("movie")
                                   .asList(tx,
                                           returning("movie.title").orderBy("movie.released, movie.title"),
                                           r -> r.get(0).asString());
            assertThat(tomHanksMovies).containsExactly(expectedMovies);

            // mapeador, typed way
            final var movie = Movie.NODE;
            tomHanksMovies = Person.findBy(name.eq("Tom Hanks"))
                                   .actedIn(movie)
                                   .asList(tx,
                                           returning(movie.title).orderBy(movie.released, movie.title),
                                           title);
            assertThat(tomHanksMovies).containsExactly(expectedMovies);
        });
    }

    @Test void should_be_able_to_match_who_directed_cloud_atlas()
    {
        final String[] expectedDirectors = {"Tom Tykwer", "Lana Wachowski", "Lilly Wachowski"};
        verify(tx -> {
            // raw java driver way
            var directors = tx.run("MATCH (cloudAtlas {title: 'Cloud Atlas'})<-[:DIRECTED]-(directors) RETURN directors.name")
                              .list(r -> r.get(0).asString());
            assertThat(directors).containsExactly(expectedDirectors);

            // mapeador, no-typed way
            directors = Movie.findBy(title.eq("Cloud Atlas")).directors("director")
                             .asList(tx, returning("director.name"), r -> r.get(0).asString());
            assertThat(directors).containsExactly(expectedDirectors);

            // mapeador, typed way
            final var director = Person.NODE;
            directors = Movie.findBy(title.eq("Cloud Atlas")).directors(director)
                             .asList(tx, returning(director.name), name);
            assertThat(directors).containsExactly(expectedDirectors);
        });
    }

    @Test void should_be_able_to_fetch_tom_hanks_coActors()
    {
        final String[] expectedCoActors = {"Audrey Tautou", "Bill Paxton", "Bill Paxton", "Bill Pullman", "Bonnie Hunt"};
        verify(tx -> {
            // raw java driver way
            var coActors = tx.run("MATCH (tom:Person {name:'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActor) " +
                                  "RETURN coActor.name " +
                                  "ORDER BY coActor.name " +
                                  "LIMIT 5")
                             .list(r -> r.get(0).asString());
            assertThat(coActors).containsExactly(expectedCoActors);

            // mapeador, no-typed way
            coActors = Person.findBy(name.eq("Tom Hanks")).actedIn().actors("coActor")
                             .asList(tx, returning("coActor.name").orderBy("coActor.name").limit(5), r -> r.get(0).asString());
            assertThat(coActors).containsExactly(expectedCoActors);

            // mapeador, typed way
            final var coActor = Person.node("coActor");
            coActors = Person.findBy(name.eq("Tom Hanks")).actedIn().actors(coActor)
                             .asList(tx, returning(coActor.name).orderBy(coActor.name).limit(5), name);
            assertThat(coActors).containsExactly(expectedCoActors);
        });
    }

    @Test void should_be_able_to_see_how_people_is_related_to_Cloud_Atlas_movie()
    {
        final String[] expected = {"Halle Berry, ACTED_IN, {'roles':[\"Luisa Rey\", \"Jocasta Ayrs\", \"Ovid\", \"Meronym\"]}",
                                   "Hugo Weaving, ACTED_IN, {'roles':[\"Bill Smoke\", \"Haskell Moore\", \"Tadeusz Kesselring\", \"Nurse Noakes\", \"Boardman Mephi\", \"Old Georgie\"]}",
                                   "Jim Broadbent, ACTED_IN, {'roles':[\"Vyvyan Ayrs\", \"Captain Molyneux\", \"Timothy Cavendish\"]}",
                                   "Tom Hanks, ACTED_IN, {'roles':[\"Zachry\", \"Dr. Henry Goose\", \"Isaac Sachs\", \"Dermot Hoggins\"]}",
                                   "Lana Wachowski, DIRECTED, {}",
                                   "Lilly Wachowski, DIRECTED, {}",
                                   "Tom Tykwer, DIRECTED, {}",
                                   "Stefan Arndt, PRODUCED, {}",
                                   "Jessica Thompson, REVIEWED, {'summary':\"An amazing journey\", 'rating':95}",
                                   "David Mitchell, WROTE, {}"};
        verify(tx -> {
            // raw java driver way
            var relationsToCloudAtlas = tx.run("MATCH (people:Person)-[relatedTo]-(:Movie {title: 'Cloud Atlas'}) " +
                                               "RETURN people.name as name, Type(relatedTo) as type, relatedTo as related")
                                          .list(r -> r.get(0).asString() + ", " + r.get(1).asString() + ", " + toString(r.get(2).asRelationship()));
            assertThat(relationsToCloudAtlas).containsExactlyInAnyOrder(expected);

            // mapeador, no-typed way
            relationsToCloudAtlas = Person.all("people").related("relatedTo").with(Movie.NODE, title.eq("Cloud Atlas"))
                                          .asList(tx,
                                                  returning("people.name as name,Type(relatedTo) as type,relatedTo as related"),
                                                  r -> r.get(0).asString() + ", " + r.get(1).asString() + ", " + toString(r.get(2).asRelationship()));
            assertThat(relationsToCloudAtlas).containsExactlyInAnyOrder(expected);

            // mapeador, typed way
            final var people = Person.node("people");
            final var relatedTo = rel("relatedTo");
            relationsToCloudAtlas = Person.all(people).related(relatedTo).with(Movie.NODE, title.eq("Cloud Atlas"))
                                          .asList(tx,
                                                  returning(people.name.as("name"),
                                                            type(relatedTo).as("type"),
                                                            relatedTo.as("related")),
                                                  r -> name.of(r) + ", " + Type.of(r.get(1)) + ", " + toString(r.get(2).asRelationship()));

            assertThat(relationsToCloudAtlas).containsExactlyInAnyOrder(expected);
        });
    }

    @Test void should_be_able_to_match_Movies_and_Actors_up_to_4_hops_away_from_Kevin_Bacon()
    {
        verify(tx -> {
            // raw java driver way
            final var baconers = tx.run("MATCH (bacon:Person {name:'Kevin Bacon'})-[*1..4]-(hollywood) RETURN DISTINCT hollywood").list();
            assertThat(baconers).isNotNull().hasSize(135);

            // mapeador way
            final var mapeadorBaconers = Person.findBy(name.eq("Kevin Bacon")).related(hops(1, 4)).with("hollywood")
                                               .asList(tx, returningDistinct("hollywood"));
            assertThat(mapeadorBaconers).containsExactlyInAnyOrderElementsOf(baconers);
        });
    }

    @Test void should_be_able_to_match_the_shortest_path_from_Kevin_Bacon_to_Meg_Ryan()
    {
        verify(tx -> {
            // raw java driver way
            final var p = tx.run("MATCH p=shortestPath((bacon:Person {name:'Kevin Bacon'})-[*]-(meg:Person {name:'Meg Ryan'})) RETURN p")
                            .single().get(0).asPath();
            assertThat(p).isNotNull().hasSize(4);

            final var mapeadorShortestPath = MATCH.shortestPath("p")
                                                  .by(Person.node("bacon"), name.eq("Kevin Bacon"))
                                                  .related(anyHops())
                                                  .with(Person.node("meg"), name.eq("Meg Ryan"))
                                                  .RETURN("p")
                                                  .single(tx)
                                                  .get(0)
                                                  .asPath();

            assertThat(p).isEqualTo(mapeadorShortestPath);
        });
    }

    @Test void should_be_able_to_find_co_co_actors_who_havent_worked_with_Tom_Hanks()
    {
        verify(tx -> {
            // raw java driver way
            final var coCoActors = tx.run("MATCH (tom:Person {name:'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors)," +
                                          "      (coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cocoActors) " +
                                          "WHERE NOT (tom)-[:ACTED_IN]->()<-[:ACTED_IN]-(cocoActors) AND tom <> cocoActors " +
                                          "RETURN cocoActors.name AS Recommended, count(*) AS Strength ORDER BY Strength DESC")
                                     .list(r -> r.get(0).asString() + " (" + r.get(1).asInt() + ')');
            assertThat(coCoActors).isNotNull().hasSize(44);

            final var mapeadorCoCo = Person.findBy("tom", name.eq("Tom Hanks")).actedIn("m").actors("coActors")
                                           .plus("coActors").outgoing(ACTED_IN.T).to("m2").incoming(ACTED_IN.T).from("cocoActors")
                                           .where(NOT(n("tom").outgoing(ACTED_IN.T).to().incoming(ACTED_IN.T).from("cocoActors"))
                                                      .AND("tom <> cocoActors"))
                                           .asList(tx,
                                                   returning("cocoActors.name AS Recommended, count(*) AS Strength").orderBy("Strength DESC"),
                                                   r -> r.get(0).asString() + " (" + r.get(1).asInt() + ')');

            assertThat(coCoActors).containsExactlyInAnyOrderElementsOf(mapeadorCoCo);

            final var tom = Person.node("tom");
            final var coActors = Person.node("coActors");
            final var cocoActors = Person.node("cocoActors");

            final var typedCoCo = Person.findBy(tom, name.eq("Tom Hanks")).actedIn("m").actors(coActors)
                                        .plus(coActors).actedIn("m2").actors(cocoActors)
                                        .where(NOT(n(tom).outgoing(ACTED_IN.T).to().incoming(ACTED_IN.T).from(cocoActors)).AND(tom.NE(cocoActors)))
                                        .asList(tx,
                                                returning("cocoActors.name AS Recommended, count(*) AS Strength").orderBy("Strength DESC"),
                                                r -> r.get(0).asString() + " (" + r.get(1).asInt() + ')');

            assertThat(coCoActors).containsExactlyInAnyOrderElementsOf(typedCoCo);
        });
    }

    @Test void should_be_able_to_Find_someone_to_introduce_Tom_Hanks_to_Tom_Cruise()
    {
        final String[] expected = {"Apollo 13 <== Kevin Bacon ==> A Few Good Men",
                                   "Joe Versus the Volcano <== Meg Ryan ==> Top Gun",
                                   "Sleepless in Seattle <== Meg Ryan ==> Top Gun",
                                   "The Green Mile <== Bonnie Hunt ==> Jerry Maguire",
                                   "You've Got Mail <== Meg Ryan ==> Top Gun"};
        verify(tx -> {
            // raw java driver way
            final var recommenders = tx.run("MATCH (tom:Person {name:'Tom Hanks'})-[:ACTED_IN]->(m)<-[:ACTED_IN]-(coActors),\n" +
                                            "      (coActors)-[:ACTED_IN]->(m2)<-[:ACTED_IN]-(cruise:Person {name:'Tom Cruise'})\n" +
                                            "RETURN m.title+' <== '+coActors.name+' ==> '+m2.title")
                                       .list(r -> r.get(0).asString());
            assertThat(recommenders).containsExactlyInAnyOrder(expected);

            // mapeador untyped
            final var untyped = Person.findBy(name.eq("Tom Hanks")).actedIn("m").actors("coActors")
                .plus("coActors").outgoing(ACTED_IN.T).to("m2").incoming(ACTED_IN.T).from(Person.node("cruise"), name.eq("Tom Cruise"))
                                            .asList(tx, returning("m.title+' <== '+coActors.name+' ==> '+m2.title"), r -> r.get(0).asString());
            assertThat(untyped).containsExactlyInAnyOrder(expected);

            // mapeador typed
            final var m = Movie.node("m");
            final var m2 = Movie.node("m2");
            final var coActors = Person.node("coActors");
            final var typed = Person.findBy(name.eq("Tom Hanks")).actedIn(m).actors(coActors)
                                      .plus(coActors).actedIn(m2).actors(name.eq("Tom Cruise"))
                                      .asList(tx, returning(m.title.concat(" <== ").concat(coActors.name).concat(" ==> ").concat(m2.title)), r -> r.get(0).asString());
            assertThat(typed).containsExactlyInAnyOrder(expected);
        });
    }

    @Test void findBy_will_return_null_when_nothing_is_found()
    {
        final var tom = Person.findBy(name.eq("Tom Janks")).fetch(); // uses CurrentSession.get()
        assertThat(tom).isNull();
    }

    @Test void getBy_will_fail_with_NoSuchRecordException_when_nothing_is_found()
    {
        assertThatExceptionOfType(NoSuchRecordException.class)
            .isThrownBy(() -> Person.getBy(name.eq("Tom Janks")).fetch()) // uses CurrentSession.get()
            .withNoCause()
            .withMessage("Cannot retrieve a single record, because this result is empty.");
    }

    @Test void get_should_fail_when_no_node_has_the_id()
    {
        assertThatExceptionOfType(NoSuchRecordException.class)
            .isThrownBy(() -> Person.get(1)) // uses CurrentSession.get()
            .withNoCause()
            .withMessage("Cannot retrieve a single record, because this result is empty.");
    }

    @Test void find_should_return_null_when_no_node_has_the_id()
    {
        final var tom = Person.find(19); // uses CurrentSession.get()
        assertThat(tom).isNull();
    }

    protected ResourceScanner.Builder scan(final String location) {return scan(Location.of(location));}

    protected ResourceScanner.Builder scan(final Location location) {return scan(List.of(location));}

    protected ResourceScanner.Builder scan(final Collection<Location> locations) {return ResourceScanner.scan(locations);}

    protected void verify(final Consumer<Transaction> verificationWork)
    {
        session().readTransaction(tx -> {
            verificationWork.accept(tx);
            return null;
        });
    }

    String toString(final Relationship r)
    {
        if (r == null) {
            return "null";
        }
        if (0 == r.size()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder().append('{');
        for (final var key : r.keys()) {
            if (1 < sb.length()) {
                sb.append(", ");
            }
            appendKey(sb, r, key);
        }
        return sb.append('}').toString();
    }

    private void appendKey(final StringBuilder sb, final Relationship r, final String key)
    {
        sb.append('\'').append(key).append("':").append(r.get(key));
    }
}
