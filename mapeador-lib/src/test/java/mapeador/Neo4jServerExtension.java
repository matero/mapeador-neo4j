/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.configuration.connectors.HttpConnector;
import org.neo4j.configuration.connectors.HttpsConnector;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.harness.Neo4j;
import org.neo4j.harness.Neo4jBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Neo4jServerExtension
    implements BeforeAllCallback, AfterAllCallback, ExtensionContext.Store.CloseableResource, AutoCloseable, ParameterResolver
{
    private static final Logger LOG = LoggerFactory.getLogger(Neo4jServerExtension.class);

    private static boolean started;

    private static Neo4j server = Neo4jBuilders.newInProcessBuilder()
                                               .withConfig(BoltConnector.enabled, true)
                                               .withConfig(BoltConnector.encryption_level, BoltConnector.EncryptionLevel.DISABLED)
                                               .withConfig(HttpConnector.enabled, false)
                                               .withConfig(HttpsConnector.enabled, false)
                                               .withConfig(GraphDatabaseSettings.auth_enabled, false)
                                               .build();

    private static Driver driver;

    @Override public void beforeAll(final ExtensionContext context)
    {
        if (!this.started) {
            this.started = true;
            // The following line registers a callback hook when the root test context is shut down
            final var contextRoot = context.getRoot();
            final var globalStore = contextRoot.getStore(ExtensionContext.Namespace.GLOBAL);
            globalStore.put("#NEO4J_EXTENSION", this);
        }
    }

    @Override public void afterAll(final ExtensionContext context)
    {
        if (null != this.driver) {
            LOG.warn("CLOSING tests neo4j driver");
            this.driver.close();
            this.driver = null;
            LOG.warn("Tests neo4j driver CLOSED");
        }
    }

    @Override public void close()
    {
        if (null != this.server) {
            LOG.warn("CLOSING tests neo4j server");
            this.server.close();
            this.server = null;
            LOG.warn("Tests neo4j server CLOSED");
        }
    }

    @Override public boolean supportsParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        final var type = parameterContext.getParameter().getType();
        return Neo4j.class.equals(type) || Driver.class.equals(type);
    }

    @Override public Object resolveParameter(final ParameterContext parameterContext, final ExtensionContext extensionContext)
        throws ParameterResolutionException
    {
        final var type = parameterContext.getParameter().getType();
        if (Neo4j.class.equals(type)) {
            return server;
        }
        if (Driver.class.equals(type)) {
            if (null == driver) {
                driver = GraphDatabase.driver(server.boltURI());
            }
            return driver;
        }
        return null;
    }
}
