/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Session;
import org.neo4j.driver.Transaction;
import org.neo4j.harness.Neo4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.function.Function;

@DB
@ExtendWith(Neo4jServerExtension.class)
public abstract class Neo4jClientTest
{
    private final Neo4j neo4jServer;
    private final Driver driver;
    private Logger logger;

    protected Neo4jClientTest(final Driver driver)
    {
        this.neo4jServer = null;
        this.driver = Objects.requireNonNull(driver, "neo4jServer is required");
        logger().debug("only defining connection (neo4j driver) to db, no server info will be accessible in tests");
    }

    protected Neo4jClientTest(final Neo4j neo4jServer, final Driver driver)
    {
        this.neo4jServer = Objects.requireNonNull(neo4jServer, "neo4jServer is required");
        this.driver = Objects.requireNonNull(driver, "neo4jServer is required");
        logger().debug("server and connection (neo4j driver) defined, no info of neo4j server will be accessible in tests");
    }

    protected final Neo4j neo4jServer()
    {
        if (null == this.neo4jServer) {
            logger().warn("no ne4jServer was defined at construction, if you need this kind of info you should use the according constructor.");
        }
        return this.neo4jServer;
    }

    protected final Driver driver()
    {
        return this.driver;
    }

    protected final Logger logger()
    {
        if (null == this.logger) {
            this.logger = LoggerFactory.getLogger(getClass());
        }
        return this.logger;
    }

    public static abstract class withSessionSupport extends Neo4jClientTest
    {
        private Session session;

        protected withSessionSupport(final Driver driver) {super(driver);}

        protected withSessionSupport(final Neo4j neo4jServer, final Driver driver) {super(neo4jServer, driver);}

        @AfterEach protected final void closeSessionIfRequired()
        {
            closeSession();
        }

        protected final void closeSession()
        {
            if (null != this.session) {
                this.session.close();
                this.session = null;
            }
        }

        protected final Session session()
        {
            if (null == this.session) {
                this.session = driver().session();
            }
            return this.session;
        }

        protected boolean shouldCloseSession() { return false; }

        protected <T> T withNewSession(final Function<Session, T> work) {
            try (final var session = driver().session()) {
                return work.apply(session);
            }
        }
    }

    public static abstract class withSessionAndTransactionSupport extends withSessionSupport
    {
        private Transaction tx;

        protected withSessionAndTransactionSupport(final Driver driver) {super(driver);}

        protected withSessionAndTransactionSupport(final Neo4j neo4jServer, final Driver driver) {super(neo4jServer, driver);}

        protected final Transaction tx()
        {
            if (null == this.tx) {
                this.tx = session().beginTransaction();
            }
            return this.tx;
        }

        @AfterEach protected final void rollbackIfRequired()
        {
            if (shouldRollback()) {
                rollbackTransaction();
            }
            if (shouldDiscardTransaction()) {
                this.tx = null;
            }
        }

        protected final void rollbackTransaction()
        {
            if (null != this.tx) {
                this.tx.rollback();
            }
        }

        protected final void fixture(final String statments) {fixture(statments, false);}

        protected final void fixture(final String statments, final boolean mustCommit)
        {
            if (null != statments && !statments.isBlank()) {
                if (mustCommit) {
                    session().run(statments);
                } else {
                    tx().run(statments);
                }
            }
        }

        protected boolean shouldRollback() { return true; }

        protected boolean shouldDiscardTransaction() { return false; }
    }
}
