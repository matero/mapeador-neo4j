/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package utils;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public final class Combinator implements Iterator<String>, Iterable<String>
{

    private final String word;
    private final String zeros;
    private final String ones;
    private int current;

    private Combinator(final String word, final String zeros, final String ones)
    {
        this.word = word;
        this.zeros = zeros;
        this.ones = ones;
    }

    public static Combinator of(final String word)
    {
        final String w = Objects.requireNonNull(word, "word").trim();
        return new Combinator(w, w.toLowerCase(), w.toUpperCase());
    }

    @Override public boolean hasNext()
    {
        return this.current < (int) Math.pow(2, this.word.length());
    }

    @Override public Iterator<String> iterator()
    {
        return this;
    }

    @Override public String next()
    {
        if (!hasNext()) {
            throw new NoSuchElementException("No such combination: " + this.current + " in '" + this.word + "'");
        }
        final char[] chars = this.zeros.toCharArray();
        for (int i = this.zeros.length() - 1, bit = 1; 0 <= i; i--, bit <<= 1) {
            if (0 != (bit & this.current)) {
                chars[i] = this.ones.charAt(i);
            }
        }
        this.current++;
        return new String(chars);
    }

    @Override public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
