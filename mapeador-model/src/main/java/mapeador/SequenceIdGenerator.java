/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;
import org.neo4j.driver.Values;

import java.net.NetworkInterface;
import java.security.SecureRandom;
import java.time.Instant;
import java.util.Enumeration;

/**
 * Distributed Sequence Generator.
 * Inspired by blog post: https://www.callicoder.com/distributed-unique-id-sequence-number-generator/
 * <p>
 * This class should be used as a Singleton.
 * Make sure that you create and reuse a Single instance of SequenceGenerator per node in your distributed system cluster.
 */
public class SequenceIdGenerator implements IdGenerator
{
    private static final int UNUSED_BITS = 1; // Sign bit, Unused (always set to 0)
    private static final int EPOCH_BITS = 41;
    private static final int NODE_ID_BITS = 10;
    private static final int SEQUENCE_BITS = 12;

    private static final int maxNodeId = (int) (Math.pow(2, NODE_ID_BITS) - 1);
    private static final int maxSequence = (int) (Math.pow(2, SEQUENCE_BITS) - 1);

    // Custom Epoch (January 1, 2015 Midnight UTC = 2015-01-01T00:00:00Z)
    private static final long CUSTOM_EPOCH = 1420070400000L;

    private final int nodeId;

    private volatile long lastTimestamp = -1L;
    private volatile long sequence = 0L;

    // Create SequenceGenerator with a nodeId
    SequenceIdGenerator(final int nodeId)
    {
        this.nodeId = nodeId;
    }

    @Override public synchronized Value generateId(final Transaction tx)
    {
        long currentTimestamp = timestamp();

        if (currentTimestamp < this.lastTimestamp) {
            throw new IllegalStateException("Invalid System Clock!");
        }

        if (currentTimestamp == this.lastTimestamp) {
            this.sequence = (this.sequence + 1) & maxSequence;
            if (this.sequence == 0) {
                // Sequence Exhausted, wait till next millisecond.
                currentTimestamp = waitNextMillis(currentTimestamp);
            }
        } else {
            // reset sequence to start with zero for the next millisecond
            this.sequence = 0;
        }

        this.lastTimestamp = currentTimestamp;

        long id = currentTimestamp << (NODE_ID_BITS + SEQUENCE_BITS);
        id |= (this.nodeId << SEQUENCE_BITS);
        id |= this.sequence;

        return Values.value(id);
    }


    // Get current timestamp in milliseconds, adjust for the custom epoch.
    private static long timestamp() {return Instant.now().toEpochMilli() - CUSTOM_EPOCH;}

    // Block and wait till next millisecond
    private long waitNextMillis(long currentTimestamp)
    {
        while (currentTimestamp == this.lastTimestamp) {
            currentTimestamp = timestamp();
        }
        return currentTimestamp;
    }

    public static SequenceIdGenerator of(final int nodeId)
    {
        if (nodeId < 0 || nodeId > maxNodeId) {
            throw new IllegalArgumentException("NodeId must be between 0 .. " + maxNodeId);
        }
        return new SequenceIdGenerator(nodeId);
    }

    public static SequenceIdGenerator get() {return Unique.INSTANCE;}

    private static final class Unique
    {
        private static final SequenceIdGenerator INSTANCE = new SequenceIdGenerator(createNodeId());

        private static int createNodeId()
        {
            int nodeId;
            try {
                final StringBuilder sb = new StringBuilder();
                final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements()) {
                    final NetworkInterface networkInterface = networkInterfaces.nextElement();
                    final byte[] mac = networkInterface.getHardwareAddress();
                    if (mac != null) {
                        for (int i = 0; i < mac.length; i++) {
                            sb.append(String.format("%02X", mac[i]));
                        }
                    }
                }
                nodeId = sb.toString().hashCode();
            } catch (final Exception ex) {
                nodeId = (new SecureRandom().nextInt());
            }
            nodeId = nodeId & maxNodeId;
            return nodeId;
        }
    }
}
