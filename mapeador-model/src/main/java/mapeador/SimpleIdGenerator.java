/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import org.neo4j.driver.Transaction;
import org.neo4j.driver.Value;
import org.neo4j.driver.Values;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * produces a String which:
 *
 * - is of fixed length
 * - does not allow the original input to be easily recovered (in fact, this is very hard)
 * - does not uniquely identify the input; however, similar input will produce dissimilar message digests
 *
 * based on http://www.javapractices.com/topic/TopicAction.do?Id=56
 */
public enum SimpleIdGenerator implements IdGenerator
{
    INSTANCE;

    private final SecureRandom prng;
    private final MessageDigest sha;

    SimpleIdGenerator()
    {
        try {
            //Initialize SecureRandom
            //This is a lengthy operation, to be done only upon initialization of the application
            this.prng = SecureRandom.getInstance("SHA1PRNG");

            //get its digest
            this.sha = MessageDigest.getInstance("SHA-1");
        } catch (final NoSuchAlgorithmException e) {
            throw new IllegalStateException("Could not initialize SimpleIdGenerator", e);
        }
    }

    @Override public Value generateId(final Transaction tx)
    {
        //generate a random number
        final var id = this.prng.nextLong();
        final var digestedId = this.sha.digest(Long.toString(id).getBytes());
        final var result = Base64.getEncoder().encodeToString(digestedId);
        return Values.value(result);
    }
}
