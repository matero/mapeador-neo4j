/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador;

import org.neo4j.driver.Record;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.Node;

import java.util.Objects;
import java.util.function.Function;

public abstract class Property<E extends Entity<E>, T> implements EntityProperty<E, T>, Function<Record, T>
{
    private final String name;
    private final String property;
    private final String canonicalName;
    private final Class<? extends E> entityClass;

    protected Property(final Class<? extends E> entityClass, final String property, final String name, final String canonicalName)
    {
        this.entityClass = entityClass;
        this.name = name;
        this.property = property;
        this.canonicalName = canonicalName;
    }

    @Override public final Class<? extends E> getEntityClass() {return this.entityClass;}

    @Override public final String getName() {return this.name;}

    @Override public final String getProperty() {return this.property;}

    @Override public final String getCanonicalName() {return this.canonicalName;}

    @Override public final T interpret(final Value value)
    {
        if (null == value) {
            return interpretNull();
        } else {
            return interpretNonNull(value);
        }
    }

    protected T interpretNull() {return null;}

    protected abstract T interpretNonNull(Value value);

    @Override public T apply(final Record record) {return of(record, 0);}

    @Override public T of(final Record record) {return of(record, getProperty());}

    @Override public T of(final Record record, final String alias)
    {
        Objects.requireNonNull(record, "record is required");
        Objects.requireNonNull(alias, "alias is required");
        final var value = record.get(alias);
        return interpret(value);
    }

    @Override public T of(final Record record, final int index)
    {
        Objects.requireNonNull(record, "record is required");
        final var value = record.get(index);
        return interpret(value);
    }

    @Override public T of(final Node node)
    {
        Objects.requireNonNull(node, "node is required");
        final var value = node.get(getProperty());
        return interpret(value);
    }
}
