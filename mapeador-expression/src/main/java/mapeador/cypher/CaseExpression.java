/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.List;
import java.util.Objects;

public final class CaseExpression implements CaseExpressionWhen, CaseExpressionElse, CaseExpressionDefined
{
    private final Expression base;
    private List<Alternative> alternatives = new java.util.LinkedList<>();
    private Expression elseAlternative;
    private boolean grouped;
    private Expression when;

    CaseExpression() {this(null);}

    CaseExpression(final Expression base) {this.base = base;}

    public Expression base() {return this.base;}

    public List<Alternative> alternatives() {return this.alternatives;}

    public Expression elseAlternative() {return this.elseAlternative;}

    public CaseExpressionWhen WHEN(final Object expr) {return WHEN(Expression.of(expr));}

    public CaseExpressionWhen WHEN(final Expression expr)
    {
        if (null != this.when) {
            throw new CypherException("only THEN can be used after WHEN");
        }
        Objects.requireNonNull(expr, "expr is required");
        this.when = expr;
        return this;
    }

    @Override public CaseExpression THEN(final Object expr) {return THEN(Expression.of(expr));}

    @Override public CaseExpression THEN(final Expression then)
    {
        if (null == this.when) {
            throw new CypherException("THEN must be used only after WHEN");
        }
        Objects.requireNonNull(then, "then is required");
        this.alternatives.add(new Alternative(this.when, then));
        this.when = null;
        return this;
    }

    public CaseExpressionElse ELSE(final Object expr) {return ELSE(Expression.of(expr));}

    public CaseExpressionElse ELSE(final Expression expr)
    {
        if (null != this.when) {
            throw new CypherException("THEN must be used after WHEN");
        }
        Objects.requireNonNull(expr, "expr is required");
        this.elseAlternative = expr;
        return this;
    }

    @Override public CaseExpressionDefined END()
    {
        if (this.alternatives.isEmpty()) {
            throw new CypherException("at leas one alternative must be defined");
        }
        this.alternatives = List.copyOf(this.alternatives);
        return this;
    }

    @Override public CaseExpression sortOperand() {return this;}

    @Override public boolean grouped() {return this.grouped;}

    @Override public CaseExpression group()
    {
        this.grouped = true;
        return this;
    }

    @Override public void accept(final Visitor visitor) {visitor.visit(this);}

    public static final class Alternative implements Visitable
    {
        private final Expression when;
        private final Expression then;

        Alternative(final Expression when, final Expression then)
        {
            this.when = when;
            this.then = then;
        }

        public Expression when() {return this.when;}

        public Expression then() {return this.then;}

        @Override public void accept(final Visitor visitor) {visitor.visit(this);}
    }
}
