/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface BooleanExpression extends Predicate
{
    default Comparison EQ(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.EQUAL_TO, this, BooleanValue.of(rhs));
    }

    default Comparison NE(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, BooleanValue.of(rhs));
    }

    default Comparison GT(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN, this, BooleanValue.of(rhs));
    }

    default Comparison GE(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, BooleanValue.of(rhs));
    }

    default Comparison LT(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN, this, BooleanValue.of(rhs));
    }

    default Comparison LE(final boolean rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, BooleanValue.of(rhs));
    }

    default Comparison EQ(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.EQUAL_TO, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison NE(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison GT(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.GREATER_THAN, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison GE(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison LT(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.LESS_THAN, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison LE(final Boolean rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, BooleanValue.of(rhs.booleanValue()));
    }

    default Comparison EQ(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison NE(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GT(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GE(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LT(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LE(final BooleanExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }
}
