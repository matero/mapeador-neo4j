/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Label;
import mapeador.Labels;

import java.util.Objects;

public interface UpdateLabelsTarget extends Expression
{
    default UpdateLabels label(final String name)
    {
        Objects.requireNonNull(name, "name");
        return new UpdateLabels(this, Label.of(name));
    }

    default UpdateLabels label(final Labels label)
    {
        Objects.requireNonNull(label, "label");
        return new UpdateLabels(this, label);
    }

    default UpdateLabels labels(final Labels labels)
    {
        Objects.requireNonNull(labels, "labels");
        return new UpdateLabels(this, labels);
    }

    default UpdateLabels labels(final String firstLabelName, final String... otherLabelNames)
    {
        if (null == otherLabelNames || 0 == otherLabelNames.length) {
            return labels(firstLabelName);
        }

        final var labels = new java.util.LinkedHashSet<Label>(1 + otherLabelNames.length);
        labels.add(Label.of(firstLabelName));
        for (final var other : otherLabelNames) {
            labels.add(Label.of(other));
        }

        return new UpdateLabels(this, Labels.of(labels));
    }

    default UpdateLabels labels(final Labels firstLabels, final Labels... otherLabels)
    {
        if (null == otherLabels || 0 == otherLabels.length) {
            return labels(firstLabels);
        }
        var labels = firstLabels;
        for (final var other : otherLabels) {
            labels = labels.plus(other);
        }
        return new UpdateLabels(this, labels);
    }
}
