/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toUnmodifiableList;

public interface ListLiteral extends Literal, ListExpression
{
    boolean isEmpty();

    int size();

    Expression head();

    List<Expression> tail();

    @Override default ListLiteral group() {return this;}

    @Override default ListLiteral sortOperand() {return this;}
}

final class ListValue implements ListLiteral
{
    private final Expression head;
    private final List<Expression> tail;

    ListValue(final Expression head) {this(head, List.of());}

    ListValue(final Expression head, final List<Expression> elements)
    {
        this.head = head;
        this.tail = elements;
    }

    @Override public boolean isEmpty() {return null == this.head;}

    @Override public int size() {return isEmpty() ? 0 : 1 + this.tail.size();}

    @Override public Expression head() {return this.head;}

    @Override public List<Expression> tail() {return this.tail;}

    @Override public void accept(final Visitor visitor) {visitor.visit(this);}

    /*
     * factory methods
     */
    static ListValue of() {return Unique.EMPTY;}

    static ListValue of(final boolean head, final boolean[] tail)
    {
        final var listHead = BooleanValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(BooleanValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final boolean[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = BooleanValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(BooleanValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Boolean head, final Boolean[] tail)
    {
        final var listHead = BooleanValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(BooleanValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Boolean[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = BooleanValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(BooleanValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final byte head, final byte[] tail)
    {
        final var listHead = ByteValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(ByteValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final byte[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = ByteValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(ByteValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Byte head, final Byte[] tail)
    {
        final var listHead = ByteValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(ByteValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Byte[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = ByteValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(ByteValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final short head, final short[] tail)
    {
        final var listHead = ShortValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(ShortValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final short[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = ShortValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(ShortValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Short head, final Short[] tail)
    {
        final var listHead = ShortValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(ShortValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Short[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = ShortValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(ShortValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final int head, final int[] tail)
    {
        final var listHead = IntegerValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(IntegerValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final int[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = IntegerValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(IntegerValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Integer head, final Integer[] tail)
    {
        final var listHead = IntegerValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(IntegerValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Integer[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = IntegerValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(IntegerValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final long head, final long[] tail)
    {
        final var listHead = LongValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(LongValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final long[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = LongValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(LongValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Long head, final Long[] tail)
    {
        final var listHead = LongValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(LongValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Long[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = LongValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(LongValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final float head, final float[] tail)
    {
        final var listHead = FloatValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(FloatValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final float[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = FloatValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(FloatValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Float head, final Float[] tail)
    {
        final var listHead = FloatValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(FloatValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Float[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = FloatValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(FloatValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }


    static ListValue of(final double head, final double[] tail)
    {
        final var listHead = DoubleValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            final var listTail = new java.util.ArrayList<Expression>(tail.length);
            for (final var b : tail) {
                listTail.add(DoubleValue.of(b));
            }
            return new ListValue(listHead, List.copyOf(listTail));
        }
    }

    static ListValue of(final double[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = DoubleValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(DoubleValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Double head, final Double[] tail)
    {
        final var listHead = DoubleValue.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        } else {
            return new ListValue(listHead, Arrays.stream(tail).map(DoubleValue::of).collect(toUnmodifiableList()));
        }
    }

    static ListValue of(final Double[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = DoubleValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(head, List.of());
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(DoubleValue.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final char head, final char[] tail)
    {
        if (isEmpty(tail)) {
            return new ListValue(StringValue.of(head));
        }
        final var listTail = new java.util.ArrayList<StringLiteral>(tail.length);
        for (final var c : tail) {
            listTail.add(StringValue.of(c));
        }
        return new ListValue(StringValue.of(head), List.copyOf(listTail));
    }

    static ListValue of(final Character head, final Character[] tail)
    {
        if (isEmpty(tail)) {
            return new ListValue(StringValue.of(head));
        }
        final var listTail = new java.util.ArrayList<StringLiteral>(tail.length);
        for (final var c : tail) {
            listTail.add(StringValue.of(c));
        }
        return new ListValue(StringValue.of(head), List.copyOf(listTail));
    }

    static ListValue of(final CharSequence head, final CharSequence[] tail)
    {
        if (isEmpty(tail)) {
            return new ListValue(StringValue.of(head));
        }
        final var listTail = new java.util.ArrayList<StringLiteral>(tail.length);
        for (final var c : tail) {
            listTail.add(StringValue.of(c));
        }
        return new ListValue(StringValue.of(head), List.copyOf(listTail));
    }

    static ListValue of(final String head, final String[] tail)
    {
        if (isEmpty(tail)) {
            return new ListValue(StringValue.of(head));
        }
        final var listTail = new java.util.ArrayList<StringLiteral>(tail.length);
        for (final var c : tail) {
            listTail.add(StringValue.of(c));
        }
        return new ListValue(StringValue.of(head), List.copyOf(listTail));
    }

    static ListValue of(final char[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var listHead = StringValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(listHead, List.of());
        } else {
            final var tail = new java.util.ArrayList<StringLiteral>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(StringValue.of(elements[i]));
            }
            return new ListValue(listHead, List.copyOf(tail));
        }
    }

    static ListValue of(final Character[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var listHead = StringValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(listHead, List.of());
        } else {
            final var tail = new java.util.ArrayList<StringLiteral>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(StringValue.of(elements[i]));
            }
            return new ListValue(listHead, List.copyOf(tail));
        }
    }

    static ListValue of(final CharSequence[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var listHead = StringValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(listHead, List.of());
        } else {
            final var tail = new java.util.ArrayList<StringLiteral>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(StringValue.of(elements[i]));
            }
            return new ListValue(listHead, List.copyOf(tail));
        }
    }

    static ListValue of(final String[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var listHead = StringValue.of(elements[0]);
        if (1 == elements.length) {
            return new ListValue(listHead, List.of());
        } else {
            final var tail = new java.util.ArrayList<StringLiteral>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(StringValue.of(elements[i]));
            }
            return new ListValue(listHead, List.copyOf(tail));
        }
    }

    static ListValue of(final Expression head, final Expression[] tail)
    {
        if (isEmpty(tail)) {
            return new ListValue(head);
        } else {
            return new ListValue(head, List.of(tail));
        }
    }

    static ListValue of(final Expression[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = Expression.of(elements[0]);
        if (hasOneElement(elements)) {
            return new ListValue(head);
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(elements[i]);
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Object head, final Object[] tail)
    {
        final var listHead = Expression.of(head);
        if (isEmpty(tail)) {
            return new ListValue(listHead);
        }
        final var listTail = new java.util.ArrayList<Expression>(tail.length);
        for (final var c : tail) {
            listTail.add(Expression.of(c));
        }
        return new ListValue(listHead, List.copyOf(listTail));
    }

    static ListValue of(final Object[] elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (isEmpty(elements)) {
            return ListValue.Unique.EMPTY;
        }
        final var head = Expression.of(elements[0]);
        if (hasOneElement(elements)) {
            return new ListValue(head);
        } else {
            final var tail = new java.util.ArrayList<Expression>(elements.length - 1);
            for (int i = 1; i < elements.length; i++) {
                tail.add(Expression.of(elements[i]));
            }
            return new ListValue(head, List.copyOf(tail));
        }
    }

    static ListValue of(final Collection<?> elements)
    {
        if (null == elements) {
            return ListValue.Unique.NULL;
        }
        if (elements.isEmpty()) {
            return ListValue.Unique.EMPTY;
        }
        final var list = List.copyOf(elements);
        final var head = Expression.of(list.get(0));
        if (1 == elements.size()) {
            return new ListValue(head);
        } else {
            return new ListValue(head, list.subList(1, elements.size()).stream().map(Expression::of).collect(toUnmodifiableList()));
        }
    }

    private static boolean isEmpty(final boolean[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final byte[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final char[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final short[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final int[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final long[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final float[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final double[] tail) {return null == tail || 0 == tail.length;}

    private static boolean isEmpty(final Object[] tail) {return null == tail || 0 == tail.length;}

    private static boolean hasOneElement(final Object[] elements) {return 1 == elements.length;}

    private static class Unique
    {
        private static final ListValue NULL = new ListValue(null, null);
        private static final ListValue EMPTY = new ListValue(null, List.of());
    }
}
