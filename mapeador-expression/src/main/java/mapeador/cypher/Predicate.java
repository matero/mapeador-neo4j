/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface Predicate extends ComparableExpression
{
    default BinaryPredicate AND(final String rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new BinaryPredicate(BinaryPredicate.Operator.AND, this, RawExpression.of(rhs));
    }

    default BinaryPredicate AND(final Predicate rhs)
    {
        return new BinaryPredicate(BinaryPredicate.Operator.AND, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default BinaryPredicate OR(final String rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new BinaryPredicate(BinaryPredicate.Operator.OR, this, RawExpression.of(rhs));
    }


    default BinaryPredicate OR(final Predicate rhs)
    {
        return new BinaryPredicate(BinaryPredicate.Operator.OR, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default BinaryPredicate XOR(final String rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new BinaryPredicate(BinaryPredicate.Operator.XOR, this, RawExpression.of(rhs));
    }

    default BinaryPredicate XOR(final Predicate rhs)
    {
        return new BinaryPredicate(BinaryPredicate.Operator.XOR, this, Objects.requireNonNull(rhs, "rhs is required"));
    }
}
