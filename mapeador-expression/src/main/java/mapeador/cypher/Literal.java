/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.Collection;

public interface Literal extends ComparableExpression, Alias.Target, Atom
{
    Null NULL = Null.INSTANCE;

    @Override default boolean grouped() {return false;}

    static Literal of(final Object value)
    {
        if (null == value) {
            return Literal.NULL;
        }

        if (value instanceof Literal) {
            return (Literal) value;
        }

        if (value instanceof Boolean) {
            return BooleanValue.of((boolean) value);
        }
        if (value instanceof boolean[]) {
            return ListValue.of((boolean[]) value);
        }
        if (value instanceof Boolean[]) {
            return ListValue.of((Boolean[]) value);
        }

        if (value instanceof Byte) {
            return ByteValue.of((byte) value);
        }
        if (value instanceof byte[]) {
            return ListValue.of((byte[]) value);
        }
        if (value instanceof Byte[]) {
            return ListValue.of((Byte[]) value);
        }

        if (value instanceof Short) {
            return ShortValue.of((short) value);
        }
        if (value instanceof short[]) {
            return ListValue.of((short[]) value);
        }
        if (value instanceof Short[]) {
            return ListValue.of((Short[]) value);
        }

        if (value instanceof Integer) {
            return IntegerValue.of((int) value);
        }
        if (value instanceof int[]) {
            return ListValue.of((int[]) value);
        }
        if (value instanceof Integer[]) {
            return ListValue.of((Integer[]) value);
        }

        if (value instanceof Long) {
            return LongValue.of((long) value);
        }
        if (value instanceof long[]) {
            return ListValue.of((long[]) value);
        }
        if (value instanceof Long[]) {
            return ListValue.of((Long[]) value);
        }

        if (value instanceof Float) {
            return FloatValue.of((float) value);
        }
        if (value instanceof float[]) {
            return ListValue.of((float[]) value);
        }
        if (value instanceof Float[]) {
            return ListValue.of((Float[]) value);
        }

        if (value instanceof Double) {
            return DoubleValue.of((double) value);
        }
        if (value instanceof double[]) {
            return ListValue.of((double[]) value);
        }
        if (value instanceof Double[]) {
            return ListValue.of((Double[]) value);
        }

        if (value instanceof Character) {
            return StringValue.of((char) value);
        }
        if (value instanceof String) {
            return StringValue.of((String) value);
        }
        if (value instanceof CharSequence) {
            return StringValue.of((CharSequence) value);
        }
        if (value instanceof char[]) {
            return ListValue.of((char[]) value);
        }
        if (value instanceof Character[]) {
            return ListValue.of((Character[]) value);
        }
        if (value instanceof String[]) {
            return ListValue.of((String[]) value);
        }
        if (value instanceof CharSequence[]) {
            return ListValue.of((CharSequence[]) value);
        }

        if (value instanceof Expression[]) {
            return ListValue.of((Expression[]) value);
        }

        if (value instanceof Object[]) {
            return ListValue.of((Object[]) value);
        }


        if (value instanceof Collection) {
            return ListValue.of((Collection<?>) value);
        }
        // FIXME: add values supported by neo4j Values class, like Map, and java Time classes

        throw new IllegalArgumentException("Unable to convert " + value.getClass().getName() + " to Neo4j Value.");
    }
}
