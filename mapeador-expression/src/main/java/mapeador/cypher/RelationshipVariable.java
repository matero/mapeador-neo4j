/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasTypes;
import mapeador.Name;
import mapeador.Types;

import java.util.Objects;

public class RelationshipVariable extends EntityVariable implements HasTypes
{
    private final Types types;

    public RelationshipVariable(final String name, final Types types)
    {
        super(name);
        this.types = types;
    }

    @Override public void accept(final Visitor visitor) {visitor.visit(this);}

    @Override public Types getRelationTypes() {return this.types;}

    @Override public boolean grouped() {return false;}

    @Override public RelationshipVariable group() {return this;}

    @Override public RelationshipVariable sortOperand() {return this;}

    public static RelationshipVariable of(final String name) {return new RelationshipVariable(Name.at(name), Types.empty());}

    public static RelationshipVariable of(final String name, final HasTypes types)
    {
        return new RelationshipVariable(Name.at(name), Objects.requireNonNull(types, "types is required").getRelationTypes());
    }

    public static RelationshipVariable of(final String name, final Types types)
    {
        return new RelationshipVariable(Name.at(name), Objects.requireNonNull(types, "types is required"));
    }
}
