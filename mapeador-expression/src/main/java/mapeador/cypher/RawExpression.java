/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.List;
import java.util.Objects;

public final class RawExpression
    implements Predicate, StringExpression, SortPredicate, SortPredicate.Operand, Alias.Target, Entity, PropertyExpression, SetItem,
               UpdateLabelsTarget
{
    private final String content;
    private boolean grouped;

    protected RawExpression(final String content) {this.content = content;}

    public String content() {return this.content;}

    @Override public Atom atom() {return null;}

    @Override public String lookupsHead() {return null;}

    @Override public List<String> lookupsTail() {return List.of();}

    @Override public void accept(final Visitor visitor) {visitor.visit(this);}

    @Override public boolean grouped() {return this.grouped;}

    @Override public String name() {return null;}

    @Override public PropertyAccess.Static get(final String name) {return null;}

    @Override public PropertyAccess.Dynamic at(final Expression operand) {return null;}

    @Override public SetItem.Reset to(final Object o) {return to(Expression.of(o));}

    @Override public SetItem.Reset to(final Expression valueExpr)
    {
        Objects.requireNonNull(valueExpr, "valueExpr is required");
        return new SetItem.Reset(this, valueExpr);
    }

    @Override public RawExpression group()
    {
        this.grouped =true;
        return this;
    }

    @Override public RawExpression sortOperand() {return this;}

    public static RawExpression of(final CharSequence expr) {
        return new RawExpression(contentAt(expr));
    }

    public static RawExpression of(final String expr) {
        return new RawExpression(contentAt(expr));
    }

    protected static String contentAt(final CharSequence expr) {
        final var raw = Objects.requireNonNull(expr, "expr is required").toString();
        return contentAt(raw);
    }

    protected static String contentAt(final String expr)
    {
        if (expr.isEmpty()) {
            throw new CypherException("expr cant be empty.");
        }
        if (expr.isBlank()) {
            throw new CypherException("expr cant be blank.");
        }
        return expr.trim();
    }
}
