/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasLabels;
import mapeador.Labels;
import mapeador.Name;

import java.util.Objects;

public class NodeVariable extends EntityVariable implements HasLabels, UpdateLabelsTarget
{
    private final Labels labels;

    protected NodeVariable(final String name, final Labels labels)
    {
        super(name);
        this.labels = labels;
    }

    @Override public NodeVariable group() {return this;}

    @Override public NodeVariable sortOperand() {return this;}

    @Override public Labels getNodeLabels() {return this.labels;}

    @Override public void accept(final Visitor visitor) {visitor.visit(this);}

    public static NodeVariable of(final String name) {return new NodeVariable(Name.at(name), Labels.empty());}

    public static NodeVariable of(final String name, final HasLabels labels)
    {
        return new NodeVariable(Name.at(name), Objects.requireNonNull(labels, "labels is required").getNodeLabels());
    }

    public static NodeVariable of(final String name, final Labels labels)
    {
        return new NodeVariable(Name.at(name), Objects.requireNonNull(labels, "labels is required"));
    }
}
