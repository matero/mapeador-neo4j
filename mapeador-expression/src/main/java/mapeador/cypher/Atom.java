/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasProperty;
import mapeador.Name;

import java.util.List;
import java.util.Objects;

public interface Atom extends Visitable
{
    default SetItem set(final String property, final Object value) {return set(property, Expression.of(value));}

    default SetItem set(final String property, final Expression valueExpression)
    {
        Objects.requireNonNull(valueExpression, "valueExpression is required");
        return new SetItem.Reset(new SimplePropertyExpression(this, Name.at(property)), valueExpression);
    }

    default SetItem set(final HasProperty property, final Object value)
    {
        return set(property, Expression.of(value));
    }

    default SetItem set(final HasProperty property, final Expression valueExpression)
    {
        Objects.requireNonNull(property, "property is required");
        Objects.requireNonNull(valueExpression, "valueExpression is required");
        return new SetItem.Reset(new SimplePropertyExpression(this, property.getProperty()), valueExpression);
    }
}

final class SimplePropertyExpression implements PropertyExpression
{
    private final Atom atom;
    private final String property;

    SimplePropertyExpression(final Atom atom, final String property)
    {
        this.atom = atom;
        this.property = property;
    }

    @Override public Atom atom() {return this.atom;}

    @Override public String lookupsHead() {return this.property;}

    @Override public List<String> lookupsTail() {return List.of();}
}
