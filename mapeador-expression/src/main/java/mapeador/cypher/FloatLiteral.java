/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.Objects;

public interface FloatLiteral extends Literal, FloatExpression
{
    @Override default FloatLiteral group() {return this;}

    @Override default FloatLiteral sortOperand() {return this;}
}

final class FloatValue implements FloatLiteral
{
    private final float value;

    FloatValue(final float value) {this.value = value;}

    @Override public void accept(final Visitor visitor) {visitor.visit(this.value);}

    static FloatLiteral of(final float value) {return new FloatValue(value);}

    static FloatLiteral of(final Float value) {return null == value ? NULL : new FloatValue(value);}

    static FloatValue ofNonNullRhs(final Float rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new FloatValue(rhs);
    }
}
