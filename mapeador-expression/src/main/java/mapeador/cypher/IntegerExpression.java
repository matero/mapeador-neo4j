/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface IntegerExpression extends ShortExpression
{
    default Comparison EQ(final int rhs) {return new Comparison(Comparison.Operator.EQUAL_TO, this, IntegerValue.of(rhs));}

    default Comparison NE(final int rhs) {return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, IntegerValue.of(rhs));}

    default Comparison GT(final int rhs) {return new Comparison(Comparison.Operator.GREATER_THAN, this, IntegerValue.of(rhs));}

    default Comparison GE(final int rhs) {return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, IntegerValue.of(rhs));}

    default Comparison LT(final int rhs) {return new Comparison(Comparison.Operator.LESS_THAN, this, IntegerValue.of(rhs));}

    default Comparison LE(final int rhs) {return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, IntegerValue.of(rhs));}

    default Comparison EQ(final Integer rhs) {return new Comparison(Comparison.Operator.EQUAL_TO, this, IntegerValue.ofNonNullRhs(rhs));}

    default Comparison NE(final Integer rhs) {return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, IntegerValue.ofNonNullRhs(rhs));}

    default Comparison GT(final Integer rhs) {return new Comparison(Comparison.Operator.GREATER_THAN, this, IntegerValue.ofNonNullRhs(rhs));}

    default Comparison GE(final Integer rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, IntegerValue.ofNonNullRhs(rhs));
    }

    default Comparison EQ(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison NE(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GT(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GE(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LT(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LE(final IntegerExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }
}
