/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.Objects;

public interface FloatExpression extends ComparableExpression
{
    default Comparison EQ(final float rhs) {return new Comparison(Comparison.Operator.EQUAL_TO, this, FloatValue.of(rhs));}

    default Comparison NE(final float rhs) {return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, FloatValue.of(rhs));}

    default Comparison GT(final float rhs) {return new Comparison(Comparison.Operator.GREATER_THAN, this, FloatValue.of(rhs));}

    default Comparison GE(final float rhs) {return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, FloatValue.of(rhs));}

    default Comparison LT(final float rhs) {return new Comparison(Comparison.Operator.LESS_THAN, this, FloatValue.of(rhs));}

    default Comparison LE(final float rhs) {return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, FloatValue.of(rhs));}

    default Comparison EQ(final Float rhs) {return new Comparison(Comparison.Operator.EQUAL_TO, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison NE(final Float rhs) {return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison GT(final Float rhs) {return new Comparison(Comparison.Operator.GREATER_THAN, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison GE(final Float rhs) {return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison LT(final Float rhs) {return new Comparison(Comparison.Operator.LESS_THAN, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison LE(final Float rhs) {return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, FloatValue.ofNonNullRhs(rhs));}

    default Comparison EQ(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison NE(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.NOT_EQUAL_TO, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GT(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison GE(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.GREATER_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LT(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN, this, Objects.requireNonNull(rhs, "rhs is required"));
    }

    default Comparison LE(final FloatExpression rhs)
    {
        return new Comparison(Comparison.Operator.LESS_THAN_OR_EQUAL, this, Objects.requireNonNull(rhs, "rhs is required"));
    }
}
