/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.Name;

import java.util.Objects;

public abstract class Parameter
    implements Predicate,
               SortPredicate,
               SortPredicate.Operand,
               Alias.Target,
               StringExpression,
               LongExpression,
               DoubleExpression,
               Literal,
               Atom
{
    Parameter() {/*nothing to do*/}

    @Override public final boolean grouped() {return false;}

    PropertyAccess.Static get(final String propertyName) {
        return new PropertyAccess.Static(this, Name.at(propertyName));
    }

    public static NamedParameter of(final String parameter)
    {
        Objects.requireNonNull(parameter, "parameter is required");
        if (parameter.isEmpty()) {
            throw new IllegalArgumentException("parameter can not be empty");
        }
        if (parameter.isBlank()) {
            throw new IllegalArgumentException("parameter can not be blank");
        }
        if (Name.requireBackticks(parameter)) {
            throw new IllegalArgumentException("parameters should not require backticks as '" + parameter + "'.");
        }

        return new NamedParameter(parameter);
    }

    public static IndexedParameter of(final int index)
    {
        if (0 > index) {
            throw new IllegalArgumentException("indexed parameters should be 0 or positive");
        }
        return new IndexedParameter(index);
    }
}
