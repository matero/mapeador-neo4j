/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.Objects;

public interface StringLiteral extends Literal, StringExpression
{
    @Override default StringLiteral group() {return this;}

    @Override default StringLiteral sortOperand() {return this;}
}

final class StringValue implements StringLiteral {
    private static final java.util.regex.Pattern TICKS = java.util.regex.Pattern.compile("'");

    private final String value;

    StringValue(final String value) {this.value = value;}

    @Override public void accept(final Visitor visitor) {visitor.visit(this.value);}

    static StringLiteral of(final CharSequence value)
    {
        if (null == value) {
            return NULL;
        }
        return ofNoNull(value.toString());
    }

    static StringLiteral of(final String value)
    {
        if (null == value) {
            return NULL;
        }
        return ofNoNull(value);
    }

    static StringLiteral of(final char value)
    {
        if ('\'' == value) {
            return new StringValue("\"'\"");
        } else {
            return new StringValue("'" + value + '\'');
        }
    }

    static StringLiteral of(final Character value)
    {
        if (null == value) {
            return NULL;
        }
        return of(value.charValue());
    }

    static StringLiteral ofNonNullRhs(final CharSequence rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return StringValue.ofNoNull(rhs.toString());
    }

    static StringLiteral ofNonNullRhs(final String rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return StringValue.ofNoNull(rhs);
    }

    static StringLiteral ofNonNullRhs(final Character rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return of(rhs.charValue());
    }

    static StringLiteral ofNoNull(final String value)
    {
        if (value.isEmpty()) {
            return new StringValue("''");
        }
        if (value.isBlank()) {
            return new StringValue('\'' + value + '\'');
        }
        if (value.contains("'")) {
            if (value.contains("\"")) {
                return new StringValue('\'' + TICKS.matcher(value).replaceAll("\\'") + '\'');
            } else {
                return new StringValue('"' + value + '"');
            }
        } else {
            return new StringValue('\'' + value + '\'');
        }
    }

    static StringLiteral from(final Object o)
    {
        if (null == o) {
            return NULL;
        }
        if (o instanceof StringValue) {
            return (StringValue) o;
        }
        if (o instanceof String) {
            return StringValue.ofNoNull((String) o);
        }
        if (o instanceof CharSequence) {
            return StringValue.ofNoNull(o.toString());
        }
        if (o instanceof Character) {
            return StringValue.of((char) o);
        }
        throw new CypherException("value " + o + " can not be interpreted as an StringValue");
    }
}
