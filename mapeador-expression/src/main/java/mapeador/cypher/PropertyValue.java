/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import mapeador.HasProperty;
import mapeador.Name;

import java.util.Map;
import java.util.Objects;

public abstract class PropertyValue implements Visitable
{
    private final String name;

    private PropertyValue(final String name) {this.name = name;}

    public String name() {return this.name;}

    public static PropertyValue of(final Map.Entry<?, ?> entry)
    {
        Objects.requireNonNull(entry, "entry is required");
        return of(getNameAt(entry.getKey()), entry.getValue());
    }

    public static PropertyValue of(final Object id, final Object value)
    {
        final var name = getNameAt(id);
        // value is note required because it is transformed to Literal.NULL
        if (value instanceof Parameter) {
            return new WithParameter(name, (Parameter) value);
        } else {
            return new WithExpression(name, Expression.of(value));
        }
    }

    private static String getNameAt(final Object id)
    {
        if (id instanceof HasProperty) {
            return ((HasProperty) id).getProperty();
        } else {
            return Name.at(id.toString());
        }
    }

    public static PropertyValue of(final String name, final Object value)
    {
        // value is note required because it is transformed to Literal.NULL
        if (value instanceof Parameter) {
            return new WithParameter(Name.at(name), (Parameter) value);
        } else {
            return new WithExpression(Name.at(name), Expression.of(value));
        }
    }

    public static PropertyValue of(final String name, final Expression value)
    {
        Objects.requireNonNull(value, "value is required");
        if (value instanceof Parameter) {
            return of(name, (Parameter) value);
        } else {
            return new WithExpression(Name.at(name), value);
        }
    }

    public static PropertyValue of(final String name, final Literal value)
    {
        Objects.requireNonNull(value, "value is required");
        return new WithExpression(Name.at(name), value);
    }

    public static PropertyValue of(final String name, final Parameter value)
    {
        Objects.requireNonNull(value, "value is required");
        return new WithParameter(Name.at(name), value);
    }

    public static final class WithExpression extends PropertyValue
    {
        private final Expression value;

        private WithExpression(final String name, final Expression value)
        {
            super(name);
            this.value = value;
        }

        public Expression value() {return this.value;}

        @Override public void accept(final Visitor visitor) {visitor.visit(this);}
    }

    public static final class WithParameter extends PropertyValue
    {
        private final Parameter value;

        private WithParameter(final String name, final Parameter value)
        {
            super(name);
            this.value = value;
        }

        public Parameter value() {return this.value;}

        @Override public void accept(final Visitor visitor) {visitor.visit(this);}
    }
}
