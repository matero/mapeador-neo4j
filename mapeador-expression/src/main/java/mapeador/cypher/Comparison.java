/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public final class Comparison extends BinaryOperation<Comparison> implements Predicate
{
    Comparison(final Operator operator, final ComparableExpression lhs, final ComparableExpression rhs)
    {
        super(operator.cypher, lhs, rhs);
    }

    @Override public Comparison sortOperand() {return this;}

    public enum Operator
    {
        EQUAL_TO(" = "),
        NOT_EQUAL_TO(" <> "),
        LESS_THAN(" < "),
        LESS_THAN_OR_EQUAL(" <= "),
        GREATER_THAN(" > "),
        GREATER_THAN_OR_EQUAL(" >= "),
        STARTS_WITH(" STARTS WITH "),
        ENDS_WITH(" ENDS WITH "),
        CONTAINS(" CONTAINS "),
        MATCHES(" ~= ");

        private final String cypher;

        Operator(final String cypher) {this.cypher = cypher;}
    }

    static Comparison EQ(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.EQUAL_TO, lhs, rhs);
    }

    static Comparison NE(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.NOT_EQUAL_TO, lhs, rhs);
    }

    static Comparison GT(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.GREATER_THAN, lhs, rhs);
    }

    static Comparison GE(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.GREATER_THAN_OR_EQUAL, lhs, rhs);
    }

    static Comparison LT(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.LESS_THAN, lhs, rhs);
    }

    static Comparison LE(final ComparableExpression lhs, final ComparableExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.LESS_THAN_OR_EQUAL, lhs, rhs);
    }

    static Comparison MATCHES(final StringExpression lhs, final StringExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.MATCHES, lhs, rhs);
    }

    public static Comparison CONTAINS(final StringExpression lhs, final StringExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.CONTAINS, lhs, rhs);
    }

    public static Comparison ENDS_WITH(final StringExpression lhs, final StringExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.ENDS_WITH, lhs, rhs);
    }

    public static Comparison STARTS_WITH(final StringExpression lhs, final StringExpression rhs)
    {
        Objects.requireNonNull(lhs, "lhs is required");
        Objects.requireNonNull(lhs, "rhs is required");
        return new Comparison(Operator.STARTS_WITH, lhs, rhs);
    }
}
