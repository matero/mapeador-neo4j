/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface StringExpression extends ComparableExpression
{
    default Comparison STARTS_WITH(final char rhs) {return Comparison.STARTS_WITH(this, StringValue.of(rhs));}

    default Comparison STARTS_WITH(final Character rhs) {return Comparison.STARTS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison STARTS_WITH(final CharSequence rhs) {return Comparison.STARTS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison STARTS_WITH(final String rhs) {return Comparison.STARTS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison STARTS_WITH(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return Comparison.STARTS_WITH(this, rhs);
    }

    default Comparison ENDS_WITH(final char rhs) {return Comparison.ENDS_WITH(this, StringValue.of(rhs));}

    default Comparison ENDS_WITH(final Character rhs) {return Comparison.ENDS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison ENDS_WITH(final CharSequence rhs) {return Comparison.ENDS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison ENDS_WITH(final String rhs) {return Comparison.ENDS_WITH(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison ENDS_WITH(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return Comparison.ENDS_WITH(this, rhs);
    }

    default Comparison CONTAINS(final char rhs) {return Comparison.CONTAINS(this, StringValue.of(rhs));}

    default Comparison CONTAINS(final Character rhs) {return Comparison.CONTAINS(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison CONTAINS(final CharSequence rhs) {return Comparison.CONTAINS(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison CONTAINS(final String rhs) {return Comparison.CONTAINS(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison CONTAINS(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return Comparison.CONTAINS(this, rhs);
    }

    default Comparison MATCHES(final CharSequence regex)
    {
        Objects.requireNonNull(regex, "regex is required");
        return Comparison.MATCHES(this, StringValue.ofNoNull(regex.toString()));
    }

    default Comparison MATCHES(final String regex)
    {
        Objects.requireNonNull(regex, "regex is required");
        return Comparison.MATCHES(this, StringValue.ofNoNull(regex));
    }

    default Comparison MATCHES(final StringExpression regex)
    {
        Objects.requireNonNull(regex, "regex is required");
        return Comparison.MATCHES(this, regex);
    }

    default Comparison EQ(final char rhs) {return Comparison.EQ(this, StringValue.of(rhs));}

    default Comparison NE(final char rhs) {return Comparison.NE(this, StringValue.of(rhs));}

    default Comparison GT(final char rhs) {return Comparison.GT(this, StringValue.of(rhs));}

    default Comparison GE(final char rhs) {return Comparison.GE(this, StringValue.of(rhs));}

    default Comparison LT(final char rhs) {return Comparison.LT(this, StringValue.of(rhs));}

    default Comparison LE(final char rhs) {return Comparison.LE(this, StringValue.of(rhs));}

    default Comparison EQ(final Character rhs) {return Comparison.EQ(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison NE(final Character rhs) {return Comparison.NE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GT(final Character rhs) {return Comparison.GT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GE(final Character rhs) {return Comparison.GE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LT(final Character rhs) {return Comparison.LT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LE(final Character rhs) {return Comparison.LE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison EQ(final CharSequence rhs) {return Comparison.EQ(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison NE(final CharSequence rhs) {return Comparison.NE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GT(final CharSequence rhs) {return Comparison.GT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GE(final CharSequence rhs) {return Comparison.GE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LT(final CharSequence rhs) {return Comparison.LT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LE(final CharSequence rhs) {return Comparison.LE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison EQ(final String rhs) {return Comparison.EQ(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison NE(final String rhs) {return Comparison.NE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GT(final String rhs) {return Comparison.GT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison GE(final String rhs) {return Comparison.GE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LT(final String rhs) {return Comparison.LT(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison LE(final String rhs) {return Comparison.LE(this, StringValue.ofNonNullRhs(rhs));}

    default Comparison EQ(final StringExpression rhs) {return Comparison.EQ(this, rhs);}

    default Comparison NE(final StringExpression rhs) {return Comparison.NE(this, rhs);}

    default Comparison GT(final StringExpression rhs) {return Comparison.GT(this, rhs);}

    default Comparison GE(final StringExpression rhs) {return Comparison.GE(this, rhs);}

    default Comparison LT(final StringExpression rhs) {return Comparison.LT(this, rhs);}

    default Comparison LE(final StringExpression rhs) {return Comparison.LE(this, rhs);}

    default StringConcatenation concat(final char rhs) {return new StringConcatenation(this, StringValue.of(rhs));}

    default StringConcatenation concat(final Character rhs) {return new StringConcatenation(this, StringValue.ofNonNullRhs(rhs));}

    default StringConcatenation concat(final CharSequence rhs) {return new StringConcatenation(this, StringValue.ofNonNullRhs(rhs));}

    default StringConcatenation concat(final String rhs) {return new StringConcatenation(this, StringValue.ofNonNullRhs(rhs));}

    default StringConcatenation concat(final StringExpression rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new StringConcatenation(this, rhs);
    }
}
