/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.List;

public abstract class PropertyAccess implements SortPredicate, SortPredicate.Operand, GroupableExpression, Alias.Target
{
    private final Atom atom;

    private PropertyAccess(final Atom atom) {this.atom = atom;}

    public final Atom atom() {return this.atom;}

    @Override public final boolean grouped() {return false;}

    public static class Static extends PropertyAccess implements PropertyExpression
    {
        private final String name;

        protected Static(final Atom entity, final String name)
        {
            super(entity);
            this.name = name;
        }

        public String name() {return this.name;}

        @Override public PropertyAccess.Static group() {return this;}

        @Override public PropertyAccess.Static sortOperand() {return this;}

        @Override public String lookupsHead() {return this.name;}

        @Override public List<String> lookupsTail() {return List.of();}

        @Override public void accept(final Visitor visitor) {visitor.visit(this);}
    }

    public static final class Dynamic extends PropertyAccess implements Predicate, StringExpression
    {
        private final Expression operand;

        protected Dynamic(final EntityVariable entity, final Expression operand)
        {
            super(entity);
            this.operand = operand;
        }

        public Expression operand() {return this.operand;}

        @Override public PropertyAccess.Dynamic group() {return this;}

        @Override public PropertyAccess.Dynamic sortOperand() {return this;}

        @Override public void accept(final Visitor visitor) {visitor.visit(this);}
    }

}
