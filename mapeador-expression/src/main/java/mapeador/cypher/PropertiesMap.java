/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toUnmodifiableList;

public interface PropertiesMap extends Expression, Atom
{
    boolean isEmpty();

    PropertyValue head();

    List<PropertyValue> tail();

    @Override default void accept(final Visitor visitor) {visitor.visit(this);}


    static PropertiesMap of() {return EmptyPropertiesMap.INSTANCE;}

    static PropertiesMap of(final PropertyValue entry)
    {
        if (null == entry) {
            throw new CypherException("property entries cant be null");
        }
        return new SinglePropertiesMap(entry);
    }

    static PropertiesMap of(final PropertyValue firstEntry, final PropertyValue... otherEntries)
    {
        if (null == firstEntry) {
            throw new CypherException("property entries cant be null");
        }
        if (null == otherEntries || 0 == otherEntries.length) {
            return new SinglePropertiesMap(firstEntry);
        }
        try {
            return new MultiplePropertiesMap(firstEntry, List.of(otherEntries));
        } catch (final NullPointerException e) {
            throw new CypherException("property entries cant be null", e);
        }
    }

    static PropertiesMap of(final PropertyValue firstEntry, final List<PropertyValue> otherEntries)
    {
        if (null == firstEntry) {
            throw new CypherException("property entries cant be null");
        }
        if (null == otherEntries || otherEntries.isEmpty()) {
            return new SinglePropertiesMap(firstEntry);
        }
        try {
            return new MultiplePropertiesMap(firstEntry, List.copyOf(otherEntries));
        } catch (final NullPointerException e) {
            throw new CypherException("property entries cant be null", e);
        }
    }

    static PropertiesMap of(final List<PropertyValue> entries)
    {
        Objects.requireNonNull(entries, "entries is required");

        if (entries.isEmpty()) {
            return EmptyPropertiesMap.INSTANCE;
        }

        final var firstEntry = entries.get(0);
        if (null == firstEntry) {
            throw new CypherException("property entries cant be null");
        }

        if (1 == entries.size()) {
            return new SinglePropertiesMap(firstEntry);
        } else {
            try {
                return new MultiplePropertiesMap(firstEntry, List.copyOf(entries.subList(1, entries.size())));
            } catch (final NullPointerException e) {
                throw new CypherException("property entries cant be null", e);
            }
        }
    }

    static PropertiesMap of(final PropertyValue[] entries)
    {
        Objects.requireNonNull(entries, "entries is required");

        if (0 == entries.length) {
            return EmptyPropertiesMap.INSTANCE;
        }

        final var firstEntry = entries[0];
        if (null == firstEntry) {
            throw new CypherException("property entries cant be null");
        }

        if (1 == entries.length) {
            return new SinglePropertiesMap(firstEntry);
        } else {
            try {
                return new MultiplePropertiesMap(firstEntry, List.copyOf(Arrays.asList(entries).subList(1, entries.length)));
            } catch (final NullPointerException e) {
                throw new CypherException("property entries cant be null", e);
            }
        }
    }

    static PropertiesMap of(final Map<?, ?> javaMap)
    {
        Objects.requireNonNull(javaMap, "javaMap is required");

        if (javaMap.isEmpty()) {
            return EmptyPropertiesMap.INSTANCE;
        }

        final var entries = new java.util.ArrayList<>(javaMap.entrySet()); // we don't need to check for null entries
        final var firstEntry = entries.get(0);
        if (null == firstEntry) {
            throw new CypherException("property entries cant be null");
        }

        if (1 == entries.size()) {
            return new SinglePropertiesMap(PropertyValue.of(firstEntry));
        } else {
            try {
                return new MultiplePropertiesMap(PropertyValue.of(firstEntry), entries.subList(1, entries.size())
                                                                                      .stream()
                                                                                      .map(PropertyValue::of)
                                                                                      .collect(toUnmodifiableList()));
            } catch (final NullPointerException e) {
                throw new CypherException("property entries cant be null", e);
            }
        }
    }

    static PropertiesMap of(final Object name, final Object value, final Object... otherEntries)
    {
        if (null == otherEntries || 0 == otherEntries.length) {
            return new SinglePropertiesMap(PropertyValue.of(name, value));
        }

        if (0 != otherEntries.length % 2) {
            throw new CypherException("otherEntries required an even number of arguments, alternating key (can be string or property) and value. " +
                                      "Arguments were: " + Arrays.toString(otherEntries) + ".");
        }

        final var tail = new java.util.ArrayList<PropertyValue>(otherEntries.length / 2);
        for (int i = 0; i < otherEntries.length; i += 2) {
            final var propertyName = otherEntries[i];
            final var propertyValue = otherEntries[i + 1];
            tail.add(PropertyValue.of(propertyName, propertyValue));
        }
        return new MultiplePropertiesMap(PropertyValue.of(name, value), List.copyOf(tail));
    }
}

enum EmptyPropertiesMap implements PropertiesMap
{
    INSTANCE;

    @Override public boolean isEmpty() {return true;}

    @Override public PropertyValue head() {return null;}

    @Override public List<PropertyValue> tail() {return List.of();}
}

final class SinglePropertiesMap implements PropertiesMap
{
    private final PropertyValue property;

    SinglePropertiesMap(final PropertyValue property) {this.property = property;}

    @Override public boolean isEmpty() {return false;}

    @Override public PropertyValue head() {return this.property;}

    @Override public List<PropertyValue> tail() {return List.of();}
}

final class MultiplePropertiesMap implements PropertiesMap
{
    private final PropertyValue head;
    private final List<PropertyValue> tail;

    MultiplePropertiesMap(final PropertyValue head, final List<PropertyValue> tail)
    {
        this.head = head;
        this.tail = tail;
    }

    @Override public boolean isEmpty() {return false;}

    @Override public PropertyValue head() {return this.head;}

    @Override public List<PropertyValue> tail() {return this.tail;}
}
