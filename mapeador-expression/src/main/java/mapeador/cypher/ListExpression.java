/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.cypher;

import java.util.Objects;

public interface ListExpression extends Expression
{
    default ListConcatenation plus(final ListExpression rhs)
    {
        Objects.requireNonNull(rhs, "rhs is required");
        return new ListConcatenation(this, rhs);
    }

    default GetElement get(final int index) {return new GetElement(this, IntegerValue.of(index));}

    default GetElement get(final String alias) {return new GetElement(this, AliasName.of(alias));}

    default GetElement get(final IntegerExpression index) {return new GetElement(this, index);}

    default GetSubList subList(final int start, final int end) {return new GetSubList(this, IntegerValue.of(start), IntegerValue.of(end));}

    default GetSubList subList(final int start, final String end) {return new GetSubList(this, IntegerValue.of(start), AliasName.of(end));}

    default GetSubList subList(final int start, final IntegerExpression end) {return new GetSubList(this, IntegerValue.of(start), end);}

    default GetSubList subList(final String start, final int end) {return new GetSubList(this, AliasName.of(start), IntegerValue.of(end));}

    default GetSubList subList(final String start, final String end) {return new GetSubList(this, AliasName.of(start), AliasName.of(end));}

    default GetSubList subList(final String start, final IntegerExpression end) {return new GetSubList(this, AliasName.of(start), end);}

    default GetSubList subList(final IntegerExpression start, final int end) {return new GetSubList(this, start, IntegerValue.of(end));}

    default GetSubList subList(final IntegerExpression start, final String end) {return new GetSubList(this, start, AliasName.of(end));}

    default GetSubList subList(final IntegerExpression start, final IntegerExpression end) {return new GetSubList(this, start, end);}
}
