/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

public interface Visitor
{
    void visit(Alias alias);
    void visit(AliasName aliasName);

    // literals
    void visit(boolean value);

    void visit(byte value);

    void visit(short value);

    void visit(int value);

    void visit(long value);

    void visit(float value);

    void visit(double value);

    void visit(String value);

    void visit(ListLiteral value);

    // operations
    void visit(UnaryOperation.Prefix<?> operation);

    void visit(UnaryOperation.Postfix<?> operation);

    void visit(BinaryOperation<?> operation);

    // complex expressions
    void visit(NodeVariable node);

    void visit(RelationshipVariable relation);

    void visit(PropertyAccess.Static access);

    void visit(PropertyAccess.Dynamic access);

    void visit(RawExpression expr);

    void visit(SortPredicate.Explicit sortPredicate);

    void visit(CaseExpression expr);

    void visit(CaseExpression.Alternative alternative);

    void visit(PropertyExpression propertyExpression);

    void visit(SetItem.Reset item);

    void visit(SetItem.Append item);

    void visit(UpdateLabels item);

    void visit(PropertyValue.WithExpression value);

    void visit(PropertyValue.WithParameter value);

    void visit(PropertiesMap map);

    void visit(NamedParameter parameter);

    void visit(IndexedParameter parameter);

    void visit(GetElement expr);

    void visit(GetSubList expr);
}
