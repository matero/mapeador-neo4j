/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.cypher;

import java.util.List;

public enum Null
    implements Literal, Predicate, ByteLiteral, BooleanLiteral, ListLiteral, ShortLiteral, IntegerLiteral, LongLiteral, FloatLiteral, DoubleLiteral,
               StringLiteral
{
    INSTANCE;

    @Override public boolean isEmpty() {return true;}

    @Override public int size() {return 0;}

    @Override public Expression head() {return null;}

    @Override public List<Expression> tail() {return null;}

    @Override public Null group() {return this;}

    @Override public Null sortOperand() {return this;}

    @Override public void accept(final Visitor visitor) {visitor.visit("NULL");}
}
