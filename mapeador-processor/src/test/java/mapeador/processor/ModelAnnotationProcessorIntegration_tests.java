/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import org.junit.jupiter.api.Test;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

class ModelAnnotationProcessorIntegration_tests
{
    @Test void should_compile_class_without_id_defined()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Movie.java"));
        assertThat(compilation).succeeded();
    }

    @Test void should_compile_class_with_id_defined()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Person.java"));
        assertThat(compilation).succeeded();
    }

    @Test void should_be_able_to_compile_multiple_node_classes_defined()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Movie.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/Person.java"));
        assertThat(compilation).succeeded();
    }

    @Test void should_be_able_to_interpret_relationships_with_properties()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor(), new RelationshipAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Movie.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/Person.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/ACTED_IN.java"));
        assertThat(compilation).succeeded();
    }

    @Test void should_be_able_to_interpret_relationships_without_properties()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor(), new RelationshipAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Movie.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/Person.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/DIRECTED.java"));
        assertThat(compilation).succeeded();
    }

    @Test void should_fail_when_class_doesnt_define_superclass()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/NoSuperClass.java"));
        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("No base class defined!");
    }

    @Test void should_fail_when_class_doesnt_define_record()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/NoRecord.java"));
        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("No Record class found!");
    }

    @Test void should_fail_when_class_defines_more_than_one_record()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/TooManyRecords.java"));
        assertThat(compilation).failed();
        assertThat(compilation).hadErrorContaining("Only one Record can be defined per Model!");
    }

    @Test void should_be_able_to_create_implicit_relations()
    {
        final Compilation compilation = javac().withProcessors(new NodeAnnotationProcessor(), new RelationshipAnnotationProcessor())
                                               .compile(JavaFileObjects.forResource("mapeador/processor/samples/Movie.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/People.java"),
                                                        JavaFileObjects.forResource("mapeador/processor/samples/HAS_DIRECTED.java"));
        assertThat(compilation).succeeded();
    }
}
