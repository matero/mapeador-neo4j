/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.List;

abstract class Interpreter
{
    static final Modifier[] OF_MODIFIERS = {};

    static final TypeName BOOLEAN = TypeName.BOOLEAN;
    static final TypeName BYTE = TypeName.BYTE;
    static final TypeName SHORT = TypeName.SHORT;
    static final TypeName INT = TypeName.INT;
    static final TypeName LONG = TypeName.LONG;
    static final TypeName CHAR = TypeName.CHAR;
    static final TypeName FLOAT = TypeName.FLOAT;
    static final TypeName DOUBLE = TypeName.DOUBLE;
    static final ClassName BOXED_BOOLEAN = ClassName.get("java.lang", "Boolean");
    static final ClassName BOXED_BYTE = ClassName.get("java.lang", "Byte");
    static final ClassName BOXED_SHORT = ClassName.get("java.lang", "Short");
    static final ClassName BOXED_INT = ClassName.get("java.lang", "Integer");
    static final ClassName BOXED_LONG = ClassName.get("java.lang", "Long");
    static final ClassName BOXED_CHAR = ClassName.get("java.lang", "Character");
    static final ClassName BOXED_FLOAT = ClassName.get("java.lang", "Float");
    static final ClassName BOXED_DOUBLE = ClassName.get("java.lang", "Double");
    static final ClassName STRING = ClassName.get("java.lang", "String");

    final ProcessingEnvironment environment;

    Interpreter(final ProcessingEnvironment environment)
    {
        this.environment = environment;
    }

    final Types types() {return this.environment.getTypeUtils();}

    final Elements elements() {return this.environment.getElementUtils();}

    Modifier[] modifiersOf(final VariableElement variable)
    {
        final var modifiers = new java.util.HashSet<>(variable.getModifiers());
        modifiers.add(Modifier.FINAL);
        modifiers.add(Modifier.STATIC);
        return modifiers.toArray(OF_MODIFIERS);
    }

    final Modifier[] modifiersOf(final TypeElement entity) {return entity.getModifiers().toArray(OF_MODIFIERS);}

    Name nameOf(final VariableElement variable)
    {
        return variable.getSimpleName();
    }

    TypeName typeNameOf(final VariableElement variable)
    {
        final TypeMirror type = variable.asType();
        if (type.getKind().isPrimitive()) {
            return ClassName.get(type);
        }
        if (TypeKind.DECLARED == type.getKind()) {
            final String typename = type.toString();
            if ("java.util.List".equals(typename) || typename.startsWith("java.util.List<")) {
                final DeclaredType listElement = (DeclaredType) type;
                final List<? extends TypeMirror> listElementClass = listElement.getTypeArguments();
                final TypeName t;
                if (listElementClass.isEmpty()) {
                    t = ClassName.get(Object.class);
                } else {
                    final TypeMirror asType = listElementClass.get(0);
                    t = ClassName.get(asType);
                }
                return ParameterizedTypeName.get(ClassName.get(List.class), t);
            } else {
                return ClassName.get(type);
            }
        }
        throw new ModelException(variable, "type not supported");
    }

    TypeMirror getTypeMirror(final Class<?> aClass)
    {
        return getTypeMirror(aClass.getCanonicalName());
    }

    TypeMirror getTypeMirror(final String name)
    {
        switch (name) {
            case "boolean":
                return types().getPrimitiveType(TypeKind.BOOLEAN);
            case "char":
                return types().getPrimitiveType(TypeKind.CHAR);
            case "byte":
                return types().getPrimitiveType(TypeKind.BYTE);
            case "short":
                return types().getPrimitiveType(TypeKind.SHORT);
            case "int":
                return types().getPrimitiveType(TypeKind.INT);
            case "long":
                return types().getPrimitiveType(TypeKind.LONG);
            case "float":
                return types().getPrimitiveType(TypeKind.FLOAT);
            case "double":
                return types().getPrimitiveType(TypeKind.DOUBLE);
        }
        return getTypeElement(name).asType();
    }

    TypeElement getTypeElement(final Class<?> aClass) {return getTypeElement(aClass.getCanonicalName());}

    TypeElement getTypeElement(final String name) {return elements().getTypeElement(name);}

    boolean isAssignable(final TypeMirror t1, final TypeElement t2) {return isAssignable(t1, t2.asType());}

    boolean isAssignable(final TypeMirror t1, final TypeMirror t2) {return types().isAssignable(t1, t2);}
}
