/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import mapeador.Node;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class NodesRegistry extends EntityRegistry<MetaNode>
{
    final Map<List<String>, MetaNode> byLabels;
    final Map<String, List<MetaNode>> byPackage;
    final Map<String, MetaNode> byCannonicalName;

    NodesRegistry() {this(new HashMap<>(), new HashMap<>(), new HashMap<>());}

    NodesRegistry(
        final Map<List<String>, MetaNode> byLabels,
        final Map<String, List<MetaNode>> byPackage,
        final Map<String, MetaNode> byCannonicalClassName)
    {
        super("nodes", ClassName.get(Node.class));
        this.byLabels = byLabels;
        this.byPackage = byPackage;
        this.byCannonicalName = byCannonicalClassName;
    }

    Set<String> getPackages() {return Collections.unmodifiableSet(this.byPackage.keySet());}

    List<MetaNode> atPackage(final String packageName) {return this.byPackage.get(packageName);}

    @Override Collection<MetaNode> all() {return Collections.unmodifiableCollection(this.byCannonicalName.values());}

    int size() {return this.byCannonicalName.size(); }

    @Override void register(final MetaNode node)
    {
        this.byLabels.put(node.labels, node);
        this.byPackage.putIfAbsent(node.packageName, new ArrayList<>());
        this.byPackage.get(node.packageName).add(node);
        this.byCannonicalName.put(node.canonicalName, node);
    }

    @Override void setupDone(final ProcessingEnvironment processingEnvironment) {/*nothing to do*/}

    @Override void codeGenerationDone()
    {
        this.byCannonicalName.clear();
        this.byLabels.clear();
        this.byPackage.clear();
    }

    @Override public boolean hasElementsRegistered()
    {
        return !this.byPackage.isEmpty();
    }

    public static NodesRegistry get() {return NodesRegistry.Unique.INSTANCE;}

    private static final class Unique
    {
        private static final NodesRegistry INSTANCE = new NodesRegistry();
    }
}
