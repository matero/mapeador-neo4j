/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import mapeador.Utility;

import javax.lang.model.element.Modifier;

abstract class EntityRegistryClassBuilder<E extends MetaEntity> extends RegistryClassBuilder<E>
{
    EntityRegistryClassBuilder(final Registry<E> registry, final String date)
    {
        super(TypeSpec.classBuilder(registry.getRegistryType()), registry, date);
        this.builder.addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .superclass(Utility.class);
    }

    @Override void defineElements()
    {
        for (final var entity : this.registry.all()) {
            this.builder.addField(FieldSpec.builder(entity.type(), entity.identifier, entity.getStaticBuilderModifiers())
                                           .initializer("new $T()", entity.type())
                                           .build());
        }
    }

    @Override void declareAttributes() {/*nothing to do*/}

    @Override protected void buildConstructor(final MethodSpec.Builder constructor)
    {
        constructor.addModifiers(Modifier.PRIVATE)
                   .addStatement("super($T.class)", this.registry.getRegistryType());
    }

    @Override void defineOverridenMethods() {/*nothing to do*/}
}

final class NodesRegistryClassBuilder extends EntityRegistryClassBuilder<MetaNode>
{
    NodesRegistryClassBuilder(final String date) {super(NodesRegistry.get(), date);}

    NodesRegistryClassBuilder(final NodesRegistry registry, final String date) {super(registry, date);}


    @Override void defineElements()
    {
        super.defineElements();
        for (final var entity : this.registry.all()) {
            this.builder.addMethod(MethodSpec.methodBuilder(entity.identifier)
                                             .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                                             .returns(ClassName.get(entity.packageName, entity.identifier, "Variable"))
                                             .addParameter(String.class, "nodeName", Modifier.FINAL)
                                             .addStatement("return $T.node(nodeName)", entity.type())
                                             .build());
        }
    }
}

final class RelationshipsRegistryClassBuilder extends EntityRegistryClassBuilder<MetaRelationship>
{
    RelationshipsRegistryClassBuilder(final String date) {super(RelationshipsRegistry.get(), date);}

    RelationshipsRegistryClassBuilder(final RelationshipsRegistry registry, final String date) {super(registry, date);}


    @Override void defineElements()
    {
        super.defineElements();
        for (final var entity : this.registry.all()) {
            this.builder.addMethod(MethodSpec.methodBuilder(entity.identifier)
                                             .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                                             .returns(entity.getVariableClassName())
                                             .addParameter(String.class, "relationName", Modifier.FINAL)
                                             .addStatement("return $T.relation(relationName)", entity.type())
                                             .build());
        }
    }
}
