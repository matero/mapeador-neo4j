/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.TypeName;
import mapeador.EntityRecord;
import mapeador.Name;
import mapeador.node;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.VariableElement;
import java.util.List;

final class EntityPropertyInterpreter extends AttributeInterpreter
{
    private static final List<TypeName> POSSIBLE_ID_TYPES = List.of(INT, BOXED_INT, LONG, BOXED_LONG, STRING);

    EntityPropertyInterpreter(final ProcessingEnvironment environment) {super(environment);}

    MetaProperty read(final VariableElement variable)
    {
        if (hasSupportedPropertyType(variable)) {
            checkModifiersOf(variable);
            final var type = typeNameOf(variable);
            checkAnnotationsOf(variable, type);
            final var id = variable.getAnnotation(node.Record.id.class);
            return new MetaProperty(type, nameOf(variable), modifiersOf(variable), propertyOf(variable), null != id);
        }
        if (isRelation(variable)) {
            RelationshipsRegistry.get().registerImplicitRelationshipAt(variable);
            return null;
        }
        throw new IllegalFieldType(variable, getSupportedTypeNames());
    }

    void checkModifiersOf(final VariableElement variable) {checkModifiersOf(variable, EntityRecord.property.class);}

    void checkAnnotationsOf(final VariableElement variable, final TypeName type)
    {
        if (null != variable.getAnnotation(node.Record.id.class)) {
            if (!isNodeProperty(variable)) {
                throw new ModelException(variable, "@Id fields can be used only with nodes");
            }
            if (!canBeUsedAsId(type)) {
                throw new ModelException(variable, "@Id fields can not be used on fields of type " + type);
            }
        }
    }

    private boolean isNodeProperty(final VariableElement variable)
    {
        final var record = variable.getEnclosingElement().asType();
        final var nodeRecordClass = getTypeElement(node.Record.class);
        return isAssignable(record, nodeRecordClass);
    }

    private boolean canBeUsedAsId(final TypeName type) {return POSSIBLE_ID_TYPES.contains(type);}

    String propertyOf(final VariableElement variable)
    {
        final var property = variable.getAnnotation(EntityRecord.property.class);
        if (null == property) {
            return variable.getSimpleName().toString();
        }
        final var name = property.value();
        if (null == name) {
            throw new ModelException(variable, "property can not be defined as null");
        }
        if (name.isEmpty()) {
            throw new ModelException(variable, "property can not be empty");
        }
        if (name.isBlank()) {
            throw new ModelException(variable, "property can not be blank");
        }
        return Name.at(name);
    }
}
