/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import javax.lang.model.element.Element;
import javax.lang.model.element.VariableElement;
import java.util.Collection;

final class IllegalFieldType extends ModelException
{
    IllegalFieldType(final VariableElement variable, final Collection<String> supportedTypes)
    {
        this(variable, make_message(variable.asType().toString(), supportedTypes));
    }

    private static String make_message(final String type, final Collection<String> supportedTypes)
    {
        final var msg = new StringBuilder()
            .append("Type [")
            .append(type)
            .append("] isn't supported. Supported types are:");
        for (final var t : supportedTypes) {
            if (null != t && !t.isBlank()) {
                msg.append("\n\t").append(t);
            }
        }
        return msg.toString();
    }

    IllegalFieldType(final Element element, final String message) {super(element, message);}
}
