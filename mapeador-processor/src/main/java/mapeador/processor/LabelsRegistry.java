/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import mapeador.Label;
import mapeador.Labels;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class LabelsRegistry extends Registry<MetaClassifier>
{
    private final Map<String, MetaClassifier> labels = new java.util.HashMap<>();

    LabelsRegistry() {this("labels");}

    LabelsRegistry(final String generatedEnumName) {super(generatedEnumName, ClassName.get(Label.class));}

    @Override Collection<MetaClassifier> all() {return Collections.unmodifiableCollection(this.labels.values());}

    CodeBlock getLabelsDefinition(final List<String> labels)
    {
        final var registryType = this.getRegistryType();
        final var l = labels.iterator();
        var label = registerLabel(l.next());

        if (l.hasNext()) {
            final var definition = new StringBuilder(64).append("$T.of($T.$L");
            final Object[] parameters = new Object[1 + 2 * labels.size()];
            parameters[0] = Labels.class;
            parameters[1] = registryType;
            parameters[2] = label.identifier;
            int p = 3;
            do {
                definition.append(", $T.$L");
                label = registerLabel(l.next());
                parameters[p++] = registryType;
                parameters[p++] = label.identifier;
            }
            while (l.hasNext());

            return CodeBlock.of(definition.append(')').toString(), parameters);
        } else {
            return CodeBlock.of("$T.$L", registryType, label.identifier);
        }
    }

    MetaClassifier registerLabel(final String name) {return this.labels.computeIfAbsent(name, MetaClassifier::of);}

    public static LabelsRegistry get() {return Unique.INSTANCE;}

    private static final class Unique
    {
        private static final LabelsRegistry INSTANCE = new LabelsRegistry();
    }
}
