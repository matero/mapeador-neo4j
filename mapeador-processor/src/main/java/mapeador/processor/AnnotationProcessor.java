/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.JavaFile;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Set;

import static javax.tools.Diagnostic.Kind.ERROR;
import static javax.tools.Diagnostic.Kind.NOTE;

abstract class AnnotationProcessor extends AbstractProcessor
{
    final String today;

    AnnotationProcessor(final LocalDateTime today) {this.today = today.format(DateTimeFormatter.ISO_DATE_TIME);}

    abstract void readMetadataFor(final Set<? extends Element> annotatedElements);

    void generateCode(final CodeGenerator codeGenerator)
    {
        info("[%s] STARTED", codeGenerator.phaseName());

        final Collection<JavaFile> javaFiles = codeGenerator.generateCode(this.today, this.processingEnv);

        if (javaFiles.isEmpty()) {
            info("[%s] no java files generated. DONE", codeGenerator.phaseName());
        } else {
            for (final JavaFile generatedCode : javaFiles) {
                try {
                    generatedCode.writeTo(filer());
                } catch (final IOException e) {
                    throw new IllegalStateException("could not complete " + codeGenerator.phaseName(), e);
                }
            }
            final int javaFilesCount = javaFiles.size();
            if (1 == javaFilesCount) {
                info("[%s] 1 java file generated. DONE", codeGenerator.phaseName());
            } else {
                info("[%s] %d java files generated. DONE", codeGenerator.phaseName(), javaFilesCount);
            }
        }
    }

    final Messager messager() {return this.processingEnv.getMessager();}

    Filer filer() {return this.processingEnv.getFiler();}

    final void info(final String msg, final Object... args) {message(NOTE, String.format(msg, args));}

    void error(final Throwable failure) {error(message(failure));}

    void error(final String msg) {message(ERROR, msg);}

    /**
     * Prints a msg of the specified kind.
     *
     * @param kind the kind of msg
     * @param msg  the msg, or an empty string if none
     */
    final void message(final Diagnostic.Kind kind, final CharSequence msg) {messager().printMessage(kind, msg);}

    /**
     * Prints a msg of the specified kind at the location of the e.
     *
     * @param kind the kind of msg
     * @param msg  the msg, or an empty string if none
     * @param e    the e to use as a position hint
     */
    final void message(final Diagnostic.Kind kind, final CharSequence msg, final Element e) {messager().printMessage(kind, msg, e);}

    final String message(final Throwable t)
    {
        final String msg = t.getMessage();
        return null == msg ? "unknown error" : msg;
    }

    final void error(final ModelException failure)
    {
        if (null == failure.element) {
            error(message(failure));
        } else {
            message(ERROR, message(failure), failure.element);
        }
    }
}
