/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.WildcardTypeName;
import mapeador.Entity;
import mapeador.Node;
import mapeador.Property;
import mapeador.Relationship;
import mapeador.cypher.BooleanPropertyAccess;
import mapeador.cypher.BytePropertyAccess;
import mapeador.cypher.DoublePropertyAccess;
import mapeador.cypher.FloatPropertyAccess;
import mapeador.cypher.IntegerPropertyAccess;
import mapeador.cypher.LongPropertyAccess;
import mapeador.cypher.ShortPropertyAccess;
import mapeador.cypher.StringPropertyAccess;

import javax.lang.model.element.Modifier;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.squareup.javapoet.MethodSpec.methodBuilder;

abstract class BaseEntityClassBuilder<E extends MetaEntity> extends ClassBuilder
{
    static final ClassName NODE = ClassName.get(Node.class);
    static final ClassName RELATIONSHIP = ClassName.get(Relationship.class);

    private static final Map<TypeName, TypeName> PROPERTY_ACCESS_BY_TYPE;

    static {
        final var propertyAccessByType = new java.util.HashMap<TypeName, TypeName>(20);
        propertyAccessByType.put(TypeName.BOOLEAN, ClassName.get(BooleanPropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Boolean.class), ClassName.get(BooleanPropertyAccess.class));
        propertyAccessByType.put(TypeName.BYTE, ClassName.get(BytePropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Byte.class), ClassName.get(BytePropertyAccess.class));
        propertyAccessByType.put(TypeName.SHORT, ClassName.get(ShortPropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Short.class), ClassName.get(ShortPropertyAccess.class));
        propertyAccessByType.put(TypeName.INT, ClassName.get(IntegerPropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Integer.class), ClassName.get(IntegerPropertyAccess.class));
        propertyAccessByType.put(TypeName.LONG, ClassName.get(LongPropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Long.class), ClassName.get(LongPropertyAccess.class));
        propertyAccessByType.put(TypeName.FLOAT, ClassName.get(FloatPropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Float.class), ClassName.get(FloatPropertyAccess.class));
        propertyAccessByType.put(TypeName.DOUBLE, ClassName.get(DoublePropertyAccess.class));
        propertyAccessByType.put(ClassName.get(Double.class), ClassName.get(DoublePropertyAccess.class));
        propertyAccessByType.put(ClassName.get(String.class), ClassName.get(StringPropertyAccess.class));

        PROPERTY_ACCESS_BY_TYPE = Map.copyOf(propertyAccessByType);
    }

    final E entity;

    BaseEntityClassBuilder(final E entity, final String date)
    {
        super(TypeSpec.classBuilder(entity.baseClass), ClassName.get(entity.packageName, entity.identifier), date);
        this.entity = entity;
    }

    @Override void buildTypeContent()
    {
        defineModifiers();
        defineSuperClass();
        defineLogger();

        defineProperties();
        defineAttributes();

        defineConstructor();

        defineOverridenMethods();

        extraDefinitions();
    }

    abstract void defineModifiers();

    void defineSuperClass() {this.builder.superclass(superClass());}

    abstract TypeName superClass();

    void defineProperties()
    {
        for (final MetaProperty metaProperty : this.entity.properties) {
            getPropertyGenerator(metaProperty).defineAt(this.builder);
        }
    }

    void defineAttributes()
    {
        if (this.entity.hasProperties()) {
            this.builder.addField(FieldSpec.builder(propertiesListType(), "ENTITY_PROPERTIES", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                                           .initializer("$T.of($L)", List.class, getEntityProperties())
                                           .build());
        }
    }

    String getEntityProperties()
    {
        final var values = this.entity.propertyNames();

        if (values.isEmpty()) {
            return "";
        }

        if (1 == values.size()) {
            return values.get(0);
        }

        final var result = new StringBuilder();
        final var i = values.iterator();
        result.append(i.next());
        while (i.hasNext()) {
            result.append(", ").append(i.next());
        }
        return result.toString();
    }

    @Override protected void buildConstructor(final MethodSpec.Builder constructor)
    {
        constructor.addStatement("super(LOGGER)");
    }

    @Override void defineOverridenMethods()
    {
        this.builder.addMethod(getProperties());
        this.builder.addMethod(hasPropertyNamed());
        this.builder.addMethod(getPropertyNamed());
        this.builder.addMethod(variableName());
    }

    MethodSpec getProperties()
    {
        final var result = this.entity.hasProperties() ? "ENTITY_PROPERTIES" : "List.of()";
        return methodBuilder("getProperties")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(propertiesListType())
            .addStatement("return $L", result)
            .build();
    }

    MethodSpec hasPropertyNamed()
    {
        final var method = methodBuilder("hasPropertyNamed")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(TypeName.BOOLEAN)
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("$T.requireNonNull(name, $S)", Objects.class, "name is required");

        if (this.entity.hasProperties()) {
            final var properties = this.entity.propertyNames();
            if (1 == properties.size()) {
                method.addStatement("return $S.equals(name)", properties.get(0));
            } else {
                method.beginControlFlow("switch (name)");
                for (final var property : properties) {
                    method.addCode("case $S:\n", property);
                }
                method.addStatement("$>return true$<")
                      .addCode("default:\n")
                      .addStatement("$>return false$<")
                      .endControlFlow();
            }
        } else {
            method.addStatement("return false");
        }
        return method.build();
    }

    MethodSpec getPropertyNamed()
    {
        final var method = methodBuilder("getPropertyNamed")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(propertyType())
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("$T.requireNonNull(name, $S)", Objects.class, "name is required");

        if (this.entity.hasProperties()) {
            final var properties = this.entity.propertyNames();
            final var entityClassName = ClassName.get(this.entity.packageName, this.entity.identifier);
            if (1 == properties.size()) {
                method.beginControlFlow("if ($S.equals(name))", properties.get(0))
                      .addStatement("return $T.$L", entityClassName, properties.get(0))
                      .endControlFlow();
            } else {
                method.beginControlFlow("switch (name)");
                for (final var property : properties) {
                    method.addCode("case $S:\n", property)
                          .addStatement("$>return $T.$L$<", entityClassName, property);
                }
                method.endControlFlow();
            }
        }
        method.addStatement("throw new IllegalArgumentException(\"no property named '\" + name + \"'is known.\")");
        return method.build();
    }

    private MethodSpec variableName()
    {
        return methodBuilder("variableName")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PROTECTED)
            .returns(String.class)
            .addStatement("return $T.INSTANCE.nextName($S)",
                          ClassName.get(Entity.class.getPackageName(), Entity.class.getSimpleName(), "VariableNameGenerator"),
                          this.entity.variable)
            .build();
    }

    protected void extraDefinitions() {/*nothing to do*/}

    EntityPropertyGenerator getPropertyGenerator(final MetaProperty field) {return new EntityPropertyGenerator(field, this.className);}

    private ParameterizedTypeName propertiesListType()
    {
        return ParameterizedTypeName.get(ClassName.get(List.class), propertyType());
    }

    final ParameterizedTypeName propertyType()
    {
        return ParameterizedTypeName.get(ClassName.get(Property.class),
                                         ClassName.get(this.entity.packageName, this.entity.identifier),
                                         WildcardTypeName.subtypeOf(Object.class));
    }

    final TypeName typedAccessPropertyFor(final MetaProperty metaProperty) {return PROPERTY_ACCESS_BY_TYPE.get(metaProperty.type);}

    final ClassName variableClassName() {return ClassName.get(this.entity.packageName, this.entity.identifier, "Variable");}
}

