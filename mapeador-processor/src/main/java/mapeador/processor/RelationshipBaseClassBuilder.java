/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import mapeador.Name;
import mapeador.Types;
import mapeador.cypher.CypherException;
import mapeador.cypher.PropertyAccess;
import mapeador.cypher.RelationshipVariable;

import javax.lang.model.element.Modifier;
import java.util.Objects;

import static com.squareup.javapoet.MethodSpec.methodBuilder;

final class RelationshipBaseClassBuilder extends BaseEntityClassBuilder<MetaRelationship>
{
    private final CodeBlock relationshipType;

    RelationshipBaseClassBuilder(final MetaRelationship node, final String date)
    {
        super(node, date);
        this.relationshipType = TypesRegistry.get().getTypeDefinition(this.entity.type);
    }

    @Override void defineModifiers()
    {
        if (this.entity.isImplicit) {
            this.builder.addModifiers(Modifier.PUBLIC, Modifier.FINAL);
        } else {
            this.builder.addModifiers(Modifier.ABSTRACT);
        }
    }

    @Override TypeName superClass() {return ParameterizedTypeName.get(RELATIONSHIP, this.entity.fromNode, this.entity.toNode, this.entity.type());}

    @Override protected void buildConstructor(final MethodSpec.Builder constructor)
    {
        super.buildConstructor(constructor);
        if (this.entity.isImplicit) {
            constructor.addModifiers(Modifier.PUBLIC);
        }
    }

    @Override void defineOverridenMethods()
    {
        this.builder.addMethod(fromNode());
        this.builder.addMethod(toNode());
        this.builder.addMethod(getRelationTypes());
        super.defineOverridenMethods();
        this.builder.addMethod(variable());
        this.builder.addMethod(relation());
    }

    MethodSpec getRelationTypes()
    {
        return methodBuilder("getRelationTypes")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(Types.class)
            .addStatement("return $L", this.relationshipType)
            .build();
    }

    MethodSpec fromNode()
    {
        return methodBuilder("fromNode")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(this.entity.fromNode)
            .addStatement("return $T.$L", NodesRegistry.get().getRegistryType(), this.entity.fromNode.simpleName())
            .build();
    }

    MethodSpec toNode()
    {
        return methodBuilder("toNode")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(this.entity.toNode)
            .addStatement("return $T.$L", NodesRegistry.get().getRegistryType(), this.entity.toNode.simpleName())
            .build();
    }

    private MethodSpec variable()
    {
        final var returnClass = this.entity.getVariableClassName();
        return methodBuilder("variable")
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(returnClass)
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("return new $T($T.at(name), $T.empty())", returnClass, Name.class, Types.class)
            .build();
    }

    MethodSpec relation()
    {
        final var returnClass = this.entity.getVariableClassName();
        return methodBuilder("relation")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(returnClass)
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("return new $T($T.at(name), $L)", returnClass, Name.class, this.relationshipType)
            .build();
    }

    @Override protected void extraDefinitions()
    {
        super.extraDefinitions();
        if (this.entity.hasProperties()) {
            final var variable = TypeSpec.classBuilder("Variable")
                                         .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                                         .superclass(RelationshipVariable.class);
            for (final MetaProperty metaProperty : this.entity.properties) {
                final var type = typedAccessPropertyFor(metaProperty);
                variable.addField(FieldSpec.builder(type, metaProperty.identifier, Modifier.PUBLIC, Modifier.FINAL)
                                           .initializer("new $T(this, $S)", type, metaProperty.propertyName)
                                           .build());
            }
            variable.addMethod(MethodSpec.constructorBuilder()
                                         .addModifiers(Modifier.PRIVATE)
                                         .addParameter(String.class, "name", Modifier.FINAL)
                                         .addParameter(Types.class, "type", Modifier.FINAL)
                                         .addStatement("super(name, type)")
                                         .build())
                    .addMethod(MethodSpec.methodBuilder("group")
                                         .addAnnotation(Override.class)
                                         .addModifiers(Modifier.PUBLIC)
                                         .returns(variableClassName())
                                         .addStatement("return this")
                                         .build())
                    .addMethod(MethodSpec.methodBuilder("sortOperand")
                                         .addAnnotation(Override.class)
                                         .addModifiers(Modifier.PUBLIC)
                                         .returns(variableClassName())
                                         .addStatement("return this")
                                         .build());

            final var get = methodBuilder("get")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(PropertyAccess.Static.class)
                .addParameter(String.class, "name", Modifier.FINAL)
                .addStatement("$T.requireNonNull(name, $S)", Objects.class, "name is required");

            final var properties = this.entity.propertyNames();
            if (1 == properties.size()) {
                final String property = properties.get(0);
                get.beginControlFlow("if ($S.equals(name))", property)
                   .addStatement("return this.$L", property);
            } else {
                get.beginControlFlow("switch (name)");
                for (final var property : properties) {
                    get.addCode("case $S:\n", property)
                       .addStatement("$>return this.$L$<", property);
                }
            }
            get.endControlFlow();
            get.addStatement("throw new $T(\"no property named '\" + name + \"'is known.\")", CypherException.class);
            variable.addMethod(get.build());
            this.builder.addType(variable.build());
        }
    }
}
