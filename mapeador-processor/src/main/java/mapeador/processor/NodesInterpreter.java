/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import mapeador.node;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import java.util.List;

class NodesInterpreter extends EntityInterpreter<MetaNode>
{
    NodesInterpreter(final ProcessingEnvironment environment)
    {
        this(environment, new EntityPropertyInterpreter(environment));
    }

    NodesInterpreter(final ProcessingEnvironment environment, final EntityPropertyInterpreter fieldInterpreter)
    {
        super(environment, mapeador.node.class, fieldInterpreter);
    }

    @Override MetaNode read(final Element modelElement)
    {
        final var nodeClass = annotatedClass(modelElement);
        final var nodePackage = elementPackage(nodeClass);
        final var record = recordAt(nodeClass);
        final var attrs = getAttributes(record);
        final var properties = propertiesOf(nodeClass, attrs);

        return new MetaNode(nodePackage.getQualifiedName(),
                            nodeClass.getSimpleName(),
                            nodeClass.getQualifiedName(),
                            modifiersOf(nodeClass),
                            baseClassOf(nodeClass),
                            properties,
                            variableOf(nodeClass),
                            labelsOf(nodeClass),
                            nodeIdAt(properties));
    }

    @Override TypeElement recordAt(final TypeElement entity)
    {
        final var record = super.recordAt(entity);
        if (null == record) {
            throw new ModelRecordNotDefined(entity);
        }
        return record;
    }

    @Override MetaEntity.Properties propertiesOf(final TypeElement entity, final List<VariableElement> attributes)
    {
        final var propertiesDeclarations = new java.util.ArrayList<MetaProperty>();
        int declaredIds = 0;
        for (final VariableElement attr : attributes) {
            final MetaProperty property = this.fieldInterpreter.read(attr);
            if (shouldRegisterProperty(property)) {
                propertiesDeclarations.add(property);
                if (property.isId) {
                    if (1 == declaredIds) {
                        throw new ModelIdAlreadyDefined(attr);
                    }
                    declaredIds++;
                }
            }
        }
        if (0 == declaredIds) {
            final Modifier[] modifiers = propertyModifierUnder(entity);
            propertiesDeclarations.add(new MetaProperty(ClassName.get(String.class), elements().getName("id"), modifiers, "id", true));
        }
        propertiesDeclarations.trimToSize();
        return MetaEntity.properties(propertiesDeclarations);
    }

    private boolean shouldRegisterProperty(final MetaProperty property) {return null != property;}

    List<String> labelsOf(final TypeElement nodeClass)
    {
        final var labels = nodeClass.getAnnotation(node.class).labels();
        if (null == labels || 0 == labels.length) {
            final var label = nodeClass.getSimpleName().toString();
            LabelsRegistry.get().registerLabel(label);
            return List.of(label);
        } else {
            for (final var label : labels) {
                LabelsRegistry.get().registerLabel(label);
            }
            return List.of(labels);
        }
    }

    String variableOf(final TypeElement nodeClass)
    {
        final var variable = nodeClass.getAnnotation(node.class).variable();
        if (null == variable || variable.isEmpty() || variable.isBlank()) {
            return nodeClass.getSimpleName().toString().toLowerCase();
        } else {
            return variable.trim();
        }
    }

    MetaProperty nodeIdAt(final MetaEntity.Properties properties)
    {
        for (final var prop : properties) {
            if (prop.isId) {
                return prop;
            }
        }
        throw new IllegalStateException("no id found :-|");
    }
}
