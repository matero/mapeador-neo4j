/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import mapeador.Relationship;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class RelationshipsRegistry extends EntityRegistry<MetaRelationship>
{
    private static final int TO_NODE = 0;
    private static final int RELATIONSHIP = 1;

    final Map<String, MetaRelationship> byType;
    final Map<String, List<MetaRelationship>> byPackage;
    final Map<String, MetaRelationship> byCannonicalName;
    private Set<VariableElement> implicitRelationships;

    RelationshipsRegistry() {this(new HashMap<>(), new HashMap<>(), new HashMap<>());}

    RelationshipsRegistry(
        final Map<String, MetaRelationship> byType,
        final Map<String, List<MetaRelationship>> byPackage,
        final Map<String, MetaRelationship> byCannonicalClassName)
    {
        super("relationships", ClassName.get(Relationship.class));
        this.byType = byType;
        this.byPackage = byPackage;
        this.byCannonicalName = byCannonicalClassName;
    }

    Set<String> getPackages() {return Collections.unmodifiableSet(this.byPackage.keySet());}

    List<MetaRelationship> atPackage(final String packageName) {return this.byPackage.get(packageName);}

    @Override Collection<MetaRelationship> all() {return Collections.unmodifiableCollection(this.byCannonicalName.values());}

    int size() {return this.byCannonicalName.size(); }

    @Override void register(final MetaRelationship relation)
    {
        TypesRegistry.get().registerType(relation.type);
        this.byType.put(relation.type, relation);
        this.byPackage.putIfAbsent(relation.packageName, new ArrayList<>());
        this.byPackage.get(relation.packageName).add(relation);
        this.byCannonicalName.put(relation.canonicalName, relation);
    }

    static RelationshipsRegistry get() {return RelationshipsRegistry.Unique.INSTANCE;}

    void registerImplicitRelationshipAt(final VariableElement variable)
    {
        if (null == this.implicitRelationships) {
            this.implicitRelationships = new java.util.HashSet<>();
        }
        this.implicitRelationships.add(variable);
    }

    @Override void setupDone(final ProcessingEnvironment processingEnvironment)
    {
        if (null != this.implicitRelationships && !this.implicitRelationships.isEmpty()) {
            processImplicitRelations(processingEnvironment.getElementUtils(), processingEnvironment.getTypeUtils());
        }
    }

    @Override void codeGenerationDone()
    {
        this.byCannonicalName.clear();
        this.byType.clear();
        this.byPackage.clear();
    }

    void processImplicitRelations(final Elements elements, final Types types)
    {
        for (final var implicitRelation : this.implicitRelationships) {
            final var type = (DeclaredType) implicitRelation.asType();
            final var record = implicitRelation.getEnclosingElement();
            final var fromNode = (TypeElement) record.getEnclosingElement();
            final var relationship = type.getTypeArguments().get(RELATIONSHIP);
            final var relationElement = types.asElement(relationship);
            final Name relationPackage;
            {
                final var pkg = elements.getPackageOf(relationElement);
                if (pkg.isUnnamed() || !pkg.toString().isEmpty()) {
                    relationPackage = pkg.getQualifiedName();
                } else {
                    relationPackage = elements.getPackageOf(fromNode).getQualifiedName();
                }
            }

            final var relationName = relationElement.getSimpleName();
            final var relationType = relationName.toString();
            final var canonicalName = relationPackage.toString() + '.' + relationType;

            if (!this.byCannonicalName.containsKey(canonicalName)) {
                final var toNode = (TypeElement) types.asElement(type.getTypeArguments().get(TO_NODE));
                final var relationDefaultVariableName = relationType.toLowerCase();

                register(new MetaRelationship(relationPackage,
                                              relationName,
                                              elements.getName(canonicalName),
                                              new Modifier[]{Modifier.PUBLIC, Modifier.FINAL},
                                              relationType,
                                              MetaEntity.Properties.none(),
                                              relationDefaultVariableName,
                                              relationType,
                                              ClassName.get(fromNode),
                                              ClassName.get(toNode),
                                              MetaRelationship.Kind.IMPLICIT));
            }
        }
        this.implicitRelationships = null;
    }

    private static final class Unique
    {
        private static final RelationshipsRegistry INSTANCE = new RelationshipsRegistry();
    }

    @Override public boolean hasElementsRegistered()
    {
        return !this.byPackage.isEmpty();
    }
}
