/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import mapeador.GraphNode;
import mapeador.Node;
import mapeador.relation;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.util.List;

class RelationshipsInterpreter extends EntityInterpreter<MetaRelationship>
{
    RelationshipsInterpreter(final ProcessingEnvironment environment)
    {
        this(environment, new EntityPropertyInterpreter(environment));
    }

    RelationshipsInterpreter(final ProcessingEnvironment environment, final EntityPropertyInterpreter fieldInterpreter)
    {
        super(environment, relation.class, fieldInterpreter);
    }

    @Override MetaRelationship read(final Element entity)
    {
        final var relationClass = annotatedClass(entity);
        final var relationPackage = elementPackage(relationClass);
        final var record = recordAt(relationClass);
        final var attrs = getAttributes(record);
        final var properties = propertiesOf(relationClass, attrs);
        final var relation = getAnnotationMirror(entity, mapeador.relation.class);

        return new MetaRelationship(relationPackage.getQualifiedName(),
                                    relationClass.getSimpleName(),
                                    relationClass.getQualifiedName(),
                                    modifiersOf(relationClass),
                                    baseClassOf(relationClass),
                                    properties,
                                    variableOf(relationClass),
                                    typeOf(relationClass),
                                    fromNodeAt(relationClass, relation),
                                    toNodeAt(relationClass, relation),
                                    MetaRelationship.Kind.DECLARED);
    }

    private ClassName fromNodeAt(final TypeElement entity, final AnnotationMirror relation)
    {
        return getClassNameFrom(entity, relation, "to");
    }

    private ClassName toNodeAt(final TypeElement entity, final AnnotationMirror relation)
    {
        return getClassNameFrom(entity, relation, "to");
    }

    private ClassName getClassNameFrom(final TypeElement entity, final AnnotationMirror relation, final String property)
    {
        for (final var entry : relation.getElementValues().entrySet()) {
            final var key = entry.getKey().getSimpleName().toString();
            if (property.equals(key)) {
                final var typeMirror = (TypeMirror) entry.getValue().getValue();
                if (isntDefined(typeMirror)) {
                    throw new ModelException(entity, "'" + property + "' is required to define the relationship");
                }
                return ClassName.bestGuess(typeMirror.toString());
            }
        }
        throw new ModelException(entity, "'" + property + "' is required to define the relationship");
    }

    private boolean isntDefined(final TypeMirror nodeType)
    {
        return null == nodeType || types().isSameType(getGraphNodeType(), nodeType)|| types().isSameType(getNodeType(), nodeType);
    }

    private TypeMirror getGraphNodeType()
    {
        return elements().getTypeElement(GraphNode.class.getCanonicalName()).asType();
    }

    private TypeMirror getNodeType()
    {
        return elements().getTypeElement(Node.class.getCanonicalName()).asType();
    }

    @Override MetaEntity.Properties propertiesOf(final TypeElement entity, final List<VariableElement> attributes)
    {
        final var propertiesDeclarations = new java.util.ArrayList<MetaProperty>();
        for (final VariableElement attr : attributes) {
            final MetaProperty property = this.fieldInterpreter.read(attr);
            if (property.isId) {
                throw new ModelException(entity, "relations cant define an @id");
            }
            propertiesDeclarations.add(property);
        }
        propertiesDeclarations.trimToSize();
        return MetaEntity.properties(propertiesDeclarations);
    }

    String typeOf(final TypeElement entity)
    {
        final var type = entity.getAnnotation(relation.class).type();
        if (null == type || type.isEmpty() || type.isBlank()) {
            final var defaultType = entity.getSimpleName().toString();
            return defaultType;
        } else {
            return type;
        }
    }

    String variableOf(final TypeElement nodeClass)
    {
        final var variable = nodeClass.getAnnotation(relation.class).variable();
        if (null == variable || variable.isEmpty() || variable.isBlank()) {
            return nodeClass.getSimpleName().toString().toLowerCase();
        } else {
            return variable.trim();
        }
    }
}
