/*
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MetaEntity extends MetaData
{
    final String packageName;
    final String canonicalName;
    final String baseClass;
    final Properties properties;
    final String variable;

    public MetaEntity(
        final Name packageName,
        final Name name,
        final Name canonicalName,
        final Modifier[] modifiers,
        final String baseClass,
        final Properties properties,
        final String variable)
    {
        super(name, modifiers);
        this.packageName = packageName.toString();
        this.canonicalName = canonicalName.toString();
        this.baseClass = baseClass;
        this.properties = properties;
        this.variable = variable;
    }

    public TypeName type() {return ClassName.get(this.packageName, this.identifier);}

    boolean hasProperties() {return !this.properties.isEmpty();}

    List<String> propertyNames()
    {
        final var result = new java.util.ArrayList<String>(this.properties.size());
        for (final MetaProperty field : this.properties) {
            result.add(field.identifier);
        }
        return result;
    }

    public Modifier[] getBuilderModifiers()
    {
        if (null == this.visibility) {
            return new Modifier[]{Modifier.FINAL};
        } else {
            return new Modifier[]{this.visibility, Modifier.FINAL};
        }
    }

    public Modifier[] getStaticBuilderModifiers()
    {
        if (null == this.visibility) {
            return new Modifier[]{Modifier.FINAL, Modifier.STATIC};
        } else {
            return new Modifier[]{this.visibility, Modifier.STATIC, Modifier.FINAL};
        }
    }

    static Properties properties(final Collection<MetaProperty> properties)
    {
        if (null == properties || properties.isEmpty()) {
            return Properties.none();
        } else {
            return new Properties(properties);
        }
    }

    static final class Properties implements Iterable<MetaProperty>
    {
        static Properties none() {return Empty.INSTANCE;}

        private static final class Empty
        {
            private static final Properties INSTANCE = new Properties(List.of());
        }

        private final List<MetaProperty> data;

        private Properties(final Collection<MetaProperty> fields) {this.data = List.copyOf(fields);}

        @Override public Iterator<MetaProperty> iterator() {return this.data.iterator();}

        boolean isEmpty() {return this.data.isEmpty();}

        int size() {return this.data.size();}
    }
}
