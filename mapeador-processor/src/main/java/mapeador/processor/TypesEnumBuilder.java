/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import mapeador.Type;

import javax.lang.model.element.Modifier;
import java.util.Iterator;
import java.util.List;

final class TypesEnumBuilder extends ClassifierEnumBuilder
{
    TypesEnumBuilder(final String date) {this(TypesRegistry.get(), date);}

    TypesEnumBuilder(final TypesRegistry registry, final String date)
    {
        super(TypeSpec.enumBuilder(registry.getRegistryType()), registry, date);
        this.builder.addModifiers(Modifier.PUBLIC);
        this.builder.addSuperinterface(mapeador.Type.class);
    }

    @Override void declareAttributes()
    {
        super.declareAttributes();
        this.builder.addField(ParameterizedTypeName.get(List.class, mapeador.Type.class), "types", Modifier.PRIVATE, Modifier.FINAL);
    }

    @Override protected void buildConstructor(final MethodSpec.Builder constructor)
    {
        super.buildConstructor(constructor);
        constructor.addStatement("this.types = $T.of(this)", List.class);
    }

    @Override void defineOverridenMethods()
    {
        super.defineOverridenMethods();
        this.builder.addMethod(getTypes());
        this.builder.addMethod(iterator());
    }

    MethodSpec getTypes()
    {
        return MethodSpec.methodBuilder("getTypes")
                         .returns(ParameterizedTypeName.get(List.class, Type.class))
                         .addAnnotation(Override.class)
                         .addModifiers(Modifier.PUBLIC)
                         .addStatement("return this.types")
                         .build();
    }

    MethodSpec iterator()
    {
        return MethodSpec.methodBuilder("iterator")
                         .returns(Iterator.class)
                         .addAnnotation(Override.class)
                         .addModifiers(Modifier.PUBLIC)
                         .addStatement("return this.types.iterator()")
                         .build();
    }
}
