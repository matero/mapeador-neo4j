/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import mapeador.IntegerProperty;
import mapeador.StringProperty;

import java.util.Map;

final class EntityPropertyGenerator extends AttributeGenerator
{
    private static final Map<TypeName, ClassName> PROPERTY_BY_TYPE = Map.of(
        TypeName.INT, ClassName.get(IntegerProperty.class),
        ClassName.get(Integer.class), ClassName.get(IntegerProperty.class),
        ClassName.get(String.class), ClassName.get(StringProperty.class));

    private final TypeName propertyClass;
    final MetaProperty property;

    EntityPropertyGenerator(final MetaProperty property, final ClassName entityClass)
    {
        super(entityClass);
        this.propertyClass = getPropertyClass(property);
        this.property = property;
    }

    @Override String canonicalName() {return this.property.canonicalNameAt(this.entityClass.toString());}

    @Override void defineAt(final TypeSpec.Builder entity)
    {
        entity.addField(FieldSpec.builder(this.propertyClass, this.property.identifier, this.property.getDefinitionModifiers())
                                 .initializer("$T.of($T.class, $S, $S, $S)",
                                              getBasePropertyClass(),
                                              this.entityClass,
                                              this.property.identifier,
                                              this.property.propertyName,
                                              canonicalName())
                                 .build());
    }

    private TypeName getBasePropertyClass()
    {
        if (this.propertyClass instanceof ParameterizedTypeName) {
            return ((ParameterizedTypeName) this.propertyClass).rawType;
        } else {
            return this.propertyClass;
        }
    }

    TypeName getPropertyClass(final MetaProperty field)
    {
        if (!PROPERTY_BY_TYPE.containsKey(field.type)) {
            throw new ModelException("No Property can manage type '" + field.type + "' used at '" + field.identifier + "'.");
        }
        return ParameterizedTypeName.get(PROPERTY_BY_TYPE.get(field.type), this.entityClass);
    }
}
