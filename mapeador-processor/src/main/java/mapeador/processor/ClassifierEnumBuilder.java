/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import javax.lang.model.element.Modifier;

class ClassifierEnumBuilder extends RegistryClassBuilder<MetaClassifier>
{
    ClassifierEnumBuilder(final TypeSpec.Builder builder, final Registry<MetaClassifier> registry, final String date)
    {
        super(builder, registry, date);
    }

    @Override void defineElements()
    {
        for (final var elem : this.registry.all()) {
            this.builder.addEnumConstant(elem.identifier, TypeSpec.anonymousClassBuilder("$S", elem.cypher).build());
        }
    }

    @Override void declareAttributes()
    {
        this.builder.addField(String.class, "cypher", Modifier.PRIVATE, Modifier.FINAL);
    }

    @Override protected void buildConstructor(final MethodSpec.Builder constructor)
    {
        constructor.addParameter(String.class, "cypher", Modifier.FINAL);
        constructor.addStatement("this.cypher = cypher");
    }

    @Override void defineOverridenMethods()
    {
        this.builder.addMethod(cypher());
    }

    MethodSpec cypher()
    {
        return MethodSpec.methodBuilder("cypher")
                         .returns(String.class)
                         .addAnnotation(Override.class)
                         .addModifiers(Modifier.PUBLIC)
                         .addStatement("return this.cypher")
                         .build();
    }
}
