/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import mapeador.Type;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

class TypesRegistry extends Registry<MetaClassifier>
{
    private final Map<String, MetaClassifier> types = new java.util.HashMap<>();

    TypesRegistry() {this("types");}

    TypesRegistry(final String generatedEnumName) {super(generatedEnumName, ClassName.get(Type.class));}

    @Override Collection<MetaClassifier> all() {return Collections.unmodifiableCollection(this.types.values());}

    CodeBlock getTypeDefinition(final String name)
    {
        final var type = registerType(name);
        return CodeBlock.of("$T.$L", getRegistryType(), type.identifier);
    }

    MetaClassifier registerType(final String name) {return this.types.computeIfAbsent(name, MetaClassifier::of);}

    public static TypesRegistry get() {return Unique.INSTANCE;}

    private static final class Unique
    {
        private static final TypesRegistry INSTANCE = new TypesRegistry();
    }
}
