/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.processing.Generated;
import javax.lang.model.element.Modifier;

abstract class ClassBuilder
{
    final TypeSpec.Builder builder;
    final ClassName className;
    final String date;

    public ClassBuilder(final TypeSpec.Builder builder, final ClassName className, final String date)
    {
        this.builder = builder;
        this.className = className;
        this.date = date;
    }

    final JavaFile build()
    {
        return JavaFile.builder(this.className.packageName(), buildType())
                       .skipJavaLangImports(true)
                       .build();
    }

    final TypeSpec buildType()
    {
        defineGenerated();
        buildTypeContent();
        return this.builder.build();
    }

    abstract void buildTypeContent();

    final void defineGenerated()
    {
        this.builder.addAnnotation(
            AnnotationSpec.builder(Generated.class)
                          .addMember("value", "$S", "mapeador-neo4j")
                          .addMember("date", "$S", this.date)
                          .build());
    }

    final void defineLogger()
    {
        this.builder.addField(
            FieldSpec.builder(Logger.class, "LOGGER", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                     .initializer("$T.getLogger($S)", LoggerFactory.class, this.className.canonicalName())
                     .build());
    }

    final void defineConstructor()
    {
        final var constructor = MethodSpec.constructorBuilder();
        buildConstructor(constructor);
        this.builder.addMethod(constructor.build());
    }

    protected abstract void buildConstructor(MethodSpec.Builder constructor);

    abstract void defineOverridenMethods();
}
