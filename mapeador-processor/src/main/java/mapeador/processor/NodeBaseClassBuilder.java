/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import mapeador.Labels;
import mapeador.Name;
import mapeador.cypher.CypherException;
import mapeador.cypher.NodeVariable;
import mapeador.cypher.PropertyAccess;

import javax.lang.model.element.Modifier;
import java.util.Objects;

import static com.squareup.javapoet.MethodSpec.methodBuilder;

final class NodeBaseClassBuilder extends BaseEntityClassBuilder<MetaNode>
{
    private final CodeBlock labelsDefinition;
    private TypeName idPropertyClassName;

    NodeBaseClassBuilder(final MetaNode node, final String date)
    {
        super(node, date);
        this.labelsDefinition = LabelsRegistry.get().getLabelsDefinition(this.entity.labels);
    }

    @Override void defineModifiers() {this.builder.addModifiers(Modifier.ABSTRACT);}

    @Override TypeName superClass() {return ParameterizedTypeName.get(NODE, this.entity.type());}

    @Override void defineAttributes()
    {
        super.defineAttributes();
        if (this.entity.isMultiLabel()) {
            this.builder.addField(FieldSpec.builder(mapeador.Labels.class, "LABELS", Modifier.PRIVATE, Modifier.STATIC, Modifier.FINAL)
                                           .initializer("$L", this.labelsDefinition)
                                           .build());
        }
    }

    @Override EntityPropertyGenerator getPropertyGenerator(final MetaProperty property)
    {
        final var entityPropertyGenerator = super.getPropertyGenerator(property);
        if (property.isId) {
            this.idPropertyClassName = entityPropertyGenerator.getPropertyClass(property);
        }
        return entityPropertyGenerator;
    }

    @Override void defineOverridenMethods()
    {
        this.builder.addMethod(getIdProperty());
        this.builder.addMethod(getNodeLabels());
        super.defineOverridenMethods();
        this.builder.addMethod(variable());
        this.builder.addMethod(node());
    }

    MethodSpec getIdProperty()
    {
        return methodBuilder("getIdProperty")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(this.idPropertyClassName)
            .addStatement("return $L", this.entity.id.propertyName)
            .build();
    }

    MethodSpec getNodeLabels()
    {
        final var getNodeLabels = methodBuilder("getNodeLabels")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(Labels.class);
        if (this.entity.isMultiLabel()) {
            getNodeLabels.addStatement("return LABELS");
        } else {
            getNodeLabels.addStatement("return $L", this.labelsDefinition);
        }
        return getNodeLabels.build();
    }

    private MethodSpec variable()
    {
        final var returnClass = ClassName.get(this.entity.packageName, this.entity.identifier, "Variable");
        return methodBuilder("variable")
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(returnClass)
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("return new $T($T.at(name))", returnClass, Name.class)
            .build();
    }

    MethodSpec node()
    {
        final var node = methodBuilder("node")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .returns(ClassName.get(this.entity.packageName, this.entity.identifier, "Variable"))
            .addParameter(String.class, "name", Modifier.FINAL);
        if (this.entity.isMultiLabel()) {
            node.addStatement("return new Variable($T.at(name), LABELS)", Name.class);
        } else {
            node.addStatement("return new Variable($T.at(name), $L)", Name.class, this.labelsDefinition);
        }
        return node.build();
    }

    @Override protected void extraDefinitions()
    {
        super.extraDefinitions();

        final var variable = TypeSpec.classBuilder("Variable")
                                     .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL)
                                     .superclass(NodeVariable.class);
        for (final MetaProperty metaProperty : this.entity.properties) {
            final var type = typedAccessPropertyFor(metaProperty);
            variable.addField(FieldSpec.builder(type, metaProperty.identifier, Modifier.PUBLIC, Modifier.FINAL)
                                       .initializer("new $T(this, $S)", type, metaProperty.propertyName)
                                       .build());
        }
        variable.addMethod(MethodSpec.constructorBuilder()
                                     .addModifiers(Modifier.PRIVATE)
                                     .addParameter(String.class, "name", Modifier.FINAL)
                                     .addStatement("super(name, $T.empty())", Labels.class)
                                     .build())
                .addMethod(MethodSpec.constructorBuilder()
                                     .addModifiers(Modifier.PRIVATE)
                                     .addParameter(String.class, "name", Modifier.FINAL)
                                     .addParameter(Labels.class, "labels", Modifier.FINAL)
                                     .addStatement("super(name, labels)")
                                     .build())
                .addMethod(MethodSpec.methodBuilder("group")
                                     .addAnnotation(Override.class)
                                     .addModifiers(Modifier.PUBLIC)
                                     .returns(variableClassName())
                                     .addStatement("return this")
                                     .build())
                .addMethod(MethodSpec.methodBuilder("sortOperand")
                                     .addAnnotation(Override.class)
                                     .addModifiers(Modifier.PUBLIC)
                                     .returns(variableClassName())
                                     .addStatement("return this")
                                     .build());

        final var get = methodBuilder("get")
            .addAnnotation(Override.class)
            .addModifiers(Modifier.PUBLIC)
            .returns(PropertyAccess.Static.class)
            .addParameter(String.class, "name", Modifier.FINAL)
            .addStatement("$T.requireNonNull(name, $S)", Objects.class, "name is required");

        if (this.entity.hasProperties()) {
            final var properties = this.entity.propertyNames();
            if (1 == properties.size()) {
                get.beginControlFlow("if ($S.equals(name))", properties.get(0))
                   .addStatement("return this.$L", properties.get(0))
                   .endControlFlow();
            } else {
                get.beginControlFlow("switch (name)");
                for (final var property : this.entity.properties) {
                    get.addCode("case $S:\n", property.identifier)
                       .addStatement("$>return this.$L$<", property.identifier);
                }
                get.endControlFlow();
            }
        }
        get.addStatement("throw new $T(\"no property named '\" + name + \"'is known.\")", CypherException.class);
        variable.addMethod(get.build());
        this.builder.addType(variable.build());
    }
}

