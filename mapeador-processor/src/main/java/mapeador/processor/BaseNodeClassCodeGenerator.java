/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import com.squareup.javapoet.JavaFile;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

enum BaseNodeClassCodeGenerator implements CodeGenerator
{
    INSTANCE;

    @Override public String phaseName() {return "'base models'";}

    @Override public List<JavaFile> generateCode(final String date, final ProcessingEnvironment processingEnvironment) {
        final var registry = NodesRegistry.get();
        registry.setupDone(processingEnvironment);
        if (registry.hasElementsRegistered()) {
            final var javaFiles = generateCode(registry.all(), date);
            registry.codeGenerationDone();
            return javaFiles;
        } else {
            return List.of();
        }
    }

    public List<JavaFile> generateCode(final Collection<MetaNode> metadata, final String date)
    {
        final var generatedJavaFiles = new ArrayList<JavaFile>(metadata.size() + 2);
        generatedJavaFiles.add(labelsJavaFileFor(date));
        for (final var node : metadata) {
            generatedJavaFiles.add(baseModelJavaFileFor(node, date));
        }
        generatedJavaFiles.add(nodesJavaFileFor(date));
        return generatedJavaFiles;
    }

    JavaFile labelsJavaFileFor(final String date)
    {
        final var labelsBuilder = new LabelsEnumBuilder(date);
        return labelsBuilder.build();
    }

    JavaFile baseModelJavaFileFor(final MetaNode node, final String date) {return new NodeBaseClassBuilder(node, date).build();}

    JavaFile nodesJavaFileFor(final String date)
    {
        final var nodesBuilder = new NodesRegistryClassBuilder(date);
        return nodesBuilder.build();
    }
}
