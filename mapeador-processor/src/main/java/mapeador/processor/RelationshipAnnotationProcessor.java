/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.time.LocalDateTime;
import java.util.Set;

@SupportedAnnotationTypes("mapeador.relation")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class RelationshipAnnotationProcessor extends MapeadorAnnotationProcessor
{
    public RelationshipAnnotationProcessor() {this(LocalDateTime.now());}

    RelationshipAnnotationProcessor(final LocalDateTime today) {super(today, mapeador.relation.class);}

    @Override void readMetadataFor(final Set<? extends Element> annotatedElements)
    {
        final var relationships = RelationshipsRegistry.get();
        final var interpreter = new RelationshipsInterpreter(this.processingEnv);

        for (final var annotated : annotatedElements) {
            final var relationship = (TypeElement) annotated;
            final var qualifiedName = relationship.getQualifiedName().toString();

            info("reading meta-data of [%s].", qualifiedName);
            relationships.register(interpreter.read(annotated));
            info("meta-data of [%s] read.", qualifiedName);
        }
    }

    @Override void generateCode() {generateCode(BaseRelationshipClassCodeGenerator.INSTANCE);}
}
