/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.processor;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.util.Set;

abstract class MapeadorAnnotationProcessor extends AnnotationProcessor
{
    final Class<? extends Annotation> annotation;
    final String annotationName;

    MapeadorAnnotationProcessor(final LocalDateTime today, final Class<? extends Annotation> annotation)
    {
        super(today);
        this.annotation = annotation;
        this.annotationName = '@' + annotation.getCanonicalName();
    }

    @Override
    public boolean process(final Set<? extends TypeElement> annotations, final RoundEnvironment roundEnvironment)
    {
        info("START " + this.annotationName + " processing");
        final Set<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(this.annotation);
        if (annotatedElements.isEmpty()) {
            info("DONE, no classes annotated  with " + this.annotationName + " to process");
        } else {
            try {
                readMetadataFor(annotatedElements);
            } catch (final ModelException e) {
                error(e);
                info("HALT processing %d classes annotated with " + this.annotationName, annotatedElements.size());
                return true;
            } catch (final RuntimeException e) {
                error(e);
                info("HALT processing %d classes annotated with " + this.annotationName, annotatedElements.size());
                return true;
            }
            generateCode();
            info("DONE processing %d classes annotated with " + this.annotationName, annotatedElements.size());
        }
        return true;
    }

    abstract void generateCode();

}
