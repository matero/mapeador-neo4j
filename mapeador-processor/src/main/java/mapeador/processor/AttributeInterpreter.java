/*
 * The MIT License
 *
 * Copyright 2020-2020 matero@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package mapeador.processor;

import mapeador.node;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

abstract class AttributeInterpreter extends Interpreter
{
    private final TypeMirror stringClass;
    private final TypeMirror listClass;
    private final List<TypeMirror> relationTypes;
    private final List<TypeMirror> propertyTypes;
    private final List<TypeMirror> supportedTypes;

    private List<String> supportedTypeNames;

    AttributeInterpreter(final ProcessingEnvironment environment)
    {
        super(environment);
        this.listClass = getTypeMirror(List.class);
        this.stringClass = getTypeMirror(String.class);
        this.relationTypes = buildRelationTypes();
        this.propertyTypes = buildPropertyTypes();
        this.supportedTypes = buildSupportedTypes();
    }

    private List<TypeMirror> buildRelationTypes()
    {
        final var recordClassName = node.Record.class.getCanonicalName();
        return List.of(types().erasure(getTypeMirror(recordClassName + ".HasMany.Outgoing")),
                       types().erasure(getTypeMirror(recordClassName + ".HasMany.Incoming")),
                       types().erasure(getTypeMirror(recordClassName + ".HasOne.Outgoing")),
                       types().erasure(getTypeMirror(recordClassName + ".HasOne.Incoming")));
    }

    private List<TypeMirror> buildPropertyTypes()
    {
        final Class<?>[] propertyClasses = {
            boolean.class, Boolean.class, byte.class, Byte.class, short.class, Short.class, int.class, Integer.class, long.class, Long.class,
            float.class, Float.class, double.class, Double.class, String.class};

        final var types = new java.util.ArrayList<TypeMirror>(propertyClasses.length);
        for (final var c : propertyClasses) {
            types.add(getTypeMirror(c));
        }
        return List.copyOf(types);
    }

    private List<TypeMirror> buildSupportedTypes()
    {
        final var types = new java.util.ArrayList<TypeMirror>(this.propertyTypes.size() + this.relationTypes.size());
        types.addAll(this.propertyTypes);
        types.addAll(this.relationTypes);
        return List.copyOf(types);
    }

    void checkModifiersOf(final VariableElement variable, final Class<? extends Annotation> annotation)
    {
        final Set<Modifier> modifiers = variable.getModifiers();
        if (modifiers.contains(Modifier.STATIC)) {
            throw new ModelException(variable, "only member fields can be annotated with @" + annotation.getCanonicalName());
        }
        if (modifiers.contains(Modifier.TRANSIENT)) {
            throw new ModelException(variable, "transient fields can not be annotated with @" + annotation.getCanonicalName());
        }
        if (modifiers.contains(Modifier.VOLATILE)) {
            throw new ModelException(variable, "volatile fields can not be annotated with @" + annotation.getCanonicalName());
        }
    }

    boolean hasSupportedPropertyType(final VariableElement variable) {return isSupportedPropertyType(variable.asType());}

    boolean isSupportedPropertyType(final TypeMirror type)
    {
        for (final var t : this.propertyTypes) {
            if (isAssignable(type, t)) {
                return true;
            }
        }
        return false;
    }

    public boolean isRelation(final VariableElement attr)
    {
        final var type = types().erasure(attr.asType());
        for (final var t : this.relationTypes) {
            if (types().isSameType(type, types().erasure(t))) {
                return true;
            }
        }
        return false;
    }

    public List<String> getSupportedTypeNames()
    {
        if (null == this.supportedTypeNames) {
            this.supportedTypeNames = new ArrayList<>(this.supportedTypes.size());
            for (final var t : this.supportedTypes) {
                this.supportedTypeNames.add(t.toString());
            }
            this.supportedTypeNames = List.copyOf(this.supportedTypeNames);
        }
        return this.supportedTypeNames;
    }
}
