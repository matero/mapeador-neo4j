/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package mapeador.processor;

import mapeador.Node;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import java.lang.annotation.Annotation;
import java.util.List;

abstract class EntityInterpreter<E extends MetaEntity> extends Interpreter
{
    final EntityPropertyInterpreter fieldInterpreter;
    final TypeMirror recordClass;
    final Class<? extends Annotation> annotation;

    EntityInterpreter(
        final ProcessingEnvironment environment,
        final Class<? extends Annotation> annotation,
        final EntityPropertyInterpreter fieldInterpreter)
    {
        super(environment);
        this.fieldInterpreter = fieldInterpreter;
        this.recordClass = getTypeMirror(annotation.getCanonicalName() + ".Record");
        this.annotation = annotation;
    }

    abstract E read(final Element modelElement);

    abstract MetaEntity.Properties propertiesOf(final TypeElement entity, final List<VariableElement> attributes);

    final TypeElement annotatedClass(final Element element)
        throws IllegalArgumentException
    {
        if (ElementKind.CLASS != element.getKind()) {
            throw new ModelException(element, "only classes can be annotated as @" + this.annotation.getCanonicalName());
        }
        return (TypeElement) element;
    }

    final PackageElement elementPackage(final TypeElement annotatedClass)
        throws IllegalArgumentException
    {
        final Element enclosingElement = annotatedClass.getEnclosingElement();
        if (ElementKind.PACKAGE != enclosingElement.getKind()) {
            throw new IllegalArgumentException("only ROOT classes can be annotated as @" + this.annotation.getCanonicalName());
        }
        return (PackageElement) enclosingElement;
    }

    final String baseClassOf(final TypeElement annotatedElement)
    {
        final TypeMirror superclass = annotatedElement.getSuperclass();
        final TypeElement baseClass = (TypeElement) types().asElement(superclass);

        if (Object.class.getCanonicalName().equals(baseClass.getQualifiedName().toString())) {
            throw new ModelException(annotatedElement, "No base class defined!");
        }

        return baseClass.getSimpleName().toString();
    }

    final List<VariableElement> getAttributes(final TypeElement record) {
        if (null == record) {
            return List.of();
        }
        return ElementFilter.fieldsIn(record.getEnclosedElements());
    }

    TypeElement recordAt(final TypeElement entity)
    {
        TypeElement record = null;
        for (final TypeElement type : innerClassesOf(entity)) {
            if (definesRecord(type)) {
                if (null != record) {
                    throw new ModelRecordAlreadyDefined(type);
                }
                record = type;
            }
        }
        return record;
    }

    final Modifier[] propertyModifierUnder(final TypeElement entity)
    {
        if (isPackagePrivate(entity)) {
            return new Modifier[]{Modifier.FINAL};
        } else {
            return new Modifier[]{visibilityOf(entity), Modifier.FINAL};
        }
    }

    boolean isPackagePrivate(final TypeElement entity) {return null == visibilityOf(entity);}

    final Modifier visibilityOf(final TypeElement entity)
    {
        for (final var m : entity.getModifiers()) {
            switch (m) {
                case PRIVATE:
                case PROTECTED:
                case PUBLIC:
                    return m;
            }
        }
        return null;
    }

    final boolean definesRecord(final TypeElement type)
    {
        if (null == type.getSuperclass()) {
            return false;
        }
        return types().isAssignable(type.asType(), this.recordClass);
    }

    final Iterable<TypeElement> innerClassesOf(final TypeElement entity) {return ElementFilter.typesIn(entity.getEnclosedElements());}


    final AnnotationMirror getAnnotationMirror(final Element entity, final Class<? extends Annotation> annotation)
    {
        return getAnnotationMirror(elements().getTypeElement(entity.asType().toString()), annotation.getCanonicalName());
    }

    final AnnotationMirror getAnnotationMirror(final TypeElement entity, final String annotationName)
    {
        final var nodeType = elements().getTypeElement(Node.class.getCanonicalName()).asType();

        // Get the annotation element from the type element

        final List<? extends AnnotationMirror> annotationMirrors = entity.getAnnotationMirrors();
        for (final var mirror : annotationMirrors) {
            final var annotationQualifiedName = mirror.getAnnotationType().asElement().toString();
            if (annotationName.equals(annotationQualifiedName)) {
                return mirror;
            }
        }
        throw new ModelException(entity, "annotation @" + annotationName + " not found.");
    }
}
