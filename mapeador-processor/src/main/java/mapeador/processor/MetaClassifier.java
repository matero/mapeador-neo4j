/*
 * Copyright 2020-2020 matero@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package mapeador.processor;

import mapeador.Name;

final class MetaClassifier
{
    final String identifier;
    final String cypher;
    final String parameterClass;

    MetaClassifier(final String identifier, final String cypher, final String parameterClass)
    {
        this.identifier = identifier;
        this.cypher = cypher;
        this.parameterClass = parameterClass;
    }

    @Override public boolean equals(final Object o)
    {
        if (this == o) {
            return true;
        }
        if (o instanceof MetaClassifier) {
            final MetaClassifier other = (MetaClassifier) o;
            return this.identifier.equals(other.identifier) && this.cypher.equals(other.cypher) && this.parameterClass.equals(other.parameterClass);
        }
        return false;
    }

    @Override public int hashCode()
    {
        int result = this.identifier.hashCode();
        result = 31 * result + this.cypher.hashCode();
        result = 31 * result + this.parameterClass.hashCode();
        return result;
    }

    @Override public String toString()
    {
        return "MetaLabel(identifier='" + this.identifier + "', cypher='" + this.cypher + "', parameterClass='" + this.parameterClass + "')";
    }

    static MetaClassifier of(final String name)
    {
        final var cypher = ':' + Name.at(name);
        final var identifier = Name.withoutBackticksNecessity(name);
        return new MetaClassifier(identifier, cypher, identifier);
    }
}
